%% Script to test the ActiveSetC function with a sinusoidal input
% This scripts calculate the inverter voltage with minimun xy component
% under faults in the multilevel inverter.


%% Load data
clearvars
close all
load('data.mat')

wr=1.7*[cos(0:pi/200:9*pi);sin(0:pi/200:9*pi)];
N=length(wr);

levels=5*ones(data.P,1);
lower=-(levels-1)/2;
upper=(levels-1)/2;

x=zeros(data.P,N);
x0=zeros(data.P,1);
bn=zeros(data.P,1);
 for j=1:N
     if j==floor(N/3)
         lower(1,1)=lower(1,1)+1;
         upper(1,1)=upper(1,1)-1;
     end
     if j==floor(2*N/3)
         lower(3,1)=lower(3,1)+1;
         upper(3,1)=upper(3,1)-1;
     end
        [x(:,j),bn] = ActiveSetC(wr(:,j),...
                            lower, upper,...
                            x0, bn,...
                            data);
        x0=x(:,j);
 end
 
%% Plots
subplot(3,1,1)
plot(wr');
grid 'on'
title('Reference in the alpha-beta plane')
xlim([0,N])

subplot(3,1,2)
plot(x');
grid 'on'
title('Output in natural coordinates')
legend(['a':'e']')
xlim([0,N])

subplot(3,1,3)
plot((data.C*x)')
grid 'on'
title('Output in the alpha-beta plane')
xlim([0,N])

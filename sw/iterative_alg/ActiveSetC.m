function [x,bn] = ActiveSetC(wr,lower,upper,x,bn,d)
%ACTIVESET Generalized algorithm to calculate the voltage vector that minimizes the xy components for a given reference wr in the alpha-beta plane.
% Input parameters:
%   wr = voltage reference in the alpha-beta frame (column vector).
%   lower = minimum inverter level for each phase.
%   upper = maximum inverter level for each phase.
%   x = intial guess for x (solution).
%   bn = initial guess for bn (active bounds).
%   d = structure with constant data required for the algorithm.
% Output results:
%   x = solution.

% FIXME a conversion automática a C non soporta matrices de tamaño variable

%% Ancillary variables
% Warm start:
x=max(min(x,upper),lower); % Protection in case x is out of bounds
mu=zeros(d.P,1); % Initialice mu
p=zeros(d.P,1); % Initialice p
lmb=zeros(2,1); % Initialice lambda

% Step 2: For k>0, proceed as follows:
b2d=2.^(d.P-1:-1:0); % Binary to decimal conversion vector.
for k=1:5*d.P % Maximun number of iterations
    ps=1+b2d*abs(bn); % Memory position of iK
    sW=fsA(d.P,ps); % Working set
    sF=fsF(d.P,ps); % Free set
    nc=fnc(d.P,ps); % Number of active constraints
    iK=d.iK{ps};
    
    % Compute p
    v=[d.B(sF,:)*x;d.C*x-wr];
    p(sF)=-iK(1:end-2,:)*v;
    %p(sW)=0; % (Not used)
    
    if abs(p(sF))<1e-3
        % Case 1: If p = 0, compute the multipliers mu(nu)
        lmb=iK(end-1:end,:)*v;
        mu(sW)=d.B(sW,:)*x-d.C(:,sW)'*lmb;
        mu=-mu.*bn;
        if all(mu>=0)
            % Then stop the algorithm.
            % The solution is x*=x.
            % exitflag=1; % Algorithm converged
            break
        else
            % Otherwise, determine j in sW such that mu_j<mu_i (i in sW)
            % Set x(k+1) = x(k) and sW(k+1)=sW(k)\{j}.
            [~,j]=min(mu);
            bn(j)=0;
        end
    else
        % Case 2: If p is not null compute alpha
        alpha=1;
        bc=0; % Blocking constraint
        for j=find(sF) % Look for the blocking constraint in the free set
            if p(j)>0 % Going towards the upper limit
                alphaj=(upper(j)-x(j))/p(j);
                if alphaj<alpha
                    alpha=alphaj;
                    bc=j;
                end
            elseif p(j)<0 % Going towards the lower limit
                alphaj=(lower(j)-x(j))/p(j);
                if alphaj<alpha
                    alpha=alphaj;
                    bc=j;
                end
            end
        end
        
        % and set x(k+1)=x(k)+alpha*p
        x(sF)=x(sF)+alpha*p(sF);
        % In case of blocking constraints sW(k+1)=sW(k)+{j}
        if bc>0
            if nc>=d.P-2 % Remove one constraint to maintain the linear independance of the constraints
                lmb=iK(end-1:end,:)*v;
                mu(sW)=d.B(sW,:)*x-d.C(:,sW)'*lmb;
                mu(sW)=-mu(sW).*bn(sW);
                [~,j]=min(mu(sW)); % FIXME Realmente elimina o primeiro sempre porque os mus sempre son positivos %
                bn(sW(j))=0;
            end
            if p(bc)<0 % The blocking contraint is at the lower bound
                bn(bc)=-1;
                x(bc)=lower(bc);
            else
                bn(bc)=1; % The blocking contraint is at the upper bound
                x(bc)=upper(bc);
            end
        end
    end
    % exitflag=0; Convergence error!!
end

end

% Local functions
function s=fsA(P,ps)
    cn=dec2bin(ps-1,P);
    s= cn=='1';
end

function s=fsF(P,ps)
    cn=dec2bin(ps-1,P);
    s= cn=='0';
end

function n=fnc(P,ps)
    cn=dec2bin(ps-1,P);
    n=sum(cn-'0');
end
    
function x_mod = Modulation(Vafbt)
    % init - load stored matrices and vectors prepared offline (A1_ls, A2, u, ind)
    load param_fault A1_ls_all A2_all u_all ind_all;
    %load param_fault;
    
    % Fault-tolerant mode
    % y is vector of known variables - actual vector Vafbt and 0 (last row)
    % for fault-tolerant mode
    y = [Vafbt;0];
    
    yu_all = zeros(10,1);
    
    for i = 1:5    
        yu_all(i,1) = abs(y'*u_all(:,i));
    end
    
    [yu0, n_u] = max(yu_all);
    
    % Note: Final y'u0 is the minimum inf. norm value.
    if (Vafbt(2) < 0)
        u0 = -1*u_all(:,n_u);
    else
        u0 = u_all(:,n_u);
    end
    
    A2 = A2_all(:,:,n_u);
    
    x2 = yu0*sign(A2'*u0);

    A1x1 = y - A2*x2;
    
    A1_ls = A1_ls_all(:,:,n_u);
    
    x1 = A1_ls*A1x1;
    
    x_temp = [x1;x2];
    
    ind = ind_all(n_u,:);
    
    x_mod(ind(1)) = x_temp(1);
    x_mod(ind(2)) = x_temp(2);
    x_mod(ind(3)) = x_temp(3);
    x_mod(ind(4)) = x_temp(4);
    x_mod(ind(5)) = x_temp(5);
    x_mod(ind(6)) = x_temp(6);
    x_mod(ind(7)) = x_temp(7);
    x_mod(ind(8)) = x_temp(8);
    x_mod(ind(9)) = x_temp(9);
    x_mod(ind(10)) = x_temp(10);
    
    for i = 3:2:9
        tmp = 0.5*(x_mod(i)+x_mod(i+1));
        x_mod(i)   = tmp;
        x_mod(i+1) = tmp;
    end

 end 

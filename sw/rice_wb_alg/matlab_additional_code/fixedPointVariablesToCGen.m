%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% floatingPointVariablesToCGen %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Source file variables load
sourceFile = 'param_fault.mat';
variablesToMove = load(sourceFile, 'A1_ls_all', 'A2_all', 'u_all', 'ind_all');
variablesNames = fieldnames(variablesToMove);
variablesToMove.(variablesNames{4}) = variablesToMove.(variablesNames{4}) - 1;

% Destination file opening
cd ..;
cd ./pc_version/fixed_point_c_code;
sourceDestinationFile = 'fixedPoint_ParamFault.c';
sourceDestinationFileID = fopen(sourceDestinationFile,'w');
headerDestinationFile = 'fixedPoint_ParamFault.h';
headerDestinationFileID = fopen(headerDestinationFile,'w');

% Variables array creation
variablesTypeString = ["int32_t", "int32_t", "int32_t", "int32_t"];
variablesAssumedTypeString = ["int32", "int32", "int32", "int32"];
variablesShiftAmount = [24 24 24 0];

% Header of the source destination file
fprintf(sourceDestinationFileID, '%s\n', '/**');
fprintf(sourceDestinationFileID, ' * @file %s\n', sourceDestinationFile);
fprintf(sourceDestinationFileID, '%s\n', ' * @author Fernando Flores (Fer6Flores)');
fprintf(sourceDestinationFileID, '%s\n', ' * @date March 4, 2021');
fprintf(sourceDestinationFileID, '%s\n', ' * @brief Fixed-point parameters generated offline');
fprintf(sourceDestinationFileID, '%s\n', ' * ');
fprintf(sourceDestinationFileID, '%s\n', ' * This file contains the variables calculated offline');
fprintf(sourceDestinationFileID, '%s\n', ' * and generated and 24-bits shifted through Matlab from the param_fault.mat');
fprintf(sourceDestinationFileID, '%s\n\n', ' */');

% Necessary library writing in the source destination file
fprintf(sourceDestinationFileID, '%s\n', '/* Libraries */');
fprintf(sourceDestinationFileID, '%s\n', '#include <stdint.h>');
fprintf(sourceDestinationFileID, '#include "%s"\n\n', headerDestinationFile);

% Header of the header destination file
fprintf(headerDestinationFileID, '%s\n', '/**');
fprintf(headerDestinationFileID, ' * @file %s\n', headerDestinationFile);
fprintf(headerDestinationFileID, '%s\n', ' * @author Fernando Flores (Fer6Flores)');
fprintf(headerDestinationFileID, '%s\n', ' * @date March 4, 2021');
fprintf(headerDestinationFileID, '%s\n', ' * @brief Floating-point parameters generated offline');
fprintf(headerDestinationFileID, '%s\n', ' * ');
fprintf(headerDestinationFileID, '%s\n', ' * This file contains the declaration of the variables calculated');
fprintf(headerDestinationFileID, '%s\n', ' * offline and generated through Matlab from the param_fault.mat');
fprintf(headerDestinationFileID, '%s\n\n', ' */');
fprintf(headerDestinationFileID, '%s\n', '#ifndef __FIXEDPOINT_PARAMFAULT_H__');
fprintf(headerDestinationFileID, '%s\n\n', '#define __FIXEDPOINT_PARAMFAULT_H__');

% Shift masks MACROs writing in the header destination file
fprintf(headerDestinationFileID, '/* Shift masks MACROs */\n');
fprintf(headerDestinationFileID, '#define SHIFT_MASK_24 %iU\n', 24);
fprintf(headerDestinationFileID, '\n');

% Indexing param_fault.mat variables
fprintf(sourceDestinationFileID, '%s\n', '/* Variables */');
fprintf(headerDestinationFileID, '%s\n', '/* Variables */');
for index=1:length(variablesNames)
    
    % Write type and name of the variable in both files
    variableInitString = [variablesTypeString(index) variablesNames{index}];
    variableDimensions = size(variablesToMove.(variablesNames{index}));
    fprintf(sourceDestinationFileID, '%s %s', variableInitString);
    fprintf(headerDestinationFileID, 'extern %s %s', variableInitString);
    
    % Write matrix dimensions if needed
    if ~((length(variableDimensions) == 2) && ((variableDimensions(1)+variableDimensions(2)) == 2))
        if (length(variableDimensions) == 2)
            if (variableDimensions(2) == 1)
                fprintf(sourceDestinationFileID, '[%i]', variableDimensions(1));
                fprintf(headerDestinationFileID, '[%i];\n\n', variableDimensions(1));
            else
                fprintf(sourceDestinationFileID, '[%i][%i]', variableDimensions(1), variableDimensions(2));
                fprintf(headerDestinationFileID, '[%i][%i];\n\n', variableDimensions(1), variableDimensions(2));
            end
        elseif (length(variableDimensions) == 3)
            fprintf(sourceDestinationFileID, '[%i][%i][%i]', variableDimensions(3), variableDimensions(1), variableDimensions(2));
            fprintf(headerDestinationFileID, '[%i][%i][%i];\n\n', variableDimensions(3), variableDimensions(1), variableDimensions(2));
        else
            fprintf(sourceDestinationFileID, '[Error]');
            fprintf(headerDestinationFileID, '[Error];\n\n');
        end
    end
    
    % Write variable content
    if ((length(variableDimensions) == 2) && ((variableDimensions(1)+variableDimensions(2)) == 2))
        fprintf(sourceDestinationFileID, ' = %i ;\n\n', round(variablesToMove.(variablesNames{index})*2^variablesShiftAmount(index)));
    else
        fprintf(sourceDestinationFileID, ' = {');
        if length(variableDimensions) == 2
            for i=1:variableDimensions(1)
                if variableDimensions(2) ~= 1
                    fprintf(sourceDestinationFileID, '{');
                end
                for j=1:variableDimensions(2)
                    variableContentValue = round(variablesToMove.(variablesNames{index})(i,j)*2^variablesShiftAmount(index));
                    if j==variableDimensions(2)
                        fprintf(sourceDestinationFileID, '%i', variableContentValue);
                    else
                        fprintf(sourceDestinationFileID, '%i, ', variableContentValue);
                    end
                end
                if variableDimensions(2) ~= 1
                    if i==variableDimensions(1)
                        fprintf(sourceDestinationFileID, '}');
                    else
                        fprintf(sourceDestinationFileID, '},\n');
                    end
                else
                    if i~=variableDimensions(1)
                        fprintf(sourceDestinationFileID, ', ');
                    end
                end
            end
        elseif length(variableDimensions) == 3
            for k=1:variableDimensions(3)
                fprintf(sourceDestinationFileID, '{');
                for i=1:variableDimensions(1)
                    fprintf(sourceDestinationFileID, '{');
                    for j=1:variableDimensions(2)
                        variableContentValue = round(variablesToMove.(variablesNames{index})(i, j, k)*2^variablesShiftAmount(index));
                        if j==variableDimensions(2)
                            fprintf(sourceDestinationFileID, '%i', variableContentValue);
                        else
                            fprintf(sourceDestinationFileID, '%i, ', variableContentValue);
                        end
                    end
                    if i==variableDimensions(1)
                        fprintf(sourceDestinationFileID, '}');
                    else
                        fprintf(sourceDestinationFileID, '},\n');
                    end
                end
                if k==variableDimensions(3)
                    fprintf(sourceDestinationFileID, '}');
                else
                    fprintf(sourceDestinationFileID, '},\n');
                end
            end
        else
            fprintf(sourceDestinationFileID, 'Error');
        end
        fprintf(sourceDestinationFileID, '};\n\n');
    end
end

% Close destination file :)
fprintf(headerDestinationFileID, '#endif');
fclose(sourceDestinationFileID);
fclose(headerDestinationFileID);
cd ..;
cd MatlabOriginalCode;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% floatingPointVariablesToCGen %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Source file variables load
sourceFile = 'param_fault.mat';
variablesToMove = load(sourceFile, 'A1_ls_all', 'A2_all', 'u_all', 'ind_all');
variablesNames = fieldnames(variablesToMove);
variablesToMove.(variablesNames{4}) = variablesToMove.(variablesNames{4}) - 1;

% Destination files opening
cd ..;
cd ./FloatingPointInC;
sourceDestinationFile = 'floatingPoint_ParamFault.c';
sourceDestinationFileID = fopen(sourceDestinationFile,'w');
headerDestinationFile = 'floatingPoint_ParamFault.h';
headerDestinationFileID = fopen(headerDestinationFile,'w');

% Variables type string creation
variablesTypeString = 'double ';

% Header of the source destination file
fprintf(sourceDestinationFileID, '%s\n', '/**');
fprintf(sourceDestinationFileID, ' * @file %s\n', sourceDestinationFile);
fprintf(sourceDestinationFileID, '%s\n', ' * @author Fernando Flores (Fer6Flores)');
fprintf(sourceDestinationFileID, '%s\n', ' * @date March 4, 2021');
fprintf(sourceDestinationFileID, '%s\n', ' * @brief Floating-point parameters generated offline');
fprintf(sourceDestinationFileID, '%s\n', ' * ');
fprintf(sourceDestinationFileID, '%s\n', ' * This file contains the variables calculated offline');
fprintf(sourceDestinationFileID, '%s\n', ' * and generated through Matlab from the param_fault.mat');
fprintf(sourceDestinationFileID, '%s\n\n', ' */');

% Necessary libraries writing in the source destination file
fprintf(sourceDestinationFileID, '%s\n', '/* Libraries */');
fprintf(sourceDestinationFileID, '#include "%s"\n\n', headerDestinationFile);

% Header of the header destination file
fprintf(headerDestinationFileID, '%s\n', '/**');
fprintf(headerDestinationFileID, ' * @file %s\n', headerDestinationFile);
fprintf(headerDestinationFileID, '%s\n', ' * @author Fernando Flores (Fer6Flores)');
fprintf(headerDestinationFileID, '%s\n', ' * @date March 4, 2021');
fprintf(headerDestinationFileID, '%s\n', ' * @brief Floating-point parameters generated offline');
fprintf(headerDestinationFileID, '%s\n', ' * ');
fprintf(headerDestinationFileID, '%s\n', ' * This file contains the declaration of the variables calculated');
fprintf(headerDestinationFileID, '%s\n', ' * offline and generated through Matlab from the param_fault.mat');
fprintf(headerDestinationFileID, '%s\n\n', ' */');
fprintf(headerDestinationFileID, '%s\n', '#ifndef __FLOATINGPOINT_PARAMFAULT_H__');
fprintf(headerDestinationFileID, '%s\n\n', '#define __FLOATINGPOINT_PARAMFAULT_H__');

% Indexing param_fault.mat variables
fprintf(sourceDestinationFileID, '%s\n', '/* Variables */');
fprintf(headerDestinationFileID, '%s\n', '/* Variables */');
for index=1:length(variablesNames)
    
    % Write type and name of the variable in both files
    variableInitString = [variablesTypeString variablesNames{index}];
    variableDimensions = size(variablesToMove.(variablesNames{index}));
    fprintf(sourceDestinationFileID, '%s', variableInitString);
    fprintf(headerDestinationFileID, 'extern %s', variableInitString);
    
    % Write matrix dimensions if needed
    if ~((length(variableDimensions) == 2) && ((variableDimensions(1)+variableDimensions(2)) == 2))
        if (length(variableDimensions) == 2)
            if (variableDimensions(2) == 1)
                fprintf(sourceDestinationFileID, '[%i]', variableDimensions(1));
                fprintf(headerDestinationFileID, '[%i];\n\n', variableDimensions(1));
            else
                fprintf(sourceDestinationFileID, '[%i][%i]', variableDimensions(1), variableDimensions(2));
                fprintf(headerDestinationFileID, '[%i][%i];\n\n', variableDimensions(1), variableDimensions(2));
            end
            
        elseif (length(variableDimensions) == 3)
            fprintf(sourceDestinationFileID, '[%i][%i][%i]', variableDimensions(3), variableDimensions(1), variableDimensions(2));
            fprintf(headerDestinationFileID, '[%i][%i][%i];\n\n', variableDimensions(3), variableDimensions(1), variableDimensions(2));
        else
            fprintf(sourceDestinationFileID, '[Error]');
            fprintf(headerDestinationFileID, '[Error];\n\n');
        end
    end
    
    % Write variable content
    if ((length(variableDimensions) == 2) && ((variableDimensions(1)+variableDimensions(2)) == 2))
        fprintf(sourceDestinationFileID, ' = %f ;\n\n', variablesToMove.(variablesNames{index}));
    else
        fprintf(sourceDestinationFileID, ' = {');
        if length(variableDimensions) == 2
            for i=1:variableDimensions(1)
                if variableDimensions(2) ~= 1
                    fprintf(sourceDestinationFileID, '{');
                end
                for j=1:variableDimensions(2)
                    variableContentValue = variablesToMove.(variablesNames{index})(i,j);
                    if j==variableDimensions(2)
                        fprintf(sourceDestinationFileID, '%f', variableContentValue);
                    else
                        fprintf(sourceDestinationFileID, '%f, ', variableContentValue);
                    end
                end
                if variableDimensions(2) ~= 1
                    if i==variableDimensions(1)
                        fprintf(sourceDestinationFileID, '}');
                    else
                        fprintf(sourceDestinationFileID, '},\n');
                    end
                else
                    if i~=variableDimensions(1)
                        fprintf(sourceDestinationFileID, ', ');
                    end
                end
            end
        elseif length(variableDimensions) == 3
            for k=1:variableDimensions(3)
                fprintf(sourceDestinationFileID, '{');
                for i=1:variableDimensions(1)
                    fprintf(sourceDestinationFileID, '{');
                    for j=1:variableDimensions(2)
                        variableContentValue = variablesToMove.(variablesNames{index})(i, j, k);
                        if j==variableDimensions(2)
                            fprintf(sourceDestinationFileID, '%f', variableContentValue);
                        else
                            fprintf(sourceDestinationFileID, '%f, ', variableContentValue);
                        end
                    end
                    if i==variableDimensions(1)
                        fprintf(sourceDestinationFileID, '}');
                    else
                        fprintf(sourceDestinationFileID, '},\n');
                    end
                end
                if k==variableDimensions(3)
                    fprintf(sourceDestinationFileID, '}');
                else
                    fprintf(sourceDestinationFileID, '},\n');
                end
            end
        else
            fprintf(sourceDestinationFileID, 'Error');
        end
        fprintf(sourceDestinationFileID, '};\n\n');
    end
end

% Close destination file :)
fprintf(headerDestinationFileID, '%s', '#endif');
fclose(sourceDestinationFileID);
fclose(headerDestinationFileID);
cd ..;
cd MatlabOriginalCode;
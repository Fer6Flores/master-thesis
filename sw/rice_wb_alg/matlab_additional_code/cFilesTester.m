%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% cFilesTester %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Original algorithm results graphs
MinInfNorm_5fCHB_Fault_FPGA

%% Floating-point results graphs
cd ..;
csvResults = readmatrix('pc_version/floating_point_c_code/floatingPoint_MinInfNorm_sim.csv');

% Plot resulting leg voltages and required Vafbt vector
figure;
plot(csvResults(:,1),[csvResults(:,4),csvResults(:,5),csvResults(:,6),csvResults(:,7),csvResults(:,8),csvResults(:,9),csvResults(:,10),csvResults(:,11),csvResults(:,12),csvResults(:,13),csvResults(:,14)], 'LineWidth', 2);
grid on;
xlabel('Simulation time (s)')
ylabel('Voltage (p.u.)')
%title('Floating-point C resulting leg voltages and required Vafbt vector');

% Plot resulting leg voltages
figure;
subplot(5,1,1);
plot(csvResults(:,1), [csvResults(:,4), csvResults(:,5)], 'LineWidth', 2); grid on;
lgd = legend('1st leg: 1st cell voltage', '1st leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
%title('Floating-point C resulting 1st leg voltages');
subplot(5,1,2);
plot(csvResults(:,1), [csvResults(:,6), csvResults(:,7)], 'LineWidth', 2); grid on;
lgd = legend('2nd leg: 1st cell voltage', '2nd leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
%title('Floating-point C resulting 2nd leg voltages');
subplot(5,1,3);
plot(csvResults(:,1), [csvResults(:,8), csvResults(:,9)], 'LineWidth', 2); grid on;
lgd = legend('3rd leg: 1st cell voltage', '3rd leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
%title('Floating-point C resulting 3rd leg voltages');
subplot(5,1,4);
plot(csvResults(:,1), [csvResults(:,10), csvResults(:,11)], 'LineWidth', 2); grid on;
lgd = legend('4th leg: 1st cell voltage', '4th leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
%title('Floating-point C resulting 4th leg voltages');
subplot(5,1,5);
plot(csvResults(:,1), [csvResults(:,12), csvResults(:,13)], 'LineWidth', 2); grid on;
lgd = legend('5th leg: 1st cell voltage', '5th leg: 2nd cell voltage');
xlabel('Simulation time (s)')
ylabel('Voltage (p.u.)')
lgd.FontSize = 13;
%title('Floating-point C resulting 5th leg voltages');

%% Floating-point results error comparing with original results
figure;
csvResults = [csvResults(:,1) csvResults(:,2:14) - o(:, 2:14)];
plot(csvResults(:,1),[csvResults(:,4),csvResults(:,5),csvResults(:,6),csvResults(:,7),csvResults(:,8),csvResults(:,9),csvResults(:,10),csvResults(:,11),csvResults(:,12),csvResults(:,13),csvResults(:,14)], 'LineWidth', 2);
%title('Floating-point Modulation C file errors');
xlabel('Simulation time (s)')
ylabel('Voltage (p.u.)')

%% Fixed-point results graphs
csvResults = readmatrix('pc_version/fixed_point_c_code/fixedPoint_MinInfNorm_sim.csv');

% Plot resulting leg voltages and required Vafbt vector
figure;
plot(csvResults(:,1),[csvResults(:,2),csvResults(:,3),csvResults(:,4),csvResults(:,5),csvResults(:,6),csvResults(:,7),csvResults(:,8),csvResults(:,9),csvResults(:,10),csvResults(:,11),csvResults(:,12),csvResults(:,13),csvResults(:,14)], 'LineWidth', 2);
grid on;
%title('Fixed-point C resulting leg voltages and required Vafbt vector');
xlabel('Simulation time (s)')
ylabel('Voltage (p.u.)')

% Plot resulting leg voltages
figure;
subplot(5,1,1);
plot(csvResults(:,1), [csvResults(:,4), csvResults(:,5)], 'LineWidth', 2); grid on;
lgd = legend('1st leg: 1st cell voltage', '1st leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
%title('Floating-point C resulting 1st leg voltages');
subplot(5,1,2);
plot(csvResults(:,1), [csvResults(:,6), csvResults(:,7)], 'LineWidth', 2); grid on;
lgd = legend('2nd leg: 1st cell voltage', '2nd leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
%title('Floating-point C resulting 2nd leg voltages');
subplot(5,1,3);
plot(csvResults(:,1), [csvResults(:,8), csvResults(:,9)], 'LineWidth', 2); grid on;
lgd = legend('3rd leg: 1st cell voltage', '3rd leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
%title('Floating-point C resulting 3rd leg voltages');
subplot(5,1,4);
plot(csvResults(:,1), [csvResults(:,10), csvResults(:,11)], 'LineWidth', 2); grid on;
lgd = legend('4th leg: 1st cell voltage', '4th leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
%title('Floating-point C resulting 4th leg voltages');
subplot(5,1,5);
plot(csvResults(:,1), [csvResults(:,12), csvResults(:,13)], 'LineWidth', 2); grid on;
lgd = legend('5th leg: 1st cell voltage', '5th leg: 2nd cell voltage');
lgd.FontSize = 13;
xlabel('Simulation time (s)')
ylabel('Voltage (p.u.)')
%title('Floating-point C resulting 5th leg voltages');

%% Fixed-point results error comparing with original results
figure;
csvResults = [csvResults(:,1) csvResults(:,2:14) - o(:, 2:14)];
plot(csvResults(:,1),[csvResults(:,4),csvResults(:,5),csvResults(:,6),csvResults(:,7),csvResults(:,8),csvResults(:,9),csvResults(:,10),csvResults(:,11),csvResults(:,12),csvResults(:,13),csvResults(:,14)], 'LineWidth', 2);
%title('Fixed-point Modulation C file errors');
xlabel('Simulation time (s)')
ylabel('Voltage (p.u.)')

%% ARM-A9 Zynq-7000 implementation results graphs
csvResults = readmatrix('embedded_version/arm_imp/fixedPoint_zynqARM_test.csv');
plottingExecutionTimes = [csvResults(:,1) csvResults(:, 15) csvResults(:, 16)];

% Plot resulting leg voltages and required Vafbt vector
figure;
plot(csvResults(:,1),[csvResults(:,4),csvResults(:,5),csvResults(:,6),csvResults(:,7),csvResults(:,8),csvResults(:,9),csvResults(:,10),csvResults(:,11),csvResults(:,12),csvResults(:,13),csvResults(:,14)], 'LineWidth', 2);
grid on;
%title('ARM-A9 Zynq-7000 implementation resulting leg voltages and required Vafbt vector');
xlabel('Simulation time (s)')
ylabel('Voltage (p.u.)')

% Plot resulting leg voltages
figure;
subplot(5,1,1);
plot(csvResults(:,1), [csvResults(:,4), csvResults(:,5)], 'LineWidth', 2); grid on;
lgd = legend('1st leg: 1st cell voltage', '1st leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
%title('Floating-point C resulting 1st leg voltages');
subplot(5,1,2);
plot(csvResults(:,1), [csvResults(:,6), csvResults(:,7)], 'LineWidth', 2); grid on;
lgd = legend('2nd leg: 1st cell voltage', '2nd leg: 2nd cell voltage');
lgd.FontSize = 13;ylabel('Voltage (p.u.)')
%title('Floating-point C resulting 2nd leg voltages');
subplot(5,1,3);
plot(csvResults(:,1), [csvResults(:,8), csvResults(:,9)], 'LineWidth', 2); grid on;
lgd = legend('3rd leg: 1st cell voltage', '3rd leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
%title('Floating-point C resulting 3rd leg voltages');
subplot(5,1,4);
plot(csvResults(:,1), [csvResults(:,10), csvResults(:,11)], 'LineWidth', 2); grid on;
lgd = legend('4th leg: 1st cell voltage', '4th leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
%title('Floating-point C resulting 4th leg voltages');
subplot(5,1,5);
plot(csvResults(:,1), [csvResults(:,12), csvResults(:,13)], 'LineWidth', 2); grid on;
lgd = legend('5th leg: 1st cell voltage', '5th leg: 2nd cell voltage');
lgd.FontSize = 13;
xlabel('Simulation time (s)')
ylabel('Voltage (p.u.)')
%title('Floating-point C resulting 5th leg voltages');

%% ARM-A9 Zynq-7000 implementation results error comparing with original results
figure;
csvResults = [csvResults(:,1) csvResults(:, 2:14) - o(:, 2:14)];
plot(csvResults(:,1),[csvResults(:,4),csvResults(:,5),csvResults(:,6),csvResults(:,7),csvResults(:,8),csvResults(:,9),csvResults(:,10),csvResults(:,11),csvResults(:,12),csvResults(:,13),csvResults(:,14)], 'LineWidth', 2);
%title('ARM-A9 Zynq-7000 implementation errors');
xlabel('Simulation time (s)')
ylabel('Voltage (p.u.)')

%% ARM-A9 Zynq-7000 implementation execution time
figure;
plot(plottingExecutionTimes(:,1), [plottingExecutionTimes(:, 2), plottingExecutionTimes(:, 3), plottingExecutionTimes(:, 2) + plottingExecutionTimes(:, 3)], 'LineWidth', 2);
%title('ARM-A9 Zynq-7000 implementation execution times');
ylim([0 0.00002])
lgd = legend('Execution time', 'AXI bus communication time', 'Total time');
lgd.FontSize = 13;
xlabel('Simulation time (s)')
ylabel('Time (s)')

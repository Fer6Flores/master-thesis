%% Parameters
% frequency = 50 Hz
f = 50;
% angular frequency = 314.16 rad/s
w = 2*pi*f;

% time
t = 0;

% delta time (sampling period)
dt = 20e-6;

% discrete time k
k = 1;

% dicrete end time N
N = round(0.06/dt);
 
o = zeros(N,14);

x_mod = zeros(10,1);

% Periods count (periods of fundamental, 50 Hz default)
n = 0;

for k = 1:N
        
    % Rotating voltage vector (Valpha, Vbeta): |Vafbt| = 1.5772, f = 50 Hz
    Vafbt = 1.5772*[cos(w*t);sin(w*t)];
    
    x_mod = Modulation(Vafbt); % Function to be implemented in the FPGA
    
    % Auxiliary matrix for plots.
    o(k,:) = [t, Vafbt(1), Vafbt(2), x_mod,0];
    t = t + dt;
end

%% Plot required Vafbt vector
figure;
plot(o(:,1),[o(:,2),o(:,3)], 'LineWidth', 2);
lgd = legend('\alpha voltage', '\beta voltage');
lgd.FontSize = 13;
xlabel('Time (s)')
ylabel('Voltage (p.u.)')
grid on;

%% Plot resulting leg voltages
figure;
subplot(5,1,1);
plot(o(:,1), [o(:,4), o(:,5)], 'LineWidth', 2); grid on;
lgd = legend('1st leg: 1st cell voltage', '1st leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
subplot(5,1,2);
plot(o(:,1), [o(:,6), o(:,7)], 'LineWidth', 2); grid on;
lgd = legend('2nd leg: 1st cell voltage', '2nd leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
subplot(5,1,3);
plot(o(:,1), [o(:,8), o(:,9)], 'LineWidth', 2); grid on;
lgd = legend('3rd leg: 1st cell voltage', '3rd leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
subplot(5,1,4);
plot(o(:,1), [o(:,10), o(:,11)], 'LineWidth', 2); grid on;
lgd = legend('4th leg: 1st cell voltage', '4th leg: 2nd cell voltage');
lgd.FontSize = 13;
ylabel('Voltage (p.u.)')
subplot(5,1,5);
plot(o(:,1), [o(:,12), o(:,13)], 'LineWidth', 2); grid on;
lgd = legend('5th leg: 1st cell voltage', '5th leg: 2nd cell voltage');
xlabel('Time (s)')
ylabel('Voltage (p.u.)')
lgd.FontSize = 13;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% No more
#include "xparameters.h"
#include "sleep.h"
#include <stdint.h>                	// For using uint8_t and more types
#include <stdlib.h>                	// For pointers
#include "fixedPoint_Modulation.h" 	// Modulation function prototype
#include "xscutimer.h" 				// Include scutimer header file
#include "xuartps.h"				// Include UART ps header file
#include "axi_echo_ip.h"
//====================================================

#define DATA_SENT_BUFFER_SIZE 4
#define DATA_RECV_BUFFER_SIZE 8

int main (void) 
{
    //xil_printf("-- Start of the Program --\r\n");

    // Algorithm variables inits
	int32_t *x_mod;
	int32_t Vafbt[2] = {0};
	Vafbt[0] = 26461025;
	Vafbt[1] = 0;

	// UART PS variables inits
	XUartPs Uart_Ps;
	XUartPs_Config *XUartPs_ConfigPtr;
	int32_t uart_status = 0;
	int32_t sent_count = 0;
	int32_t received_count = 0;
	static u8 send_buffer[DATA_SENT_BUFFER_SIZE];
	static u8 recv_buffer[DATA_RECV_BUFFER_SIZE];
	uint32_t loop_count = 0;

	// ARM-A9 private timer variables inits
	XScuTimer Timer;
	XScuTimer_Config *XScuTimer_ConfigPtr;
	XScuTimer *TimerInstancePtr = &Timer;
	int32_t timer_initial_value = 100000;
	int32_t timer_status = 0;
	int32_t timer_value = 0;

	// Time measurements variables inits
	int32_t elapsed_time = 0;
	int32_t axi_time = 0;
	int32_t axi_index = 0;

	// UART PS initialization and configuration
	XUartPs_ConfigPtr = XUartPs_LookupConfig(XPAR_PS7_UART_1_DEVICE_ID);
	if (NULL == XUartPs_ConfigPtr)
	{
		return XST_FAILURE;
	}

	uart_status = XUartPs_CfgInitialize(&Uart_Ps, XUartPs_ConfigPtr, XUartPs_ConfigPtr->BaseAddress);
	if (uart_status != XST_SUCCESS)
	{
		return XST_FAILURE;
	}

	uart_status = XUartPs_SelfTest(&Uart_Ps);
	if (uart_status != XST_SUCCESS)
	{
		return XST_FAILURE;
	}

	XUartPs_SetBaudRate(&Uart_Ps, 115200);

	// Private timer initialization and configuration
	XScuTimer_ConfigPtr = XScuTimer_LookupConfig (XPAR_PS7_SCUTIMER_0_DEVICE_ID);
	timer_status = XScuTimer_CfgInitialize	(TimerInstancePtr, XScuTimer_ConfigPtr, XScuTimer_ConfigPtr->BaseAddr);
	if(timer_status != XST_SUCCESS)
	{
		xil_printf("Timer init() failed\r\n");
		return XST_FAILURE;
	}

	XScuTimer_EnableAutoReload(TimerInstancePtr);

	while (1)
	{
		// Block receiving the Vafbt input buffer
		received_count = 0;
		while (received_count < DATA_RECV_BUFFER_SIZE)
		{
			received_count +=XUartPs_Recv(&Uart_Ps, &recv_buffer[received_count], (DATA_RECV_BUFFER_SIZE - received_count));
		}
		Vafbt[0] = recv_buffer[0] << 24 | recv_buffer[1] << 16 | recv_buffer[2] << 8 | recv_buffer[3];
		Vafbt[1] = recv_buffer[4] << 24 | recv_buffer[5] << 16 | recv_buffer[6] << 8 | recv_buffer[7];

		// Load timer with initial value 100000 and start
		XScuTimer_LoadTimer(TimerInstancePtr, timer_initial_value);
		XScuTimer_Start(TimerInstancePtr);

		// Function performance
		x_mod = Modulation(Vafbt);

		// Stop the timer, get its value and elapsed time
		XScuTimer_Stop(TimerInstancePtr);
		timer_value = XScuTimer_GetCounterValue(TimerInstancePtr);
		elapsed_time = timer_initial_value - timer_value;


		// Load timer with initial value 100000 and start
		XScuTimer_LoadTimer(TimerInstancePtr, timer_initial_value);
		XScuTimer_Start(TimerInstancePtr);

		// AXI4-Lite performance
		for (int axi_write_index = 0; axi_write_index < 5; axi_write_index++)
		{
			AXI_ECHO_IP_mWriteReg(XPAR_AXI_ECHO_IP_0_S_AXI_BASEADDR, AXI_ECHO_IP_S_AXI_SLV_REG0_OFFSET, axi_index);
			while (AXI_ECHO_IP_mReadReg(XPAR_AXI_ECHO_IP_0_S_AXI_BASEADDR, AXI_ECHO_IP_S_AXI_SLV_REG0_OFFSET) != axi_index)
			{
				continue;
			}
			axi_index++;
		}

		// Stop the timer, get its value and axi time
		XScuTimer_Stop(TimerInstancePtr);
		timer_value = XScuTimer_GetCounterValue(TimerInstancePtr);
		axi_time = timer_initial_value - timer_value;

		// Block sending the x_mod and elapsed time values through UART
		for (int i = 0; i < 10; i++)
		{
			send_buffer[0] = (*x_mod >> 24) & 0xFF;
			send_buffer[1] = (*x_mod >> 16) & 0xFF;
			send_buffer[2] = (*x_mod >> 8) & 0xFF;
			send_buffer[3] = *x_mod & 0xFF;
			sent_count = XUartPs_Send(&Uart_Ps, send_buffer, DATA_SENT_BUFFER_SIZE);
			if (sent_count != DATA_SENT_BUFFER_SIZE)
			{
				return XST_FAILURE;
			}
			loop_count = 0;
			while (XUartPs_IsSending(&Uart_Ps))
			{
				loop_count++;
			}
			x_mod++;
		}

		// Block sending the elapsed time through UART
		send_buffer[0] = (elapsed_time >> 24) & 0xFF;
		send_buffer[1] = (elapsed_time >> 16) & 0xFF;
		send_buffer[2] = (elapsed_time >> 8) & 0xFF;
		send_buffer[3] = elapsed_time & 0xFF;
		sent_count = XUartPs_Send(&Uart_Ps, send_buffer, DATA_SENT_BUFFER_SIZE);
		if (sent_count != DATA_SENT_BUFFER_SIZE) {
			return XST_FAILURE;
		}

		// Block sending the axi time through UART
		send_buffer[0] = (axi_time >> 24) & 0xFF;
		send_buffer[1] = (axi_time >> 16) & 0xFF;
		send_buffer[2] = (axi_time >> 8) & 0xFF;
		send_buffer[3] = axi_time & 0xFF;
		sent_count = XUartPs_Send(&Uart_Ps, send_buffer, DATA_SENT_BUFFER_SIZE);
		if (sent_count != DATA_SENT_BUFFER_SIZE) {
			return XST_FAILURE;
		}

		// The block
		loop_count = 0;
		while (XUartPs_IsSending(&Uart_Ps)) {
			loop_count++;
		}
	}
}
 

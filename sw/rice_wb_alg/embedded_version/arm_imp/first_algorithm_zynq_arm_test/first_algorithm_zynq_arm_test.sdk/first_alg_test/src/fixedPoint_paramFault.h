/**
 * @file fixedPoint_ParamFault.h
 * @author Fernando Flores (Fer6Flores)
 * @date March 4, 2021
 * @brief Floating-point parameters generated offline
 * 
 * This file contains the declaration of the variables calculated
 * offline and generated through Matlab from the param_fault.mat
 */

#ifndef __FIXEDPOINT_PARAMFAULT_H__
#define __FIXEDPOINT_PARAMFAULT_H__

/* Shift masks MACROs */
#define SHIFT_MASK_24 24U

/* Variables */
extern int32_t A1_ls_all[5][2][3];

extern int32_t A2_all[5][3][8];

extern int32_t u_all[3][5];

extern int32_t ind_all[5][10];

#endif
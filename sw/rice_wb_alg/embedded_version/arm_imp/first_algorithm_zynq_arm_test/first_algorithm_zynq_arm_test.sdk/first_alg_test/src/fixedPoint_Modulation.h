/**
 * @file fixedPoint_Modulation.h
 * @author Fernando Flores (Fer6Flores)
 * @date March 4, 2021
 * @brief Fixed-point Modulation file
 */

#ifndef __FIXEDPOINT_MODULATION__
#define __FIXEDPOINT_MODULATION__

int32_t *Modulation(int32_t Vabft[2]);

#endif
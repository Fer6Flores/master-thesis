import serial
import csv
from math import *

if __name__ == "__main__":

    # Inits
    time = 0
    samplingPeriod = 20e-6
    freq = 50
    angularFreq = 2 * pi * freq
    shiftMask24 = 24
    zynqRegisterSizeInBytes = 4
    zynqResponse = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    cpu_3x2x = 333e6

    # Open Zynq serial port
    zynqSerial = serial.Serial(port="COM11", baudrate=115200, bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE)

    # Write header to test csv file
    with open('fixedPoint_zynqARM_test.csv', mode='w', newline='') as fixedPoint_Zynq_ARM_test:
        test_csv_writer = csv.writer(fixedPoint_Zynq_ARM_test, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        test_csv_writer.writerow(['o[0]', 'o[1]', 'o[2]', 'o[3]', 'o[4]', 'o[5]', 'o[6]', 'o[7]', 'o[8]', 'o[9]', 'o[10]', 'o[11]', 'o[12]', 'o[13]', 'calcTime', 'axiTime'])

    # 3000 data transmitter and receiver
    for i in range(3000):

        # Send the Vafbt values to the Zynq
        Vafbt = [round(1.5772 * cos(angularFreq * time) * pow(2, shiftMask24)), round(1.5772 * sin(angularFreq * time) * pow(2, shiftMask24))]
        zynqSerial.write(Vafbt[0].to_bytes(zynqRegisterSizeInBytes, 'big', signed="True")) 
        zynqSerial.write(Vafbt[1].to_bytes(zynqRegisterSizeInBytes, 'big', signed="True")) 

        # Receive the modulation data from the Zynq
        zynqResponse[0] = time
        zynqResponse[1] = Vafbt[0] / pow(2, shiftMask24)
        zynqResponse[2] = Vafbt[1] / pow(2, shiftMask24)
        for i in range(12):
            msg = zynqSerial.read(zynqRegisterSizeInBytes)
            if i < 10:
                zynqResponse[3+i] = int.from_bytes(msg, "big", signed="True") / pow(2, shiftMask24)
            else:
                zynqResponse[4+i] = int.from_bytes(msg, "big", signed="True") / cpu_3x2x # Check this value
        zynqResponse[13] = 0

        # Write data received to test csv file
        with open('fixedPoint_zynqARM_test.csv', mode='a', newline='') as fixedPoint_Zynq_ARM_test:
            test_csv_writer = csv.writer(fixedPoint_Zynq_ARM_test, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            test_csv_writer.writerow(zynqResponse)

        # Increment time
        time += samplingPeriod

    # Close serial port
    zynqSerial.close()
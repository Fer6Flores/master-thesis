/*
 * main.c
 *
 *  Created on: 22 abr. 2021
 *      Author: Fer6Flores
 */

#include "xparameters.h"
#include "xgpio.h"
// #include "pwm_x40_ip.h"
#include "xil_printf.h"
// #include "dspace_com_ip.h"

int main()
{
	xil_printf("ARM Test start.\r\n");

	// Variables to access I/O connected to GPIO interfaces
    XGpio switches, pushbtn, leds;

    // I/O values
	int switches_value = 0; // 3 bits
	int pushbtn_value = 0; 	// 4 bits
	int leds_value = 0;     // 8 bits
	int status;

	// Initialize GPIO interface for switches
    status = XGpio_Initialize(&switches, XPAR_SWITCHES_DEVICE_ID);
	if (status != XST_SUCCESS) {
		xil_printf("Switches gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

	// Initialize GPIO interface for push buttons
    status = XGpio_Initialize(&pushbtn, XPAR_BUTTONS_DEVICE_ID);
	if (status != XST_SUCCESS) {
		xil_printf("Push buttons gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

	// Initialize GPIO interface for push buttons
    status = XGpio_Initialize(&leds, XPAR_LEDS_DEVICE_ID);
	if (status != XST_SUCCESS) {
		xil_printf("Push buttons gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

    // Set data pointers for I/O
	// Only channel 1 is used
	// Switches and push buttons are configured as outputs, LEDs as input
    XGpio_SetDataDirection(&switches, 1, 0xFFFFFFFF);
    XGpio_SetDataDirection(&pushbtn, 1, 0xFFFFFFFF);
    XGpio_SetDataDirection(&leds, 1, 0x00000000);

    while (1)
    {
    	// Read switches push buttons
    	switches_value = XGpio_DiscreteRead(&switches, 1);
    	pushbtn_value = XGpio_DiscreteRead(&pushbtn, 1);

    	// Write LEDs value
    	leds_value = switches_value * 16 + pushbtn_value;
    	XGpio_DiscreteWrite(&leds, 1, leds_value);

//    	DSPACE_COM_IP_mWriteReg(XPAR_DSPACE_COM_IP_0_S_AXI_BASEADDR, DSPACE_COM_IP_S_AXI_SLV_REG0_OFFSET, 0x00000001);
//    	PWM_X40_IP_mWriteReg(XPAR_PWM_X40_IP_0_S_AXI_BASEADDR, PWM_X40_IP_S_AXI_SLV_REG0_OFFSET, 0x99999999);
//    	PWM_X40_IP_mWriteReg(XPAR_PWM_X40_IP_0_S_AXI_BASEADDR, PWM_X40_IP_S_AXI_SLV_REG1_OFFSET, 0x99999999);
//    	PWM_X40_IP_mWriteReg(XPAR_PWM_X40_IP_0_S_AXI_BASEADDR, PWM_X40_IP_S_AXI_SLV_REG2_OFFSET, 0x99999999);
//    	PWM_X40_IP_mWriteReg(XPAR_PWM_X40_IP_0_S_AXI_BASEADDR, PWM_X40_IP_S_AXI_SLV_REG3_OFFSET, 0x99999999);
//    	PWM_X40_IP_mWriteReg(XPAR_PWM_X40_IP_0_S_AXI_BASEADDR, PWM_X40_IP_S_AXI_SLV_REG4_OFFSET, 0x99999999);
//    	PWM_X40_IP_mWriteReg(XPAR_PWM_X40_IP_0_S_AXI_BASEADDR, PWM_X40_IP_S_AXI_SLV_REG5_OFFSET, 0x99999999);
//    	PWM_X40_IP_mWriteReg(XPAR_PWM_X40_IP_0_S_AXI_BASEADDR, PWM_X40_IP_S_AXI_SLV_REG6_OFFSET, 0x99999999);
//    	PWM_X40_IP_mWriteReg(XPAR_PWM_X40_IP_0_S_AXI_BASEADDR, PWM_X40_IP_S_AXI_SLV_REG7_OFFSET, 0x99999999);
//    	PWM_X40_IP_mWriteReg(XPAR_PWM_X40_IP_0_S_AXI_BASEADDR, PWM_X40_IP_S_AXI_SLV_REG8_OFFSET, 0x99999999);
//    	PWM_X40_IP_mWriteReg(XPAR_PWM_X40_IP_0_S_AXI_BASEADDR, PWM_X40_IP_S_AXI_SLV_REG9_OFFSET, 0x99999999);
//    	DSPACE_COM_IP_mWriteReg(XPAR_DSPACE_COM_IP_0_S_AXI_BASEADDR, DSPACE_COM_IP_S_AXI_SLV_REG0_OFFSET, 0x00000000);

    	// Debug prints
    	xil_printf("Switches Status: %x\r\n", switches_value);
    	xil_printf("Push buttons Status: %x\r\n", pushbtn_value);
    	xil_printf("LEDs Status: %x\r\n", leds_value);
    }
}

connect -url tcp:127.0.0.1:3121
source /home/fer6flores/git/master-thesis/sw/rice_wb_alg/embedded_version/UZedTest/uzed_test/uzed_test.sdk/system_wrapper_hw_platform_0/ps7_init.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Platform Cable USB II 000013c9e87901"} -index 0
loadhw -hw /home/fer6flores/git/master-thesis/sw/rice_wb_alg/embedded_version/UZedTest/uzed_test/uzed_test.sdk/system_wrapper_hw_platform_0/system.hdf -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Platform Cable USB II 000013c9e87901"} -index 0
stop
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Platform Cable USB II 000013c9e87901"} -index 0
rst -processor
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Platform Cable USB II 000013c9e87901"} -index 0
dow /home/fer6flores/git/master-thesis/sw/rice_wb_alg/embedded_version/UZedTest/uzed_test/uzed_test.sdk/test/Debug/test.elf
configparams force-mem-access 0
bpadd -addr &main

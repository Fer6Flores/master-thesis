-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
-- Date        : Mon Jun 14 19:33:49 2021
-- Host        : FloresToWin running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               H:/GitR/master-thesis/sw/FirstAlg/UZedTest/uzed_test/uzed_test.srcs/sources_1/bd/system/ip/system_dspace_com_ip_0_0/system_dspace_com_ip_0_0_sim_netlist.vhdl
-- Design      : system_dspace_com_ip_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_dspace_com_ip_0_0_N_bits_counter is
  port (
    TIMEOUT_DISABLE_reg : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \ctr_state_reg[1]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    TIMEOUT_DISABLE : in STD_LOGIC;
    RESET : in STD_LOGIC;
    TIMEOUT : in STD_LOGIC;
    timeout_i_reg : in STD_LOGIC;
    \ctr_state_reg[1]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_100MHZ : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_dspace_com_ip_0_0_N_bits_counter : entity is "N_bits_counter";
end system_dspace_com_ip_0_0_N_bits_counter;

architecture STRUCTURE of system_dspace_com_ip_0_0_N_bits_counter is
  signal \ctr_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \ctr_state[1]_i_1_n_0\ : STD_LOGIC;
  signal q : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ctr_state[0]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \ctr_state[1]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \v_alpha[11]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \v_beta[11]_i_1\ : label is "soft_lutpair12";
begin
\ctr_state[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => q(1),
      I1 => q(0),
      O => \ctr_state[0]_i_1_n_0\
    );
\ctr_state[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => q(1),
      I1 => q(0),
      O => \ctr_state[1]_i_1_n_0\
    );
\ctr_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state_reg[1]_1\(0),
      CLR => AR(0),
      D => \ctr_state[0]_i_1_n_0\,
      Q => q(0)
    );
\ctr_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state_reg[1]_1\(0),
      CLR => AR(0),
      D => \ctr_state[1]_i_1_n_0\,
      Q => q(1)
    );
timeout_i_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1010101011111011"
    )
        port map (
      I0 => TIMEOUT_DISABLE,
      I1 => RESET,
      I2 => TIMEOUT,
      I3 => q(1),
      I4 => q(0),
      I5 => timeout_i_reg,
      O => TIMEOUT_DISABLE_reg
    );
\v_alpha[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => q(1),
      I1 => \ctr_state_reg[1]_1\(0),
      I2 => q(0),
      O => \ctr_state_reg[1]_0\(0)
    );
\v_beta[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \ctr_state_reg[1]_1\(0),
      I1 => q(1),
      I2 => q(0),
      O => E(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \system_dspace_com_ip_0_0_N_bits_counter__parameterized1\ is
  port (
    \ctr_state_reg[0]_0\ : out STD_LOGIC;
    init_i_reg : out STD_LOGIC;
    \ctr_state_reg[13]_0\ : in STD_LOGIC;
    INIT_EN : in STD_LOGIC;
    RESET : in STD_LOGIC;
    CLK_100MHZ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \system_dspace_com_ip_0_0_N_bits_counter__parameterized1\ : entity is "N_bits_counter";
end \system_dspace_com_ip_0_0_N_bits_counter__parameterized1\;

architecture STRUCTURE of \system_dspace_com_ip_0_0_N_bits_counter__parameterized1\ is
  signal \ctr_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \ctr_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \ctr_state[13]_i_1_n_0\ : STD_LOGIC;
  signal \ctr_state[13]_i_2_n_0\ : STD_LOGIC;
  signal \^ctr_state_reg[0]_0\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \ctr_state_reg[13]_i_3_n_7\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \ctr_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \ctr_state_reg_n_0_[10]\ : STD_LOGIC;
  signal \ctr_state_reg_n_0_[11]\ : STD_LOGIC;
  signal \ctr_state_reg_n_0_[12]\ : STD_LOGIC;
  signal \ctr_state_reg_n_0_[13]\ : STD_LOGIC;
  signal \ctr_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \ctr_state_reg_n_0_[2]\ : STD_LOGIC;
  signal \ctr_state_reg_n_0_[3]\ : STD_LOGIC;
  signal \ctr_state_reg_n_0_[4]\ : STD_LOGIC;
  signal \ctr_state_reg_n_0_[5]\ : STD_LOGIC;
  signal \ctr_state_reg_n_0_[6]\ : STD_LOGIC;
  signal \ctr_state_reg_n_0_[7]\ : STD_LOGIC;
  signal \ctr_state_reg_n_0_[8]\ : STD_LOGIC;
  signal \ctr_state_reg_n_0_[9]\ : STD_LOGIC;
  signal init_i_i_3_n_0 : STD_LOGIC;
  signal init_i_i_4_n_0 : STD_LOGIC;
  signal init_i_i_5_n_0 : STD_LOGIC;
  signal \NLW_ctr_state_reg[13]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ctr_state_reg[13]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
begin
  \ctr_state_reg[0]_0\ <= \^ctr_state_reg[0]_0\;
\ctr_state[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3002"
    )
        port map (
      I0 => \ctr_state[0]_i_2_n_0\,
      I1 => INIT_EN,
      I2 => \ctr_state_reg[13]_0\,
      I3 => \ctr_state_reg_n_0_[0]\,
      O => \ctr_state[0]_i_1_n_0\
    );
\ctr_state[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFBFFFFFF"
    )
        port map (
      I0 => init_i_i_5_n_0,
      I1 => \ctr_state_reg_n_0_[4]\,
      I2 => \ctr_state_reg_n_0_[5]\,
      I3 => \ctr_state_reg_n_0_[7]\,
      I4 => \ctr_state_reg_n_0_[6]\,
      I5 => init_i_i_3_n_0,
      O => \ctr_state[0]_i_2_n_0\
    );
\ctr_state[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F1"
    )
        port map (
      I0 => \^ctr_state_reg[0]_0\,
      I1 => \ctr_state_reg[13]_0\,
      I2 => INIT_EN,
      O => \ctr_state[13]_i_1_n_0\
    );
\ctr_state[13]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \ctr_state_reg[13]_0\,
      O => \ctr_state[13]_i_2_n_0\
    );
\ctr_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[0]_i_1_n_0\,
      Q => \ctr_state_reg_n_0_[0]\,
      R => '0'
    );
\ctr_state_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state[13]_i_2_n_0\,
      D => \ctr_state_reg[12]_i_1_n_6\,
      Q => \ctr_state_reg_n_0_[10]\,
      R => \ctr_state[13]_i_1_n_0\
    );
\ctr_state_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state[13]_i_2_n_0\,
      D => \ctr_state_reg[12]_i_1_n_5\,
      Q => \ctr_state_reg_n_0_[11]\,
      R => \ctr_state[13]_i_1_n_0\
    );
\ctr_state_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state[13]_i_2_n_0\,
      D => \ctr_state_reg[12]_i_1_n_4\,
      Q => \ctr_state_reg_n_0_[12]\,
      R => \ctr_state[13]_i_1_n_0\
    );
\ctr_state_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[8]_i_1_n_0\,
      CO(3) => \ctr_state_reg[12]_i_1_n_0\,
      CO(2) => \ctr_state_reg[12]_i_1_n_1\,
      CO(1) => \ctr_state_reg[12]_i_1_n_2\,
      CO(0) => \ctr_state_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[12]_i_1_n_4\,
      O(2) => \ctr_state_reg[12]_i_1_n_5\,
      O(1) => \ctr_state_reg[12]_i_1_n_6\,
      O(0) => \ctr_state_reg[12]_i_1_n_7\,
      S(3) => \ctr_state_reg_n_0_[12]\,
      S(2) => \ctr_state_reg_n_0_[11]\,
      S(1) => \ctr_state_reg_n_0_[10]\,
      S(0) => \ctr_state_reg_n_0_[9]\
    );
\ctr_state_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state[13]_i_2_n_0\,
      D => \ctr_state_reg[13]_i_3_n_7\,
      Q => \ctr_state_reg_n_0_[13]\,
      R => \ctr_state[13]_i_1_n_0\
    );
\ctr_state_reg[13]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_ctr_state_reg[13]_i_3_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_ctr_state_reg[13]_i_3_O_UNCONNECTED\(3 downto 1),
      O(0) => \ctr_state_reg[13]_i_3_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \ctr_state_reg_n_0_[13]\
    );
\ctr_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state[13]_i_2_n_0\,
      D => \ctr_state_reg[4]_i_1_n_7\,
      Q => \ctr_state_reg_n_0_[1]\,
      R => \ctr_state[13]_i_1_n_0\
    );
\ctr_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state[13]_i_2_n_0\,
      D => \ctr_state_reg[4]_i_1_n_6\,
      Q => \ctr_state_reg_n_0_[2]\,
      R => \ctr_state[13]_i_1_n_0\
    );
\ctr_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state[13]_i_2_n_0\,
      D => \ctr_state_reg[4]_i_1_n_5\,
      Q => \ctr_state_reg_n_0_[3]\,
      R => \ctr_state[13]_i_1_n_0\
    );
\ctr_state_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state[13]_i_2_n_0\,
      D => \ctr_state_reg[4]_i_1_n_4\,
      Q => \ctr_state_reg_n_0_[4]\,
      R => \ctr_state[13]_i_1_n_0\
    );
\ctr_state_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ctr_state_reg[4]_i_1_n_0\,
      CO(2) => \ctr_state_reg[4]_i_1_n_1\,
      CO(1) => \ctr_state_reg[4]_i_1_n_2\,
      CO(0) => \ctr_state_reg[4]_i_1_n_3\,
      CYINIT => \ctr_state_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[4]_i_1_n_4\,
      O(2) => \ctr_state_reg[4]_i_1_n_5\,
      O(1) => \ctr_state_reg[4]_i_1_n_6\,
      O(0) => \ctr_state_reg[4]_i_1_n_7\,
      S(3) => \ctr_state_reg_n_0_[4]\,
      S(2) => \ctr_state_reg_n_0_[3]\,
      S(1) => \ctr_state_reg_n_0_[2]\,
      S(0) => \ctr_state_reg_n_0_[1]\
    );
\ctr_state_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state[13]_i_2_n_0\,
      D => \ctr_state_reg[8]_i_1_n_7\,
      Q => \ctr_state_reg_n_0_[5]\,
      R => \ctr_state[13]_i_1_n_0\
    );
\ctr_state_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state[13]_i_2_n_0\,
      D => \ctr_state_reg[8]_i_1_n_6\,
      Q => \ctr_state_reg_n_0_[6]\,
      R => \ctr_state[13]_i_1_n_0\
    );
\ctr_state_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state[13]_i_2_n_0\,
      D => \ctr_state_reg[8]_i_1_n_5\,
      Q => \ctr_state_reg_n_0_[7]\,
      R => \ctr_state[13]_i_1_n_0\
    );
\ctr_state_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state[13]_i_2_n_0\,
      D => \ctr_state_reg[8]_i_1_n_4\,
      Q => \ctr_state_reg_n_0_[8]\,
      R => \ctr_state[13]_i_1_n_0\
    );
\ctr_state_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[4]_i_1_n_0\,
      CO(3) => \ctr_state_reg[8]_i_1_n_0\,
      CO(2) => \ctr_state_reg[8]_i_1_n_1\,
      CO(1) => \ctr_state_reg[8]_i_1_n_2\,
      CO(0) => \ctr_state_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[8]_i_1_n_4\,
      O(2) => \ctr_state_reg[8]_i_1_n_5\,
      O(1) => \ctr_state_reg[8]_i_1_n_6\,
      O(0) => \ctr_state_reg[8]_i_1_n_7\,
      S(3) => \ctr_state_reg_n_0_[8]\,
      S(2) => \ctr_state_reg_n_0_[7]\,
      S(1) => \ctr_state_reg_n_0_[6]\,
      S(0) => \ctr_state_reg_n_0_[5]\
    );
\ctr_state_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => \ctr_state[13]_i_2_n_0\,
      D => \ctr_state_reg[12]_i_1_n_7\,
      Q => \ctr_state_reg_n_0_[9]\,
      R => \ctr_state[13]_i_1_n_0\
    );
init_i_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF1B"
    )
        port map (
      I0 => \ctr_state_reg[13]_0\,
      I1 => \^ctr_state_reg[0]_0\,
      I2 => INIT_EN,
      I3 => RESET,
      O => init_i_reg
    );
init_i_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => init_i_i_3_n_0,
      I1 => init_i_i_4_n_0,
      I2 => init_i_i_5_n_0,
      I3 => \ctr_state_reg_n_0_[0]\,
      O => \^ctr_state_reg[0]_0\
    );
init_i_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFFFFFF"
    )
        port map (
      I0 => \ctr_state_reg_n_0_[1]\,
      I1 => \ctr_state_reg_n_0_[12]\,
      I2 => \ctr_state_reg_n_0_[13]\,
      I3 => \ctr_state_reg_n_0_[3]\,
      I4 => \ctr_state_reg_n_0_[2]\,
      O => init_i_i_3_n_0
    );
init_i_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => \ctr_state_reg_n_0_[4]\,
      I1 => \ctr_state_reg_n_0_[5]\,
      I2 => \ctr_state_reg_n_0_[7]\,
      I3 => \ctr_state_reg_n_0_[6]\,
      O => init_i_i_4_n_0
    );
init_i_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => \ctr_state_reg_n_0_[9]\,
      I1 => \ctr_state_reg_n_0_[8]\,
      I2 => \ctr_state_reg_n_0_[10]\,
      I3 => \ctr_state_reg_n_0_[11]\,
      O => init_i_i_5_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_dspace_com_ip_0_0_dspace_com_ip_v1_0_S_AXI is
  port (
    s_axi_awready : out STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    INIT_EN : out STD_LOGIC;
    TIMEOUT_DISABLE : out STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    AR : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_aclk : in STD_LOGIC;
    RESET : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    TIMEOUT : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 11 downto 0 );
    \axi_rdata_reg[11]_0\ : in STD_LOGIC_VECTOR ( 11 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_dspace_com_ip_0_0_dspace_com_ip_v1_0_S_AXI : entity is "dspace_com_ip_v1_0_S_AXI";
end system_dspace_com_ip_0_0_dspace_com_ip_v1_0_S_AXI;

architecture STRUCTURE of system_dspace_com_ip_0_0_dspace_com_ip_v1_0_S_AXI is
  signal \^init_en\ : STD_LOGIC;
  signal INIT_EN0 : STD_LOGIC;
  signal TIMEOUT_DISABLE0 : STD_LOGIC;
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal axi_araddr : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \axi_araddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal axi_awaddr : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \axi_awaddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal p_0_in2_in : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 7 );
  signal past_slv_reg0_0 : STD_LOGIC;
  signal past_slv_reg0_1 : STD_LOGIC;
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_bvalid\ : STD_LOGIC;
  signal \^s_axi_rvalid\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[0]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[10]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[11]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[12]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[13]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[14]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[15]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[16]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[17]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[18]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[19]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[20]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[21]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[22]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[23]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[24]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[25]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[26]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[27]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[28]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[29]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[30]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[31]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[3]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[4]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[5]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[6]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[7]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[8]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[9]\ : STD_LOGIC;
  signal slv_reg_rden : STD_LOGIC;
  signal \slv_reg_wren__2\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_arready_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \axi_rdata[12]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \axi_rdata[13]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \axi_rdata[14]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \axi_rdata[15]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \axi_rdata[16]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \axi_rdata[17]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \axi_rdata[18]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \axi_rdata[19]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \axi_rdata[20]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \axi_rdata[21]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \axi_rdata[22]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \axi_rdata[23]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \axi_rdata[24]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \axi_rdata[25]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \axi_rdata[26]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \axi_rdata[27]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \axi_rdata[28]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \axi_rdata[29]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \axi_rdata[30]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \axi_rdata[31]_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of axi_rvalid_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg0[31]_i_2\ : label is "soft_lutpair1";
begin
  INIT_EN <= \^init_en\;
  s_axi_arready <= \^s_axi_arready\;
  s_axi_awready <= \^s_axi_awready\;
  s_axi_bvalid <= \^s_axi_bvalid\;
  s_axi_rvalid <= \^s_axi_rvalid\;
  s_axi_wready <= \^s_axi_wready\;
INIT_EN_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => past_slv_reg0_0,
      I1 => \slv_reg0_reg_n_0_[0]\,
      O => INIT_EN0
    );
INIT_EN_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => INIT_EN0,
      Q => \^init_en\,
      R => '0'
    );
TIMEOUT_DISABLE_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => past_slv_reg0_1,
      I1 => p_0_in2_in,
      O => TIMEOUT_DISABLE0
    );
TIMEOUT_DISABLE_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => TIMEOUT_DISABLE0,
      Q => TIMEOUT_DISABLE,
      R => '0'
    );
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFBF00BF00BF00"
    )
        port map (
      I0 => \^s_axi_awready\,
      I1 => s_axi_wvalid,
      I2 => s_axi_awvalid,
      I3 => aw_en_reg_n_0,
      I4 => s_axi_bready,
      I5 => \^s_axi_bvalid\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => p_0_in
    );
\axi_araddr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s_axi_araddr(0),
      I1 => s_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => axi_araddr(2),
      O => \axi_araddr[2]_i_1_n_0\
    );
\axi_araddr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s_axi_araddr(1),
      I1 => s_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => axi_araddr(3),
      O => \axi_araddr[3]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => \axi_araddr[2]_i_1_n_0\,
      Q => axi_araddr(2),
      S => p_0_in
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => \axi_araddr[3]_i_1_n_0\,
      Q => axi_araddr(3),
      S => p_0_in
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s_axi_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s_axi_arready\,
      R => p_0_in
    );
\axi_awaddr[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s_axi_awaddr(0),
      I1 => aw_en_reg_n_0,
      I2 => s_axi_awvalid,
      I3 => s_axi_wvalid,
      I4 => \^s_axi_awready\,
      I5 => axi_awaddr(2),
      O => \axi_awaddr[2]_i_1_n_0\
    );
\axi_awaddr[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s_axi_awaddr(1),
      I1 => aw_en_reg_n_0,
      I2 => s_axi_awvalid,
      I3 => s_axi_wvalid,
      I4 => \^s_axi_awready\,
      I5 => axi_awaddr(3),
      O => \axi_awaddr[3]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => \axi_awaddr[2]_i_1_n_0\,
      Q => axi_awaddr(2),
      R => p_0_in
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => \axi_awaddr[3]_i_1_n_0\,
      Q => axi_awaddr(3),
      R => p_0_in
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s_axi_aresetn,
      O => p_0_in
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => s_axi_awvalid,
      I2 => s_axi_wvalid,
      I3 => \^s_axi_awready\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s_axi_awready\,
      R => p_0_in
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s_axi_wvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s_axi_awvalid,
      I4 => s_axi_bready,
      I5 => \^s_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s_axi_bvalid\,
      R => p_0_in
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => TIMEOUT,
      I1 => Q(0),
      I2 => axi_araddr(2),
      I3 => \axi_rdata_reg[11]_0\(0),
      I4 => axi_araddr(3),
      I5 => \slv_reg0_reg_n_0_[0]\,
      O => reg_data_out(0)
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => Q(10),
      I1 => axi_araddr(2),
      I2 => \axi_rdata_reg[11]_0\(10),
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[10]\,
      O => reg_data_out(10)
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => Q(11),
      I1 => axi_araddr(2),
      I2 => \axi_rdata_reg[11]_0\(11),
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[11]\,
      O => reg_data_out(11)
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[12]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(12)
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[13]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(13)
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[14]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(14)
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[15]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(15)
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[16]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(16)
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[17]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(17)
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[18]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(18)
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[19]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(19)
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => Q(1),
      I1 => axi_araddr(2),
      I2 => \axi_rdata_reg[11]_0\(1),
      I3 => axi_araddr(3),
      I4 => p_0_in2_in,
      O => reg_data_out(1)
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[20]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(20)
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[21]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(21)
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[22]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(22)
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[23]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(23)
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[24]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(24)
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[25]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(25)
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[26]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(26)
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[27]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(27)
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[28]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(28)
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[29]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(29)
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => Q(2),
      I1 => axi_araddr(2),
      I2 => \axi_rdata_reg[11]_0\(2),
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[2]\,
      O => reg_data_out(2)
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[30]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(30)
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s_axi_arvalid,
      I2 => \^s_axi_rvalid\,
      O => slv_reg_rden
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[31]\,
      I1 => axi_araddr(2),
      I2 => axi_araddr(3),
      O => reg_data_out(31)
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => Q(3),
      I1 => axi_araddr(2),
      I2 => \axi_rdata_reg[11]_0\(3),
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[3]\,
      O => reg_data_out(3)
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => Q(4),
      I1 => axi_araddr(2),
      I2 => \axi_rdata_reg[11]_0\(4),
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[4]\,
      O => reg_data_out(4)
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => Q(5),
      I1 => axi_araddr(2),
      I2 => \axi_rdata_reg[11]_0\(5),
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[5]\,
      O => reg_data_out(5)
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => Q(6),
      I1 => axi_araddr(2),
      I2 => \axi_rdata_reg[11]_0\(6),
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[6]\,
      O => reg_data_out(6)
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => Q(7),
      I1 => axi_araddr(2),
      I2 => \axi_rdata_reg[11]_0\(7),
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[7]\,
      O => reg_data_out(7)
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => Q(8),
      I1 => axi_araddr(2),
      I2 => \axi_rdata_reg[11]_0\(8),
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[8]\,
      O => reg_data_out(8)
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => Q(9),
      I1 => axi_araddr(2),
      I2 => \axi_rdata_reg[11]_0\(9),
      I3 => axi_araddr(3),
      I4 => \slv_reg0_reg_n_0_[9]\,
      O => reg_data_out(9)
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(0),
      Q => s_axi_rdata(0),
      R => p_0_in
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(10),
      Q => s_axi_rdata(10),
      R => p_0_in
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(11),
      Q => s_axi_rdata(11),
      R => p_0_in
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(12),
      Q => s_axi_rdata(12),
      R => p_0_in
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(13),
      Q => s_axi_rdata(13),
      R => p_0_in
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(14),
      Q => s_axi_rdata(14),
      R => p_0_in
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(15),
      Q => s_axi_rdata(15),
      R => p_0_in
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(16),
      Q => s_axi_rdata(16),
      R => p_0_in
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(17),
      Q => s_axi_rdata(17),
      R => p_0_in
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(18),
      Q => s_axi_rdata(18),
      R => p_0_in
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(19),
      Q => s_axi_rdata(19),
      R => p_0_in
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(1),
      Q => s_axi_rdata(1),
      R => p_0_in
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(20),
      Q => s_axi_rdata(20),
      R => p_0_in
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(21),
      Q => s_axi_rdata(21),
      R => p_0_in
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(22),
      Q => s_axi_rdata(22),
      R => p_0_in
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(23),
      Q => s_axi_rdata(23),
      R => p_0_in
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(24),
      Q => s_axi_rdata(24),
      R => p_0_in
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(25),
      Q => s_axi_rdata(25),
      R => p_0_in
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(26),
      Q => s_axi_rdata(26),
      R => p_0_in
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(27),
      Q => s_axi_rdata(27),
      R => p_0_in
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(28),
      Q => s_axi_rdata(28),
      R => p_0_in
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(29),
      Q => s_axi_rdata(29),
      R => p_0_in
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(2),
      Q => s_axi_rdata(2),
      R => p_0_in
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(30),
      Q => s_axi_rdata(30),
      R => p_0_in
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(31),
      Q => s_axi_rdata(31),
      R => p_0_in
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(3),
      Q => s_axi_rdata(3),
      R => p_0_in
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(4),
      Q => s_axi_rdata(4),
      R => p_0_in
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(5),
      Q => s_axi_rdata(5),
      R => p_0_in
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(6),
      Q => s_axi_rdata(6),
      R => p_0_in
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(7),
      Q => s_axi_rdata(7),
      R => p_0_in
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(8),
      Q => s_axi_rdata(8),
      R => p_0_in
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(9),
      Q => s_axi_rdata(9),
      R => p_0_in
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => s_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \^s_axi_rvalid\,
      I3 => s_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s_axi_rvalid\,
      R => p_0_in
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => aw_en_reg_n_0,
      I1 => s_axi_awvalid,
      I2 => s_axi_wvalid,
      I3 => \^s_axi_wready\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s_axi_wready\,
      R => p_0_in
    );
\ctr_state[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^init_en\,
      I1 => RESET,
      O => AR(0)
    );
past_slv_reg0_0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => \slv_reg0_reg_n_0_[0]\,
      Q => past_slv_reg0_0,
      R => '0'
    );
past_slv_reg0_1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => p_0_in2_in,
      Q => past_slv_reg0_1,
      R => '0'
    );
\slv_reg0[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(2),
      I3 => s_axi_wstrb(1),
      O => p_1_in(15)
    );
\slv_reg0[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(2),
      I3 => s_axi_wstrb(2),
      O => p_1_in(23)
    );
\slv_reg0[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(2),
      I3 => s_axi_wstrb(3),
      O => p_1_in(31)
    );
\slv_reg0[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s_axi_awvalid,
      O => \slv_reg_wren__2\
    );
\slv_reg0[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(2),
      I3 => s_axi_wstrb(0),
      O => p_1_in(7)
    );
\slv_reg0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(0),
      Q => \slv_reg0_reg_n_0_[0]\,
      R => p_0_in
    );
\slv_reg0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(15),
      D => s_axi_wdata(10),
      Q => \slv_reg0_reg_n_0_[10]\,
      R => p_0_in
    );
\slv_reg0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(15),
      D => s_axi_wdata(11),
      Q => \slv_reg0_reg_n_0_[11]\,
      R => p_0_in
    );
\slv_reg0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(15),
      D => s_axi_wdata(12),
      Q => \slv_reg0_reg_n_0_[12]\,
      R => p_0_in
    );
\slv_reg0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(15),
      D => s_axi_wdata(13),
      Q => \slv_reg0_reg_n_0_[13]\,
      R => p_0_in
    );
\slv_reg0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(15),
      D => s_axi_wdata(14),
      Q => \slv_reg0_reg_n_0_[14]\,
      R => p_0_in
    );
\slv_reg0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(15),
      D => s_axi_wdata(15),
      Q => \slv_reg0_reg_n_0_[15]\,
      R => p_0_in
    );
\slv_reg0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(16),
      Q => \slv_reg0_reg_n_0_[16]\,
      R => p_0_in
    );
\slv_reg0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(17),
      Q => \slv_reg0_reg_n_0_[17]\,
      R => p_0_in
    );
\slv_reg0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(18),
      Q => \slv_reg0_reg_n_0_[18]\,
      R => p_0_in
    );
\slv_reg0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(19),
      Q => \slv_reg0_reg_n_0_[19]\,
      R => p_0_in
    );
\slv_reg0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(1),
      Q => p_0_in2_in,
      R => p_0_in
    );
\slv_reg0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(20),
      Q => \slv_reg0_reg_n_0_[20]\,
      R => p_0_in
    );
\slv_reg0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(21),
      Q => \slv_reg0_reg_n_0_[21]\,
      R => p_0_in
    );
\slv_reg0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(22),
      Q => \slv_reg0_reg_n_0_[22]\,
      R => p_0_in
    );
\slv_reg0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(23),
      Q => \slv_reg0_reg_n_0_[23]\,
      R => p_0_in
    );
\slv_reg0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(24),
      Q => \slv_reg0_reg_n_0_[24]\,
      R => p_0_in
    );
\slv_reg0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(25),
      Q => \slv_reg0_reg_n_0_[25]\,
      R => p_0_in
    );
\slv_reg0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(26),
      Q => \slv_reg0_reg_n_0_[26]\,
      R => p_0_in
    );
\slv_reg0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(27),
      Q => \slv_reg0_reg_n_0_[27]\,
      R => p_0_in
    );
\slv_reg0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(28),
      Q => \slv_reg0_reg_n_0_[28]\,
      R => p_0_in
    );
\slv_reg0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(29),
      Q => \slv_reg0_reg_n_0_[29]\,
      R => p_0_in
    );
\slv_reg0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(2),
      Q => \slv_reg0_reg_n_0_[2]\,
      R => p_0_in
    );
\slv_reg0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(30),
      Q => \slv_reg0_reg_n_0_[30]\,
      R => p_0_in
    );
\slv_reg0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(31),
      Q => \slv_reg0_reg_n_0_[31]\,
      R => p_0_in
    );
\slv_reg0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(3),
      Q => \slv_reg0_reg_n_0_[3]\,
      R => p_0_in
    );
\slv_reg0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(4),
      Q => \slv_reg0_reg_n_0_[4]\,
      R => p_0_in
    );
\slv_reg0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(5),
      Q => \slv_reg0_reg_n_0_[5]\,
      R => p_0_in
    );
\slv_reg0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(6),
      Q => \slv_reg0_reg_n_0_[6]\,
      R => p_0_in
    );
\slv_reg0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(7),
      Q => \slv_reg0_reg_n_0_[7]\,
      R => p_0_in
    );
\slv_reg0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(15),
      D => s_axi_wdata(8),
      Q => \slv_reg0_reg_n_0_[8]\,
      R => p_0_in
    );
\slv_reg0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(15),
      D => s_axi_wdata(9),
      Q => \slv_reg0_reg_n_0_[9]\,
      R => p_0_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_dspace_com_ip_0_0_dspace_com_ip_v1_0 is
  port (
    init_i_reg_0 : out STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    TIMEOUT : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    RESET : in STD_LOGIC;
    DSPACE_VECTOR_DATA : in STD_LOGIC_VECTOR ( 11 downto 0 );
    CLK_100MHZ : in STD_LOGIC;
    DSPACE_VECTOR_OK : in STD_LOGIC;
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_aresetn : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_dspace_com_ip_0_0_dspace_com_ip_v1_0 : entity is "dspace_com_ip_v1_0";
end system_dspace_com_ip_0_0_dspace_com_ip_v1_0;

architecture STRUCTURE of system_dspace_com_ip_0_0_dspace_com_ip_v1_0 is
  signal INIT_EN : STD_LOGIC;
  signal \^timeout\ : STD_LOGIC;
  signal TIMEOUT_DISABLE : STD_LOGIC;
  signal counters_reset : STD_LOGIC;
  signal end_com_flag_counter_n_0 : STD_LOGIC;
  signal end_com_flag_counter_n_1 : STD_LOGIC;
  signal \^init_i_reg_0\ : STD_LOGIC;
  signal past_dspace_vector_ok : STD_LOGIC;
  signal timein_counter_n_0 : STD_LOGIC;
  signal v_alpha : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal v_alpha_1 : STD_LOGIC;
  signal v_beta : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal v_beta_0 : STD_LOGIC;
  signal vector_ok_fe : STD_LOGIC;
  signal vector_ok_fe0 : STD_LOGIC;
begin
  TIMEOUT <= \^timeout\;
  init_i_reg_0 <= \^init_i_reg_0\;
dspace_com_ip_v1_0_S_AXI_inst: entity work.system_dspace_com_ip_0_0_dspace_com_ip_v1_0_S_AXI
     port map (
      AR(0) => counters_reset,
      INIT_EN => INIT_EN,
      Q(11 downto 0) => v_alpha(11 downto 0),
      RESET => RESET,
      TIMEOUT => \^timeout\,
      TIMEOUT_DISABLE => TIMEOUT_DISABLE,
      \axi_rdata_reg[11]_0\(11 downto 0) => v_beta(11 downto 0),
      s_axi_aclk => s_axi_aclk,
      s_axi_araddr(1 downto 0) => s_axi_araddr(1 downto 0),
      s_axi_aresetn => s_axi_aresetn,
      s_axi_arready => s_axi_arready,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awaddr(1 downto 0) => s_axi_awaddr(1 downto 0),
      s_axi_awready => s_axi_awready,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_bready => s_axi_bready,
      s_axi_bvalid => s_axi_bvalid,
      s_axi_rdata(31 downto 0) => s_axi_rdata(31 downto 0),
      s_axi_rready => s_axi_rready,
      s_axi_rvalid => s_axi_rvalid,
      s_axi_wdata(31 downto 0) => s_axi_wdata(31 downto 0),
      s_axi_wready => s_axi_wready,
      s_axi_wstrb(3 downto 0) => s_axi_wstrb(3 downto 0),
      s_axi_wvalid => s_axi_wvalid
    );
end_com_flag_counter: entity work.\system_dspace_com_ip_0_0_N_bits_counter__parameterized1\
     port map (
      CLK_100MHZ => CLK_100MHZ,
      INIT_EN => INIT_EN,
      RESET => RESET,
      \ctr_state_reg[0]_0\ => end_com_flag_counter_n_0,
      \ctr_state_reg[13]_0\ => \^init_i_reg_0\,
      init_i_reg => end_com_flag_counter_n_1
    );
init_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => end_com_flag_counter_n_1,
      Q => \^init_i_reg_0\,
      R => '0'
    );
past_dspace_vector_ok_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => DSPACE_VECTOR_OK,
      Q => past_dspace_vector_ok,
      R => '0'
    );
timein_counter: entity work.system_dspace_com_ip_0_0_N_bits_counter
     port map (
      AR(0) => counters_reset,
      CLK_100MHZ => CLK_100MHZ,
      E(0) => v_beta_0,
      RESET => RESET,
      TIMEOUT => \^timeout\,
      TIMEOUT_DISABLE => TIMEOUT_DISABLE,
      TIMEOUT_DISABLE_reg => timein_counter_n_0,
      \ctr_state_reg[1]_0\(0) => v_alpha_1,
      \ctr_state_reg[1]_1\(0) => vector_ok_fe,
      timeout_i_reg => end_com_flag_counter_n_0
    );
timeout_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => timein_counter_n_0,
      Q => \^timeout\,
      R => '0'
    );
\v_alpha_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_alpha_1,
      D => DSPACE_VECTOR_DATA(0),
      Q => v_alpha(0),
      R => RESET
    );
\v_alpha_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_alpha_1,
      D => DSPACE_VECTOR_DATA(10),
      Q => v_alpha(10),
      R => RESET
    );
\v_alpha_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_alpha_1,
      D => DSPACE_VECTOR_DATA(11),
      Q => v_alpha(11),
      R => RESET
    );
\v_alpha_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_alpha_1,
      D => DSPACE_VECTOR_DATA(1),
      Q => v_alpha(1),
      R => RESET
    );
\v_alpha_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_alpha_1,
      D => DSPACE_VECTOR_DATA(2),
      Q => v_alpha(2),
      R => RESET
    );
\v_alpha_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_alpha_1,
      D => DSPACE_VECTOR_DATA(3),
      Q => v_alpha(3),
      R => RESET
    );
\v_alpha_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_alpha_1,
      D => DSPACE_VECTOR_DATA(4),
      Q => v_alpha(4),
      R => RESET
    );
\v_alpha_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_alpha_1,
      D => DSPACE_VECTOR_DATA(5),
      Q => v_alpha(5),
      R => RESET
    );
\v_alpha_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_alpha_1,
      D => DSPACE_VECTOR_DATA(6),
      Q => v_alpha(6),
      R => RESET
    );
\v_alpha_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_alpha_1,
      D => DSPACE_VECTOR_DATA(7),
      Q => v_alpha(7),
      R => RESET
    );
\v_alpha_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_alpha_1,
      D => DSPACE_VECTOR_DATA(8),
      Q => v_alpha(8),
      R => RESET
    );
\v_alpha_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_alpha_1,
      D => DSPACE_VECTOR_DATA(9),
      Q => v_alpha(9),
      R => RESET
    );
\v_beta_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_beta_0,
      D => DSPACE_VECTOR_DATA(0),
      Q => v_beta(0),
      R => RESET
    );
\v_beta_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_beta_0,
      D => DSPACE_VECTOR_DATA(10),
      Q => v_beta(10),
      R => RESET
    );
\v_beta_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_beta_0,
      D => DSPACE_VECTOR_DATA(11),
      Q => v_beta(11),
      R => RESET
    );
\v_beta_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_beta_0,
      D => DSPACE_VECTOR_DATA(1),
      Q => v_beta(1),
      R => RESET
    );
\v_beta_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_beta_0,
      D => DSPACE_VECTOR_DATA(2),
      Q => v_beta(2),
      R => RESET
    );
\v_beta_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_beta_0,
      D => DSPACE_VECTOR_DATA(3),
      Q => v_beta(3),
      R => RESET
    );
\v_beta_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_beta_0,
      D => DSPACE_VECTOR_DATA(4),
      Q => v_beta(4),
      R => RESET
    );
\v_beta_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_beta_0,
      D => DSPACE_VECTOR_DATA(5),
      Q => v_beta(5),
      R => RESET
    );
\v_beta_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_beta_0,
      D => DSPACE_VECTOR_DATA(6),
      Q => v_beta(6),
      R => RESET
    );
\v_beta_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_beta_0,
      D => DSPACE_VECTOR_DATA(7),
      Q => v_beta(7),
      R => RESET
    );
\v_beta_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_beta_0,
      D => DSPACE_VECTOR_DATA(8),
      Q => v_beta(8),
      R => RESET
    );
\v_beta_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => v_beta_0,
      D => DSPACE_VECTOR_DATA(9),
      Q => v_beta(9),
      R => RESET
    );
vector_ok_fe_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => past_dspace_vector_ok,
      I1 => DSPACE_VECTOR_OK,
      O => vector_ok_fe0
    );
vector_ok_fe_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => vector_ok_fe0,
      Q => vector_ok_fe,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_dspace_com_ip_0_0 is
  port (
    DSPACE_INIT : out STD_LOGIC;
    DSPACE_VECTOR_OK : in STD_LOGIC;
    DSPACE_VECTOR_DATA : in STD_LOGIC_VECTOR ( 11 downto 0 );
    TIMEOUT : out STD_LOGIC;
    CLK_100MHZ : in STD_LOGIC;
    RESET : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of system_dspace_com_ip_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of system_dspace_com_ip_0_0 : entity is "system_dspace_com_ip_0_0,dspace_com_ip_v1_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of system_dspace_com_ip_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of system_dspace_com_ip_0_0 : entity is "dspace_com_ip_v1_0,Vivado 2018.3";
end system_dspace_com_ip_0_0;

architecture STRUCTURE of system_dspace_com_ip_0_0 is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of RESET : signal is "xilinx.com:signal:reset:1.0 RESET RST";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of RESET : signal is "XIL_INTERFACENAME RESET, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of s_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S_AXI_CLK CLK";
  attribute x_interface_parameter of s_axi_aclk : signal is "XIL_INTERFACENAME S_AXI_CLK, ASSOCIATED_BUSIF S_AXI, ASSOCIATED_RESET s_axi_aresetn:RESET, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute x_interface_info of s_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S_AXI_RST RST";
  attribute x_interface_parameter of s_axi_aresetn : signal is "XIL_INTERFACENAME S_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARREADY";
  attribute x_interface_info of s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARVALID";
  attribute x_interface_info of s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWREADY";
  attribute x_interface_info of s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWVALID";
  attribute x_interface_info of s_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S_AXI BREADY";
  attribute x_interface_info of s_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI BVALID";
  attribute x_interface_info of s_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S_AXI RREADY";
  attribute x_interface_info of s_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI RVALID";
  attribute x_interface_info of s_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S_AXI WREADY";
  attribute x_interface_info of s_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI WVALID";
  attribute x_interface_info of s_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARADDR";
  attribute x_interface_info of s_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARPROT";
  attribute x_interface_info of s_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWADDR";
  attribute x_interface_parameter of s_axi_awaddr : signal is "XIL_INTERFACENAME S_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of s_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWPROT";
  attribute x_interface_info of s_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S_AXI BRESP";
  attribute x_interface_info of s_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S_AXI RDATA";
  attribute x_interface_info of s_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S_AXI RRESP";
  attribute x_interface_info of s_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S_AXI WDATA";
  attribute x_interface_info of s_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S_AXI WSTRB";
begin
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.system_dspace_com_ip_0_0_dspace_com_ip_v1_0
     port map (
      CLK_100MHZ => CLK_100MHZ,
      DSPACE_VECTOR_DATA(11 downto 0) => DSPACE_VECTOR_DATA(11 downto 0),
      DSPACE_VECTOR_OK => DSPACE_VECTOR_OK,
      RESET => RESET,
      TIMEOUT => TIMEOUT,
      init_i_reg_0 => DSPACE_INIT,
      s_axi_aclk => s_axi_aclk,
      s_axi_araddr(1 downto 0) => s_axi_araddr(3 downto 2),
      s_axi_aresetn => s_axi_aresetn,
      s_axi_arready => s_axi_arready,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awaddr(1 downto 0) => s_axi_awaddr(3 downto 2),
      s_axi_awready => s_axi_awready,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_bready => s_axi_bready,
      s_axi_bvalid => s_axi_bvalid,
      s_axi_rdata(31 downto 0) => s_axi_rdata(31 downto 0),
      s_axi_rready => s_axi_rready,
      s_axi_rvalid => s_axi_rvalid,
      s_axi_wdata(31 downto 0) => s_axi_wdata(31 downto 0),
      s_axi_wready => s_axi_wready,
      s_axi_wstrb(3 downto 0) => s_axi_wstrb(3 downto 0),
      s_axi_wvalid => s_axi_wvalid
    );
end STRUCTURE;

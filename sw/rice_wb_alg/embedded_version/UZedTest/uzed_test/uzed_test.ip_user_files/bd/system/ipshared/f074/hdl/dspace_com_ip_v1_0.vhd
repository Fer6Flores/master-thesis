library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dspace_com_ip_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S_AXI
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 4
	);
	port (
		-- Users to add ports here

		-- User ports ends
		DSPACE_INIT       : out std_logic;
		DSPACE_VECTOR_OK  : in std_logic;
		DSPACE_VECTOR_DATA: in std_logic_vector(11 downto 0);
		TIMEOUT           : out std_logic;
		CLK_100MHZ        : in std_logic;
		RESET             : in std_logic;
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S_AXI
		s_axi_aclk	: in std_logic;
		s_axi_aresetn	: in std_logic;
		s_axi_awaddr	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_awprot	: in std_logic_vector(2 downto 0);
		s_axi_awvalid	: in std_logic;
		s_axi_awready	: out std_logic;
		s_axi_wdata	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		s_axi_wstrb	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		s_axi_wvalid	: in std_logic;
		s_axi_wready	: out std_logic;
		s_axi_bresp	: out std_logic_vector(1 downto 0);
		s_axi_bvalid	: out std_logic;
		s_axi_bready	: in std_logic;
		s_axi_araddr	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_arprot	: in std_logic_vector(2 downto 0);
		s_axi_arvalid	: in std_logic;
		s_axi_arready	: out std_logic;
		s_axi_rdata	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		s_axi_rresp	: out std_logic_vector(1 downto 0);
		s_axi_rvalid	: out std_logic;
		s_axi_rready	: in std_logic
	);
end dspace_com_ip_v1_0;

architecture arch_imp of dspace_com_ip_v1_0 is
    
    -- signals declaration
    signal v_alpha       : std_logic_vector(11 downto 0) := (others => '0');
    signal v_beta       : std_logic_vector(11 downto 0) := (others => '0');
    signal init_en_axi  : std_logic := '0';
    signal timeout_disable : std_logic := '0';
    signal end_com_flag : std_logic := '0';
    signal timein_flag : std_logic := '0';
    signal init_i : std_logic := '1';
    signal not_init_i : std_logic := '0';
    signal vector_ok_fe : std_logic := '0'; 
	signal vector_ok_re : std_logic := '0';
	signal past_dspace_vector_ok: std_logic := '0';
	signal clk_100mhz_i : std_logic := '0';
	signal counters_reset : std_logic := '0';
	signal timeout_i : std_logic := '0';
	signal reg_count : std_logic_vector(1 downto 0) := "00";
	
	-- component declaration
	component dspace_com_ip_v1_0_S_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 4
		);
		port (
		V_ALPHA    : in std_logic_vector(11 downto 0);
		V_BETA    : in std_logic_vector(11 downto 0);
		TIMEOUT   : in std_logic;
		INIT_EN   : out std_logic;
		TIMEOUT_DISABLE : out std_logic;
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component dspace_com_ip_v1_0_S_AXI;
    
    component N_bits_counter is
	   Generic (
	   number_of_bits: integer range 0 to 1023:= 14; -- number of bits
	   initial_state: integer range 0 to 1023:= 0; -- initial state of the counter
	   last_state: integer range 0 to 100000:= 3 -- last state of the counter
	   );
       Port ( clk : in  STD_LOGIC; -- global clock
       async_reset : in  STD_LOGIC; -- global reset
       sync_reset : in  STD_LOGIC; -- synchronous reset
       ce : in  STD_LOGIC; -- clock enable
	   load : in  STD_LOGIC; -- load enable
	   dir : in  STD_LOGIC; -- selection of the count direction (0: descending; 1: ascending)
       d : in std_logic_vector (number_of_bits - 1 downto 0); -- data inputs
       q : out std_logic_vector (number_of_bits - 1 downto 0); -- data outputs
       tc: out std_logic; -- asynchronous terminal count 
	   ceo: out std_logic -- synchronous terminal count 
	   );
    end component N_bits_counter;begin

-- Instantiation of Axi Bus Interface S_AXI
dspace_com_ip_v1_0_S_AXI_inst : dspace_com_ip_v1_0_S_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S_AXI_ADDR_WIDTH
	)
	port map (
	    V_ALPHA     => v_alpha,
	    V_BETA     => v_beta,
	    TIMEOUT    => timeout_i,
	    INIT_EN    => init_en_axi,
	    TIMEOUT_DISABLE => timeout_disable,
		S_AXI_ACLK	=> s_axi_aclk,
		S_AXI_ARESETN	=> s_axi_aresetn,
		S_AXI_AWADDR	=> s_axi_awaddr,
		S_AXI_AWPROT	=> s_axi_awprot,
		S_AXI_AWVALID	=> s_axi_awvalid,
		S_AXI_AWREADY	=> s_axi_awready,
		S_AXI_WDATA	=> s_axi_wdata,
		S_AXI_WSTRB	=> s_axi_wstrb,
		S_AXI_WVALID	=> s_axi_wvalid,
		S_AXI_WREADY	=> s_axi_wready,
		S_AXI_BRESP	=> s_axi_bresp,
		S_AXI_BVALID	=> s_axi_bvalid,
		S_AXI_BREADY	=> s_axi_bready,
		S_AXI_ARADDR	=> s_axi_araddr,
		S_AXI_ARPROT	=> s_axi_arprot,
		S_AXI_ARVALID	=> s_axi_arvalid,
		S_AXI_ARREADY	=> s_axi_arready,
		S_AXI_RDATA	=> s_axi_rdata,
		S_AXI_RRESP	=> s_axi_rresp,
		S_AXI_RVALID	=> s_axi_rvalid,
		S_AXI_RREADY	=> s_axi_rready
	);

    timein_counter : N_bits_counter
    generic map (
        number_of_bits  => 2,
        initial_state   => 0,
        last_state      => 2
    )
    port map (
        clk => clk_100mhz_i,
        async_reset => counters_reset,
        sync_reset => '0',
        ce => vector_ok_fe,               
        load => '0',
        dir => '1',                         
        d => "00",
        q => reg_count,
        tc => timein_flag,
        ceo => open
    );
    
    end_com_flag_counter : N_bits_counter
    generic map (
        number_of_bits  => 14,
        initial_state   => 0,
        last_state      => 10204
    )
    port map (
        clk => clk_100mhz_i,
        async_reset => '0',
        sync_reset => init_en_axi,
        ce => not_init_i,               
        load => '0',
        dir => '1',                         
        d => (others => '0'),
        q => open,
        tc => end_com_flag,
        ceo => open
    );
    
	-- Add user logic here
    init_gen : process(CLK_100MHZ, RESET, init_en_axi, end_com_flag) is
    begin
        if rising_edge(CLK_100MHZ) then
            if (RESET = '1') then
                init_i <= '1';
            else
                if ((init_en_axi = '1') and (end_com_flag = '0')) then
                    init_i <= '0';
                elsif ((init_en_axi = '0') and (end_com_flag = '1')) then
                    init_i <= '1';
                elsif ((init_en_axi = '0') and (end_com_flag = '0')) then
                    init_i <= init_i;
                else 
                    init_i <= not init_i;  
                end if;
            end if;
        end if;
    end process;
    
    vector_ok_edges_gen : process(CLK_100MHZ, init_i, DSPACE_VECTOR_OK) is
    begin
        if rising_edge(CLK_100MHZ) then
            if ((past_dspace_vector_ok = '1') and (DSPACE_VECTOR_OK = '0')) then
                vector_ok_fe <= '1';
            else
                vector_ok_fe <= '0';
            end if;
            if ((past_dspace_vector_ok = '0') and (DSPACE_VECTOR_OK = '1')) then
                vector_ok_re <= '1';
            else
                vector_ok_re <= '0';
            end if;
            past_dspace_vector_ok <= DSPACE_VECTOR_OK;
        end if;
    end process;
    
    timeout_gen : process(CLK_100MHZ, RESET, timeout_disable, end_com_flag, timein_flag) is
    begin
        if rising_edge(CLK_100MHZ) then
            if ((RESET = '1') or (timeout_disable = '1')) then
                timeout_i <= '0';
            elsif ((timein_flag = '0') and (end_com_flag = '1')) then
                timeout_i <= '1';
            else
                timeout_i <= timeout_i;
            end if;
        end if;    
    end process;
        
    data_to_axi : process(CLK_100MHZ, RESET, vector_ok_fe, DSPACE_VECTOR_DATA) is
    begin
        if rising_edge(CLK_100MHZ) then
            if (RESET = '1') then
                v_alpha <= (others => '0');
                v_beta <= (others => '0');
            else
                if (vector_ok_fe = '1') then
                    if (reg_count = "01") then
                        v_alpha <= DSPACE_VECTOR_DATA;
                        v_beta <= v_beta;
                    elsif (reg_count = "10") then
                        v_alpha <= v_alpha;
                        v_beta <= DSPACE_VECTOR_DATA;
                    else 
                        v_alpha <= v_alpha;
                        v_beta <= v_beta;
                    end if;
                end if;
            end if;
        end if; 
    end process;
    
    -- Signals assignment
    clk_100mhz_i <= CLK_100MHZ;
    DSPACE_INIT <= init_i;
    not_init_i <= not init_i;
    counters_reset <= init_en_axi or RESET;
    TIMEOUT <= timeout_i;
   
	-- User logic ends

end arch_imp;

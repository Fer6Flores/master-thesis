// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Mon Jun 14 19:33:49 2021
// Host        : FloresToWin running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               H:/GitR/master-thesis/sw/FirstAlg/UZedTest/uzed_test/uzed_test.srcs/sources_1/bd/system/ip/system_dspace_com_ip_0_0/system_dspace_com_ip_0_0_sim_netlist.v
// Design      : system_dspace_com_ip_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "system_dspace_com_ip_0_0,dspace_com_ip_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "dspace_com_ip_v1_0,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module system_dspace_com_ip_0_0
   (DSPACE_INIT,
    DSPACE_VECTOR_OK,
    DSPACE_VECTOR_DATA,
    TIMEOUT,
    CLK_100MHZ,
    RESET,
    s_axi_awaddr,
    s_axi_awprot,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_araddr,
    s_axi_arprot,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_rready,
    s_axi_aclk,
    s_axi_aresetn);
  output DSPACE_INIT;
  input DSPACE_VECTOR_OK;
  input [11:0]DSPACE_VECTOR_DATA;
  output TIMEOUT;
  input CLK_100MHZ;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 RESET RST" *) (* x_interface_parameter = "XIL_INTERFACENAME RESET, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input RESET;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [3:0]s_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWPROT" *) input [2:0]s_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWVALID" *) input s_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWREADY" *) output s_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WDATA" *) input [31:0]s_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WSTRB" *) input [3:0]s_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WVALID" *) input s_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WREADY" *) output s_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI BRESP" *) output [1:0]s_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI BVALID" *) output s_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI BREADY" *) input s_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARADDR" *) input [3:0]s_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARPROT" *) input [2:0]s_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARVALID" *) input s_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARREADY" *) output s_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RDATA" *) output [31:0]s_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RRESP" *) output [1:0]s_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RVALID" *) output s_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RREADY" *) input s_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S_AXI_CLK, ASSOCIATED_BUSIF S_AXI, ASSOCIATED_RESET s_axi_aresetn:RESET, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s_axi_aresetn;

  wire \<const0> ;
  wire CLK_100MHZ;
  wire DSPACE_INIT;
  wire [11:0]DSPACE_VECTOR_DATA;
  wire DSPACE_VECTOR_OK;
  wire RESET;
  wire TIMEOUT;
  wire s_axi_aclk;
  wire [3:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [3:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire [3:0]s_axi_wstrb;
  wire s_axi_wvalid;

  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_rresp[1] = \<const0> ;
  assign s_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  system_dspace_com_ip_0_0_dspace_com_ip_v1_0 U0
       (.CLK_100MHZ(CLK_100MHZ),
        .DSPACE_VECTOR_DATA(DSPACE_VECTOR_DATA),
        .DSPACE_VECTOR_OK(DSPACE_VECTOR_OK),
        .RESET(RESET),
        .TIMEOUT(TIMEOUT),
        .init_i_reg_0(DSPACE_INIT),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_araddr(s_axi_araddr[3:2]),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr[3:2]),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata(s_axi_wdata),
        .s_axi_wready(s_axi_wready),
        .s_axi_wstrb(s_axi_wstrb),
        .s_axi_wvalid(s_axi_wvalid));
endmodule

(* ORIG_REF_NAME = "N_bits_counter" *) 
module system_dspace_com_ip_0_0_N_bits_counter
   (TIMEOUT_DISABLE_reg,
    E,
    \ctr_state_reg[1]_0 ,
    TIMEOUT_DISABLE,
    RESET,
    TIMEOUT,
    timeout_i_reg,
    \ctr_state_reg[1]_1 ,
    CLK_100MHZ,
    AR);
  output TIMEOUT_DISABLE_reg;
  output [0:0]E;
  output [0:0]\ctr_state_reg[1]_0 ;
  input TIMEOUT_DISABLE;
  input RESET;
  input TIMEOUT;
  input timeout_i_reg;
  input [0:0]\ctr_state_reg[1]_1 ;
  input CLK_100MHZ;
  input [0:0]AR;

  wire [0:0]AR;
  wire CLK_100MHZ;
  wire [0:0]E;
  wire RESET;
  wire TIMEOUT;
  wire TIMEOUT_DISABLE;
  wire TIMEOUT_DISABLE_reg;
  wire \ctr_state[0]_i_1_n_0 ;
  wire \ctr_state[1]_i_1_n_0 ;
  wire [0:0]\ctr_state_reg[1]_0 ;
  wire [0:0]\ctr_state_reg[1]_1 ;
  wire [1:0]q;
  wire timeout_i_reg;

  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \ctr_state[0]_i_1 
       (.I0(q[1]),
        .I1(q[0]),
        .O(\ctr_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \ctr_state[1]_i_1 
       (.I0(q[1]),
        .I1(q[0]),
        .O(\ctr_state[1]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \ctr_state_reg[0] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state_reg[1]_1 ),
        .CLR(AR),
        .D(\ctr_state[0]_i_1_n_0 ),
        .Q(q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \ctr_state_reg[1] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state_reg[1]_1 ),
        .CLR(AR),
        .D(\ctr_state[1]_i_1_n_0 ),
        .Q(q[1]));
  LUT6 #(
    .INIT(64'h1010101011111011)) 
    timeout_i_i_1
       (.I0(TIMEOUT_DISABLE),
        .I1(RESET),
        .I2(TIMEOUT),
        .I3(q[1]),
        .I4(q[0]),
        .I5(timeout_i_reg),
        .O(TIMEOUT_DISABLE_reg));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \v_alpha[11]_i_1 
       (.I0(q[1]),
        .I1(\ctr_state_reg[1]_1 ),
        .I2(q[0]),
        .O(\ctr_state_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \v_beta[11]_i_1 
       (.I0(\ctr_state_reg[1]_1 ),
        .I1(q[1]),
        .I2(q[0]),
        .O(E));
endmodule

(* ORIG_REF_NAME = "N_bits_counter" *) 
module system_dspace_com_ip_0_0_N_bits_counter__parameterized1
   (\ctr_state_reg[0]_0 ,
    init_i_reg,
    \ctr_state_reg[13]_0 ,
    INIT_EN,
    RESET,
    CLK_100MHZ);
  output \ctr_state_reg[0]_0 ;
  output init_i_reg;
  input \ctr_state_reg[13]_0 ;
  input INIT_EN;
  input RESET;
  input CLK_100MHZ;

  wire CLK_100MHZ;
  wire INIT_EN;
  wire RESET;
  wire \ctr_state[0]_i_1_n_0 ;
  wire \ctr_state[0]_i_2_n_0 ;
  wire \ctr_state[13]_i_1_n_0 ;
  wire \ctr_state[13]_i_2_n_0 ;
  wire \ctr_state_reg[0]_0 ;
  wire \ctr_state_reg[12]_i_1_n_0 ;
  wire \ctr_state_reg[12]_i_1_n_1 ;
  wire \ctr_state_reg[12]_i_1_n_2 ;
  wire \ctr_state_reg[12]_i_1_n_3 ;
  wire \ctr_state_reg[12]_i_1_n_4 ;
  wire \ctr_state_reg[12]_i_1_n_5 ;
  wire \ctr_state_reg[12]_i_1_n_6 ;
  wire \ctr_state_reg[12]_i_1_n_7 ;
  wire \ctr_state_reg[13]_0 ;
  wire \ctr_state_reg[13]_i_3_n_7 ;
  wire \ctr_state_reg[4]_i_1_n_0 ;
  wire \ctr_state_reg[4]_i_1_n_1 ;
  wire \ctr_state_reg[4]_i_1_n_2 ;
  wire \ctr_state_reg[4]_i_1_n_3 ;
  wire \ctr_state_reg[4]_i_1_n_4 ;
  wire \ctr_state_reg[4]_i_1_n_5 ;
  wire \ctr_state_reg[4]_i_1_n_6 ;
  wire \ctr_state_reg[4]_i_1_n_7 ;
  wire \ctr_state_reg[8]_i_1_n_0 ;
  wire \ctr_state_reg[8]_i_1_n_1 ;
  wire \ctr_state_reg[8]_i_1_n_2 ;
  wire \ctr_state_reg[8]_i_1_n_3 ;
  wire \ctr_state_reg[8]_i_1_n_4 ;
  wire \ctr_state_reg[8]_i_1_n_5 ;
  wire \ctr_state_reg[8]_i_1_n_6 ;
  wire \ctr_state_reg[8]_i_1_n_7 ;
  wire \ctr_state_reg_n_0_[0] ;
  wire \ctr_state_reg_n_0_[10] ;
  wire \ctr_state_reg_n_0_[11] ;
  wire \ctr_state_reg_n_0_[12] ;
  wire \ctr_state_reg_n_0_[13] ;
  wire \ctr_state_reg_n_0_[1] ;
  wire \ctr_state_reg_n_0_[2] ;
  wire \ctr_state_reg_n_0_[3] ;
  wire \ctr_state_reg_n_0_[4] ;
  wire \ctr_state_reg_n_0_[5] ;
  wire \ctr_state_reg_n_0_[6] ;
  wire \ctr_state_reg_n_0_[7] ;
  wire \ctr_state_reg_n_0_[8] ;
  wire \ctr_state_reg_n_0_[9] ;
  wire init_i_i_3_n_0;
  wire init_i_i_4_n_0;
  wire init_i_i_5_n_0;
  wire init_i_reg;
  wire [3:0]\NLW_ctr_state_reg[13]_i_3_CO_UNCONNECTED ;
  wire [3:1]\NLW_ctr_state_reg[13]_i_3_O_UNCONNECTED ;

  LUT4 #(
    .INIT(16'h3002)) 
    \ctr_state[0]_i_1 
       (.I0(\ctr_state[0]_i_2_n_0 ),
        .I1(INIT_EN),
        .I2(\ctr_state_reg[13]_0 ),
        .I3(\ctr_state_reg_n_0_[0] ),
        .O(\ctr_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFBFFFFFF)) 
    \ctr_state[0]_i_2 
       (.I0(init_i_i_5_n_0),
        .I1(\ctr_state_reg_n_0_[4] ),
        .I2(\ctr_state_reg_n_0_[5] ),
        .I3(\ctr_state_reg_n_0_[7] ),
        .I4(\ctr_state_reg_n_0_[6] ),
        .I5(init_i_i_3_n_0),
        .O(\ctr_state[0]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hF1)) 
    \ctr_state[13]_i_1 
       (.I0(\ctr_state_reg[0]_0 ),
        .I1(\ctr_state_reg[13]_0 ),
        .I2(INIT_EN),
        .O(\ctr_state[13]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[13]_i_2 
       (.I0(\ctr_state_reg[13]_0 ),
        .O(\ctr_state[13]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[0] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[0]_i_1_n_0 ),
        .Q(\ctr_state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[10] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state[13]_i_2_n_0 ),
        .D(\ctr_state_reg[12]_i_1_n_6 ),
        .Q(\ctr_state_reg_n_0_[10] ),
        .R(\ctr_state[13]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[11] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state[13]_i_2_n_0 ),
        .D(\ctr_state_reg[12]_i_1_n_5 ),
        .Q(\ctr_state_reg_n_0_[11] ),
        .R(\ctr_state[13]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[12] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state[13]_i_2_n_0 ),
        .D(\ctr_state_reg[12]_i_1_n_4 ),
        .Q(\ctr_state_reg_n_0_[12] ),
        .R(\ctr_state[13]_i_1_n_0 ));
  CARRY4 \ctr_state_reg[12]_i_1 
       (.CI(\ctr_state_reg[8]_i_1_n_0 ),
        .CO({\ctr_state_reg[12]_i_1_n_0 ,\ctr_state_reg[12]_i_1_n_1 ,\ctr_state_reg[12]_i_1_n_2 ,\ctr_state_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[12]_i_1_n_4 ,\ctr_state_reg[12]_i_1_n_5 ,\ctr_state_reg[12]_i_1_n_6 ,\ctr_state_reg[12]_i_1_n_7 }),
        .S({\ctr_state_reg_n_0_[12] ,\ctr_state_reg_n_0_[11] ,\ctr_state_reg_n_0_[10] ,\ctr_state_reg_n_0_[9] }));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[13] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state[13]_i_2_n_0 ),
        .D(\ctr_state_reg[13]_i_3_n_7 ),
        .Q(\ctr_state_reg_n_0_[13] ),
        .R(\ctr_state[13]_i_1_n_0 ));
  CARRY4 \ctr_state_reg[13]_i_3 
       (.CI(\ctr_state_reg[12]_i_1_n_0 ),
        .CO(\NLW_ctr_state_reg[13]_i_3_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_ctr_state_reg[13]_i_3_O_UNCONNECTED [3:1],\ctr_state_reg[13]_i_3_n_7 }),
        .S({1'b0,1'b0,1'b0,\ctr_state_reg_n_0_[13] }));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[1] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state[13]_i_2_n_0 ),
        .D(\ctr_state_reg[4]_i_1_n_7 ),
        .Q(\ctr_state_reg_n_0_[1] ),
        .R(\ctr_state[13]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[2] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state[13]_i_2_n_0 ),
        .D(\ctr_state_reg[4]_i_1_n_6 ),
        .Q(\ctr_state_reg_n_0_[2] ),
        .R(\ctr_state[13]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[3] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state[13]_i_2_n_0 ),
        .D(\ctr_state_reg[4]_i_1_n_5 ),
        .Q(\ctr_state_reg_n_0_[3] ),
        .R(\ctr_state[13]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[4] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state[13]_i_2_n_0 ),
        .D(\ctr_state_reg[4]_i_1_n_4 ),
        .Q(\ctr_state_reg_n_0_[4] ),
        .R(\ctr_state[13]_i_1_n_0 ));
  CARRY4 \ctr_state_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\ctr_state_reg[4]_i_1_n_0 ,\ctr_state_reg[4]_i_1_n_1 ,\ctr_state_reg[4]_i_1_n_2 ,\ctr_state_reg[4]_i_1_n_3 }),
        .CYINIT(\ctr_state_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[4]_i_1_n_4 ,\ctr_state_reg[4]_i_1_n_5 ,\ctr_state_reg[4]_i_1_n_6 ,\ctr_state_reg[4]_i_1_n_7 }),
        .S({\ctr_state_reg_n_0_[4] ,\ctr_state_reg_n_0_[3] ,\ctr_state_reg_n_0_[2] ,\ctr_state_reg_n_0_[1] }));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[5] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state[13]_i_2_n_0 ),
        .D(\ctr_state_reg[8]_i_1_n_7 ),
        .Q(\ctr_state_reg_n_0_[5] ),
        .R(\ctr_state[13]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[6] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state[13]_i_2_n_0 ),
        .D(\ctr_state_reg[8]_i_1_n_6 ),
        .Q(\ctr_state_reg_n_0_[6] ),
        .R(\ctr_state[13]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[7] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state[13]_i_2_n_0 ),
        .D(\ctr_state_reg[8]_i_1_n_5 ),
        .Q(\ctr_state_reg_n_0_[7] ),
        .R(\ctr_state[13]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[8] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state[13]_i_2_n_0 ),
        .D(\ctr_state_reg[8]_i_1_n_4 ),
        .Q(\ctr_state_reg_n_0_[8] ),
        .R(\ctr_state[13]_i_1_n_0 ));
  CARRY4 \ctr_state_reg[8]_i_1 
       (.CI(\ctr_state_reg[4]_i_1_n_0 ),
        .CO({\ctr_state_reg[8]_i_1_n_0 ,\ctr_state_reg[8]_i_1_n_1 ,\ctr_state_reg[8]_i_1_n_2 ,\ctr_state_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[8]_i_1_n_4 ,\ctr_state_reg[8]_i_1_n_5 ,\ctr_state_reg[8]_i_1_n_6 ,\ctr_state_reg[8]_i_1_n_7 }),
        .S({\ctr_state_reg_n_0_[8] ,\ctr_state_reg_n_0_[7] ,\ctr_state_reg_n_0_[6] ,\ctr_state_reg_n_0_[5] }));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[9] 
       (.C(CLK_100MHZ),
        .CE(\ctr_state[13]_i_2_n_0 ),
        .D(\ctr_state_reg[12]_i_1_n_7 ),
        .Q(\ctr_state_reg_n_0_[9] ),
        .R(\ctr_state[13]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFF1B)) 
    init_i_i_1
       (.I0(\ctr_state_reg[13]_0 ),
        .I1(\ctr_state_reg[0]_0 ),
        .I2(INIT_EN),
        .I3(RESET),
        .O(init_i_reg));
  LUT4 #(
    .INIT(16'hFFFE)) 
    init_i_i_2
       (.I0(init_i_i_3_n_0),
        .I1(init_i_i_4_n_0),
        .I2(init_i_i_5_n_0),
        .I3(\ctr_state_reg_n_0_[0] ),
        .O(\ctr_state_reg[0]_0 ));
  LUT5 #(
    .INIT(32'hEFFFFFFF)) 
    init_i_i_3
       (.I0(\ctr_state_reg_n_0_[1] ),
        .I1(\ctr_state_reg_n_0_[12] ),
        .I2(\ctr_state_reg_n_0_[13] ),
        .I3(\ctr_state_reg_n_0_[3] ),
        .I4(\ctr_state_reg_n_0_[2] ),
        .O(init_i_i_3_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    init_i_i_4
       (.I0(\ctr_state_reg_n_0_[4] ),
        .I1(\ctr_state_reg_n_0_[5] ),
        .I2(\ctr_state_reg_n_0_[7] ),
        .I3(\ctr_state_reg_n_0_[6] ),
        .O(init_i_i_4_n_0));
  LUT4 #(
    .INIT(16'hFF7F)) 
    init_i_i_5
       (.I0(\ctr_state_reg_n_0_[9] ),
        .I1(\ctr_state_reg_n_0_[8] ),
        .I2(\ctr_state_reg_n_0_[10] ),
        .I3(\ctr_state_reg_n_0_[11] ),
        .O(init_i_i_5_n_0));
endmodule

(* ORIG_REF_NAME = "dspace_com_ip_v1_0" *) 
module system_dspace_com_ip_0_0_dspace_com_ip_v1_0
   (init_i_reg_0,
    s_axi_awready,
    s_axi_wready,
    s_axi_arready,
    s_axi_rdata,
    TIMEOUT,
    s_axi_rvalid,
    s_axi_bvalid,
    s_axi_aclk,
    s_axi_awaddr,
    s_axi_awvalid,
    s_axi_wvalid,
    s_axi_wdata,
    s_axi_araddr,
    s_axi_arvalid,
    RESET,
    DSPACE_VECTOR_DATA,
    CLK_100MHZ,
    DSPACE_VECTOR_OK,
    s_axi_wstrb,
    s_axi_aresetn,
    s_axi_bready,
    s_axi_rready);
  output init_i_reg_0;
  output s_axi_awready;
  output s_axi_wready;
  output s_axi_arready;
  output [31:0]s_axi_rdata;
  output TIMEOUT;
  output s_axi_rvalid;
  output s_axi_bvalid;
  input s_axi_aclk;
  input [1:0]s_axi_awaddr;
  input s_axi_awvalid;
  input s_axi_wvalid;
  input [31:0]s_axi_wdata;
  input [1:0]s_axi_araddr;
  input s_axi_arvalid;
  input RESET;
  input [11:0]DSPACE_VECTOR_DATA;
  input CLK_100MHZ;
  input DSPACE_VECTOR_OK;
  input [3:0]s_axi_wstrb;
  input s_axi_aresetn;
  input s_axi_bready;
  input s_axi_rready;

  wire CLK_100MHZ;
  wire [11:0]DSPACE_VECTOR_DATA;
  wire DSPACE_VECTOR_OK;
  wire INIT_EN;
  wire RESET;
  wire TIMEOUT;
  wire TIMEOUT_DISABLE;
  wire counters_reset;
  wire end_com_flag_counter_n_0;
  wire end_com_flag_counter_n_1;
  wire init_i_reg_0;
  wire past_dspace_vector_ok;
  wire s_axi_aclk;
  wire [1:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [1:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire [3:0]s_axi_wstrb;
  wire s_axi_wvalid;
  wire timein_counter_n_0;
  wire [11:0]v_alpha;
  wire v_alpha_1;
  wire [11:0]v_beta;
  wire v_beta_0;
  wire vector_ok_fe;
  wire vector_ok_fe0;

  system_dspace_com_ip_0_0_dspace_com_ip_v1_0_S_AXI dspace_com_ip_v1_0_S_AXI_inst
       (.AR(counters_reset),
        .INIT_EN(INIT_EN),
        .Q(v_alpha),
        .RESET(RESET),
        .TIMEOUT(TIMEOUT),
        .TIMEOUT_DISABLE(TIMEOUT_DISABLE),
        .\axi_rdata_reg[11]_0 (v_beta),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_araddr(s_axi_araddr),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata(s_axi_wdata),
        .s_axi_wready(s_axi_wready),
        .s_axi_wstrb(s_axi_wstrb),
        .s_axi_wvalid(s_axi_wvalid));
  system_dspace_com_ip_0_0_N_bits_counter__parameterized1 end_com_flag_counter
       (.CLK_100MHZ(CLK_100MHZ),
        .INIT_EN(INIT_EN),
        .RESET(RESET),
        .\ctr_state_reg[0]_0 (end_com_flag_counter_n_0),
        .\ctr_state_reg[13]_0 (init_i_reg_0),
        .init_i_reg(end_com_flag_counter_n_1));
  FDRE #(
    .INIT(1'b1)) 
    init_i_reg
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(end_com_flag_counter_n_1),
        .Q(init_i_reg_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    past_dspace_vector_ok_reg
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(DSPACE_VECTOR_OK),
        .Q(past_dspace_vector_ok),
        .R(1'b0));
  system_dspace_com_ip_0_0_N_bits_counter timein_counter
       (.AR(counters_reset),
        .CLK_100MHZ(CLK_100MHZ),
        .E(v_beta_0),
        .RESET(RESET),
        .TIMEOUT(TIMEOUT),
        .TIMEOUT_DISABLE(TIMEOUT_DISABLE),
        .TIMEOUT_DISABLE_reg(timein_counter_n_0),
        .\ctr_state_reg[1]_0 (v_alpha_1),
        .\ctr_state_reg[1]_1 (vector_ok_fe),
        .timeout_i_reg(end_com_flag_counter_n_0));
  FDRE #(
    .INIT(1'b0)) 
    timeout_i_reg
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(timein_counter_n_0),
        .Q(TIMEOUT),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \v_alpha_reg[0] 
       (.C(CLK_100MHZ),
        .CE(v_alpha_1),
        .D(DSPACE_VECTOR_DATA[0]),
        .Q(v_alpha[0]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_alpha_reg[10] 
       (.C(CLK_100MHZ),
        .CE(v_alpha_1),
        .D(DSPACE_VECTOR_DATA[10]),
        .Q(v_alpha[10]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_alpha_reg[11] 
       (.C(CLK_100MHZ),
        .CE(v_alpha_1),
        .D(DSPACE_VECTOR_DATA[11]),
        .Q(v_alpha[11]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_alpha_reg[1] 
       (.C(CLK_100MHZ),
        .CE(v_alpha_1),
        .D(DSPACE_VECTOR_DATA[1]),
        .Q(v_alpha[1]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_alpha_reg[2] 
       (.C(CLK_100MHZ),
        .CE(v_alpha_1),
        .D(DSPACE_VECTOR_DATA[2]),
        .Q(v_alpha[2]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_alpha_reg[3] 
       (.C(CLK_100MHZ),
        .CE(v_alpha_1),
        .D(DSPACE_VECTOR_DATA[3]),
        .Q(v_alpha[3]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_alpha_reg[4] 
       (.C(CLK_100MHZ),
        .CE(v_alpha_1),
        .D(DSPACE_VECTOR_DATA[4]),
        .Q(v_alpha[4]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_alpha_reg[5] 
       (.C(CLK_100MHZ),
        .CE(v_alpha_1),
        .D(DSPACE_VECTOR_DATA[5]),
        .Q(v_alpha[5]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_alpha_reg[6] 
       (.C(CLK_100MHZ),
        .CE(v_alpha_1),
        .D(DSPACE_VECTOR_DATA[6]),
        .Q(v_alpha[6]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_alpha_reg[7] 
       (.C(CLK_100MHZ),
        .CE(v_alpha_1),
        .D(DSPACE_VECTOR_DATA[7]),
        .Q(v_alpha[7]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_alpha_reg[8] 
       (.C(CLK_100MHZ),
        .CE(v_alpha_1),
        .D(DSPACE_VECTOR_DATA[8]),
        .Q(v_alpha[8]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_alpha_reg[9] 
       (.C(CLK_100MHZ),
        .CE(v_alpha_1),
        .D(DSPACE_VECTOR_DATA[9]),
        .Q(v_alpha[9]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_beta_reg[0] 
       (.C(CLK_100MHZ),
        .CE(v_beta_0),
        .D(DSPACE_VECTOR_DATA[0]),
        .Q(v_beta[0]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_beta_reg[10] 
       (.C(CLK_100MHZ),
        .CE(v_beta_0),
        .D(DSPACE_VECTOR_DATA[10]),
        .Q(v_beta[10]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_beta_reg[11] 
       (.C(CLK_100MHZ),
        .CE(v_beta_0),
        .D(DSPACE_VECTOR_DATA[11]),
        .Q(v_beta[11]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_beta_reg[1] 
       (.C(CLK_100MHZ),
        .CE(v_beta_0),
        .D(DSPACE_VECTOR_DATA[1]),
        .Q(v_beta[1]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_beta_reg[2] 
       (.C(CLK_100MHZ),
        .CE(v_beta_0),
        .D(DSPACE_VECTOR_DATA[2]),
        .Q(v_beta[2]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_beta_reg[3] 
       (.C(CLK_100MHZ),
        .CE(v_beta_0),
        .D(DSPACE_VECTOR_DATA[3]),
        .Q(v_beta[3]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_beta_reg[4] 
       (.C(CLK_100MHZ),
        .CE(v_beta_0),
        .D(DSPACE_VECTOR_DATA[4]),
        .Q(v_beta[4]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_beta_reg[5] 
       (.C(CLK_100MHZ),
        .CE(v_beta_0),
        .D(DSPACE_VECTOR_DATA[5]),
        .Q(v_beta[5]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_beta_reg[6] 
       (.C(CLK_100MHZ),
        .CE(v_beta_0),
        .D(DSPACE_VECTOR_DATA[6]),
        .Q(v_beta[6]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_beta_reg[7] 
       (.C(CLK_100MHZ),
        .CE(v_beta_0),
        .D(DSPACE_VECTOR_DATA[7]),
        .Q(v_beta[7]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_beta_reg[8] 
       (.C(CLK_100MHZ),
        .CE(v_beta_0),
        .D(DSPACE_VECTOR_DATA[8]),
        .Q(v_beta[8]),
        .R(RESET));
  FDRE #(
    .INIT(1'b0)) 
    \v_beta_reg[9] 
       (.C(CLK_100MHZ),
        .CE(v_beta_0),
        .D(DSPACE_VECTOR_DATA[9]),
        .Q(v_beta[9]),
        .R(RESET));
  LUT2 #(
    .INIT(4'h2)) 
    vector_ok_fe_i_1
       (.I0(past_dspace_vector_ok),
        .I1(DSPACE_VECTOR_OK),
        .O(vector_ok_fe0));
  FDRE #(
    .INIT(1'b0)) 
    vector_ok_fe_reg
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(vector_ok_fe0),
        .Q(vector_ok_fe),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "dspace_com_ip_v1_0_S_AXI" *) 
module system_dspace_com_ip_0_0_dspace_com_ip_v1_0_S_AXI
   (s_axi_awready,
    s_axi_wready,
    INIT_EN,
    TIMEOUT_DISABLE,
    s_axi_arready,
    s_axi_bvalid,
    s_axi_rvalid,
    AR,
    s_axi_rdata,
    s_axi_aclk,
    RESET,
    s_axi_aresetn,
    s_axi_wvalid,
    s_axi_awvalid,
    s_axi_bready,
    s_axi_arvalid,
    s_axi_rready,
    s_axi_awaddr,
    s_axi_wdata,
    s_axi_araddr,
    s_axi_wstrb,
    TIMEOUT,
    Q,
    \axi_rdata_reg[11]_0 );
  output s_axi_awready;
  output s_axi_wready;
  output INIT_EN;
  output TIMEOUT_DISABLE;
  output s_axi_arready;
  output s_axi_bvalid;
  output s_axi_rvalid;
  output [0:0]AR;
  output [31:0]s_axi_rdata;
  input s_axi_aclk;
  input RESET;
  input s_axi_aresetn;
  input s_axi_wvalid;
  input s_axi_awvalid;
  input s_axi_bready;
  input s_axi_arvalid;
  input s_axi_rready;
  input [1:0]s_axi_awaddr;
  input [31:0]s_axi_wdata;
  input [1:0]s_axi_araddr;
  input [3:0]s_axi_wstrb;
  input TIMEOUT;
  input [11:0]Q;
  input [11:0]\axi_rdata_reg[11]_0 ;

  wire [0:0]AR;
  wire INIT_EN;
  wire INIT_EN0;
  wire [11:0]Q;
  wire RESET;
  wire TIMEOUT;
  wire TIMEOUT_DISABLE;
  wire TIMEOUT_DISABLE0;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire [3:2]axi_araddr;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire axi_arready0;
  wire [3:2]axi_awaddr;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_bvalid_i_1_n_0;
  wire [11:0]\axi_rdata_reg[11]_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire p_0_in;
  wire p_0_in2_in;
  wire [31:7]p_1_in;
  wire past_slv_reg0_0;
  wire past_slv_reg0_1;
  wire [31:0]reg_data_out;
  wire s_axi_aclk;
  wire [1:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [1:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire [3:0]s_axi_wstrb;
  wire s_axi_wvalid;
  wire \slv_reg0_reg_n_0_[0] ;
  wire \slv_reg0_reg_n_0_[10] ;
  wire \slv_reg0_reg_n_0_[11] ;
  wire \slv_reg0_reg_n_0_[12] ;
  wire \slv_reg0_reg_n_0_[13] ;
  wire \slv_reg0_reg_n_0_[14] ;
  wire \slv_reg0_reg_n_0_[15] ;
  wire \slv_reg0_reg_n_0_[16] ;
  wire \slv_reg0_reg_n_0_[17] ;
  wire \slv_reg0_reg_n_0_[18] ;
  wire \slv_reg0_reg_n_0_[19] ;
  wire \slv_reg0_reg_n_0_[20] ;
  wire \slv_reg0_reg_n_0_[21] ;
  wire \slv_reg0_reg_n_0_[22] ;
  wire \slv_reg0_reg_n_0_[23] ;
  wire \slv_reg0_reg_n_0_[24] ;
  wire \slv_reg0_reg_n_0_[25] ;
  wire \slv_reg0_reg_n_0_[26] ;
  wire \slv_reg0_reg_n_0_[27] ;
  wire \slv_reg0_reg_n_0_[28] ;
  wire \slv_reg0_reg_n_0_[29] ;
  wire \slv_reg0_reg_n_0_[2] ;
  wire \slv_reg0_reg_n_0_[30] ;
  wire \slv_reg0_reg_n_0_[31] ;
  wire \slv_reg0_reg_n_0_[3] ;
  wire \slv_reg0_reg_n_0_[4] ;
  wire \slv_reg0_reg_n_0_[5] ;
  wire \slv_reg0_reg_n_0_[6] ;
  wire \slv_reg0_reg_n_0_[7] ;
  wire \slv_reg0_reg_n_0_[8] ;
  wire \slv_reg0_reg_n_0_[9] ;
  wire slv_reg_rden;
  wire slv_reg_wren__2;

  LUT2 #(
    .INIT(4'h6)) 
    INIT_EN_i_1
       (.I0(past_slv_reg0_0),
        .I1(\slv_reg0_reg_n_0_[0] ),
        .O(INIT_EN0));
  FDRE INIT_EN_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(INIT_EN0),
        .Q(INIT_EN),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h6)) 
    TIMEOUT_DISABLE_i_1
       (.I0(past_slv_reg0_1),
        .I1(p_0_in2_in),
        .O(TIMEOUT_DISABLE0));
  FDRE TIMEOUT_DISABLE_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(TIMEOUT_DISABLE0),
        .Q(TIMEOUT_DISABLE),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBFFFBF00BF00BF00)) 
    aw_en_i_1
       (.I0(s_axi_awready),
        .I1(s_axi_wvalid),
        .I2(s_axi_awvalid),
        .I3(aw_en_reg_n_0),
        .I4(s_axi_bready),
        .I5(s_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(p_0_in));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s_axi_araddr[0]),
        .I1(s_axi_arvalid),
        .I2(s_axi_arready),
        .I3(axi_araddr[2]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s_axi_araddr[1]),
        .I1(s_axi_arvalid),
        .I2(s_axi_arready),
        .I3(axi_araddr[3]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(axi_araddr[2]),
        .S(p_0_in));
  FDSE \axi_araddr_reg[3] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(axi_araddr[3]),
        .S(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s_axi_arvalid),
        .I1(s_axi_arready),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(s_axi_arready),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[2]_i_1 
       (.I0(s_axi_awaddr[0]),
        .I1(aw_en_reg_n_0),
        .I2(s_axi_awvalid),
        .I3(s_axi_wvalid),
        .I4(s_axi_awready),
        .I5(axi_awaddr[2]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[3]_i_1 
       (.I0(s_axi_awaddr[1]),
        .I1(aw_en_reg_n_0),
        .I2(s_axi_awvalid),
        .I3(s_axi_wvalid),
        .I4(s_axi_awready),
        .I5(axi_awaddr[3]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(axi_awaddr[2]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[3] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(axi_awaddr[3]),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s_axi_aresetn),
        .O(p_0_in));
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(aw_en_reg_n_0),
        .I1(s_axi_awvalid),
        .I2(s_axi_wvalid),
        .I3(s_axi_awready),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(s_axi_awready),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s_axi_wvalid),
        .I1(s_axi_awready),
        .I2(s_axi_wready),
        .I3(s_axi_awvalid),
        .I4(s_axi_bready),
        .I5(s_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s_axi_bvalid),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_1 
       (.I0(TIMEOUT),
        .I1(Q[0]),
        .I2(axi_araddr[2]),
        .I3(\axi_rdata_reg[11]_0 [0]),
        .I4(axi_araddr[3]),
        .I5(\slv_reg0_reg_n_0_[0] ),
        .O(reg_data_out[0]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[10]_i_1 
       (.I0(Q[10]),
        .I1(axi_araddr[2]),
        .I2(\axi_rdata_reg[11]_0 [10]),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[10] ),
        .O(reg_data_out[10]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[11]_i_1 
       (.I0(Q[11]),
        .I1(axi_araddr[2]),
        .I2(\axi_rdata_reg[11]_0 [11]),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[11] ),
        .O(reg_data_out[11]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[12]_i_1 
       (.I0(\slv_reg0_reg_n_0_[12] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[12]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[13]_i_1 
       (.I0(\slv_reg0_reg_n_0_[13] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[13]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[14]_i_1 
       (.I0(\slv_reg0_reg_n_0_[14] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[14]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[15]_i_1 
       (.I0(\slv_reg0_reg_n_0_[15] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[15]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[16]_i_1 
       (.I0(\slv_reg0_reg_n_0_[16] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[16]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[17]_i_1 
       (.I0(\slv_reg0_reg_n_0_[17] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[17]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[18]_i_1 
       (.I0(\slv_reg0_reg_n_0_[18] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[18]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[19]_i_1 
       (.I0(\slv_reg0_reg_n_0_[19] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[19]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[1]_i_1 
       (.I0(Q[1]),
        .I1(axi_araddr[2]),
        .I2(\axi_rdata_reg[11]_0 [1]),
        .I3(axi_araddr[3]),
        .I4(p_0_in2_in),
        .O(reg_data_out[1]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[20]_i_1 
       (.I0(\slv_reg0_reg_n_0_[20] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[20]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[21]_i_1 
       (.I0(\slv_reg0_reg_n_0_[21] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[21]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[22]_i_1 
       (.I0(\slv_reg0_reg_n_0_[22] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[22]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[23]_i_1 
       (.I0(\slv_reg0_reg_n_0_[23] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[23]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[24]_i_1 
       (.I0(\slv_reg0_reg_n_0_[24] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[24]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[25]_i_1 
       (.I0(\slv_reg0_reg_n_0_[25] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[25]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[26]_i_1 
       (.I0(\slv_reg0_reg_n_0_[26] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[26]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[27]_i_1 
       (.I0(\slv_reg0_reg_n_0_[27] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[27]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[28]_i_1 
       (.I0(\slv_reg0_reg_n_0_[28] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[28]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[29]_i_1 
       (.I0(\slv_reg0_reg_n_0_[29] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[29]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[2]_i_1 
       (.I0(Q[2]),
        .I1(axi_araddr[2]),
        .I2(\axi_rdata_reg[11]_0 [2]),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[2] ),
        .O(reg_data_out[2]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[30]_i_1 
       (.I0(\slv_reg0_reg_n_0_[30] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[30]));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(s_axi_arready),
        .I1(s_axi_arvalid),
        .I2(s_axi_rvalid),
        .O(slv_reg_rden));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \axi_rdata[31]_i_2 
       (.I0(\slv_reg0_reg_n_0_[31] ),
        .I1(axi_araddr[2]),
        .I2(axi_araddr[3]),
        .O(reg_data_out[31]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[3]_i_1 
       (.I0(Q[3]),
        .I1(axi_araddr[2]),
        .I2(\axi_rdata_reg[11]_0 [3]),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[3] ),
        .O(reg_data_out[3]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[4]_i_1 
       (.I0(Q[4]),
        .I1(axi_araddr[2]),
        .I2(\axi_rdata_reg[11]_0 [4]),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[4] ),
        .O(reg_data_out[4]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[5]_i_1 
       (.I0(Q[5]),
        .I1(axi_araddr[2]),
        .I2(\axi_rdata_reg[11]_0 [5]),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[5] ),
        .O(reg_data_out[5]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[6]_i_1 
       (.I0(Q[6]),
        .I1(axi_araddr[2]),
        .I2(\axi_rdata_reg[11]_0 [6]),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[6] ),
        .O(reg_data_out[6]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[7]_i_1 
       (.I0(Q[7]),
        .I1(axi_araddr[2]),
        .I2(\axi_rdata_reg[11]_0 [7]),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[7] ),
        .O(reg_data_out[7]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[8]_i_1 
       (.I0(Q[8]),
        .I1(axi_araddr[2]),
        .I2(\axi_rdata_reg[11]_0 [8]),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[8] ),
        .O(reg_data_out[8]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[9]_i_1 
       (.I0(Q[9]),
        .I1(axi_araddr[2]),
        .I2(\axi_rdata_reg[11]_0 [9]),
        .I3(axi_araddr[3]),
        .I4(\slv_reg0_reg_n_0_[9] ),
        .O(reg_data_out[9]));
  FDRE \axi_rdata_reg[0] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[0]),
        .Q(s_axi_rdata[0]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[10] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[10]),
        .Q(s_axi_rdata[10]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[11] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[11]),
        .Q(s_axi_rdata[11]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[12] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[12]),
        .Q(s_axi_rdata[12]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[13] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[13]),
        .Q(s_axi_rdata[13]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[14] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[14]),
        .Q(s_axi_rdata[14]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[15] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[15]),
        .Q(s_axi_rdata[15]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[16] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[16]),
        .Q(s_axi_rdata[16]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[17] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[17]),
        .Q(s_axi_rdata[17]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[18] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[18]),
        .Q(s_axi_rdata[18]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[19] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[19]),
        .Q(s_axi_rdata[19]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[1] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[1]),
        .Q(s_axi_rdata[1]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[20] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[20]),
        .Q(s_axi_rdata[20]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[21] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[21]),
        .Q(s_axi_rdata[21]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[22] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[22]),
        .Q(s_axi_rdata[22]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[23] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[23]),
        .Q(s_axi_rdata[23]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[24] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[24]),
        .Q(s_axi_rdata[24]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[25] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[25]),
        .Q(s_axi_rdata[25]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[26] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[26]),
        .Q(s_axi_rdata[26]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[27] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[27]),
        .Q(s_axi_rdata[27]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[28] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[28]),
        .Q(s_axi_rdata[28]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[29] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[29]),
        .Q(s_axi_rdata[29]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[2] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[2]),
        .Q(s_axi_rdata[2]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[30] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[30]),
        .Q(s_axi_rdata[30]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[31] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[31]),
        .Q(s_axi_rdata[31]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[3] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[3]),
        .Q(s_axi_rdata[3]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[4] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[4]),
        .Q(s_axi_rdata[4]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[5] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[5]),
        .Q(s_axi_rdata[5]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[6] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[6]),
        .Q(s_axi_rdata[6]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[7] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[7]),
        .Q(s_axi_rdata[7]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[8] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[8]),
        .Q(s_axi_rdata[8]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[9] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[9]),
        .Q(s_axi_rdata[9]),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s_axi_arvalid),
        .I1(s_axi_arready),
        .I2(s_axi_rvalid),
        .I3(s_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s_axi_rvalid),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(aw_en_reg_n_0),
        .I1(s_axi_awvalid),
        .I2(s_axi_wvalid),
        .I3(s_axi_wready),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(s_axi_wready),
        .R(p_0_in));
  LUT2 #(
    .INIT(4'hE)) 
    \ctr_state[1]_i_2 
       (.I0(INIT_EN),
        .I1(RESET),
        .O(AR));
  FDRE #(
    .INIT(1'b0)) 
    past_slv_reg0_0_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\slv_reg0_reg_n_0_[0] ),
        .Q(past_slv_reg0_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    past_slv_reg0_1_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(p_0_in2_in),
        .Q(past_slv_reg0_1),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(s_axi_wstrb[1]),
        .O(p_1_in[15]));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(s_axi_wstrb[2]),
        .O(p_1_in[23]));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(s_axi_wstrb[3]),
        .O(p_1_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg0[31]_i_2 
       (.I0(s_axi_wvalid),
        .I1(s_axi_awready),
        .I2(s_axi_wready),
        .I3(s_axi_awvalid),
        .O(slv_reg_wren__2));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(s_axi_wstrb[0]),
        .O(p_1_in[7]));
  FDRE \slv_reg0_reg[0] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[0]),
        .Q(\slv_reg0_reg_n_0_[0] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[10] 
       (.C(s_axi_aclk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[10]),
        .Q(\slv_reg0_reg_n_0_[10] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[11] 
       (.C(s_axi_aclk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[11]),
        .Q(\slv_reg0_reg_n_0_[11] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[12] 
       (.C(s_axi_aclk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[12]),
        .Q(\slv_reg0_reg_n_0_[12] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[13] 
       (.C(s_axi_aclk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[13]),
        .Q(\slv_reg0_reg_n_0_[13] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[14] 
       (.C(s_axi_aclk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[14]),
        .Q(\slv_reg0_reg_n_0_[14] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[15] 
       (.C(s_axi_aclk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[15]),
        .Q(\slv_reg0_reg_n_0_[15] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[16] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[16]),
        .Q(\slv_reg0_reg_n_0_[16] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[17] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[17]),
        .Q(\slv_reg0_reg_n_0_[17] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[18] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[18]),
        .Q(\slv_reg0_reg_n_0_[18] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[19] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[19]),
        .Q(\slv_reg0_reg_n_0_[19] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[1] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[1]),
        .Q(p_0_in2_in),
        .R(p_0_in));
  FDRE \slv_reg0_reg[20] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[20]),
        .Q(\slv_reg0_reg_n_0_[20] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[21] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[21]),
        .Q(\slv_reg0_reg_n_0_[21] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[22] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[22]),
        .Q(\slv_reg0_reg_n_0_[22] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[23] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[23]),
        .Q(\slv_reg0_reg_n_0_[23] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[24] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[24]),
        .Q(\slv_reg0_reg_n_0_[24] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[25] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[25]),
        .Q(\slv_reg0_reg_n_0_[25] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[26] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[26]),
        .Q(\slv_reg0_reg_n_0_[26] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[27] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[27]),
        .Q(\slv_reg0_reg_n_0_[27] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[28] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[28]),
        .Q(\slv_reg0_reg_n_0_[28] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[29] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[29]),
        .Q(\slv_reg0_reg_n_0_[29] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[2] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[2]),
        .Q(\slv_reg0_reg_n_0_[2] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[30] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[30]),
        .Q(\slv_reg0_reg_n_0_[30] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[31] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[31]),
        .Q(\slv_reg0_reg_n_0_[31] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[3] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[3]),
        .Q(\slv_reg0_reg_n_0_[3] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[4] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[4]),
        .Q(\slv_reg0_reg_n_0_[4] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[5] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[5]),
        .Q(\slv_reg0_reg_n_0_[5] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[6] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[6]),
        .Q(\slv_reg0_reg_n_0_[6] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[7] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[7]),
        .Q(\slv_reg0_reg_n_0_[7] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[8] 
       (.C(s_axi_aclk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[8]),
        .Q(\slv_reg0_reg_n_0_[8] ),
        .R(p_0_in));
  FDRE \slv_reg0_reg[9] 
       (.C(s_axi_aclk),
        .CE(p_1_in[15]),
        .D(s_axi_wdata[9]),
        .Q(\slv_reg0_reg_n_0_[9] ),
        .R(p_0_in));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

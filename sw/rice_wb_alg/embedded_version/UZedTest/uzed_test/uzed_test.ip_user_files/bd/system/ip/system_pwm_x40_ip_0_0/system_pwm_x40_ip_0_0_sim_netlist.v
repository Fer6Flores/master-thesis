// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Mon Jun 14 21:18:38 2021
// Host        : FloresToWin running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top system_pwm_x40_ip_0_0 -prefix
//               system_pwm_x40_ip_0_0_ system_pwm_x40_ip_0_0_sim_netlist.v
// Design      : system_pwm_x40_ip_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module system_pwm_x40_ip_0_0_N_bits_counter
   (q,
    S,
    \ctr_state_reg[12]_0 ,
    DI,
    \ctr_state_reg[11]_0 ,
    \ctr_state_reg[7]_0 ,
    \ctr_state_reg[11]_1 ,
    \ctr_state_reg[7]_1 ,
    \ctr_state_reg[11]_2 ,
    \ctr_state_reg[7]_2 ,
    \ctr_state_reg[11]_3 ,
    tc,
    \ctr_state_reg[12]_1 ,
    \ctr_state_reg[12]_2 ,
    \ctr_state_reg[12]_3 ,
    \ctr_state_reg[12]_4 ,
    external_reset_i,
    dir,
    Q,
    \pwm_a_p_i0_inferred__1/i__carry__0 ,
    \pwm_a_p_i0_inferred__3/i__carry__0 ,
    \pwm_a_p_i0_inferred__5/i__carry__0 ,
    \pwm_a_p_i0_inferred__7/i__carry__0 ,
    CLK_100MHZ);
  output [12:0]q;
  output [3:0]S;
  output [2:0]\ctr_state_reg[12]_0 ;
  output [3:0]DI;
  output [1:0]\ctr_state_reg[11]_0 ;
  output [3:0]\ctr_state_reg[7]_0 ;
  output [1:0]\ctr_state_reg[11]_1 ;
  output [3:0]\ctr_state_reg[7]_1 ;
  output [1:0]\ctr_state_reg[11]_2 ;
  output [3:0]\ctr_state_reg[7]_2 ;
  output [1:0]\ctr_state_reg[11]_3 ;
  output tc;
  output [0:0]\ctr_state_reg[12]_1 ;
  output [0:0]\ctr_state_reg[12]_2 ;
  output [0:0]\ctr_state_reg[12]_3 ;
  output [0:0]\ctr_state_reg[12]_4 ;
  input external_reset_i;
  input dir;
  input [12:0]Q;
  input [12:0]\pwm_a_p_i0_inferred__1/i__carry__0 ;
  input [12:0]\pwm_a_p_i0_inferred__3/i__carry__0 ;
  input [12:0]\pwm_a_p_i0_inferred__5/i__carry__0 ;
  input [12:0]\pwm_a_p_i0_inferred__7/i__carry__0 ;
  input CLK_100MHZ;

  wire CLK_100MHZ;
  wire [3:0]DI;
  wire [12:0]Q;
  wire [3:0]S;
  wire \ctr_state[10]_i_1_n_0 ;
  wire \ctr_state[11]_i_1__1_n_0 ;
  wire \ctr_state[12]_i_10_n_0 ;
  wire \ctr_state[12]_i_11_n_0 ;
  wire \ctr_state[12]_i_12_n_0 ;
  wire \ctr_state[12]_i_1__0_n_0 ;
  wire \ctr_state[12]_i_2_n_0 ;
  wire \ctr_state[12]_i_5_n_0 ;
  wire \ctr_state[12]_i_6_n_0 ;
  wire \ctr_state[12]_i_7_n_0 ;
  wire \ctr_state[12]_i_8_n_0 ;
  wire \ctr_state[12]_i_9_n_0 ;
  wire \ctr_state[1]_i_1_n_0 ;
  wire \ctr_state[2]_i_1__1_n_0 ;
  wire \ctr_state[3]_i_1__0_n_0 ;
  wire \ctr_state[4]_i_1_n_0 ;
  wire \ctr_state[4]_i_4_n_0 ;
  wire \ctr_state[4]_i_5_n_0 ;
  wire \ctr_state[4]_i_6_n_0 ;
  wire \ctr_state[4]_i_7_n_0 ;
  wire \ctr_state[5]_i_1_n_0 ;
  wire \ctr_state[6]_i_1__1_n_0 ;
  wire \ctr_state[7]_i_1__2_n_0 ;
  wire \ctr_state[8]_i_1__2_n_0 ;
  wire \ctr_state[8]_i_4_n_0 ;
  wire \ctr_state[8]_i_5_n_0 ;
  wire \ctr_state[8]_i_6_n_0 ;
  wire \ctr_state[8]_i_7_n_0 ;
  wire \ctr_state[9]_i_1__0_n_0 ;
  wire [1:0]\ctr_state_reg[11]_0 ;
  wire [1:0]\ctr_state_reg[11]_1 ;
  wire [1:0]\ctr_state_reg[11]_2 ;
  wire [1:0]\ctr_state_reg[11]_3 ;
  wire [2:0]\ctr_state_reg[12]_0 ;
  wire [0:0]\ctr_state_reg[12]_1 ;
  wire [0:0]\ctr_state_reg[12]_2 ;
  wire [0:0]\ctr_state_reg[12]_3 ;
  wire [0:0]\ctr_state_reg[12]_4 ;
  wire \ctr_state_reg[12]_i_3_n_1 ;
  wire \ctr_state_reg[12]_i_3_n_2 ;
  wire \ctr_state_reg[12]_i_3_n_3 ;
  wire \ctr_state_reg[12]_i_4_n_1 ;
  wire \ctr_state_reg[12]_i_4_n_2 ;
  wire \ctr_state_reg[12]_i_4_n_3 ;
  wire \ctr_state_reg[12]_i_4_n_4 ;
  wire \ctr_state_reg[12]_i_4_n_5 ;
  wire \ctr_state_reg[12]_i_4_n_6 ;
  wire \ctr_state_reg[12]_i_4_n_7 ;
  wire \ctr_state_reg[4]_i_2_n_0 ;
  wire \ctr_state_reg[4]_i_2_n_1 ;
  wire \ctr_state_reg[4]_i_2_n_2 ;
  wire \ctr_state_reg[4]_i_2_n_3 ;
  wire \ctr_state_reg[4]_i_3_n_0 ;
  wire \ctr_state_reg[4]_i_3_n_1 ;
  wire \ctr_state_reg[4]_i_3_n_2 ;
  wire \ctr_state_reg[4]_i_3_n_3 ;
  wire \ctr_state_reg[4]_i_3_n_4 ;
  wire \ctr_state_reg[4]_i_3_n_5 ;
  wire \ctr_state_reg[4]_i_3_n_6 ;
  wire \ctr_state_reg[4]_i_3_n_7 ;
  wire [3:0]\ctr_state_reg[7]_0 ;
  wire [3:0]\ctr_state_reg[7]_1 ;
  wire [3:0]\ctr_state_reg[7]_2 ;
  wire \ctr_state_reg[8]_i_2_n_0 ;
  wire \ctr_state_reg[8]_i_2_n_1 ;
  wire \ctr_state_reg[8]_i_2_n_2 ;
  wire \ctr_state_reg[8]_i_2_n_3 ;
  wire \ctr_state_reg[8]_i_3_n_0 ;
  wire \ctr_state_reg[8]_i_3_n_1 ;
  wire \ctr_state_reg[8]_i_3_n_2 ;
  wire \ctr_state_reg[8]_i_3_n_3 ;
  wire \ctr_state_reg[8]_i_3_n_4 ;
  wire \ctr_state_reg[8]_i_3_n_5 ;
  wire \ctr_state_reg[8]_i_3_n_6 ;
  wire \ctr_state_reg[8]_i_3_n_7 ;
  wire [12:1]data0;
  wire dir;
  wire external_reset_i;
  wire [0:0]p_1_in;
  wire [12:0]\pwm_a_p_i0_inferred__1/i__carry__0 ;
  wire [12:0]\pwm_a_p_i0_inferred__3/i__carry__0 ;
  wire [12:0]\pwm_a_p_i0_inferred__5/i__carry__0 ;
  wire [12:0]\pwm_a_p_i0_inferred__7/i__carry__0 ;
  wire [12:0]q;
  wire tc;
  wire [3:3]\NLW_ctr_state_reg[12]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_ctr_state_reg[12]_i_4_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    counter_phase0_dir_i_2
       (.I0(\ctr_state[12]_i_5_n_0 ),
        .I1(dir),
        .I2(\ctr_state[12]_i_2_n_0 ),
        .O(tc));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00011101)) 
    \ctr_state[0]_i_1 
       (.I0(external_reset_i),
        .I1(q[0]),
        .I2(\ctr_state[12]_i_2_n_0 ),
        .I3(dir),
        .I4(\ctr_state[12]_i_5_n_0 ),
        .O(p_1_in));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[10]_i_1 
       (.I0(external_reset_i),
        .I1(data0[10]),
        .I2(\ctr_state[12]_i_2_n_0 ),
        .I3(dir),
        .I4(\ctr_state_reg[12]_i_4_n_6 ),
        .I5(\ctr_state[12]_i_5_n_0 ),
        .O(\ctr_state[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[11]_i_1__1 
       (.I0(external_reset_i),
        .I1(data0[11]),
        .I2(\ctr_state[12]_i_2_n_0 ),
        .I3(dir),
        .I4(\ctr_state_reg[12]_i_4_n_5 ),
        .I5(\ctr_state[12]_i_5_n_0 ),
        .O(\ctr_state[11]_i_1__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_10 
       (.I0(q[10]),
        .O(\ctr_state[12]_i_10_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_11 
       (.I0(q[9]),
        .O(\ctr_state[12]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \ctr_state[12]_i_12 
       (.I0(q[9]),
        .I1(q[8]),
        .I2(q[3]),
        .I3(q[7]),
        .I4(q[12]),
        .O(\ctr_state[12]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h0054005455540054)) 
    \ctr_state[12]_i_1__0 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2_n_0 ),
        .I2(data0[12]),
        .I3(dir),
        .I4(\ctr_state_reg[12]_i_4_n_4 ),
        .I5(\ctr_state[12]_i_5_n_0 ),
        .O(\ctr_state[12]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \ctr_state[12]_i_2 
       (.I0(\ctr_state[12]_i_6_n_0 ),
        .I1(q[1]),
        .I2(q[0]),
        .I3(q[10]),
        .I4(q[2]),
        .I5(\ctr_state[12]_i_7_n_0 ),
        .O(\ctr_state[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \ctr_state[12]_i_5 
       (.I0(\ctr_state[12]_i_6_n_0 ),
        .I1(q[1]),
        .I2(q[0]),
        .I3(q[10]),
        .I4(q[2]),
        .I5(\ctr_state[12]_i_12_n_0 ),
        .O(\ctr_state[12]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \ctr_state[12]_i_6 
       (.I0(q[5]),
        .I1(q[4]),
        .I2(q[11]),
        .I3(q[6]),
        .O(\ctr_state[12]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \ctr_state[12]_i_7 
       (.I0(q[3]),
        .I1(q[7]),
        .I2(q[12]),
        .I3(q[9]),
        .I4(q[8]),
        .O(\ctr_state[12]_i_7_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_8 
       (.I0(q[12]),
        .O(\ctr_state[12]_i_8_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_9 
       (.I0(q[11]),
        .O(\ctr_state[12]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[1]_i_1 
       (.I0(external_reset_i),
        .I1(data0[1]),
        .I2(\ctr_state[12]_i_2_n_0 ),
        .I3(dir),
        .I4(\ctr_state_reg[4]_i_3_n_7 ),
        .I5(\ctr_state[12]_i_5_n_0 ),
        .O(\ctr_state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[2]_i_1__1 
       (.I0(external_reset_i),
        .I1(data0[2]),
        .I2(\ctr_state[12]_i_2_n_0 ),
        .I3(dir),
        .I4(\ctr_state_reg[4]_i_3_n_6 ),
        .I5(\ctr_state[12]_i_5_n_0 ),
        .O(\ctr_state[2]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h0054005455540054)) 
    \ctr_state[3]_i_1__0 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2_n_0 ),
        .I2(data0[3]),
        .I3(dir),
        .I4(\ctr_state_reg[4]_i_3_n_5 ),
        .I5(\ctr_state[12]_i_5_n_0 ),
        .O(\ctr_state[3]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[4]_i_1 
       (.I0(external_reset_i),
        .I1(data0[4]),
        .I2(\ctr_state[12]_i_2_n_0 ),
        .I3(dir),
        .I4(\ctr_state_reg[4]_i_3_n_4 ),
        .I5(\ctr_state[12]_i_5_n_0 ),
        .O(\ctr_state[4]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_4 
       (.I0(q[4]),
        .O(\ctr_state[4]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_5 
       (.I0(q[3]),
        .O(\ctr_state[4]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_6 
       (.I0(q[2]),
        .O(\ctr_state[4]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_7 
       (.I0(q[1]),
        .O(\ctr_state[4]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[5]_i_1 
       (.I0(external_reset_i),
        .I1(data0[5]),
        .I2(\ctr_state[12]_i_2_n_0 ),
        .I3(dir),
        .I4(\ctr_state_reg[8]_i_3_n_7 ),
        .I5(\ctr_state[12]_i_5_n_0 ),
        .O(\ctr_state[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[6]_i_1__1 
       (.I0(external_reset_i),
        .I1(data0[6]),
        .I2(\ctr_state[12]_i_2_n_0 ),
        .I3(dir),
        .I4(\ctr_state_reg[8]_i_3_n_6 ),
        .I5(\ctr_state[12]_i_5_n_0 ),
        .O(\ctr_state[6]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h0054005455540054)) 
    \ctr_state[7]_i_1__2 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2_n_0 ),
        .I2(data0[7]),
        .I3(dir),
        .I4(\ctr_state_reg[8]_i_3_n_5 ),
        .I5(\ctr_state[12]_i_5_n_0 ),
        .O(\ctr_state[7]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'h0054005455540054)) 
    \ctr_state[8]_i_1__2 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2_n_0 ),
        .I2(data0[8]),
        .I3(dir),
        .I4(\ctr_state_reg[8]_i_3_n_4 ),
        .I5(\ctr_state[12]_i_5_n_0 ),
        .O(\ctr_state[8]_i_1__2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_4 
       (.I0(q[8]),
        .O(\ctr_state[8]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_5 
       (.I0(q[7]),
        .O(\ctr_state[8]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_6 
       (.I0(q[6]),
        .O(\ctr_state[8]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_7 
       (.I0(q[5]),
        .O(\ctr_state[8]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0054005455540054)) 
    \ctr_state[9]_i_1__0 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2_n_0 ),
        .I2(data0[9]),
        .I3(dir),
        .I4(\ctr_state_reg[12]_i_4_n_7 ),
        .I5(\ctr_state[12]_i_5_n_0 ),
        .O(\ctr_state[9]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[0] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in),
        .Q(q[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[10] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[10]_i_1_n_0 ),
        .Q(q[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[11] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[11]_i_1__1_n_0 ),
        .Q(q[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[12] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[12]_i_1__0_n_0 ),
        .Q(q[12]),
        .R(1'b0));
  CARRY4 \ctr_state_reg[12]_i_3 
       (.CI(\ctr_state_reg[8]_i_2_n_0 ),
        .CO({\NLW_ctr_state_reg[12]_i_3_CO_UNCONNECTED [3],\ctr_state_reg[12]_i_3_n_1 ,\ctr_state_reg[12]_i_3_n_2 ,\ctr_state_reg[12]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,q[11:9]}),
        .O(data0[12:9]),
        .S({\ctr_state[12]_i_8_n_0 ,\ctr_state[12]_i_9_n_0 ,\ctr_state[12]_i_10_n_0 ,\ctr_state[12]_i_11_n_0 }));
  CARRY4 \ctr_state_reg[12]_i_4 
       (.CI(\ctr_state_reg[8]_i_3_n_0 ),
        .CO({\NLW_ctr_state_reg[12]_i_4_CO_UNCONNECTED [3],\ctr_state_reg[12]_i_4_n_1 ,\ctr_state_reg[12]_i_4_n_2 ,\ctr_state_reg[12]_i_4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[12]_i_4_n_4 ,\ctr_state_reg[12]_i_4_n_5 ,\ctr_state_reg[12]_i_4_n_6 ,\ctr_state_reg[12]_i_4_n_7 }),
        .S(q[12:9]));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[1] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[1]_i_1_n_0 ),
        .Q(q[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[2] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[2]_i_1__1_n_0 ),
        .Q(q[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[3] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[3]_i_1__0_n_0 ),
        .Q(q[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[4] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[4]_i_1_n_0 ),
        .Q(q[4]),
        .R(1'b0));
  CARRY4 \ctr_state_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\ctr_state_reg[4]_i_2_n_0 ,\ctr_state_reg[4]_i_2_n_1 ,\ctr_state_reg[4]_i_2_n_2 ,\ctr_state_reg[4]_i_2_n_3 }),
        .CYINIT(q[0]),
        .DI(q[4:1]),
        .O(data0[4:1]),
        .S({\ctr_state[4]_i_4_n_0 ,\ctr_state[4]_i_5_n_0 ,\ctr_state[4]_i_6_n_0 ,\ctr_state[4]_i_7_n_0 }));
  CARRY4 \ctr_state_reg[4]_i_3 
       (.CI(1'b0),
        .CO({\ctr_state_reg[4]_i_3_n_0 ,\ctr_state_reg[4]_i_3_n_1 ,\ctr_state_reg[4]_i_3_n_2 ,\ctr_state_reg[4]_i_3_n_3 }),
        .CYINIT(q[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[4]_i_3_n_4 ,\ctr_state_reg[4]_i_3_n_5 ,\ctr_state_reg[4]_i_3_n_6 ,\ctr_state_reg[4]_i_3_n_7 }),
        .S(q[4:1]));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[5] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[5]_i_1_n_0 ),
        .Q(q[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[6] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[6]_i_1__1_n_0 ),
        .Q(q[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[7] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[7]_i_1__2_n_0 ),
        .Q(q[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[8] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[8]_i_1__2_n_0 ),
        .Q(q[8]),
        .R(1'b0));
  CARRY4 \ctr_state_reg[8]_i_2 
       (.CI(\ctr_state_reg[4]_i_2_n_0 ),
        .CO({\ctr_state_reg[8]_i_2_n_0 ,\ctr_state_reg[8]_i_2_n_1 ,\ctr_state_reg[8]_i_2_n_2 ,\ctr_state_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI(q[8:5]),
        .O(data0[8:5]),
        .S({\ctr_state[8]_i_4_n_0 ,\ctr_state[8]_i_5_n_0 ,\ctr_state[8]_i_6_n_0 ,\ctr_state[8]_i_7_n_0 }));
  CARRY4 \ctr_state_reg[8]_i_3 
       (.CI(\ctr_state_reg[4]_i_3_n_0 ),
        .CO({\ctr_state_reg[8]_i_3_n_0 ,\ctr_state_reg[8]_i_3_n_1 ,\ctr_state_reg[8]_i_3_n_2 ,\ctr_state_reg[8]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[8]_i_3_n_4 ,\ctr_state_reg[8]_i_3_n_5 ,\ctr_state_reg[8]_i_3_n_6 ,\ctr_state_reg[8]_i_3_n_7 }),
        .S(q[8:5]));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[9] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[9]_i_1__0_n_0 ),
        .Q(q[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_2__0
       (.I0(q[11]),
        .I1(\pwm_a_p_i0_inferred__1/i__carry__0 [11]),
        .I2(\pwm_a_p_i0_inferred__1/i__carry__0 [10]),
        .I3(q[10]),
        .O(\ctr_state_reg[11]_0 [1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_2__2
       (.I0(q[11]),
        .I1(\pwm_a_p_i0_inferred__3/i__carry__0 [11]),
        .I2(\pwm_a_p_i0_inferred__3/i__carry__0 [10]),
        .I3(q[10]),
        .O(\ctr_state_reg[11]_1 [1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_2__4
       (.I0(q[11]),
        .I1(\pwm_a_p_i0_inferred__5/i__carry__0 [11]),
        .I2(\pwm_a_p_i0_inferred__5/i__carry__0 [10]),
        .I3(q[10]),
        .O(\ctr_state_reg[11]_2 [1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_2__6
       (.I0(q[11]),
        .I1(\pwm_a_p_i0_inferred__7/i__carry__0 [11]),
        .I2(\pwm_a_p_i0_inferred__7/i__carry__0 [10]),
        .I3(q[10]),
        .O(\ctr_state_reg[11]_3 [1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_3__0
       (.I0(q[9]),
        .I1(\pwm_a_p_i0_inferred__1/i__carry__0 [9]),
        .I2(\pwm_a_p_i0_inferred__1/i__carry__0 [8]),
        .I3(q[8]),
        .O(\ctr_state_reg[11]_0 [0]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_3__2
       (.I0(q[9]),
        .I1(\pwm_a_p_i0_inferred__3/i__carry__0 [9]),
        .I2(\pwm_a_p_i0_inferred__3/i__carry__0 [8]),
        .I3(q[8]),
        .O(\ctr_state_reg[11]_1 [0]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_3__4
       (.I0(q[9]),
        .I1(\pwm_a_p_i0_inferred__5/i__carry__0 [9]),
        .I2(\pwm_a_p_i0_inferred__5/i__carry__0 [8]),
        .I3(q[8]),
        .O(\ctr_state_reg[11]_2 [0]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_3__6
       (.I0(q[9]),
        .I1(\pwm_a_p_i0_inferred__7/i__carry__0 [9]),
        .I2(\pwm_a_p_i0_inferred__7/i__carry__0 [8]),
        .I3(q[8]),
        .O(\ctr_state_reg[11]_3 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__0
       (.I0(q[12]),
        .I1(\pwm_a_p_i0_inferred__1/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_1 ));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__2
       (.I0(q[12]),
        .I1(\pwm_a_p_i0_inferred__3/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_2 ));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__4
       (.I0(q[12]),
        .I1(\pwm_a_p_i0_inferred__5/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_3 ));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__6
       (.I0(q[12]),
        .I1(\pwm_a_p_i0_inferred__7/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_4 ));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_1__0
       (.I0(q[7]),
        .I1(\pwm_a_p_i0_inferred__1/i__carry__0 [7]),
        .I2(\pwm_a_p_i0_inferred__1/i__carry__0 [6]),
        .I3(q[6]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_1__2
       (.I0(q[7]),
        .I1(\pwm_a_p_i0_inferred__3/i__carry__0 [7]),
        .I2(\pwm_a_p_i0_inferred__3/i__carry__0 [6]),
        .I3(q[6]),
        .O(\ctr_state_reg[7]_0 [3]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_1__4
       (.I0(q[7]),
        .I1(\pwm_a_p_i0_inferred__5/i__carry__0 [7]),
        .I2(\pwm_a_p_i0_inferred__5/i__carry__0 [6]),
        .I3(q[6]),
        .O(\ctr_state_reg[7]_1 [3]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_1__6
       (.I0(q[7]),
        .I1(\pwm_a_p_i0_inferred__7/i__carry__0 [7]),
        .I2(\pwm_a_p_i0_inferred__7/i__carry__0 [6]),
        .I3(q[6]),
        .O(\ctr_state_reg[7]_2 [3]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_2__0
       (.I0(q[5]),
        .I1(\pwm_a_p_i0_inferred__1/i__carry__0 [5]),
        .I2(\pwm_a_p_i0_inferred__1/i__carry__0 [4]),
        .I3(q[4]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_2__2
       (.I0(q[5]),
        .I1(\pwm_a_p_i0_inferred__3/i__carry__0 [5]),
        .I2(\pwm_a_p_i0_inferred__3/i__carry__0 [4]),
        .I3(q[4]),
        .O(\ctr_state_reg[7]_0 [2]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_2__4
       (.I0(q[5]),
        .I1(\pwm_a_p_i0_inferred__5/i__carry__0 [5]),
        .I2(\pwm_a_p_i0_inferred__5/i__carry__0 [4]),
        .I3(q[4]),
        .O(\ctr_state_reg[7]_1 [2]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_2__6
       (.I0(q[5]),
        .I1(\pwm_a_p_i0_inferred__7/i__carry__0 [5]),
        .I2(\pwm_a_p_i0_inferred__7/i__carry__0 [4]),
        .I3(q[4]),
        .O(\ctr_state_reg[7]_2 [2]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_3__0
       (.I0(q[3]),
        .I1(\pwm_a_p_i0_inferred__1/i__carry__0 [3]),
        .I2(\pwm_a_p_i0_inferred__1/i__carry__0 [2]),
        .I3(q[2]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_3__2
       (.I0(q[3]),
        .I1(\pwm_a_p_i0_inferred__3/i__carry__0 [3]),
        .I2(\pwm_a_p_i0_inferred__3/i__carry__0 [2]),
        .I3(q[2]),
        .O(\ctr_state_reg[7]_0 [1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_3__4
       (.I0(q[3]),
        .I1(\pwm_a_p_i0_inferred__5/i__carry__0 [3]),
        .I2(\pwm_a_p_i0_inferred__5/i__carry__0 [2]),
        .I3(q[2]),
        .O(\ctr_state_reg[7]_1 [1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_3__6
       (.I0(q[3]),
        .I1(\pwm_a_p_i0_inferred__7/i__carry__0 [3]),
        .I2(\pwm_a_p_i0_inferred__7/i__carry__0 [2]),
        .I3(q[2]),
        .O(\ctr_state_reg[7]_2 [1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_4__0
       (.I0(q[1]),
        .I1(\pwm_a_p_i0_inferred__1/i__carry__0 [1]),
        .I2(\pwm_a_p_i0_inferred__1/i__carry__0 [0]),
        .I3(q[0]),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_4__2
       (.I0(q[1]),
        .I1(\pwm_a_p_i0_inferred__3/i__carry__0 [1]),
        .I2(\pwm_a_p_i0_inferred__3/i__carry__0 [0]),
        .I3(q[0]),
        .O(\ctr_state_reg[7]_0 [0]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_4__4
       (.I0(q[1]),
        .I1(\pwm_a_p_i0_inferred__5/i__carry__0 [1]),
        .I2(\pwm_a_p_i0_inferred__5/i__carry__0 [0]),
        .I3(q[0]),
        .O(\ctr_state_reg[7]_1 [0]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_4__6
       (.I0(q[1]),
        .I1(\pwm_a_p_i0_inferred__7/i__carry__0 [1]),
        .I2(\pwm_a_p_i0_inferred__7/i__carry__0 [0]),
        .I3(q[0]),
        .O(\ctr_state_reg[7]_2 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    pwm_a_p_i0_carry__0_i_4
       (.I0(q[12]),
        .I1(Q[12]),
        .O(\ctr_state_reg[12]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    pwm_a_p_i0_carry__0_i_5
       (.I0(q[11]),
        .I1(Q[11]),
        .I2(q[10]),
        .I3(Q[10]),
        .O(\ctr_state_reg[12]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    pwm_a_p_i0_carry__0_i_6
       (.I0(q[9]),
        .I1(Q[9]),
        .I2(q[8]),
        .I3(Q[8]),
        .O(\ctr_state_reg[12]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    pwm_a_p_i0_carry_i_5
       (.I0(q[7]),
        .I1(Q[7]),
        .I2(q[6]),
        .I3(Q[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    pwm_a_p_i0_carry_i_6
       (.I0(q[5]),
        .I1(Q[5]),
        .I2(q[4]),
        .I3(Q[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    pwm_a_p_i0_carry_i_7
       (.I0(q[3]),
        .I1(Q[3]),
        .I2(q[2]),
        .I3(Q[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    pwm_a_p_i0_carry_i_8
       (.I0(q[1]),
        .I1(Q[1]),
        .I2(q[0]),
        .I3(Q[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "N_bits_counter" *) 
module system_pwm_x40_ip_0_0_N_bits_counter_0
   (counter_phase180_output,
    S,
    \ctr_state_reg[12]_0 ,
    \ctr_state_reg[7]_0 ,
    \ctr_state_reg[12]_1 ,
    \ctr_state_reg[7]_1 ,
    \ctr_state_reg[12]_2 ,
    \ctr_state_reg[7]_2 ,
    \ctr_state_reg[12]_3 ,
    \ctr_state_reg[7]_3 ,
    \ctr_state_reg[12]_4 ,
    counter_phase180_tc,
    external_reset_i,
    counter_phase180_dir_reg,
    Q,
    \pwm_b_p_i0_inferred__1/i__carry__0 ,
    \pwm_b_p_i0_inferred__3/i__carry__0 ,
    \pwm_b_p_i0_inferred__5/i__carry__0 ,
    \pwm_b_p_i0_inferred__7/i__carry__0 ,
    CLK_100MHZ);
  output [12:0]counter_phase180_output;
  output [3:0]S;
  output [2:0]\ctr_state_reg[12]_0 ;
  output [3:0]\ctr_state_reg[7]_0 ;
  output [2:0]\ctr_state_reg[12]_1 ;
  output [3:0]\ctr_state_reg[7]_1 ;
  output [2:0]\ctr_state_reg[12]_2 ;
  output [3:0]\ctr_state_reg[7]_2 ;
  output [2:0]\ctr_state_reg[12]_3 ;
  output [3:0]\ctr_state_reg[7]_3 ;
  output [2:0]\ctr_state_reg[12]_4 ;
  output counter_phase180_tc;
  input external_reset_i;
  input counter_phase180_dir_reg;
  input [12:0]Q;
  input [12:0]\pwm_b_p_i0_inferred__1/i__carry__0 ;
  input [12:0]\pwm_b_p_i0_inferred__3/i__carry__0 ;
  input [12:0]\pwm_b_p_i0_inferred__5/i__carry__0 ;
  input [12:0]\pwm_b_p_i0_inferred__7/i__carry__0 ;
  input CLK_100MHZ;

  wire CLK_100MHZ;
  wire [12:0]Q;
  wire [3:0]S;
  wire counter_phase180_dir_reg;
  wire [12:0]counter_phase180_output;
  wire counter_phase180_tc;
  wire \ctr_state[10]_i_1__1_n_0 ;
  wire \ctr_state[11]_i_1__2_n_0 ;
  wire \ctr_state[12]_i_10__1_n_0 ;
  wire \ctr_state[12]_i_11__1_n_0 ;
  wire \ctr_state[12]_i_12__1_n_0 ;
  wire \ctr_state[12]_i_13__0_n_0 ;
  wire \ctr_state[12]_i_2__1_n_0 ;
  wire \ctr_state[12]_i_4_n_0 ;
  wire \ctr_state[12]_i_6__1_n_0 ;
  wire \ctr_state[12]_i_7__1_n_0 ;
  wire \ctr_state[12]_i_8__1_n_0 ;
  wire \ctr_state[12]_i_9__1_n_0 ;
  wire \ctr_state[1]_i_1__1_n_0 ;
  wire \ctr_state[2]_i_1__2_n_0 ;
  wire \ctr_state[4]_i_1__1_n_0 ;
  wire \ctr_state[4]_i_4__1_n_0 ;
  wire \ctr_state[4]_i_5__1_n_0 ;
  wire \ctr_state[4]_i_6__1_n_0 ;
  wire \ctr_state[4]_i_7__1_n_0 ;
  wire \ctr_state[5]_i_1__1_n_0 ;
  wire \ctr_state[6]_i_1__2_n_0 ;
  wire \ctr_state[8]_i_4__1_n_0 ;
  wire \ctr_state[8]_i_5__1_n_0 ;
  wire \ctr_state[8]_i_6__1_n_0 ;
  wire \ctr_state[8]_i_7__1_n_0 ;
  wire [2:0]\ctr_state_reg[12]_0 ;
  wire [2:0]\ctr_state_reg[12]_1 ;
  wire [2:0]\ctr_state_reg[12]_2 ;
  wire [2:0]\ctr_state_reg[12]_3 ;
  wire [2:0]\ctr_state_reg[12]_4 ;
  wire \ctr_state_reg[12]_i_3__1_n_1 ;
  wire \ctr_state_reg[12]_i_3__1_n_2 ;
  wire \ctr_state_reg[12]_i_3__1_n_3 ;
  wire \ctr_state_reg[12]_i_5_n_1 ;
  wire \ctr_state_reg[12]_i_5_n_2 ;
  wire \ctr_state_reg[12]_i_5_n_3 ;
  wire \ctr_state_reg[12]_i_5_n_4 ;
  wire \ctr_state_reg[12]_i_5_n_5 ;
  wire \ctr_state_reg[12]_i_5_n_6 ;
  wire \ctr_state_reg[12]_i_5_n_7 ;
  wire \ctr_state_reg[4]_i_2__1_n_0 ;
  wire \ctr_state_reg[4]_i_2__1_n_1 ;
  wire \ctr_state_reg[4]_i_2__1_n_2 ;
  wire \ctr_state_reg[4]_i_2__1_n_3 ;
  wire \ctr_state_reg[4]_i_3__1_n_0 ;
  wire \ctr_state_reg[4]_i_3__1_n_1 ;
  wire \ctr_state_reg[4]_i_3__1_n_2 ;
  wire \ctr_state_reg[4]_i_3__1_n_3 ;
  wire \ctr_state_reg[4]_i_3__1_n_4 ;
  wire \ctr_state_reg[4]_i_3__1_n_5 ;
  wire \ctr_state_reg[4]_i_3__1_n_6 ;
  wire \ctr_state_reg[4]_i_3__1_n_7 ;
  wire [3:0]\ctr_state_reg[7]_0 ;
  wire [3:0]\ctr_state_reg[7]_1 ;
  wire [3:0]\ctr_state_reg[7]_2 ;
  wire [3:0]\ctr_state_reg[7]_3 ;
  wire \ctr_state_reg[8]_i_2__1_n_0 ;
  wire \ctr_state_reg[8]_i_2__1_n_1 ;
  wire \ctr_state_reg[8]_i_2__1_n_2 ;
  wire \ctr_state_reg[8]_i_2__1_n_3 ;
  wire \ctr_state_reg[8]_i_3__1_n_0 ;
  wire \ctr_state_reg[8]_i_3__1_n_1 ;
  wire \ctr_state_reg[8]_i_3__1_n_2 ;
  wire \ctr_state_reg[8]_i_3__1_n_3 ;
  wire \ctr_state_reg[8]_i_3__1_n_4 ;
  wire \ctr_state_reg[8]_i_3__1_n_5 ;
  wire \ctr_state_reg[8]_i_3__1_n_6 ;
  wire \ctr_state_reg[8]_i_3__1_n_7 ;
  wire [12:1]data0;
  wire external_reset_i;
  wire [12:0]p_1_in;
  wire [12:0]\pwm_b_p_i0_inferred__1/i__carry__0 ;
  wire [12:0]\pwm_b_p_i0_inferred__3/i__carry__0 ;
  wire [12:0]\pwm_b_p_i0_inferred__5/i__carry__0 ;
  wire [12:0]\pwm_b_p_i0_inferred__7/i__carry__0 ;
  wire [3:3]\NLW_ctr_state_reg[12]_i_3__1_CO_UNCONNECTED ;
  wire [3:3]\NLW_ctr_state_reg[12]_i_5_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hE2)) 
    counter_phase180_dir_i_2
       (.I0(\ctr_state[12]_i_2__1_n_0 ),
        .I1(counter_phase180_dir_reg),
        .I2(\ctr_state[12]_i_4_n_0 ),
        .O(counter_phase180_tc));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h00001103)) 
    \ctr_state[0]_i_1__0 
       (.I0(\ctr_state[12]_i_4_n_0 ),
        .I1(counter_phase180_output[0]),
        .I2(\ctr_state[12]_i_2__1_n_0 ),
        .I3(counter_phase180_dir_reg),
        .I4(external_reset_i),
        .O(p_1_in[0]));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[10]_i_1__1 
       (.I0(external_reset_i),
        .I1(data0[10]),
        .I2(\ctr_state[12]_i_2__1_n_0 ),
        .I3(counter_phase180_dir_reg),
        .I4(\ctr_state_reg[12]_i_5_n_6 ),
        .I5(\ctr_state[12]_i_4_n_0 ),
        .O(\ctr_state[10]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[11]_i_1__2 
       (.I0(external_reset_i),
        .I1(data0[11]),
        .I2(\ctr_state[12]_i_2__1_n_0 ),
        .I3(counter_phase180_dir_reg),
        .I4(\ctr_state_reg[12]_i_5_n_5 ),
        .I5(\ctr_state[12]_i_4_n_0 ),
        .O(\ctr_state[11]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'hAAFEFFFEAAFEAAFE)) 
    \ctr_state[12]_i_1 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__1_n_0 ),
        .I2(data0[12]),
        .I3(counter_phase180_dir_reg),
        .I4(\ctr_state[12]_i_4_n_0 ),
        .I5(\ctr_state_reg[12]_i_5_n_4 ),
        .O(p_1_in[12]));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_10__1 
       (.I0(counter_phase180_output[10]),
        .O(\ctr_state[12]_i_10__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_11__1 
       (.I0(counter_phase180_output[9]),
        .O(\ctr_state[12]_i_11__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \ctr_state[12]_i_12__1 
       (.I0(counter_phase180_output[5]),
        .I1(counter_phase180_output[4]),
        .I2(counter_phase180_output[11]),
        .I3(counter_phase180_output[6]),
        .O(\ctr_state[12]_i_12__1_n_0 ));
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \ctr_state[12]_i_13__0 
       (.I0(counter_phase180_output[3]),
        .I1(counter_phase180_output[12]),
        .I2(counter_phase180_output[8]),
        .I3(counter_phase180_output[9]),
        .I4(counter_phase180_output[7]),
        .O(\ctr_state[12]_i_13__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \ctr_state[12]_i_2__1 
       (.I0(\ctr_state[12]_i_6__1_n_0 ),
        .I1(counter_phase180_output[3]),
        .I2(counter_phase180_output[2]),
        .I3(counter_phase180_output[1]),
        .I4(counter_phase180_output[0]),
        .I5(\ctr_state[12]_i_7__1_n_0 ),
        .O(\ctr_state[12]_i_2__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \ctr_state[12]_i_4 
       (.I0(\ctr_state[12]_i_12__1_n_0 ),
        .I1(counter_phase180_output[1]),
        .I2(counter_phase180_output[0]),
        .I3(counter_phase180_output[10]),
        .I4(counter_phase180_output[2]),
        .I5(\ctr_state[12]_i_13__0_n_0 ),
        .O(\ctr_state[12]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \ctr_state[12]_i_6__1 
       (.I0(counter_phase180_output[5]),
        .I1(counter_phase180_output[4]),
        .I2(counter_phase180_output[7]),
        .I3(counter_phase180_output[6]),
        .O(\ctr_state[12]_i_6__1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \ctr_state[12]_i_7__1 
       (.I0(counter_phase180_output[11]),
        .I1(counter_phase180_output[8]),
        .I2(counter_phase180_output[9]),
        .I3(counter_phase180_output[10]),
        .I4(counter_phase180_output[12]),
        .O(\ctr_state[12]_i_7__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_8__1 
       (.I0(counter_phase180_output[12]),
        .O(\ctr_state[12]_i_8__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_9__1 
       (.I0(counter_phase180_output[11]),
        .O(\ctr_state[12]_i_9__1_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[1]_i_1__1 
       (.I0(external_reset_i),
        .I1(data0[1]),
        .I2(\ctr_state[12]_i_2__1_n_0 ),
        .I3(counter_phase180_dir_reg),
        .I4(\ctr_state_reg[4]_i_3__1_n_7 ),
        .I5(\ctr_state[12]_i_4_n_0 ),
        .O(\ctr_state[1]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[2]_i_1__2 
       (.I0(external_reset_i),
        .I1(data0[2]),
        .I2(\ctr_state[12]_i_2__1_n_0 ),
        .I3(counter_phase180_dir_reg),
        .I4(\ctr_state_reg[4]_i_3__1_n_6 ),
        .I5(\ctr_state[12]_i_4_n_0 ),
        .O(\ctr_state[2]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'hCCFEFFFECCFECCFE)) 
    \ctr_state[3]_i_1 
       (.I0(\ctr_state[12]_i_2__1_n_0 ),
        .I1(external_reset_i),
        .I2(data0[3]),
        .I3(counter_phase180_dir_reg),
        .I4(\ctr_state[12]_i_4_n_0 ),
        .I5(\ctr_state_reg[4]_i_3__1_n_5 ),
        .O(p_1_in[3]));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[4]_i_1__1 
       (.I0(external_reset_i),
        .I1(data0[4]),
        .I2(\ctr_state[12]_i_2__1_n_0 ),
        .I3(counter_phase180_dir_reg),
        .I4(\ctr_state_reg[4]_i_3__1_n_4 ),
        .I5(\ctr_state[12]_i_4_n_0 ),
        .O(\ctr_state[4]_i_1__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_4__1 
       (.I0(counter_phase180_output[4]),
        .O(\ctr_state[4]_i_4__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_5__1 
       (.I0(counter_phase180_output[3]),
        .O(\ctr_state[4]_i_5__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_6__1 
       (.I0(counter_phase180_output[2]),
        .O(\ctr_state[4]_i_6__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_7__1 
       (.I0(counter_phase180_output[1]),
        .O(\ctr_state[4]_i_7__1_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[5]_i_1__1 
       (.I0(external_reset_i),
        .I1(data0[5]),
        .I2(\ctr_state[12]_i_2__1_n_0 ),
        .I3(counter_phase180_dir_reg),
        .I4(\ctr_state_reg[8]_i_3__1_n_7 ),
        .I5(\ctr_state[12]_i_4_n_0 ),
        .O(\ctr_state[5]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[6]_i_1__2 
       (.I0(external_reset_i),
        .I1(data0[6]),
        .I2(\ctr_state[12]_i_2__1_n_0 ),
        .I3(counter_phase180_dir_reg),
        .I4(\ctr_state_reg[8]_i_3__1_n_6 ),
        .I5(\ctr_state[12]_i_4_n_0 ),
        .O(\ctr_state[6]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'hCCFEFFFECCFECCFE)) 
    \ctr_state[7]_i_1__0 
       (.I0(\ctr_state[12]_i_2__1_n_0 ),
        .I1(external_reset_i),
        .I2(data0[7]),
        .I3(counter_phase180_dir_reg),
        .I4(\ctr_state[12]_i_4_n_0 ),
        .I5(\ctr_state_reg[8]_i_3__1_n_5 ),
        .O(p_1_in[7]));
  LUT6 #(
    .INIT(64'hCCFEFFFECCFECCFE)) 
    \ctr_state[8]_i_1__0 
       (.I0(\ctr_state[12]_i_2__1_n_0 ),
        .I1(external_reset_i),
        .I2(data0[8]),
        .I3(counter_phase180_dir_reg),
        .I4(\ctr_state[12]_i_4_n_0 ),
        .I5(\ctr_state_reg[8]_i_3__1_n_4 ),
        .O(p_1_in[8]));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_4__1 
       (.I0(counter_phase180_output[8]),
        .O(\ctr_state[8]_i_4__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_5__1 
       (.I0(counter_phase180_output[7]),
        .O(\ctr_state[8]_i_5__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_6__1 
       (.I0(counter_phase180_output[6]),
        .O(\ctr_state[8]_i_6__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_7__1 
       (.I0(counter_phase180_output[5]),
        .O(\ctr_state[8]_i_7__1_n_0 ));
  LUT6 #(
    .INIT(64'hCCFEFFFECCFECCFE)) 
    \ctr_state[9]_i_1 
       (.I0(\ctr_state[12]_i_2__1_n_0 ),
        .I1(external_reset_i),
        .I2(data0[9]),
        .I3(counter_phase180_dir_reg),
        .I4(\ctr_state[12]_i_4_n_0 ),
        .I5(\ctr_state_reg[12]_i_5_n_7 ),
        .O(p_1_in[9]));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[0] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[0]),
        .Q(counter_phase180_output[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[10] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[10]_i_1__1_n_0 ),
        .Q(counter_phase180_output[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[11] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[11]_i_1__2_n_0 ),
        .Q(counter_phase180_output[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[12] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[12]),
        .Q(counter_phase180_output[12]),
        .R(1'b0));
  CARRY4 \ctr_state_reg[12]_i_3__1 
       (.CI(\ctr_state_reg[8]_i_2__1_n_0 ),
        .CO({\NLW_ctr_state_reg[12]_i_3__1_CO_UNCONNECTED [3],\ctr_state_reg[12]_i_3__1_n_1 ,\ctr_state_reg[12]_i_3__1_n_2 ,\ctr_state_reg[12]_i_3__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,counter_phase180_output[11:9]}),
        .O(data0[12:9]),
        .S({\ctr_state[12]_i_8__1_n_0 ,\ctr_state[12]_i_9__1_n_0 ,\ctr_state[12]_i_10__1_n_0 ,\ctr_state[12]_i_11__1_n_0 }));
  CARRY4 \ctr_state_reg[12]_i_5 
       (.CI(\ctr_state_reg[8]_i_3__1_n_0 ),
        .CO({\NLW_ctr_state_reg[12]_i_5_CO_UNCONNECTED [3],\ctr_state_reg[12]_i_5_n_1 ,\ctr_state_reg[12]_i_5_n_2 ,\ctr_state_reg[12]_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[12]_i_5_n_4 ,\ctr_state_reg[12]_i_5_n_5 ,\ctr_state_reg[12]_i_5_n_6 ,\ctr_state_reg[12]_i_5_n_7 }),
        .S(counter_phase180_output[12:9]));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[1] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[1]_i_1__1_n_0 ),
        .Q(counter_phase180_output[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[2] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[2]_i_1__2_n_0 ),
        .Q(counter_phase180_output[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[3] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[3]),
        .Q(counter_phase180_output[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[4] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[4]_i_1__1_n_0 ),
        .Q(counter_phase180_output[4]),
        .R(1'b0));
  CARRY4 \ctr_state_reg[4]_i_2__1 
       (.CI(1'b0),
        .CO({\ctr_state_reg[4]_i_2__1_n_0 ,\ctr_state_reg[4]_i_2__1_n_1 ,\ctr_state_reg[4]_i_2__1_n_2 ,\ctr_state_reg[4]_i_2__1_n_3 }),
        .CYINIT(counter_phase180_output[0]),
        .DI(counter_phase180_output[4:1]),
        .O(data0[4:1]),
        .S({\ctr_state[4]_i_4__1_n_0 ,\ctr_state[4]_i_5__1_n_0 ,\ctr_state[4]_i_6__1_n_0 ,\ctr_state[4]_i_7__1_n_0 }));
  CARRY4 \ctr_state_reg[4]_i_3__1 
       (.CI(1'b0),
        .CO({\ctr_state_reg[4]_i_3__1_n_0 ,\ctr_state_reg[4]_i_3__1_n_1 ,\ctr_state_reg[4]_i_3__1_n_2 ,\ctr_state_reg[4]_i_3__1_n_3 }),
        .CYINIT(counter_phase180_output[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[4]_i_3__1_n_4 ,\ctr_state_reg[4]_i_3__1_n_5 ,\ctr_state_reg[4]_i_3__1_n_6 ,\ctr_state_reg[4]_i_3__1_n_7 }),
        .S(counter_phase180_output[4:1]));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[5] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[5]_i_1__1_n_0 ),
        .Q(counter_phase180_output[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[6] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[6]_i_1__2_n_0 ),
        .Q(counter_phase180_output[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[7] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[7]),
        .Q(counter_phase180_output[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[8] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[8]),
        .Q(counter_phase180_output[8]),
        .R(1'b0));
  CARRY4 \ctr_state_reg[8]_i_2__1 
       (.CI(\ctr_state_reg[4]_i_2__1_n_0 ),
        .CO({\ctr_state_reg[8]_i_2__1_n_0 ,\ctr_state_reg[8]_i_2__1_n_1 ,\ctr_state_reg[8]_i_2__1_n_2 ,\ctr_state_reg[8]_i_2__1_n_3 }),
        .CYINIT(1'b0),
        .DI(counter_phase180_output[8:5]),
        .O(data0[8:5]),
        .S({\ctr_state[8]_i_4__1_n_0 ,\ctr_state[8]_i_5__1_n_0 ,\ctr_state[8]_i_6__1_n_0 ,\ctr_state[8]_i_7__1_n_0 }));
  CARRY4 \ctr_state_reg[8]_i_3__1 
       (.CI(\ctr_state_reg[4]_i_3__1_n_0 ),
        .CO({\ctr_state_reg[8]_i_3__1_n_0 ,\ctr_state_reg[8]_i_3__1_n_1 ,\ctr_state_reg[8]_i_3__1_n_2 ,\ctr_state_reg[8]_i_3__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[8]_i_3__1_n_4 ,\ctr_state_reg[8]_i_3__1_n_5 ,\ctr_state_reg[8]_i_3__1_n_6 ,\ctr_state_reg[8]_i_3__1_n_7 }),
        .S(counter_phase180_output[8:5]));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[9] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[9]),
        .Q(counter_phase180_output[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__11
       (.I0(counter_phase180_output[12]),
        .I1(\pwm_b_p_i0_inferred__3/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_2 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__13
       (.I0(counter_phase180_output[12]),
        .I1(\pwm_b_p_i0_inferred__5/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_3 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__15
       (.I0(counter_phase180_output[12]),
        .I1(\pwm_b_p_i0_inferred__7/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_4 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__9
       (.I0(counter_phase180_output[12]),
        .I1(\pwm_b_p_i0_inferred__1/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_1 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__11
       (.I0(counter_phase180_output[11]),
        .I1(\pwm_b_p_i0_inferred__3/i__carry__0 [11]),
        .I2(counter_phase180_output[10]),
        .I3(\pwm_b_p_i0_inferred__3/i__carry__0 [10]),
        .O(\ctr_state_reg[12]_2 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__13
       (.I0(counter_phase180_output[11]),
        .I1(\pwm_b_p_i0_inferred__5/i__carry__0 [11]),
        .I2(counter_phase180_output[10]),
        .I3(\pwm_b_p_i0_inferred__5/i__carry__0 [10]),
        .O(\ctr_state_reg[12]_3 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__15
       (.I0(counter_phase180_output[11]),
        .I1(\pwm_b_p_i0_inferred__7/i__carry__0 [11]),
        .I2(counter_phase180_output[10]),
        .I3(\pwm_b_p_i0_inferred__7/i__carry__0 [10]),
        .O(\ctr_state_reg[12]_4 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__9
       (.I0(counter_phase180_output[11]),
        .I1(\pwm_b_p_i0_inferred__1/i__carry__0 [11]),
        .I2(counter_phase180_output[10]),
        .I3(\pwm_b_p_i0_inferred__1/i__carry__0 [10]),
        .O(\ctr_state_reg[12]_1 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__11
       (.I0(counter_phase180_output[9]),
        .I1(\pwm_b_p_i0_inferred__3/i__carry__0 [9]),
        .I2(counter_phase180_output[8]),
        .I3(\pwm_b_p_i0_inferred__3/i__carry__0 [8]),
        .O(\ctr_state_reg[12]_2 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__13
       (.I0(counter_phase180_output[9]),
        .I1(\pwm_b_p_i0_inferred__5/i__carry__0 [9]),
        .I2(counter_phase180_output[8]),
        .I3(\pwm_b_p_i0_inferred__5/i__carry__0 [8]),
        .O(\ctr_state_reg[12]_3 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__15
       (.I0(counter_phase180_output[9]),
        .I1(\pwm_b_p_i0_inferred__7/i__carry__0 [9]),
        .I2(counter_phase180_output[8]),
        .I3(\pwm_b_p_i0_inferred__7/i__carry__0 [8]),
        .O(\ctr_state_reg[12]_4 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__9
       (.I0(counter_phase180_output[9]),
        .I1(\pwm_b_p_i0_inferred__1/i__carry__0 [9]),
        .I2(counter_phase180_output[8]),
        .I3(\pwm_b_p_i0_inferred__1/i__carry__0 [8]),
        .O(\ctr_state_reg[12]_1 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__11
       (.I0(counter_phase180_output[7]),
        .I1(\pwm_b_p_i0_inferred__3/i__carry__0 [7]),
        .I2(counter_phase180_output[6]),
        .I3(\pwm_b_p_i0_inferred__3/i__carry__0 [6]),
        .O(\ctr_state_reg[7]_1 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__13
       (.I0(counter_phase180_output[7]),
        .I1(\pwm_b_p_i0_inferred__5/i__carry__0 [7]),
        .I2(counter_phase180_output[6]),
        .I3(\pwm_b_p_i0_inferred__5/i__carry__0 [6]),
        .O(\ctr_state_reg[7]_2 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__15
       (.I0(counter_phase180_output[7]),
        .I1(\pwm_b_p_i0_inferred__7/i__carry__0 [7]),
        .I2(counter_phase180_output[6]),
        .I3(\pwm_b_p_i0_inferred__7/i__carry__0 [6]),
        .O(\ctr_state_reg[7]_3 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__9
       (.I0(counter_phase180_output[7]),
        .I1(\pwm_b_p_i0_inferred__1/i__carry__0 [7]),
        .I2(counter_phase180_output[6]),
        .I3(\pwm_b_p_i0_inferred__1/i__carry__0 [6]),
        .O(\ctr_state_reg[7]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__11
       (.I0(counter_phase180_output[5]),
        .I1(\pwm_b_p_i0_inferred__3/i__carry__0 [5]),
        .I2(counter_phase180_output[4]),
        .I3(\pwm_b_p_i0_inferred__3/i__carry__0 [4]),
        .O(\ctr_state_reg[7]_1 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__13
       (.I0(counter_phase180_output[5]),
        .I1(\pwm_b_p_i0_inferred__5/i__carry__0 [5]),
        .I2(counter_phase180_output[4]),
        .I3(\pwm_b_p_i0_inferred__5/i__carry__0 [4]),
        .O(\ctr_state_reg[7]_2 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__15
       (.I0(counter_phase180_output[5]),
        .I1(\pwm_b_p_i0_inferred__7/i__carry__0 [5]),
        .I2(counter_phase180_output[4]),
        .I3(\pwm_b_p_i0_inferred__7/i__carry__0 [4]),
        .O(\ctr_state_reg[7]_3 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__9
       (.I0(counter_phase180_output[5]),
        .I1(\pwm_b_p_i0_inferred__1/i__carry__0 [5]),
        .I2(counter_phase180_output[4]),
        .I3(\pwm_b_p_i0_inferred__1/i__carry__0 [4]),
        .O(\ctr_state_reg[7]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__11
       (.I0(counter_phase180_output[3]),
        .I1(\pwm_b_p_i0_inferred__3/i__carry__0 [3]),
        .I2(counter_phase180_output[2]),
        .I3(\pwm_b_p_i0_inferred__3/i__carry__0 [2]),
        .O(\ctr_state_reg[7]_1 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__13
       (.I0(counter_phase180_output[3]),
        .I1(\pwm_b_p_i0_inferred__5/i__carry__0 [3]),
        .I2(counter_phase180_output[2]),
        .I3(\pwm_b_p_i0_inferred__5/i__carry__0 [2]),
        .O(\ctr_state_reg[7]_2 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__15
       (.I0(counter_phase180_output[3]),
        .I1(\pwm_b_p_i0_inferred__7/i__carry__0 [3]),
        .I2(counter_phase180_output[2]),
        .I3(\pwm_b_p_i0_inferred__7/i__carry__0 [2]),
        .O(\ctr_state_reg[7]_3 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__9
       (.I0(counter_phase180_output[3]),
        .I1(\pwm_b_p_i0_inferred__1/i__carry__0 [3]),
        .I2(counter_phase180_output[2]),
        .I3(\pwm_b_p_i0_inferred__1/i__carry__0 [2]),
        .O(\ctr_state_reg[7]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__11
       (.I0(counter_phase180_output[1]),
        .I1(\pwm_b_p_i0_inferred__3/i__carry__0 [1]),
        .I2(counter_phase180_output[0]),
        .I3(\pwm_b_p_i0_inferred__3/i__carry__0 [0]),
        .O(\ctr_state_reg[7]_1 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__13
       (.I0(counter_phase180_output[1]),
        .I1(\pwm_b_p_i0_inferred__5/i__carry__0 [1]),
        .I2(counter_phase180_output[0]),
        .I3(\pwm_b_p_i0_inferred__5/i__carry__0 [0]),
        .O(\ctr_state_reg[7]_2 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__15
       (.I0(counter_phase180_output[1]),
        .I1(\pwm_b_p_i0_inferred__7/i__carry__0 [1]),
        .I2(counter_phase180_output[0]),
        .I3(\pwm_b_p_i0_inferred__7/i__carry__0 [0]),
        .O(\ctr_state_reg[7]_3 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__9
       (.I0(counter_phase180_output[1]),
        .I1(\pwm_b_p_i0_inferred__1/i__carry__0 [1]),
        .I2(counter_phase180_output[0]),
        .I3(\pwm_b_p_i0_inferred__1/i__carry__0 [0]),
        .O(\ctr_state_reg[7]_0 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    pwm_b_p_i0_carry__0_i_4
       (.I0(counter_phase180_output[12]),
        .I1(Q[12]),
        .O(\ctr_state_reg[12]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    pwm_b_p_i0_carry__0_i_5
       (.I0(counter_phase180_output[11]),
        .I1(Q[11]),
        .I2(counter_phase180_output[10]),
        .I3(Q[10]),
        .O(\ctr_state_reg[12]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    pwm_b_p_i0_carry__0_i_6
       (.I0(counter_phase180_output[9]),
        .I1(Q[9]),
        .I2(counter_phase180_output[8]),
        .I3(Q[8]),
        .O(\ctr_state_reg[12]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    pwm_b_p_i0_carry_i_5
       (.I0(counter_phase180_output[7]),
        .I1(Q[7]),
        .I2(counter_phase180_output[6]),
        .I3(Q[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    pwm_b_p_i0_carry_i_6
       (.I0(counter_phase180_output[5]),
        .I1(Q[5]),
        .I2(counter_phase180_output[4]),
        .I3(Q[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    pwm_b_p_i0_carry_i_7
       (.I0(counter_phase180_output[3]),
        .I1(Q[3]),
        .I2(counter_phase180_output[2]),
        .I3(Q[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    pwm_b_p_i0_carry_i_8
       (.I0(counter_phase180_output[1]),
        .I1(Q[1]),
        .I2(counter_phase180_output[0]),
        .I3(Q[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "N_bits_counter" *) 
module system_pwm_x40_ip_0_0_N_bits_counter_1
   (counter_phase270_output,
    S,
    \ctr_state_reg[12]_0 ,
    \ctr_state_reg[7]_0 ,
    \ctr_state_reg[12]_1 ,
    \ctr_state_reg[7]_1 ,
    \ctr_state_reg[12]_2 ,
    \ctr_state_reg[7]_2 ,
    \ctr_state_reg[12]_3 ,
    \ctr_state_reg[7]_3 ,
    \ctr_state_reg[12]_4 ,
    counter_phase270_tc,
    external_reset_i,
    \ctr_state_reg[0]_0 ,
    Q,
    \pwm_b_p_i0_inferred__2/i__carry__0 ,
    \pwm_b_p_i0_inferred__4/i__carry__0 ,
    \pwm_b_p_i0_inferred__6/i__carry__0 ,
    \pwm_b_p_i0_inferred__8/i__carry__0 ,
    CLK_100MHZ);
  output [12:0]counter_phase270_output;
  output [3:0]S;
  output [2:0]\ctr_state_reg[12]_0 ;
  output [3:0]\ctr_state_reg[7]_0 ;
  output [2:0]\ctr_state_reg[12]_1 ;
  output [3:0]\ctr_state_reg[7]_1 ;
  output [2:0]\ctr_state_reg[12]_2 ;
  output [3:0]\ctr_state_reg[7]_2 ;
  output [2:0]\ctr_state_reg[12]_3 ;
  output [3:0]\ctr_state_reg[7]_3 ;
  output [2:0]\ctr_state_reg[12]_4 ;
  output counter_phase270_tc;
  input external_reset_i;
  input \ctr_state_reg[0]_0 ;
  input [12:0]Q;
  input [12:0]\pwm_b_p_i0_inferred__2/i__carry__0 ;
  input [12:0]\pwm_b_p_i0_inferred__4/i__carry__0 ;
  input [12:0]\pwm_b_p_i0_inferred__6/i__carry__0 ;
  input [12:0]\pwm_b_p_i0_inferred__8/i__carry__0 ;
  input CLK_100MHZ;

  wire CLK_100MHZ;
  wire [12:0]Q;
  wire [3:0]S;
  wire [12:0]counter_phase270_output;
  wire counter_phase270_tc;
  wire \ctr_state[10]_i_1__2_n_0 ;
  wire \ctr_state[12]_i_10__2_n_0 ;
  wire \ctr_state[12]_i_11__2_n_0 ;
  wire \ctr_state[12]_i_12__2_n_0 ;
  wire \ctr_state[12]_i_1__2_n_0 ;
  wire \ctr_state[12]_i_2__2_n_0 ;
  wire \ctr_state[12]_i_5__1_n_0 ;
  wire \ctr_state[12]_i_6__2_n_0 ;
  wire \ctr_state[12]_i_7__2_n_0 ;
  wire \ctr_state[12]_i_8__2_n_0 ;
  wire \ctr_state[12]_i_9__2_n_0 ;
  wire \ctr_state[1]_i_1__2_n_0 ;
  wire \ctr_state[3]_i_1__2_n_0 ;
  wire \ctr_state[4]_i_1__2_n_0 ;
  wire \ctr_state[4]_i_4__2_n_0 ;
  wire \ctr_state[4]_i_5__2_n_0 ;
  wire \ctr_state[4]_i_6__2_n_0 ;
  wire \ctr_state[4]_i_7__2_n_0 ;
  wire \ctr_state[5]_i_1__2_n_0 ;
  wire \ctr_state[8]_i_4__2_n_0 ;
  wire \ctr_state[8]_i_5__2_n_0 ;
  wire \ctr_state[8]_i_6__2_n_0 ;
  wire \ctr_state[8]_i_7__2_n_0 ;
  wire \ctr_state[9]_i_1__2_n_0 ;
  wire \ctr_state_reg[0]_0 ;
  wire [2:0]\ctr_state_reg[12]_0 ;
  wire [2:0]\ctr_state_reg[12]_1 ;
  wire [2:0]\ctr_state_reg[12]_2 ;
  wire [2:0]\ctr_state_reg[12]_3 ;
  wire [2:0]\ctr_state_reg[12]_4 ;
  wire \ctr_state_reg[12]_i_3__2_n_1 ;
  wire \ctr_state_reg[12]_i_3__2_n_2 ;
  wire \ctr_state_reg[12]_i_3__2_n_3 ;
  wire \ctr_state_reg[12]_i_4__1_n_1 ;
  wire \ctr_state_reg[12]_i_4__1_n_2 ;
  wire \ctr_state_reg[12]_i_4__1_n_3 ;
  wire \ctr_state_reg[12]_i_4__1_n_4 ;
  wire \ctr_state_reg[12]_i_4__1_n_5 ;
  wire \ctr_state_reg[12]_i_4__1_n_6 ;
  wire \ctr_state_reg[12]_i_4__1_n_7 ;
  wire \ctr_state_reg[4]_i_2__2_n_0 ;
  wire \ctr_state_reg[4]_i_2__2_n_1 ;
  wire \ctr_state_reg[4]_i_2__2_n_2 ;
  wire \ctr_state_reg[4]_i_2__2_n_3 ;
  wire \ctr_state_reg[4]_i_3__2_n_0 ;
  wire \ctr_state_reg[4]_i_3__2_n_1 ;
  wire \ctr_state_reg[4]_i_3__2_n_2 ;
  wire \ctr_state_reg[4]_i_3__2_n_3 ;
  wire \ctr_state_reg[4]_i_3__2_n_4 ;
  wire \ctr_state_reg[4]_i_3__2_n_5 ;
  wire \ctr_state_reg[4]_i_3__2_n_6 ;
  wire \ctr_state_reg[4]_i_3__2_n_7 ;
  wire [3:0]\ctr_state_reg[7]_0 ;
  wire [3:0]\ctr_state_reg[7]_1 ;
  wire [3:0]\ctr_state_reg[7]_2 ;
  wire [3:0]\ctr_state_reg[7]_3 ;
  wire \ctr_state_reg[8]_i_2__2_n_0 ;
  wire \ctr_state_reg[8]_i_2__2_n_1 ;
  wire \ctr_state_reg[8]_i_2__2_n_2 ;
  wire \ctr_state_reg[8]_i_2__2_n_3 ;
  wire \ctr_state_reg[8]_i_3__2_n_0 ;
  wire \ctr_state_reg[8]_i_3__2_n_1 ;
  wire \ctr_state_reg[8]_i_3__2_n_2 ;
  wire \ctr_state_reg[8]_i_3__2_n_3 ;
  wire \ctr_state_reg[8]_i_3__2_n_4 ;
  wire \ctr_state_reg[8]_i_3__2_n_5 ;
  wire \ctr_state_reg[8]_i_3__2_n_6 ;
  wire \ctr_state_reg[8]_i_3__2_n_7 ;
  wire [12:1]data0;
  wire external_reset_i;
  wire [11:0]p_1_in;
  wire [12:0]\pwm_b_p_i0_inferred__2/i__carry__0 ;
  wire [12:0]\pwm_b_p_i0_inferred__4/i__carry__0 ;
  wire [12:0]\pwm_b_p_i0_inferred__6/i__carry__0 ;
  wire [12:0]\pwm_b_p_i0_inferred__8/i__carry__0 ;
  wire [3:3]\NLW_ctr_state_reg[12]_i_3__2_CO_UNCONNECTED ;
  wire [3:3]\NLW_ctr_state_reg[12]_i_4__1_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    counter_phase270_dir_i_2
       (.I0(\ctr_state[12]_i_5__1_n_0 ),
        .I1(\ctr_state_reg[0]_0 ),
        .I2(\ctr_state[12]_i_2__2_n_0 ),
        .O(counter_phase270_tc));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h00011101)) 
    \ctr_state[0]_i_1__1 
       (.I0(external_reset_i),
        .I1(counter_phase270_output[0]),
        .I2(\ctr_state[12]_i_2__2_n_0 ),
        .I3(\ctr_state_reg[0]_0 ),
        .I4(\ctr_state[12]_i_5__1_n_0 ),
        .O(p_1_in[0]));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[10]_i_1__2 
       (.I0(external_reset_i),
        .I1(data0[10]),
        .I2(\ctr_state[12]_i_2__2_n_0 ),
        .I3(\ctr_state_reg[0]_0 ),
        .I4(\ctr_state_reg[12]_i_4__1_n_6 ),
        .I5(\ctr_state[12]_i_5__1_n_0 ),
        .O(\ctr_state[10]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'hAABAFFBAAABAAABA)) 
    \ctr_state[11]_i_1__0 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__2_n_0 ),
        .I2(data0[11]),
        .I3(\ctr_state_reg[0]_0 ),
        .I4(\ctr_state[12]_i_5__1_n_0 ),
        .I5(\ctr_state_reg[12]_i_4__1_n_5 ),
        .O(p_1_in[11]));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_10__2 
       (.I0(counter_phase270_output[10]),
        .O(\ctr_state[12]_i_10__2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_11__2 
       (.I0(counter_phase270_output[9]),
        .O(\ctr_state[12]_i_11__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \ctr_state[12]_i_12__2 
       (.I0(counter_phase270_output[9]),
        .I1(counter_phase270_output[8]),
        .I2(counter_phase270_output[3]),
        .I3(counter_phase270_output[7]),
        .I4(counter_phase270_output[12]),
        .O(\ctr_state[12]_i_12__2_n_0 ));
  LUT6 #(
    .INIT(64'h0054005455540054)) 
    \ctr_state[12]_i_1__2 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__2_n_0 ),
        .I2(data0[12]),
        .I3(\ctr_state_reg[0]_0 ),
        .I4(\ctr_state_reg[12]_i_4__1_n_4 ),
        .I5(\ctr_state[12]_i_5__1_n_0 ),
        .O(\ctr_state[12]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \ctr_state[12]_i_2__2 
       (.I0(\ctr_state[12]_i_6__2_n_0 ),
        .I1(\ctr_state[12]_i_7__2_n_0 ),
        .I2(counter_phase270_output[1]),
        .I3(counter_phase270_output[0]),
        .I4(counter_phase270_output[10]),
        .I5(counter_phase270_output[2]),
        .O(\ctr_state[12]_i_2__2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \ctr_state[12]_i_5__1 
       (.I0(\ctr_state[12]_i_7__2_n_0 ),
        .I1(counter_phase270_output[1]),
        .I2(counter_phase270_output[0]),
        .I3(counter_phase270_output[10]),
        .I4(counter_phase270_output[2]),
        .I5(\ctr_state[12]_i_12__2_n_0 ),
        .O(\ctr_state[12]_i_5__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \ctr_state[12]_i_6__2 
       (.I0(counter_phase270_output[9]),
        .I1(counter_phase270_output[8]),
        .I2(counter_phase270_output[3]),
        .I3(counter_phase270_output[7]),
        .I4(counter_phase270_output[12]),
        .O(\ctr_state[12]_i_6__2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \ctr_state[12]_i_7__2 
       (.I0(counter_phase270_output[5]),
        .I1(counter_phase270_output[4]),
        .I2(counter_phase270_output[11]),
        .I3(counter_phase270_output[6]),
        .O(\ctr_state[12]_i_7__2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_8__2 
       (.I0(counter_phase270_output[12]),
        .O(\ctr_state[12]_i_8__2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_9__2 
       (.I0(counter_phase270_output[11]),
        .O(\ctr_state[12]_i_9__2_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[1]_i_1__2 
       (.I0(external_reset_i),
        .I1(data0[1]),
        .I2(\ctr_state[12]_i_2__2_n_0 ),
        .I3(\ctr_state_reg[0]_0 ),
        .I4(\ctr_state_reg[4]_i_3__2_n_7 ),
        .I5(\ctr_state[12]_i_5__1_n_0 ),
        .O(\ctr_state[1]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'hAABAFFBAAABAAABA)) 
    \ctr_state[2]_i_1__0 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__2_n_0 ),
        .I2(data0[2]),
        .I3(\ctr_state_reg[0]_0 ),
        .I4(\ctr_state[12]_i_5__1_n_0 ),
        .I5(\ctr_state_reg[4]_i_3__2_n_6 ),
        .O(p_1_in[2]));
  LUT6 #(
    .INIT(64'h0054005455540054)) 
    \ctr_state[3]_i_1__2 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__2_n_0 ),
        .I2(data0[3]),
        .I3(\ctr_state_reg[0]_0 ),
        .I4(\ctr_state_reg[4]_i_3__2_n_5 ),
        .I5(\ctr_state[12]_i_5__1_n_0 ),
        .O(\ctr_state[3]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[4]_i_1__2 
       (.I0(external_reset_i),
        .I1(data0[4]),
        .I2(\ctr_state[12]_i_2__2_n_0 ),
        .I3(\ctr_state_reg[0]_0 ),
        .I4(\ctr_state_reg[4]_i_3__2_n_4 ),
        .I5(\ctr_state[12]_i_5__1_n_0 ),
        .O(\ctr_state[4]_i_1__2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_4__2 
       (.I0(counter_phase270_output[4]),
        .O(\ctr_state[4]_i_4__2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_5__2 
       (.I0(counter_phase270_output[3]),
        .O(\ctr_state[4]_i_5__2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_6__2 
       (.I0(counter_phase270_output[2]),
        .O(\ctr_state[4]_i_6__2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_7__2 
       (.I0(counter_phase270_output[1]),
        .O(\ctr_state[4]_i_7__2_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[5]_i_1__2 
       (.I0(external_reset_i),
        .I1(data0[5]),
        .I2(\ctr_state[12]_i_2__2_n_0 ),
        .I3(\ctr_state_reg[0]_0 ),
        .I4(\ctr_state_reg[8]_i_3__2_n_7 ),
        .I5(\ctr_state[12]_i_5__1_n_0 ),
        .O(\ctr_state[5]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'hAABAFFBAAABAAABA)) 
    \ctr_state[6]_i_1__0 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__2_n_0 ),
        .I2(data0[6]),
        .I3(\ctr_state_reg[0]_0 ),
        .I4(\ctr_state[12]_i_5__1_n_0 ),
        .I5(\ctr_state_reg[8]_i_3__2_n_6 ),
        .O(p_1_in[6]));
  LUT6 #(
    .INIT(64'hAAFEFFFEAAFEAAFE)) 
    \ctr_state[7]_i_1__1 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__2_n_0 ),
        .I2(data0[7]),
        .I3(\ctr_state_reg[0]_0 ),
        .I4(\ctr_state[12]_i_5__1_n_0 ),
        .I5(\ctr_state_reg[8]_i_3__2_n_5 ),
        .O(p_1_in[7]));
  LUT6 #(
    .INIT(64'hAAFEFFFEAAFEAAFE)) 
    \ctr_state[8]_i_1__1 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__2_n_0 ),
        .I2(data0[8]),
        .I3(\ctr_state_reg[0]_0 ),
        .I4(\ctr_state[12]_i_5__1_n_0 ),
        .I5(\ctr_state_reg[8]_i_3__2_n_4 ),
        .O(p_1_in[8]));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_4__2 
       (.I0(counter_phase270_output[8]),
        .O(\ctr_state[8]_i_4__2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_5__2 
       (.I0(counter_phase270_output[7]),
        .O(\ctr_state[8]_i_5__2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_6__2 
       (.I0(counter_phase270_output[6]),
        .O(\ctr_state[8]_i_6__2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_7__2 
       (.I0(counter_phase270_output[5]),
        .O(\ctr_state[8]_i_7__2_n_0 ));
  LUT6 #(
    .INIT(64'h0054005455540054)) 
    \ctr_state[9]_i_1__2 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__2_n_0 ),
        .I2(data0[9]),
        .I3(\ctr_state_reg[0]_0 ),
        .I4(\ctr_state_reg[12]_i_4__1_n_7 ),
        .I5(\ctr_state[12]_i_5__1_n_0 ),
        .O(\ctr_state[9]_i_1__2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[0] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[0]),
        .Q(counter_phase270_output[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[10] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[10]_i_1__2_n_0 ),
        .Q(counter_phase270_output[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[11] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[11]),
        .Q(counter_phase270_output[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[12] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[12]_i_1__2_n_0 ),
        .Q(counter_phase270_output[12]),
        .R(1'b0));
  CARRY4 \ctr_state_reg[12]_i_3__2 
       (.CI(\ctr_state_reg[8]_i_2__2_n_0 ),
        .CO({\NLW_ctr_state_reg[12]_i_3__2_CO_UNCONNECTED [3],\ctr_state_reg[12]_i_3__2_n_1 ,\ctr_state_reg[12]_i_3__2_n_2 ,\ctr_state_reg[12]_i_3__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,counter_phase270_output[11:9]}),
        .O(data0[12:9]),
        .S({\ctr_state[12]_i_8__2_n_0 ,\ctr_state[12]_i_9__2_n_0 ,\ctr_state[12]_i_10__2_n_0 ,\ctr_state[12]_i_11__2_n_0 }));
  CARRY4 \ctr_state_reg[12]_i_4__1 
       (.CI(\ctr_state_reg[8]_i_3__2_n_0 ),
        .CO({\NLW_ctr_state_reg[12]_i_4__1_CO_UNCONNECTED [3],\ctr_state_reg[12]_i_4__1_n_1 ,\ctr_state_reg[12]_i_4__1_n_2 ,\ctr_state_reg[12]_i_4__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[12]_i_4__1_n_4 ,\ctr_state_reg[12]_i_4__1_n_5 ,\ctr_state_reg[12]_i_4__1_n_6 ,\ctr_state_reg[12]_i_4__1_n_7 }),
        .S(counter_phase270_output[12:9]));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[1] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[1]_i_1__2_n_0 ),
        .Q(counter_phase270_output[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[2] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[2]),
        .Q(counter_phase270_output[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[3] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[3]_i_1__2_n_0 ),
        .Q(counter_phase270_output[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[4] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[4]_i_1__2_n_0 ),
        .Q(counter_phase270_output[4]),
        .R(1'b0));
  CARRY4 \ctr_state_reg[4]_i_2__2 
       (.CI(1'b0),
        .CO({\ctr_state_reg[4]_i_2__2_n_0 ,\ctr_state_reg[4]_i_2__2_n_1 ,\ctr_state_reg[4]_i_2__2_n_2 ,\ctr_state_reg[4]_i_2__2_n_3 }),
        .CYINIT(counter_phase270_output[0]),
        .DI(counter_phase270_output[4:1]),
        .O(data0[4:1]),
        .S({\ctr_state[4]_i_4__2_n_0 ,\ctr_state[4]_i_5__2_n_0 ,\ctr_state[4]_i_6__2_n_0 ,\ctr_state[4]_i_7__2_n_0 }));
  CARRY4 \ctr_state_reg[4]_i_3__2 
       (.CI(1'b0),
        .CO({\ctr_state_reg[4]_i_3__2_n_0 ,\ctr_state_reg[4]_i_3__2_n_1 ,\ctr_state_reg[4]_i_3__2_n_2 ,\ctr_state_reg[4]_i_3__2_n_3 }),
        .CYINIT(counter_phase270_output[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[4]_i_3__2_n_4 ,\ctr_state_reg[4]_i_3__2_n_5 ,\ctr_state_reg[4]_i_3__2_n_6 ,\ctr_state_reg[4]_i_3__2_n_7 }),
        .S(counter_phase270_output[4:1]));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[5] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[5]_i_1__2_n_0 ),
        .Q(counter_phase270_output[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[6] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[6]),
        .Q(counter_phase270_output[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[7] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[7]),
        .Q(counter_phase270_output[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[8] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[8]),
        .Q(counter_phase270_output[8]),
        .R(1'b0));
  CARRY4 \ctr_state_reg[8]_i_2__2 
       (.CI(\ctr_state_reg[4]_i_2__2_n_0 ),
        .CO({\ctr_state_reg[8]_i_2__2_n_0 ,\ctr_state_reg[8]_i_2__2_n_1 ,\ctr_state_reg[8]_i_2__2_n_2 ,\ctr_state_reg[8]_i_2__2_n_3 }),
        .CYINIT(1'b0),
        .DI(counter_phase270_output[8:5]),
        .O(data0[8:5]),
        .S({\ctr_state[8]_i_4__2_n_0 ,\ctr_state[8]_i_5__2_n_0 ,\ctr_state[8]_i_6__2_n_0 ,\ctr_state[8]_i_7__2_n_0 }));
  CARRY4 \ctr_state_reg[8]_i_3__2 
       (.CI(\ctr_state_reg[4]_i_3__2_n_0 ),
        .CO({\ctr_state_reg[8]_i_3__2_n_0 ,\ctr_state_reg[8]_i_3__2_n_1 ,\ctr_state_reg[8]_i_3__2_n_2 ,\ctr_state_reg[8]_i_3__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[8]_i_3__2_n_4 ,\ctr_state_reg[8]_i_3__2_n_5 ,\ctr_state_reg[8]_i_3__2_n_6 ,\ctr_state_reg[8]_i_3__2_n_7 }),
        .S(counter_phase270_output[8:5]));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[9] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[9]_i_1__2_n_0 ),
        .Q(counter_phase270_output[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__10
       (.I0(counter_phase270_output[12]),
        .I1(\pwm_b_p_i0_inferred__2/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_1 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__12
       (.I0(counter_phase270_output[12]),
        .I1(\pwm_b_p_i0_inferred__4/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_2 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__14
       (.I0(counter_phase270_output[12]),
        .I1(\pwm_b_p_i0_inferred__6/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_3 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__16
       (.I0(counter_phase270_output[12]),
        .I1(\pwm_b_p_i0_inferred__8/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_4 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__8
       (.I0(counter_phase270_output[12]),
        .I1(Q[12]),
        .O(\ctr_state_reg[12]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__10
       (.I0(counter_phase270_output[11]),
        .I1(\pwm_b_p_i0_inferred__2/i__carry__0 [11]),
        .I2(counter_phase270_output[10]),
        .I3(\pwm_b_p_i0_inferred__2/i__carry__0 [10]),
        .O(\ctr_state_reg[12]_1 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__12
       (.I0(counter_phase270_output[11]),
        .I1(\pwm_b_p_i0_inferred__4/i__carry__0 [11]),
        .I2(counter_phase270_output[10]),
        .I3(\pwm_b_p_i0_inferred__4/i__carry__0 [10]),
        .O(\ctr_state_reg[12]_2 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__14
       (.I0(counter_phase270_output[11]),
        .I1(\pwm_b_p_i0_inferred__6/i__carry__0 [11]),
        .I2(counter_phase270_output[10]),
        .I3(\pwm_b_p_i0_inferred__6/i__carry__0 [10]),
        .O(\ctr_state_reg[12]_3 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__16
       (.I0(counter_phase270_output[11]),
        .I1(\pwm_b_p_i0_inferred__8/i__carry__0 [11]),
        .I2(counter_phase270_output[10]),
        .I3(\pwm_b_p_i0_inferred__8/i__carry__0 [10]),
        .O(\ctr_state_reg[12]_4 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__8
       (.I0(counter_phase270_output[11]),
        .I1(Q[11]),
        .I2(counter_phase270_output[10]),
        .I3(Q[10]),
        .O(\ctr_state_reg[12]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__10
       (.I0(counter_phase270_output[9]),
        .I1(\pwm_b_p_i0_inferred__2/i__carry__0 [9]),
        .I2(counter_phase270_output[8]),
        .I3(\pwm_b_p_i0_inferred__2/i__carry__0 [8]),
        .O(\ctr_state_reg[12]_1 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__12
       (.I0(counter_phase270_output[9]),
        .I1(\pwm_b_p_i0_inferred__4/i__carry__0 [9]),
        .I2(counter_phase270_output[8]),
        .I3(\pwm_b_p_i0_inferred__4/i__carry__0 [8]),
        .O(\ctr_state_reg[12]_2 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__14
       (.I0(counter_phase270_output[9]),
        .I1(\pwm_b_p_i0_inferred__6/i__carry__0 [9]),
        .I2(counter_phase270_output[8]),
        .I3(\pwm_b_p_i0_inferred__6/i__carry__0 [8]),
        .O(\ctr_state_reg[12]_3 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__16
       (.I0(counter_phase270_output[9]),
        .I1(\pwm_b_p_i0_inferred__8/i__carry__0 [9]),
        .I2(counter_phase270_output[8]),
        .I3(\pwm_b_p_i0_inferred__8/i__carry__0 [8]),
        .O(\ctr_state_reg[12]_4 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__8
       (.I0(counter_phase270_output[9]),
        .I1(Q[9]),
        .I2(counter_phase270_output[8]),
        .I3(Q[8]),
        .O(\ctr_state_reg[12]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__10
       (.I0(counter_phase270_output[7]),
        .I1(\pwm_b_p_i0_inferred__2/i__carry__0 [7]),
        .I2(counter_phase270_output[6]),
        .I3(\pwm_b_p_i0_inferred__2/i__carry__0 [6]),
        .O(\ctr_state_reg[7]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__12
       (.I0(counter_phase270_output[7]),
        .I1(\pwm_b_p_i0_inferred__4/i__carry__0 [7]),
        .I2(counter_phase270_output[6]),
        .I3(\pwm_b_p_i0_inferred__4/i__carry__0 [6]),
        .O(\ctr_state_reg[7]_1 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__14
       (.I0(counter_phase270_output[7]),
        .I1(\pwm_b_p_i0_inferred__6/i__carry__0 [7]),
        .I2(counter_phase270_output[6]),
        .I3(\pwm_b_p_i0_inferred__6/i__carry__0 [6]),
        .O(\ctr_state_reg[7]_2 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__16
       (.I0(counter_phase270_output[7]),
        .I1(\pwm_b_p_i0_inferred__8/i__carry__0 [7]),
        .I2(counter_phase270_output[6]),
        .I3(\pwm_b_p_i0_inferred__8/i__carry__0 [6]),
        .O(\ctr_state_reg[7]_3 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__8
       (.I0(counter_phase270_output[7]),
        .I1(Q[7]),
        .I2(counter_phase270_output[6]),
        .I3(Q[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__10
       (.I0(counter_phase270_output[5]),
        .I1(\pwm_b_p_i0_inferred__2/i__carry__0 [5]),
        .I2(counter_phase270_output[4]),
        .I3(\pwm_b_p_i0_inferred__2/i__carry__0 [4]),
        .O(\ctr_state_reg[7]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__12
       (.I0(counter_phase270_output[5]),
        .I1(\pwm_b_p_i0_inferred__4/i__carry__0 [5]),
        .I2(counter_phase270_output[4]),
        .I3(\pwm_b_p_i0_inferred__4/i__carry__0 [4]),
        .O(\ctr_state_reg[7]_1 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__14
       (.I0(counter_phase270_output[5]),
        .I1(\pwm_b_p_i0_inferred__6/i__carry__0 [5]),
        .I2(counter_phase270_output[4]),
        .I3(\pwm_b_p_i0_inferred__6/i__carry__0 [4]),
        .O(\ctr_state_reg[7]_2 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__16
       (.I0(counter_phase270_output[5]),
        .I1(\pwm_b_p_i0_inferred__8/i__carry__0 [5]),
        .I2(counter_phase270_output[4]),
        .I3(\pwm_b_p_i0_inferred__8/i__carry__0 [4]),
        .O(\ctr_state_reg[7]_3 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__8
       (.I0(counter_phase270_output[5]),
        .I1(Q[5]),
        .I2(counter_phase270_output[4]),
        .I3(Q[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__10
       (.I0(counter_phase270_output[3]),
        .I1(\pwm_b_p_i0_inferred__2/i__carry__0 [3]),
        .I2(counter_phase270_output[2]),
        .I3(\pwm_b_p_i0_inferred__2/i__carry__0 [2]),
        .O(\ctr_state_reg[7]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__12
       (.I0(counter_phase270_output[3]),
        .I1(\pwm_b_p_i0_inferred__4/i__carry__0 [3]),
        .I2(counter_phase270_output[2]),
        .I3(\pwm_b_p_i0_inferred__4/i__carry__0 [2]),
        .O(\ctr_state_reg[7]_1 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__14
       (.I0(counter_phase270_output[3]),
        .I1(\pwm_b_p_i0_inferred__6/i__carry__0 [3]),
        .I2(counter_phase270_output[2]),
        .I3(\pwm_b_p_i0_inferred__6/i__carry__0 [2]),
        .O(\ctr_state_reg[7]_2 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__16
       (.I0(counter_phase270_output[3]),
        .I1(\pwm_b_p_i0_inferred__8/i__carry__0 [3]),
        .I2(counter_phase270_output[2]),
        .I3(\pwm_b_p_i0_inferred__8/i__carry__0 [2]),
        .O(\ctr_state_reg[7]_3 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__8
       (.I0(counter_phase270_output[3]),
        .I1(Q[3]),
        .I2(counter_phase270_output[2]),
        .I3(Q[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__10
       (.I0(counter_phase270_output[1]),
        .I1(\pwm_b_p_i0_inferred__2/i__carry__0 [1]),
        .I2(counter_phase270_output[0]),
        .I3(\pwm_b_p_i0_inferred__2/i__carry__0 [0]),
        .O(\ctr_state_reg[7]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__12
       (.I0(counter_phase270_output[1]),
        .I1(\pwm_b_p_i0_inferred__4/i__carry__0 [1]),
        .I2(counter_phase270_output[0]),
        .I3(\pwm_b_p_i0_inferred__4/i__carry__0 [0]),
        .O(\ctr_state_reg[7]_1 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__14
       (.I0(counter_phase270_output[1]),
        .I1(\pwm_b_p_i0_inferred__6/i__carry__0 [1]),
        .I2(counter_phase270_output[0]),
        .I3(\pwm_b_p_i0_inferred__6/i__carry__0 [0]),
        .O(\ctr_state_reg[7]_2 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__16
       (.I0(counter_phase270_output[1]),
        .I1(\pwm_b_p_i0_inferred__8/i__carry__0 [1]),
        .I2(counter_phase270_output[0]),
        .I3(\pwm_b_p_i0_inferred__8/i__carry__0 [0]),
        .O(\ctr_state_reg[7]_3 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__8
       (.I0(counter_phase270_output[1]),
        .I1(Q[1]),
        .I2(counter_phase270_output[0]),
        .I3(Q[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "N_bits_counter" *) 
module system_pwm_x40_ip_0_0_N_bits_counter_2
   (counter_phase90_output,
    S,
    \ctr_state_reg[12]_0 ,
    DI,
    \ctr_state_reg[11]_0 ,
    \ctr_state_reg[7]_0 ,
    \ctr_state_reg[11]_1 ,
    \ctr_state_reg[7]_1 ,
    \ctr_state_reg[11]_2 ,
    \ctr_state_reg[7]_2 ,
    \ctr_state_reg[11]_3 ,
    counter_phase90_tc,
    \ctr_state_reg[12]_1 ,
    \ctr_state_reg[12]_2 ,
    \ctr_state_reg[12]_3 ,
    \ctr_state_reg[12]_4 ,
    external_reset_i,
    counter_phase90_dir_reg,
    Q,
    \pwm_a_p_i0_inferred__2/i__carry__0 ,
    \pwm_a_p_i0_inferred__4/i__carry__0 ,
    \pwm_a_p_i0_inferred__6/i__carry__0 ,
    \pwm_a_p_i0_inferred__8/i__carry__0 ,
    CLK_100MHZ);
  output [12:0]counter_phase90_output;
  output [3:0]S;
  output [2:0]\ctr_state_reg[12]_0 ;
  output [3:0]DI;
  output [1:0]\ctr_state_reg[11]_0 ;
  output [3:0]\ctr_state_reg[7]_0 ;
  output [1:0]\ctr_state_reg[11]_1 ;
  output [3:0]\ctr_state_reg[7]_1 ;
  output [1:0]\ctr_state_reg[11]_2 ;
  output [3:0]\ctr_state_reg[7]_2 ;
  output [1:0]\ctr_state_reg[11]_3 ;
  output counter_phase90_tc;
  output [0:0]\ctr_state_reg[12]_1 ;
  output [0:0]\ctr_state_reg[12]_2 ;
  output [0:0]\ctr_state_reg[12]_3 ;
  output [0:0]\ctr_state_reg[12]_4 ;
  input external_reset_i;
  input counter_phase90_dir_reg;
  input [12:0]Q;
  input [12:0]\pwm_a_p_i0_inferred__2/i__carry__0 ;
  input [12:0]\pwm_a_p_i0_inferred__4/i__carry__0 ;
  input [12:0]\pwm_a_p_i0_inferred__6/i__carry__0 ;
  input [12:0]\pwm_a_p_i0_inferred__8/i__carry__0 ;
  input CLK_100MHZ;

  wire CLK_100MHZ;
  wire [3:0]DI;
  wire [12:0]Q;
  wire [3:0]S;
  wire counter_phase90_dir_reg;
  wire [12:0]counter_phase90_output;
  wire counter_phase90_tc;
  wire \ctr_state[0]_i_1__2_n_0 ;
  wire \ctr_state[10]_i_1__0_n_0 ;
  wire \ctr_state[12]_i_10__0_n_0 ;
  wire \ctr_state[12]_i_11__0_n_0 ;
  wire \ctr_state[12]_i_12__0_n_0 ;
  wire \ctr_state[12]_i_13_n_0 ;
  wire \ctr_state[12]_i_1__1_n_0 ;
  wire \ctr_state[12]_i_2__0_n_0 ;
  wire \ctr_state[12]_i_5__0_n_0 ;
  wire \ctr_state[12]_i_6__0_n_0 ;
  wire \ctr_state[12]_i_7__0_n_0 ;
  wire \ctr_state[12]_i_8__0_n_0 ;
  wire \ctr_state[12]_i_9__0_n_0 ;
  wire \ctr_state[1]_i_1__0_n_0 ;
  wire \ctr_state[3]_i_1__1_n_0 ;
  wire \ctr_state[4]_i_1__0_n_0 ;
  wire \ctr_state[4]_i_4__0_n_0 ;
  wire \ctr_state[4]_i_5__0_n_0 ;
  wire \ctr_state[4]_i_6__0_n_0 ;
  wire \ctr_state[4]_i_7__0_n_0 ;
  wire \ctr_state[5]_i_1__0_n_0 ;
  wire \ctr_state[8]_i_4__0_n_0 ;
  wire \ctr_state[8]_i_5__0_n_0 ;
  wire \ctr_state[8]_i_6__0_n_0 ;
  wire \ctr_state[8]_i_7__0_n_0 ;
  wire \ctr_state[9]_i_1__1_n_0 ;
  wire [1:0]\ctr_state_reg[11]_0 ;
  wire [1:0]\ctr_state_reg[11]_1 ;
  wire [1:0]\ctr_state_reg[11]_2 ;
  wire [1:0]\ctr_state_reg[11]_3 ;
  wire [2:0]\ctr_state_reg[12]_0 ;
  wire [0:0]\ctr_state_reg[12]_1 ;
  wire [0:0]\ctr_state_reg[12]_2 ;
  wire [0:0]\ctr_state_reg[12]_3 ;
  wire [0:0]\ctr_state_reg[12]_4 ;
  wire \ctr_state_reg[12]_i_3__0_n_1 ;
  wire \ctr_state_reg[12]_i_3__0_n_2 ;
  wire \ctr_state_reg[12]_i_3__0_n_3 ;
  wire \ctr_state_reg[12]_i_4__0_n_1 ;
  wire \ctr_state_reg[12]_i_4__0_n_2 ;
  wire \ctr_state_reg[12]_i_4__0_n_3 ;
  wire \ctr_state_reg[12]_i_4__0_n_4 ;
  wire \ctr_state_reg[12]_i_4__0_n_5 ;
  wire \ctr_state_reg[12]_i_4__0_n_6 ;
  wire \ctr_state_reg[12]_i_4__0_n_7 ;
  wire \ctr_state_reg[4]_i_2__0_n_0 ;
  wire \ctr_state_reg[4]_i_2__0_n_1 ;
  wire \ctr_state_reg[4]_i_2__0_n_2 ;
  wire \ctr_state_reg[4]_i_2__0_n_3 ;
  wire \ctr_state_reg[4]_i_3__0_n_0 ;
  wire \ctr_state_reg[4]_i_3__0_n_1 ;
  wire \ctr_state_reg[4]_i_3__0_n_2 ;
  wire \ctr_state_reg[4]_i_3__0_n_3 ;
  wire \ctr_state_reg[4]_i_3__0_n_4 ;
  wire \ctr_state_reg[4]_i_3__0_n_5 ;
  wire \ctr_state_reg[4]_i_3__0_n_6 ;
  wire \ctr_state_reg[4]_i_3__0_n_7 ;
  wire [3:0]\ctr_state_reg[7]_0 ;
  wire [3:0]\ctr_state_reg[7]_1 ;
  wire [3:0]\ctr_state_reg[7]_2 ;
  wire \ctr_state_reg[8]_i_2__0_n_0 ;
  wire \ctr_state_reg[8]_i_2__0_n_1 ;
  wire \ctr_state_reg[8]_i_2__0_n_2 ;
  wire \ctr_state_reg[8]_i_2__0_n_3 ;
  wire \ctr_state_reg[8]_i_3__0_n_0 ;
  wire \ctr_state_reg[8]_i_3__0_n_1 ;
  wire \ctr_state_reg[8]_i_3__0_n_2 ;
  wire \ctr_state_reg[8]_i_3__0_n_3 ;
  wire \ctr_state_reg[8]_i_3__0_n_4 ;
  wire \ctr_state_reg[8]_i_3__0_n_5 ;
  wire \ctr_state_reg[8]_i_3__0_n_6 ;
  wire \ctr_state_reg[8]_i_3__0_n_7 ;
  wire [12:1]data0;
  wire external_reset_i;
  wire [11:2]p_1_in;
  wire [12:0]\pwm_a_p_i0_inferred__2/i__carry__0 ;
  wire [12:0]\pwm_a_p_i0_inferred__4/i__carry__0 ;
  wire [12:0]\pwm_a_p_i0_inferred__6/i__carry__0 ;
  wire [12:0]\pwm_a_p_i0_inferred__8/i__carry__0 ;
  wire [3:3]\NLW_ctr_state_reg[12]_i_3__0_CO_UNCONNECTED ;
  wire [3:3]\NLW_ctr_state_reg[12]_i_4__0_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    counter_phase90_dir_i_2
       (.I0(\ctr_state[12]_i_5__0_n_0 ),
        .I1(counter_phase90_dir_reg),
        .I2(\ctr_state[12]_i_2__0_n_0 ),
        .O(counter_phase90_tc));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h00010501)) 
    \ctr_state[0]_i_1__2 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__0_n_0 ),
        .I2(counter_phase90_output[0]),
        .I3(counter_phase90_dir_reg),
        .I4(\ctr_state[12]_i_5__0_n_0 ),
        .O(\ctr_state[0]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[10]_i_1__0 
       (.I0(external_reset_i),
        .I1(data0[10]),
        .I2(\ctr_state[12]_i_2__0_n_0 ),
        .I3(counter_phase90_dir_reg),
        .I4(\ctr_state_reg[12]_i_4__0_n_6 ),
        .I5(\ctr_state[12]_i_5__0_n_0 ),
        .O(\ctr_state[10]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hAABAFFBAAABAAABA)) 
    \ctr_state[11]_i_1 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__0_n_0 ),
        .I2(data0[11]),
        .I3(counter_phase90_dir_reg),
        .I4(\ctr_state[12]_i_5__0_n_0 ),
        .I5(\ctr_state_reg[12]_i_4__0_n_5 ),
        .O(p_1_in[11]));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_10__0 
       (.I0(counter_phase90_output[10]),
        .O(\ctr_state[12]_i_10__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_11__0 
       (.I0(counter_phase90_output[9]),
        .O(\ctr_state[12]_i_11__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \ctr_state[12]_i_12__0 
       (.I0(counter_phase90_output[5]),
        .I1(counter_phase90_output[4]),
        .I2(counter_phase90_output[11]),
        .I3(counter_phase90_output[6]),
        .O(\ctr_state[12]_i_12__0_n_0 ));
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \ctr_state[12]_i_13 
       (.I0(counter_phase90_output[3]),
        .I1(counter_phase90_output[12]),
        .I2(counter_phase90_output[8]),
        .I3(counter_phase90_output[9]),
        .I4(counter_phase90_output[7]),
        .O(\ctr_state[12]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h0054005455540054)) 
    \ctr_state[12]_i_1__1 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__0_n_0 ),
        .I2(data0[12]),
        .I3(counter_phase90_dir_reg),
        .I4(\ctr_state_reg[12]_i_4__0_n_4 ),
        .I5(\ctr_state[12]_i_5__0_n_0 ),
        .O(\ctr_state[12]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \ctr_state[12]_i_2__0 
       (.I0(\ctr_state[12]_i_6__0_n_0 ),
        .I1(counter_phase90_output[1]),
        .I2(counter_phase90_output[0]),
        .I3(counter_phase90_output[3]),
        .I4(counter_phase90_output[2]),
        .I5(\ctr_state[12]_i_7__0_n_0 ),
        .O(\ctr_state[12]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \ctr_state[12]_i_5__0 
       (.I0(\ctr_state[12]_i_12__0_n_0 ),
        .I1(counter_phase90_output[1]),
        .I2(counter_phase90_output[0]),
        .I3(counter_phase90_output[10]),
        .I4(counter_phase90_output[2]),
        .I5(\ctr_state[12]_i_13_n_0 ),
        .O(\ctr_state[12]_i_5__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \ctr_state[12]_i_6__0 
       (.I0(counter_phase90_output[5]),
        .I1(counter_phase90_output[4]),
        .I2(counter_phase90_output[7]),
        .I3(counter_phase90_output[6]),
        .O(\ctr_state[12]_i_6__0_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \ctr_state[12]_i_7__0 
       (.I0(counter_phase90_output[11]),
        .I1(counter_phase90_output[8]),
        .I2(counter_phase90_output[9]),
        .I3(counter_phase90_output[10]),
        .I4(counter_phase90_output[12]),
        .O(\ctr_state[12]_i_7__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_8__0 
       (.I0(counter_phase90_output[12]),
        .O(\ctr_state[12]_i_8__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[12]_i_9__0 
       (.I0(counter_phase90_output[11]),
        .O(\ctr_state[12]_i_9__0_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[1]_i_1__0 
       (.I0(external_reset_i),
        .I1(data0[1]),
        .I2(\ctr_state[12]_i_2__0_n_0 ),
        .I3(counter_phase90_dir_reg),
        .I4(\ctr_state_reg[4]_i_3__0_n_7 ),
        .I5(\ctr_state[12]_i_5__0_n_0 ),
        .O(\ctr_state[1]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hAABAFFBAAABAAABA)) 
    \ctr_state[2]_i_1 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__0_n_0 ),
        .I2(data0[2]),
        .I3(counter_phase90_dir_reg),
        .I4(\ctr_state[12]_i_5__0_n_0 ),
        .I5(\ctr_state_reg[4]_i_3__0_n_6 ),
        .O(p_1_in[2]));
  LUT6 #(
    .INIT(64'h0054005455540054)) 
    \ctr_state[3]_i_1__1 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__0_n_0 ),
        .I2(data0[3]),
        .I3(counter_phase90_dir_reg),
        .I4(\ctr_state_reg[4]_i_3__0_n_5 ),
        .I5(\ctr_state[12]_i_5__0_n_0 ),
        .O(\ctr_state[3]_i_1__1_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[4]_i_1__0 
       (.I0(external_reset_i),
        .I1(data0[4]),
        .I2(\ctr_state[12]_i_2__0_n_0 ),
        .I3(counter_phase90_dir_reg),
        .I4(\ctr_state_reg[4]_i_3__0_n_4 ),
        .I5(\ctr_state[12]_i_5__0_n_0 ),
        .O(\ctr_state[4]_i_1__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_4__0 
       (.I0(counter_phase90_output[4]),
        .O(\ctr_state[4]_i_4__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_5__0 
       (.I0(counter_phase90_output[3]),
        .O(\ctr_state[4]_i_5__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_6__0 
       (.I0(counter_phase90_output[2]),
        .O(\ctr_state[4]_i_6__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[4]_i_7__0 
       (.I0(counter_phase90_output[1]),
        .O(\ctr_state[4]_i_7__0_n_0 ));
  LUT6 #(
    .INIT(64'h0004000455040004)) 
    \ctr_state[5]_i_1__0 
       (.I0(external_reset_i),
        .I1(data0[5]),
        .I2(\ctr_state[12]_i_2__0_n_0 ),
        .I3(counter_phase90_dir_reg),
        .I4(\ctr_state_reg[8]_i_3__0_n_7 ),
        .I5(\ctr_state[12]_i_5__0_n_0 ),
        .O(\ctr_state[5]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hAABAFFBAAABAAABA)) 
    \ctr_state[6]_i_1 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__0_n_0 ),
        .I2(data0[6]),
        .I3(counter_phase90_dir_reg),
        .I4(\ctr_state[12]_i_5__0_n_0 ),
        .I5(\ctr_state_reg[8]_i_3__0_n_6 ),
        .O(p_1_in[6]));
  LUT6 #(
    .INIT(64'hAAFEFFFEAAFEAAFE)) 
    \ctr_state[7]_i_1 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__0_n_0 ),
        .I2(data0[7]),
        .I3(counter_phase90_dir_reg),
        .I4(\ctr_state[12]_i_5__0_n_0 ),
        .I5(\ctr_state_reg[8]_i_3__0_n_5 ),
        .O(p_1_in[7]));
  LUT6 #(
    .INIT(64'hAAFEFFFEAAFEAAFE)) 
    \ctr_state[8]_i_1 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__0_n_0 ),
        .I2(data0[8]),
        .I3(counter_phase90_dir_reg),
        .I4(\ctr_state[12]_i_5__0_n_0 ),
        .I5(\ctr_state_reg[8]_i_3__0_n_4 ),
        .O(p_1_in[8]));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_4__0 
       (.I0(counter_phase90_output[8]),
        .O(\ctr_state[8]_i_4__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_5__0 
       (.I0(counter_phase90_output[7]),
        .O(\ctr_state[8]_i_5__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_6__0 
       (.I0(counter_phase90_output[6]),
        .O(\ctr_state[8]_i_6__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ctr_state[8]_i_7__0 
       (.I0(counter_phase90_output[5]),
        .O(\ctr_state[8]_i_7__0_n_0 ));
  LUT6 #(
    .INIT(64'h0054005455540054)) 
    \ctr_state[9]_i_1__1 
       (.I0(external_reset_i),
        .I1(\ctr_state[12]_i_2__0_n_0 ),
        .I2(data0[9]),
        .I3(counter_phase90_dir_reg),
        .I4(\ctr_state_reg[12]_i_4__0_n_7 ),
        .I5(\ctr_state[12]_i_5__0_n_0 ),
        .O(\ctr_state[9]_i_1__1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[0] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[0]_i_1__2_n_0 ),
        .Q(counter_phase90_output[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[10] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[10]_i_1__0_n_0 ),
        .Q(counter_phase90_output[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[11] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[11]),
        .Q(counter_phase90_output[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[12] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[12]_i_1__1_n_0 ),
        .Q(counter_phase90_output[12]),
        .R(1'b0));
  CARRY4 \ctr_state_reg[12]_i_3__0 
       (.CI(\ctr_state_reg[8]_i_2__0_n_0 ),
        .CO({\NLW_ctr_state_reg[12]_i_3__0_CO_UNCONNECTED [3],\ctr_state_reg[12]_i_3__0_n_1 ,\ctr_state_reg[12]_i_3__0_n_2 ,\ctr_state_reg[12]_i_3__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,counter_phase90_output[11:9]}),
        .O(data0[12:9]),
        .S({\ctr_state[12]_i_8__0_n_0 ,\ctr_state[12]_i_9__0_n_0 ,\ctr_state[12]_i_10__0_n_0 ,\ctr_state[12]_i_11__0_n_0 }));
  CARRY4 \ctr_state_reg[12]_i_4__0 
       (.CI(\ctr_state_reg[8]_i_3__0_n_0 ),
        .CO({\NLW_ctr_state_reg[12]_i_4__0_CO_UNCONNECTED [3],\ctr_state_reg[12]_i_4__0_n_1 ,\ctr_state_reg[12]_i_4__0_n_2 ,\ctr_state_reg[12]_i_4__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[12]_i_4__0_n_4 ,\ctr_state_reg[12]_i_4__0_n_5 ,\ctr_state_reg[12]_i_4__0_n_6 ,\ctr_state_reg[12]_i_4__0_n_7 }),
        .S(counter_phase90_output[12:9]));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[1] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[1]_i_1__0_n_0 ),
        .Q(counter_phase90_output[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[2] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[2]),
        .Q(counter_phase90_output[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[3] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[3]_i_1__1_n_0 ),
        .Q(counter_phase90_output[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[4] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[4]_i_1__0_n_0 ),
        .Q(counter_phase90_output[4]),
        .R(1'b0));
  CARRY4 \ctr_state_reg[4]_i_2__0 
       (.CI(1'b0),
        .CO({\ctr_state_reg[4]_i_2__0_n_0 ,\ctr_state_reg[4]_i_2__0_n_1 ,\ctr_state_reg[4]_i_2__0_n_2 ,\ctr_state_reg[4]_i_2__0_n_3 }),
        .CYINIT(counter_phase90_output[0]),
        .DI(counter_phase90_output[4:1]),
        .O(data0[4:1]),
        .S({\ctr_state[4]_i_4__0_n_0 ,\ctr_state[4]_i_5__0_n_0 ,\ctr_state[4]_i_6__0_n_0 ,\ctr_state[4]_i_7__0_n_0 }));
  CARRY4 \ctr_state_reg[4]_i_3__0 
       (.CI(1'b0),
        .CO({\ctr_state_reg[4]_i_3__0_n_0 ,\ctr_state_reg[4]_i_3__0_n_1 ,\ctr_state_reg[4]_i_3__0_n_2 ,\ctr_state_reg[4]_i_3__0_n_3 }),
        .CYINIT(counter_phase90_output[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[4]_i_3__0_n_4 ,\ctr_state_reg[4]_i_3__0_n_5 ,\ctr_state_reg[4]_i_3__0_n_6 ,\ctr_state_reg[4]_i_3__0_n_7 }),
        .S(counter_phase90_output[4:1]));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[5] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[5]_i_1__0_n_0 ),
        .Q(counter_phase90_output[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[6] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[6]),
        .Q(counter_phase90_output[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[7] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[7]),
        .Q(counter_phase90_output[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[8] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(p_1_in[8]),
        .Q(counter_phase90_output[8]),
        .R(1'b0));
  CARRY4 \ctr_state_reg[8]_i_2__0 
       (.CI(\ctr_state_reg[4]_i_2__0_n_0 ),
        .CO({\ctr_state_reg[8]_i_2__0_n_0 ,\ctr_state_reg[8]_i_2__0_n_1 ,\ctr_state_reg[8]_i_2__0_n_2 ,\ctr_state_reg[8]_i_2__0_n_3 }),
        .CYINIT(1'b0),
        .DI(counter_phase90_output[8:5]),
        .O(data0[8:5]),
        .S({\ctr_state[8]_i_4__0_n_0 ,\ctr_state[8]_i_5__0_n_0 ,\ctr_state[8]_i_6__0_n_0 ,\ctr_state[8]_i_7__0_n_0 }));
  CARRY4 \ctr_state_reg[8]_i_3__0 
       (.CI(\ctr_state_reg[4]_i_3__0_n_0 ),
        .CO({\ctr_state_reg[8]_i_3__0_n_0 ,\ctr_state_reg[8]_i_3__0_n_1 ,\ctr_state_reg[8]_i_3__0_n_2 ,\ctr_state_reg[8]_i_3__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ctr_state_reg[8]_i_3__0_n_4 ,\ctr_state_reg[8]_i_3__0_n_5 ,\ctr_state_reg[8]_i_3__0_n_6 ,\ctr_state_reg[8]_i_3__0_n_7 }),
        .S(counter_phase90_output[8:5]));
  FDRE #(
    .INIT(1'b0)) 
    \ctr_state_reg[9] 
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(\ctr_state[9]_i_1__1_n_0 ),
        .Q(counter_phase90_output[9]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_2__1
       (.I0(counter_phase90_output[11]),
        .I1(\pwm_a_p_i0_inferred__2/i__carry__0 [11]),
        .I2(\pwm_a_p_i0_inferred__2/i__carry__0 [10]),
        .I3(counter_phase90_output[10]),
        .O(\ctr_state_reg[11]_0 [1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_2__3
       (.I0(counter_phase90_output[11]),
        .I1(\pwm_a_p_i0_inferred__4/i__carry__0 [11]),
        .I2(\pwm_a_p_i0_inferred__4/i__carry__0 [10]),
        .I3(counter_phase90_output[10]),
        .O(\ctr_state_reg[11]_1 [1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_2__5
       (.I0(counter_phase90_output[11]),
        .I1(\pwm_a_p_i0_inferred__6/i__carry__0 [11]),
        .I2(\pwm_a_p_i0_inferred__6/i__carry__0 [10]),
        .I3(counter_phase90_output[10]),
        .O(\ctr_state_reg[11]_2 [1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_2__7
       (.I0(counter_phase90_output[11]),
        .I1(\pwm_a_p_i0_inferred__8/i__carry__0 [11]),
        .I2(\pwm_a_p_i0_inferred__8/i__carry__0 [10]),
        .I3(counter_phase90_output[10]),
        .O(\ctr_state_reg[11]_3 [1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_3__1
       (.I0(counter_phase90_output[9]),
        .I1(\pwm_a_p_i0_inferred__2/i__carry__0 [9]),
        .I2(\pwm_a_p_i0_inferred__2/i__carry__0 [8]),
        .I3(counter_phase90_output[8]),
        .O(\ctr_state_reg[11]_0 [0]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_3__3
       (.I0(counter_phase90_output[9]),
        .I1(\pwm_a_p_i0_inferred__4/i__carry__0 [9]),
        .I2(\pwm_a_p_i0_inferred__4/i__carry__0 [8]),
        .I3(counter_phase90_output[8]),
        .O(\ctr_state_reg[11]_1 [0]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_3__5
       (.I0(counter_phase90_output[9]),
        .I1(\pwm_a_p_i0_inferred__6/i__carry__0 [9]),
        .I2(\pwm_a_p_i0_inferred__6/i__carry__0 [8]),
        .I3(counter_phase90_output[8]),
        .O(\ctr_state_reg[11]_2 [0]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_3__7
       (.I0(counter_phase90_output[9]),
        .I1(\pwm_a_p_i0_inferred__8/i__carry__0 [9]),
        .I2(\pwm_a_p_i0_inferred__8/i__carry__0 [8]),
        .I3(counter_phase90_output[8]),
        .O(\ctr_state_reg[11]_3 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4
       (.I0(counter_phase90_output[12]),
        .I1(Q[12]),
        .O(\ctr_state_reg[12]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__1
       (.I0(counter_phase90_output[12]),
        .I1(\pwm_a_p_i0_inferred__2/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_1 ));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__3
       (.I0(counter_phase90_output[12]),
        .I1(\pwm_a_p_i0_inferred__4/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_2 ));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__5
       (.I0(counter_phase90_output[12]),
        .I1(\pwm_a_p_i0_inferred__6/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_3 ));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4__7
       (.I0(counter_phase90_output[12]),
        .I1(\pwm_a_p_i0_inferred__8/i__carry__0 [12]),
        .O(\ctr_state_reg[12]_4 ));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5
       (.I0(counter_phase90_output[11]),
        .I1(Q[11]),
        .I2(counter_phase90_output[10]),
        .I3(Q[10]),
        .O(\ctr_state_reg[12]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6
       (.I0(counter_phase90_output[9]),
        .I1(Q[9]),
        .I2(counter_phase90_output[8]),
        .I3(Q[8]),
        .O(\ctr_state_reg[12]_0 [0]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_1__1
       (.I0(counter_phase90_output[7]),
        .I1(\pwm_a_p_i0_inferred__2/i__carry__0 [7]),
        .I2(\pwm_a_p_i0_inferred__2/i__carry__0 [6]),
        .I3(counter_phase90_output[6]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_1__3
       (.I0(counter_phase90_output[7]),
        .I1(\pwm_a_p_i0_inferred__4/i__carry__0 [7]),
        .I2(\pwm_a_p_i0_inferred__4/i__carry__0 [6]),
        .I3(counter_phase90_output[6]),
        .O(\ctr_state_reg[7]_0 [3]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_1__5
       (.I0(counter_phase90_output[7]),
        .I1(\pwm_a_p_i0_inferred__6/i__carry__0 [7]),
        .I2(\pwm_a_p_i0_inferred__6/i__carry__0 [6]),
        .I3(counter_phase90_output[6]),
        .O(\ctr_state_reg[7]_1 [3]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_1__7
       (.I0(counter_phase90_output[7]),
        .I1(\pwm_a_p_i0_inferred__8/i__carry__0 [7]),
        .I2(\pwm_a_p_i0_inferred__8/i__carry__0 [6]),
        .I3(counter_phase90_output[6]),
        .O(\ctr_state_reg[7]_2 [3]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_2__1
       (.I0(counter_phase90_output[5]),
        .I1(\pwm_a_p_i0_inferred__2/i__carry__0 [5]),
        .I2(\pwm_a_p_i0_inferred__2/i__carry__0 [4]),
        .I3(counter_phase90_output[4]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_2__3
       (.I0(counter_phase90_output[5]),
        .I1(\pwm_a_p_i0_inferred__4/i__carry__0 [5]),
        .I2(\pwm_a_p_i0_inferred__4/i__carry__0 [4]),
        .I3(counter_phase90_output[4]),
        .O(\ctr_state_reg[7]_0 [2]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_2__5
       (.I0(counter_phase90_output[5]),
        .I1(\pwm_a_p_i0_inferred__6/i__carry__0 [5]),
        .I2(\pwm_a_p_i0_inferred__6/i__carry__0 [4]),
        .I3(counter_phase90_output[4]),
        .O(\ctr_state_reg[7]_1 [2]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_2__7
       (.I0(counter_phase90_output[5]),
        .I1(\pwm_a_p_i0_inferred__8/i__carry__0 [5]),
        .I2(\pwm_a_p_i0_inferred__8/i__carry__0 [4]),
        .I3(counter_phase90_output[4]),
        .O(\ctr_state_reg[7]_2 [2]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_3__1
       (.I0(counter_phase90_output[3]),
        .I1(\pwm_a_p_i0_inferred__2/i__carry__0 [3]),
        .I2(\pwm_a_p_i0_inferred__2/i__carry__0 [2]),
        .I3(counter_phase90_output[2]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_3__3
       (.I0(counter_phase90_output[3]),
        .I1(\pwm_a_p_i0_inferred__4/i__carry__0 [3]),
        .I2(\pwm_a_p_i0_inferred__4/i__carry__0 [2]),
        .I3(counter_phase90_output[2]),
        .O(\ctr_state_reg[7]_0 [1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_3__5
       (.I0(counter_phase90_output[3]),
        .I1(\pwm_a_p_i0_inferred__6/i__carry__0 [3]),
        .I2(\pwm_a_p_i0_inferred__6/i__carry__0 [2]),
        .I3(counter_phase90_output[2]),
        .O(\ctr_state_reg[7]_1 [1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_3__7
       (.I0(counter_phase90_output[3]),
        .I1(\pwm_a_p_i0_inferred__8/i__carry__0 [3]),
        .I2(\pwm_a_p_i0_inferred__8/i__carry__0 [2]),
        .I3(counter_phase90_output[2]),
        .O(\ctr_state_reg[7]_2 [1]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_4__1
       (.I0(counter_phase90_output[1]),
        .I1(\pwm_a_p_i0_inferred__2/i__carry__0 [1]),
        .I2(\pwm_a_p_i0_inferred__2/i__carry__0 [0]),
        .I3(counter_phase90_output[0]),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_4__3
       (.I0(counter_phase90_output[1]),
        .I1(\pwm_a_p_i0_inferred__4/i__carry__0 [1]),
        .I2(\pwm_a_p_i0_inferred__4/i__carry__0 [0]),
        .I3(counter_phase90_output[0]),
        .O(\ctr_state_reg[7]_0 [0]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_4__5
       (.I0(counter_phase90_output[1]),
        .I1(\pwm_a_p_i0_inferred__6/i__carry__0 [1]),
        .I2(\pwm_a_p_i0_inferred__6/i__carry__0 [0]),
        .I3(counter_phase90_output[0]),
        .O(\ctr_state_reg[7]_1 [0]));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_4__7
       (.I0(counter_phase90_output[1]),
        .I1(\pwm_a_p_i0_inferred__8/i__carry__0 [1]),
        .I2(\pwm_a_p_i0_inferred__8/i__carry__0 [0]),
        .I3(counter_phase90_output[0]),
        .O(\ctr_state_reg[7]_2 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5
       (.I0(counter_phase90_output[7]),
        .I1(Q[7]),
        .I2(counter_phase90_output[6]),
        .I3(Q[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6
       (.I0(counter_phase90_output[5]),
        .I1(Q[5]),
        .I2(counter_phase90_output[4]),
        .I3(Q[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7
       (.I0(counter_phase90_output[3]),
        .I1(Q[3]),
        .I2(counter_phase90_output[2]),
        .I3(Q[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8
       (.I0(counter_phase90_output[1]),
        .I1(Q[1]),
        .I2(counter_phase90_output[0]),
        .I3(Q[0]),
        .O(S[0]));
endmodule

module system_pwm_x40_ip_0_0_pwm_x40_ip_v1_0
   (S_AXI_AWREADY,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s_axi_rdata,
    s_axi_rvalid,
    s_axi_bvalid,
    PWM_A_P,
    PWM_B_P,
    PWM_A_N,
    PWM_B_N,
    s_axi_aclk,
    s_axi_awaddr,
    s_axi_wvalid,
    s_axi_wdata,
    s_axi_araddr,
    s_axi_awvalid,
    s_axi_wstrb,
    s_axi_arvalid,
    s_axi_aresetn,
    s_axi_bready,
    s_axi_rready,
    CLK_100MHZ,
    EXTERNAL_RESET);
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]s_axi_rdata;
  output s_axi_rvalid;
  output s_axi_bvalid;
  output [9:0]PWM_A_P;
  output [9:0]PWM_B_P;
  output [9:0]PWM_A_N;
  output [9:0]PWM_B_N;
  input s_axi_aclk;
  input [3:0]s_axi_awaddr;
  input s_axi_wvalid;
  input [31:0]s_axi_wdata;
  input [3:0]s_axi_araddr;
  input s_axi_awvalid;
  input [3:0]s_axi_wstrb;
  input s_axi_arvalid;
  input s_axi_aresetn;
  input s_axi_bready;
  input s_axi_rready;
  input CLK_100MHZ;
  input EXTERNAL_RESET;

  wire CLK_100MHZ;
  wire EXTERNAL_RESET;
  wire [9:0]PWM_A_N;
  wire [9:0]PWM_A_P;
  wire [9:0]PWM_B_N;
  wire [9:0]PWM_B_P;
  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire [13:1]X_MOD_0;
  wire [13:1]X_MOD_1;
  wire [13:1]X_MOD_2;
  wire [13:1]X_MOD_3;
  wire [13:1]X_MOD_4;
  wire [13:1]X_MOD_5;
  wire [13:1]X_MOD_6;
  wire [13:1]X_MOD_7;
  wire [13:1]X_MOD_8;
  wire [13:1]X_MOD_9;
  wire counter_phase0_n_13;
  wire counter_phase0_n_14;
  wire counter_phase0_n_15;
  wire counter_phase0_n_16;
  wire counter_phase0_n_17;
  wire counter_phase0_n_18;
  wire counter_phase0_n_19;
  wire counter_phase0_n_20;
  wire counter_phase0_n_21;
  wire counter_phase0_n_22;
  wire counter_phase0_n_23;
  wire counter_phase0_n_24;
  wire counter_phase0_n_25;
  wire counter_phase0_n_26;
  wire counter_phase0_n_27;
  wire counter_phase0_n_28;
  wire counter_phase0_n_29;
  wire counter_phase0_n_30;
  wire counter_phase0_n_31;
  wire counter_phase0_n_32;
  wire counter_phase0_n_33;
  wire counter_phase0_n_34;
  wire counter_phase0_n_35;
  wire counter_phase0_n_36;
  wire counter_phase0_n_37;
  wire counter_phase0_n_38;
  wire counter_phase0_n_39;
  wire counter_phase0_n_40;
  wire counter_phase0_n_41;
  wire counter_phase0_n_42;
  wire counter_phase0_n_43;
  wire counter_phase0_n_45;
  wire counter_phase0_n_46;
  wire counter_phase0_n_47;
  wire counter_phase0_n_48;
  wire counter_phase180_dir_i_1_n_0;
  wire counter_phase180_dir_reg_n_0;
  wire counter_phase180_n_13;
  wire counter_phase180_n_14;
  wire counter_phase180_n_15;
  wire counter_phase180_n_16;
  wire counter_phase180_n_17;
  wire counter_phase180_n_18;
  wire counter_phase180_n_19;
  wire counter_phase180_n_20;
  wire counter_phase180_n_21;
  wire counter_phase180_n_22;
  wire counter_phase180_n_23;
  wire counter_phase180_n_24;
  wire counter_phase180_n_25;
  wire counter_phase180_n_26;
  wire counter_phase180_n_27;
  wire counter_phase180_n_28;
  wire counter_phase180_n_29;
  wire counter_phase180_n_30;
  wire counter_phase180_n_31;
  wire counter_phase180_n_32;
  wire counter_phase180_n_33;
  wire counter_phase180_n_34;
  wire counter_phase180_n_35;
  wire counter_phase180_n_36;
  wire counter_phase180_n_37;
  wire counter_phase180_n_38;
  wire counter_phase180_n_39;
  wire counter_phase180_n_40;
  wire counter_phase180_n_41;
  wire counter_phase180_n_42;
  wire counter_phase180_n_43;
  wire counter_phase180_n_44;
  wire counter_phase180_n_45;
  wire counter_phase180_n_46;
  wire counter_phase180_n_47;
  wire [12:0]counter_phase180_output;
  wire counter_phase180_tc;
  wire counter_phase270_dir_i_1_n_0;
  wire counter_phase270_dir_reg_n_0;
  wire counter_phase270_n_13;
  wire counter_phase270_n_14;
  wire counter_phase270_n_15;
  wire counter_phase270_n_16;
  wire counter_phase270_n_17;
  wire counter_phase270_n_18;
  wire counter_phase270_n_19;
  wire counter_phase270_n_20;
  wire counter_phase270_n_21;
  wire counter_phase270_n_22;
  wire counter_phase270_n_23;
  wire counter_phase270_n_24;
  wire counter_phase270_n_25;
  wire counter_phase270_n_26;
  wire counter_phase270_n_27;
  wire counter_phase270_n_28;
  wire counter_phase270_n_29;
  wire counter_phase270_n_30;
  wire counter_phase270_n_31;
  wire counter_phase270_n_32;
  wire counter_phase270_n_33;
  wire counter_phase270_n_34;
  wire counter_phase270_n_35;
  wire counter_phase270_n_36;
  wire counter_phase270_n_37;
  wire counter_phase270_n_38;
  wire counter_phase270_n_39;
  wire counter_phase270_n_40;
  wire counter_phase270_n_41;
  wire counter_phase270_n_42;
  wire counter_phase270_n_43;
  wire counter_phase270_n_44;
  wire counter_phase270_n_45;
  wire counter_phase270_n_46;
  wire counter_phase270_n_47;
  wire [12:0]counter_phase270_output;
  wire counter_phase270_tc;
  wire counter_phase90_dir_i_1_n_0;
  wire counter_phase90_dir_reg_n_0;
  wire counter_phase90_n_13;
  wire counter_phase90_n_14;
  wire counter_phase90_n_15;
  wire counter_phase90_n_16;
  wire counter_phase90_n_17;
  wire counter_phase90_n_18;
  wire counter_phase90_n_19;
  wire counter_phase90_n_20;
  wire counter_phase90_n_21;
  wire counter_phase90_n_22;
  wire counter_phase90_n_23;
  wire counter_phase90_n_24;
  wire counter_phase90_n_25;
  wire counter_phase90_n_26;
  wire counter_phase90_n_27;
  wire counter_phase90_n_28;
  wire counter_phase90_n_29;
  wire counter_phase90_n_30;
  wire counter_phase90_n_31;
  wire counter_phase90_n_32;
  wire counter_phase90_n_33;
  wire counter_phase90_n_34;
  wire counter_phase90_n_35;
  wire counter_phase90_n_36;
  wire counter_phase90_n_37;
  wire counter_phase90_n_38;
  wire counter_phase90_n_39;
  wire counter_phase90_n_40;
  wire counter_phase90_n_41;
  wire counter_phase90_n_42;
  wire counter_phase90_n_43;
  wire counter_phase90_n_45;
  wire counter_phase90_n_46;
  wire counter_phase90_n_47;
  wire counter_phase90_n_48;
  wire [12:0]counter_phase90_output;
  wire counter_phase90_tc;
  wire dir;
  wire external_reset_i;
  wire p_0_in;
  wire [9:0]pwm_a_p_i;
  wire pwm_a_p_i0_carry__0_n_2;
  wire pwm_a_p_i0_carry__0_n_3;
  wire pwm_a_p_i0_carry_n_0;
  wire pwm_a_p_i0_carry_n_1;
  wire pwm_a_p_i0_carry_n_2;
  wire pwm_a_p_i0_carry_n_3;
  wire \pwm_a_p_i0_inferred__0/i__carry__0_n_2 ;
  wire \pwm_a_p_i0_inferred__0/i__carry__0_n_3 ;
  wire \pwm_a_p_i0_inferred__0/i__carry_n_0 ;
  wire \pwm_a_p_i0_inferred__0/i__carry_n_1 ;
  wire \pwm_a_p_i0_inferred__0/i__carry_n_2 ;
  wire \pwm_a_p_i0_inferred__0/i__carry_n_3 ;
  wire \pwm_a_p_i0_inferred__1/i__carry__0_n_2 ;
  wire \pwm_a_p_i0_inferred__1/i__carry__0_n_3 ;
  wire \pwm_a_p_i0_inferred__1/i__carry_n_0 ;
  wire \pwm_a_p_i0_inferred__1/i__carry_n_1 ;
  wire \pwm_a_p_i0_inferred__1/i__carry_n_2 ;
  wire \pwm_a_p_i0_inferred__1/i__carry_n_3 ;
  wire \pwm_a_p_i0_inferred__2/i__carry__0_n_2 ;
  wire \pwm_a_p_i0_inferred__2/i__carry__0_n_3 ;
  wire \pwm_a_p_i0_inferred__2/i__carry_n_0 ;
  wire \pwm_a_p_i0_inferred__2/i__carry_n_1 ;
  wire \pwm_a_p_i0_inferred__2/i__carry_n_2 ;
  wire \pwm_a_p_i0_inferred__2/i__carry_n_3 ;
  wire \pwm_a_p_i0_inferred__3/i__carry__0_n_2 ;
  wire \pwm_a_p_i0_inferred__3/i__carry__0_n_3 ;
  wire \pwm_a_p_i0_inferred__3/i__carry_n_0 ;
  wire \pwm_a_p_i0_inferred__3/i__carry_n_1 ;
  wire \pwm_a_p_i0_inferred__3/i__carry_n_2 ;
  wire \pwm_a_p_i0_inferred__3/i__carry_n_3 ;
  wire \pwm_a_p_i0_inferred__4/i__carry__0_n_2 ;
  wire \pwm_a_p_i0_inferred__4/i__carry__0_n_3 ;
  wire \pwm_a_p_i0_inferred__4/i__carry_n_0 ;
  wire \pwm_a_p_i0_inferred__4/i__carry_n_1 ;
  wire \pwm_a_p_i0_inferred__4/i__carry_n_2 ;
  wire \pwm_a_p_i0_inferred__4/i__carry_n_3 ;
  wire \pwm_a_p_i0_inferred__5/i__carry__0_n_2 ;
  wire \pwm_a_p_i0_inferred__5/i__carry__0_n_3 ;
  wire \pwm_a_p_i0_inferred__5/i__carry_n_0 ;
  wire \pwm_a_p_i0_inferred__5/i__carry_n_1 ;
  wire \pwm_a_p_i0_inferred__5/i__carry_n_2 ;
  wire \pwm_a_p_i0_inferred__5/i__carry_n_3 ;
  wire \pwm_a_p_i0_inferred__6/i__carry__0_n_2 ;
  wire \pwm_a_p_i0_inferred__6/i__carry__0_n_3 ;
  wire \pwm_a_p_i0_inferred__6/i__carry_n_0 ;
  wire \pwm_a_p_i0_inferred__6/i__carry_n_1 ;
  wire \pwm_a_p_i0_inferred__6/i__carry_n_2 ;
  wire \pwm_a_p_i0_inferred__6/i__carry_n_3 ;
  wire \pwm_a_p_i0_inferred__7/i__carry__0_n_2 ;
  wire \pwm_a_p_i0_inferred__7/i__carry__0_n_3 ;
  wire \pwm_a_p_i0_inferred__7/i__carry_n_0 ;
  wire \pwm_a_p_i0_inferred__7/i__carry_n_1 ;
  wire \pwm_a_p_i0_inferred__7/i__carry_n_2 ;
  wire \pwm_a_p_i0_inferred__7/i__carry_n_3 ;
  wire \pwm_a_p_i0_inferred__8/i__carry__0_n_2 ;
  wire \pwm_a_p_i0_inferred__8/i__carry__0_n_3 ;
  wire \pwm_a_p_i0_inferred__8/i__carry_n_0 ;
  wire \pwm_a_p_i0_inferred__8/i__carry_n_1 ;
  wire \pwm_a_p_i0_inferred__8/i__carry_n_2 ;
  wire \pwm_a_p_i0_inferred__8/i__carry_n_3 ;
  wire [9:0]pwm_b_p_i;
  wire pwm_b_p_i0_carry__0_n_2;
  wire pwm_b_p_i0_carry__0_n_3;
  wire pwm_b_p_i0_carry_n_0;
  wire pwm_b_p_i0_carry_n_1;
  wire pwm_b_p_i0_carry_n_2;
  wire pwm_b_p_i0_carry_n_3;
  wire \pwm_b_p_i0_inferred__0/i__carry__0_n_2 ;
  wire \pwm_b_p_i0_inferred__0/i__carry__0_n_3 ;
  wire \pwm_b_p_i0_inferred__0/i__carry_n_0 ;
  wire \pwm_b_p_i0_inferred__0/i__carry_n_1 ;
  wire \pwm_b_p_i0_inferred__0/i__carry_n_2 ;
  wire \pwm_b_p_i0_inferred__0/i__carry_n_3 ;
  wire \pwm_b_p_i0_inferred__1/i__carry__0_n_2 ;
  wire \pwm_b_p_i0_inferred__1/i__carry__0_n_3 ;
  wire \pwm_b_p_i0_inferred__1/i__carry_n_0 ;
  wire \pwm_b_p_i0_inferred__1/i__carry_n_1 ;
  wire \pwm_b_p_i0_inferred__1/i__carry_n_2 ;
  wire \pwm_b_p_i0_inferred__1/i__carry_n_3 ;
  wire \pwm_b_p_i0_inferred__2/i__carry__0_n_2 ;
  wire \pwm_b_p_i0_inferred__2/i__carry__0_n_3 ;
  wire \pwm_b_p_i0_inferred__2/i__carry_n_0 ;
  wire \pwm_b_p_i0_inferred__2/i__carry_n_1 ;
  wire \pwm_b_p_i0_inferred__2/i__carry_n_2 ;
  wire \pwm_b_p_i0_inferred__2/i__carry_n_3 ;
  wire \pwm_b_p_i0_inferred__3/i__carry__0_n_2 ;
  wire \pwm_b_p_i0_inferred__3/i__carry__0_n_3 ;
  wire \pwm_b_p_i0_inferred__3/i__carry_n_0 ;
  wire \pwm_b_p_i0_inferred__3/i__carry_n_1 ;
  wire \pwm_b_p_i0_inferred__3/i__carry_n_2 ;
  wire \pwm_b_p_i0_inferred__3/i__carry_n_3 ;
  wire \pwm_b_p_i0_inferred__4/i__carry__0_n_2 ;
  wire \pwm_b_p_i0_inferred__4/i__carry__0_n_3 ;
  wire \pwm_b_p_i0_inferred__4/i__carry_n_0 ;
  wire \pwm_b_p_i0_inferred__4/i__carry_n_1 ;
  wire \pwm_b_p_i0_inferred__4/i__carry_n_2 ;
  wire \pwm_b_p_i0_inferred__4/i__carry_n_3 ;
  wire \pwm_b_p_i0_inferred__5/i__carry__0_n_2 ;
  wire \pwm_b_p_i0_inferred__5/i__carry__0_n_3 ;
  wire \pwm_b_p_i0_inferred__5/i__carry_n_0 ;
  wire \pwm_b_p_i0_inferred__5/i__carry_n_1 ;
  wire \pwm_b_p_i0_inferred__5/i__carry_n_2 ;
  wire \pwm_b_p_i0_inferred__5/i__carry_n_3 ;
  wire \pwm_b_p_i0_inferred__6/i__carry__0_n_2 ;
  wire \pwm_b_p_i0_inferred__6/i__carry__0_n_3 ;
  wire \pwm_b_p_i0_inferred__6/i__carry_n_0 ;
  wire \pwm_b_p_i0_inferred__6/i__carry_n_1 ;
  wire \pwm_b_p_i0_inferred__6/i__carry_n_2 ;
  wire \pwm_b_p_i0_inferred__6/i__carry_n_3 ;
  wire \pwm_b_p_i0_inferred__7/i__carry__0_n_2 ;
  wire \pwm_b_p_i0_inferred__7/i__carry__0_n_3 ;
  wire \pwm_b_p_i0_inferred__7/i__carry_n_0 ;
  wire \pwm_b_p_i0_inferred__7/i__carry_n_1 ;
  wire \pwm_b_p_i0_inferred__7/i__carry_n_2 ;
  wire \pwm_b_p_i0_inferred__7/i__carry_n_3 ;
  wire \pwm_b_p_i0_inferred__8/i__carry__0_n_2 ;
  wire \pwm_b_p_i0_inferred__8/i__carry__0_n_3 ;
  wire \pwm_b_p_i0_inferred__8/i__carry_n_0 ;
  wire \pwm_b_p_i0_inferred__8/i__carry_n_1 ;
  wire \pwm_b_p_i0_inferred__8/i__carry_n_2 ;
  wire \pwm_b_p_i0_inferred__8/i__carry_n_3 ;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_0;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_1;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_100;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_101;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_102;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_103;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_117;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_118;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_119;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_120;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_121;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_122;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_123;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_137;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_138;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_139;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_140;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_141;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_142;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_143;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_157;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_158;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_159;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_160;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_161;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_162;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_163;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_17;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_177;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_178;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_179;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_18;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_180;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_181;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_182;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_183;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_19;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_197;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_198;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_199;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_2;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_20;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_200;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_201;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_202;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_203;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_204;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_205;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_206;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_207;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_208;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_209;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_21;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_210;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_211;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_212;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_213;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_214;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_215;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_216;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_217;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_218;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_219;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_22;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_220;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_221;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_222;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_223;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_224;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_225;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_226;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_227;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_228;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_229;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_23;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_230;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_231;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_232;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_233;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_234;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_235;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_236;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_237;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_238;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_239;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_240;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_241;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_242;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_243;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_244;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_245;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_246;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_247;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_248;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_249;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_250;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_251;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_252;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_253;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_254;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_255;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_256;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_257;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_258;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_259;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_260;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_261;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_262;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_263;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_264;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_265;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_266;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_267;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_268;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_269;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_3;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_37;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_38;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_39;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_40;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_41;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_42;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_43;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_57;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_58;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_59;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_60;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_61;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_62;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_63;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_77;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_78;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_79;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_80;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_81;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_82;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_83;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_97;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_98;
  wire pwm_x40_ip_v1_0_S_AXI_inst_n_99;
  wire [12:0]q;
  wire s_axi_aclk;
  wire [3:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arvalid;
  wire [3:0]s_axi_awaddr;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire [3:0]s_axi_wstrb;
  wire s_axi_wvalid;
  wire tc;
  wire [3:0]NLW_pwm_a_p_i0_carry_O_UNCONNECTED;
  wire [3:3]NLW_pwm_a_p_i0_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_pwm_a_p_i0_carry__0_O_UNCONNECTED;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_a_p_i0_inferred__0/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__1/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_a_p_i0_inferred__1/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__1/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__2/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_a_p_i0_inferred__2/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__2/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__3/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_a_p_i0_inferred__3/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__3/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__4/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_a_p_i0_inferred__4/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__4/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__5/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_a_p_i0_inferred__5/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__5/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__6/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_a_p_i0_inferred__6/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__6/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__7/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_a_p_i0_inferred__7/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__7/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__8/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_a_p_i0_inferred__8/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_a_p_i0_inferred__8/i__carry__0_O_UNCONNECTED ;
  wire [3:0]NLW_pwm_b_p_i0_carry_O_UNCONNECTED;
  wire [3:3]NLW_pwm_b_p_i0_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_pwm_b_p_i0_carry__0_O_UNCONNECTED;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_b_p_i0_inferred__0/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__1/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_b_p_i0_inferred__1/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__1/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__2/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_b_p_i0_inferred__2/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__2/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__3/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_b_p_i0_inferred__3/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__3/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__4/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_b_p_i0_inferred__4/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__4/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__5/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_b_p_i0_inferred__5/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__5/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__6/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_b_p_i0_inferred__6/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__6/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__7/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_b_p_i0_inferred__7/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__7/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__8/i__carry_O_UNCONNECTED ;
  wire [3:3]\NLW_pwm_b_p_i0_inferred__8/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_pwm_b_p_i0_inferred__8/i__carry__0_O_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_A_N[0]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_a_p_i[0]),
        .O(PWM_A_N[0]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_A_N[1]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_a_p_i[1]),
        .O(PWM_A_N[1]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_A_N[2]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_a_p_i[2]),
        .O(PWM_A_N[2]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_A_N[3]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_a_p_i[3]),
        .O(PWM_A_N[3]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_A_N[4]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_a_p_i[4]),
        .O(PWM_A_N[4]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_A_N[5]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_a_p_i[5]),
        .O(PWM_A_N[5]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_A_N[6]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_a_p_i[6]),
        .O(PWM_A_N[6]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_A_N[7]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_a_p_i[7]),
        .O(PWM_A_N[7]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_A_N[8]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_a_p_i[8]),
        .O(PWM_A_N[8]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_A_N[9]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_a_p_i[9]),
        .O(PWM_A_N[9]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_A_P[0]_INST_0 
       (.I0(pwm_a_p_i[0]),
        .I1(EXTERNAL_RESET),
        .O(PWM_A_P[0]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_A_P[1]_INST_0 
       (.I0(pwm_a_p_i[1]),
        .I1(EXTERNAL_RESET),
        .O(PWM_A_P[1]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_A_P[2]_INST_0 
       (.I0(pwm_a_p_i[2]),
        .I1(EXTERNAL_RESET),
        .O(PWM_A_P[2]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_A_P[3]_INST_0 
       (.I0(pwm_a_p_i[3]),
        .I1(EXTERNAL_RESET),
        .O(PWM_A_P[3]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_A_P[4]_INST_0 
       (.I0(pwm_a_p_i[4]),
        .I1(EXTERNAL_RESET),
        .O(PWM_A_P[4]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_A_P[5]_INST_0 
       (.I0(pwm_a_p_i[5]),
        .I1(EXTERNAL_RESET),
        .O(PWM_A_P[5]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_A_P[6]_INST_0 
       (.I0(pwm_a_p_i[6]),
        .I1(EXTERNAL_RESET),
        .O(PWM_A_P[6]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_A_P[7]_INST_0 
       (.I0(pwm_a_p_i[7]),
        .I1(EXTERNAL_RESET),
        .O(PWM_A_P[7]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_A_P[8]_INST_0 
       (.I0(pwm_a_p_i[8]),
        .I1(EXTERNAL_RESET),
        .O(PWM_A_P[8]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_A_P[9]_INST_0 
       (.I0(pwm_a_p_i[9]),
        .I1(EXTERNAL_RESET),
        .O(PWM_A_P[9]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_B_N[0]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_b_p_i[0]),
        .O(PWM_B_N[0]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_B_N[1]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_b_p_i[1]),
        .O(PWM_B_N[1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_B_N[2]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_b_p_i[2]),
        .O(PWM_B_N[2]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_B_N[3]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_b_p_i[3]),
        .O(PWM_B_N[3]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_B_N[4]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_b_p_i[4]),
        .O(PWM_B_N[4]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_B_N[5]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_b_p_i[5]),
        .O(PWM_B_N[5]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_B_N[6]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_b_p_i[6]),
        .O(PWM_B_N[6]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_B_N[7]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_b_p_i[7]),
        .O(PWM_B_N[7]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_B_N[8]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_b_p_i[8]),
        .O(PWM_B_N[8]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \PWM_B_N[9]_INST_0 
       (.I0(EXTERNAL_RESET),
        .I1(pwm_b_p_i[9]),
        .O(PWM_B_N[9]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_B_P[0]_INST_0 
       (.I0(pwm_b_p_i[0]),
        .I1(EXTERNAL_RESET),
        .O(PWM_B_P[0]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_B_P[1]_INST_0 
       (.I0(pwm_b_p_i[1]),
        .I1(EXTERNAL_RESET),
        .O(PWM_B_P[1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_B_P[2]_INST_0 
       (.I0(pwm_b_p_i[2]),
        .I1(EXTERNAL_RESET),
        .O(PWM_B_P[2]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_B_P[3]_INST_0 
       (.I0(pwm_b_p_i[3]),
        .I1(EXTERNAL_RESET),
        .O(PWM_B_P[3]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_B_P[4]_INST_0 
       (.I0(pwm_b_p_i[4]),
        .I1(EXTERNAL_RESET),
        .O(PWM_B_P[4]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_B_P[5]_INST_0 
       (.I0(pwm_b_p_i[5]),
        .I1(EXTERNAL_RESET),
        .O(PWM_B_P[5]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_B_P[6]_INST_0 
       (.I0(pwm_b_p_i[6]),
        .I1(EXTERNAL_RESET),
        .O(PWM_B_P[6]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_B_P[7]_INST_0 
       (.I0(pwm_b_p_i[7]),
        .I1(EXTERNAL_RESET),
        .O(PWM_B_P[7]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_B_P[8]_INST_0 
       (.I0(pwm_b_p_i[8]),
        .I1(EXTERNAL_RESET),
        .O(PWM_B_P[8]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \PWM_B_P[9]_INST_0 
       (.I0(pwm_b_p_i[9]),
        .I1(EXTERNAL_RESET),
        .O(PWM_B_P[9]));
  system_pwm_x40_ip_0_0_N_bits_counter counter_phase0
       (.CLK_100MHZ(CLK_100MHZ),
        .DI({counter_phase0_n_20,counter_phase0_n_21,counter_phase0_n_22,counter_phase0_n_23}),
        .Q(X_MOD_0),
        .S({counter_phase0_n_13,counter_phase0_n_14,counter_phase0_n_15,counter_phase0_n_16}),
        .\ctr_state_reg[11]_0 ({counter_phase0_n_24,counter_phase0_n_25}),
        .\ctr_state_reg[11]_1 ({counter_phase0_n_30,counter_phase0_n_31}),
        .\ctr_state_reg[11]_2 ({counter_phase0_n_36,counter_phase0_n_37}),
        .\ctr_state_reg[11]_3 ({counter_phase0_n_42,counter_phase0_n_43}),
        .\ctr_state_reg[12]_0 ({counter_phase0_n_17,counter_phase0_n_18,counter_phase0_n_19}),
        .\ctr_state_reg[12]_1 (counter_phase0_n_45),
        .\ctr_state_reg[12]_2 (counter_phase0_n_46),
        .\ctr_state_reg[12]_3 (counter_phase0_n_47),
        .\ctr_state_reg[12]_4 (counter_phase0_n_48),
        .\ctr_state_reg[7]_0 ({counter_phase0_n_26,counter_phase0_n_27,counter_phase0_n_28,counter_phase0_n_29}),
        .\ctr_state_reg[7]_1 ({counter_phase0_n_32,counter_phase0_n_33,counter_phase0_n_34,counter_phase0_n_35}),
        .\ctr_state_reg[7]_2 ({counter_phase0_n_38,counter_phase0_n_39,counter_phase0_n_40,counter_phase0_n_41}),
        .dir(dir),
        .external_reset_i(external_reset_i),
        .\pwm_a_p_i0_inferred__1/i__carry__0 (X_MOD_2),
        .\pwm_a_p_i0_inferred__3/i__carry__0 (X_MOD_4),
        .\pwm_a_p_i0_inferred__5/i__carry__0 (X_MOD_6),
        .\pwm_a_p_i0_inferred__7/i__carry__0 (X_MOD_8),
        .q(q),
        .tc(tc));
  LUT1 #(
    .INIT(2'h1)) 
    counter_phase0_dir_i_1
       (.I0(dir),
        .O(p_0_in));
  FDPE #(
    .INIT(1'b1)) 
    counter_phase0_dir_reg
       (.C(tc),
        .CE(1'b1),
        .D(p_0_in),
        .PRE(external_reset_i),
        .Q(dir));
  system_pwm_x40_ip_0_0_N_bits_counter_0 counter_phase180
       (.CLK_100MHZ(CLK_100MHZ),
        .Q(X_MOD_0),
        .S({counter_phase180_n_13,counter_phase180_n_14,counter_phase180_n_15,counter_phase180_n_16}),
        .counter_phase180_dir_reg(counter_phase180_dir_reg_n_0),
        .counter_phase180_output(counter_phase180_output),
        .counter_phase180_tc(counter_phase180_tc),
        .\ctr_state_reg[12]_0 ({counter_phase180_n_17,counter_phase180_n_18,counter_phase180_n_19}),
        .\ctr_state_reg[12]_1 ({counter_phase180_n_24,counter_phase180_n_25,counter_phase180_n_26}),
        .\ctr_state_reg[12]_2 ({counter_phase180_n_31,counter_phase180_n_32,counter_phase180_n_33}),
        .\ctr_state_reg[12]_3 ({counter_phase180_n_38,counter_phase180_n_39,counter_phase180_n_40}),
        .\ctr_state_reg[12]_4 ({counter_phase180_n_45,counter_phase180_n_46,counter_phase180_n_47}),
        .\ctr_state_reg[7]_0 ({counter_phase180_n_20,counter_phase180_n_21,counter_phase180_n_22,counter_phase180_n_23}),
        .\ctr_state_reg[7]_1 ({counter_phase180_n_27,counter_phase180_n_28,counter_phase180_n_29,counter_phase180_n_30}),
        .\ctr_state_reg[7]_2 ({counter_phase180_n_34,counter_phase180_n_35,counter_phase180_n_36,counter_phase180_n_37}),
        .\ctr_state_reg[7]_3 ({counter_phase180_n_41,counter_phase180_n_42,counter_phase180_n_43,counter_phase180_n_44}),
        .external_reset_i(external_reset_i),
        .\pwm_b_p_i0_inferred__1/i__carry__0 (X_MOD_2),
        .\pwm_b_p_i0_inferred__3/i__carry__0 (X_MOD_4),
        .\pwm_b_p_i0_inferred__5/i__carry__0 (X_MOD_6),
        .\pwm_b_p_i0_inferred__7/i__carry__0 (X_MOD_8));
  LUT1 #(
    .INIT(2'h1)) 
    counter_phase180_dir_i_1
       (.I0(counter_phase180_dir_reg_n_0),
        .O(counter_phase180_dir_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    counter_phase180_dir_reg
       (.C(counter_phase180_tc),
        .CE(1'b1),
        .CLR(external_reset_i),
        .D(counter_phase180_dir_i_1_n_0),
        .Q(counter_phase180_dir_reg_n_0));
  system_pwm_x40_ip_0_0_N_bits_counter_1 counter_phase270
       (.CLK_100MHZ(CLK_100MHZ),
        .Q(X_MOD_1),
        .S({counter_phase270_n_13,counter_phase270_n_14,counter_phase270_n_15,counter_phase270_n_16}),
        .counter_phase270_output(counter_phase270_output),
        .counter_phase270_tc(counter_phase270_tc),
        .\ctr_state_reg[0]_0 (counter_phase270_dir_reg_n_0),
        .\ctr_state_reg[12]_0 ({counter_phase270_n_17,counter_phase270_n_18,counter_phase270_n_19}),
        .\ctr_state_reg[12]_1 ({counter_phase270_n_24,counter_phase270_n_25,counter_phase270_n_26}),
        .\ctr_state_reg[12]_2 ({counter_phase270_n_31,counter_phase270_n_32,counter_phase270_n_33}),
        .\ctr_state_reg[12]_3 ({counter_phase270_n_38,counter_phase270_n_39,counter_phase270_n_40}),
        .\ctr_state_reg[12]_4 ({counter_phase270_n_45,counter_phase270_n_46,counter_phase270_n_47}),
        .\ctr_state_reg[7]_0 ({counter_phase270_n_20,counter_phase270_n_21,counter_phase270_n_22,counter_phase270_n_23}),
        .\ctr_state_reg[7]_1 ({counter_phase270_n_27,counter_phase270_n_28,counter_phase270_n_29,counter_phase270_n_30}),
        .\ctr_state_reg[7]_2 ({counter_phase270_n_34,counter_phase270_n_35,counter_phase270_n_36,counter_phase270_n_37}),
        .\ctr_state_reg[7]_3 ({counter_phase270_n_41,counter_phase270_n_42,counter_phase270_n_43,counter_phase270_n_44}),
        .external_reset_i(external_reset_i),
        .\pwm_b_p_i0_inferred__2/i__carry__0 (X_MOD_3),
        .\pwm_b_p_i0_inferred__4/i__carry__0 (X_MOD_5),
        .\pwm_b_p_i0_inferred__6/i__carry__0 (X_MOD_7),
        .\pwm_b_p_i0_inferred__8/i__carry__0 (X_MOD_9));
  LUT1 #(
    .INIT(2'h1)) 
    counter_phase270_dir_i_1
       (.I0(counter_phase270_dir_reg_n_0),
        .O(counter_phase270_dir_i_1_n_0));
  FDPE #(
    .INIT(1'b1)) 
    counter_phase270_dir_reg
       (.C(counter_phase270_tc),
        .CE(1'b1),
        .D(counter_phase270_dir_i_1_n_0),
        .PRE(external_reset_i),
        .Q(counter_phase270_dir_reg_n_0));
  system_pwm_x40_ip_0_0_N_bits_counter_2 counter_phase90
       (.CLK_100MHZ(CLK_100MHZ),
        .DI({counter_phase90_n_20,counter_phase90_n_21,counter_phase90_n_22,counter_phase90_n_23}),
        .Q(X_MOD_1),
        .S({counter_phase90_n_13,counter_phase90_n_14,counter_phase90_n_15,counter_phase90_n_16}),
        .counter_phase90_dir_reg(counter_phase90_dir_reg_n_0),
        .counter_phase90_output(counter_phase90_output),
        .counter_phase90_tc(counter_phase90_tc),
        .\ctr_state_reg[11]_0 ({counter_phase90_n_24,counter_phase90_n_25}),
        .\ctr_state_reg[11]_1 ({counter_phase90_n_30,counter_phase90_n_31}),
        .\ctr_state_reg[11]_2 ({counter_phase90_n_36,counter_phase90_n_37}),
        .\ctr_state_reg[11]_3 ({counter_phase90_n_42,counter_phase90_n_43}),
        .\ctr_state_reg[12]_0 ({counter_phase90_n_17,counter_phase90_n_18,counter_phase90_n_19}),
        .\ctr_state_reg[12]_1 (counter_phase90_n_45),
        .\ctr_state_reg[12]_2 (counter_phase90_n_46),
        .\ctr_state_reg[12]_3 (counter_phase90_n_47),
        .\ctr_state_reg[12]_4 (counter_phase90_n_48),
        .\ctr_state_reg[7]_0 ({counter_phase90_n_26,counter_phase90_n_27,counter_phase90_n_28,counter_phase90_n_29}),
        .\ctr_state_reg[7]_1 ({counter_phase90_n_32,counter_phase90_n_33,counter_phase90_n_34,counter_phase90_n_35}),
        .\ctr_state_reg[7]_2 ({counter_phase90_n_38,counter_phase90_n_39,counter_phase90_n_40,counter_phase90_n_41}),
        .external_reset_i(external_reset_i),
        .\pwm_a_p_i0_inferred__2/i__carry__0 (X_MOD_3),
        .\pwm_a_p_i0_inferred__4/i__carry__0 (X_MOD_5),
        .\pwm_a_p_i0_inferred__6/i__carry__0 (X_MOD_7),
        .\pwm_a_p_i0_inferred__8/i__carry__0 (X_MOD_9));
  LUT1 #(
    .INIT(2'h1)) 
    counter_phase90_dir_i_1
       (.I0(counter_phase90_dir_reg_n_0),
        .O(counter_phase90_dir_i_1_n_0));
  FDCE #(
    .INIT(1'b0)) 
    counter_phase90_dir_reg
       (.C(counter_phase90_tc),
        .CE(1'b1),
        .CLR(external_reset_i),
        .D(counter_phase90_dir_i_1_n_0),
        .Q(counter_phase90_dir_reg_n_0));
  FDRE #(
    .INIT(1'b1)) 
    external_reset_i_reg
       (.C(CLK_100MHZ),
        .CE(1'b1),
        .D(EXTERNAL_RESET),
        .Q(external_reset_i),
        .R(1'b0));
  CARRY4 pwm_a_p_i0_carry
       (.CI(1'b0),
        .CO({pwm_a_p_i0_carry_n_0,pwm_a_p_i0_carry_n_1,pwm_a_p_i0_carry_n_2,pwm_a_p_i0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({pwm_x40_ip_v1_0_S_AXI_inst_n_0,pwm_x40_ip_v1_0_S_AXI_inst_n_1,pwm_x40_ip_v1_0_S_AXI_inst_n_2,pwm_x40_ip_v1_0_S_AXI_inst_n_3}),
        .O(NLW_pwm_a_p_i0_carry_O_UNCONNECTED[3:0]),
        .S({counter_phase0_n_13,counter_phase0_n_14,counter_phase0_n_15,counter_phase0_n_16}));
  CARRY4 pwm_a_p_i0_carry__0
       (.CI(pwm_a_p_i0_carry_n_0),
        .CO({NLW_pwm_a_p_i0_carry__0_CO_UNCONNECTED[3],pwm_a_p_i[0],pwm_a_p_i0_carry__0_n_2,pwm_a_p_i0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_17,pwm_x40_ip_v1_0_S_AXI_inst_n_18,pwm_x40_ip_v1_0_S_AXI_inst_n_19}),
        .O(NLW_pwm_a_p_i0_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,counter_phase0_n_17,counter_phase0_n_18,counter_phase0_n_19}));
  CARRY4 \pwm_a_p_i0_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\pwm_a_p_i0_inferred__0/i__carry_n_0 ,\pwm_a_p_i0_inferred__0/i__carry_n_1 ,\pwm_a_p_i0_inferred__0/i__carry_n_2 ,\pwm_a_p_i0_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({pwm_x40_ip_v1_0_S_AXI_inst_n_20,pwm_x40_ip_v1_0_S_AXI_inst_n_21,pwm_x40_ip_v1_0_S_AXI_inst_n_22,pwm_x40_ip_v1_0_S_AXI_inst_n_23}),
        .O(\NLW_pwm_a_p_i0_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({counter_phase90_n_13,counter_phase90_n_14,counter_phase90_n_15,counter_phase90_n_16}));
  CARRY4 \pwm_a_p_i0_inferred__0/i__carry__0 
       (.CI(\pwm_a_p_i0_inferred__0/i__carry_n_0 ),
        .CO({\NLW_pwm_a_p_i0_inferred__0/i__carry__0_CO_UNCONNECTED [3],pwm_a_p_i[1],\pwm_a_p_i0_inferred__0/i__carry__0_n_2 ,\pwm_a_p_i0_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_37,pwm_x40_ip_v1_0_S_AXI_inst_n_38,pwm_x40_ip_v1_0_S_AXI_inst_n_39}),
        .O(\NLW_pwm_a_p_i0_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase90_n_17,counter_phase90_n_18,counter_phase90_n_19}));
  CARRY4 \pwm_a_p_i0_inferred__1/i__carry 
       (.CI(1'b0),
        .CO({\pwm_a_p_i0_inferred__1/i__carry_n_0 ,\pwm_a_p_i0_inferred__1/i__carry_n_1 ,\pwm_a_p_i0_inferred__1/i__carry_n_2 ,\pwm_a_p_i0_inferred__1/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({counter_phase0_n_20,counter_phase0_n_21,counter_phase0_n_22,counter_phase0_n_23}),
        .O(\NLW_pwm_a_p_i0_inferred__1/i__carry_O_UNCONNECTED [3:0]),
        .S({pwm_x40_ip_v1_0_S_AXI_inst_n_40,pwm_x40_ip_v1_0_S_AXI_inst_n_41,pwm_x40_ip_v1_0_S_AXI_inst_n_42,pwm_x40_ip_v1_0_S_AXI_inst_n_43}));
  CARRY4 \pwm_a_p_i0_inferred__1/i__carry__0 
       (.CI(\pwm_a_p_i0_inferred__1/i__carry_n_0 ),
        .CO({\NLW_pwm_a_p_i0_inferred__1/i__carry__0_CO_UNCONNECTED [3],pwm_a_p_i[2],\pwm_a_p_i0_inferred__1/i__carry__0_n_2 ,\pwm_a_p_i0_inferred__1/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_59,counter_phase0_n_24,counter_phase0_n_25}),
        .O(\NLW_pwm_a_p_i0_inferred__1/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase0_n_45,pwm_x40_ip_v1_0_S_AXI_inst_n_57,pwm_x40_ip_v1_0_S_AXI_inst_n_58}));
  CARRY4 \pwm_a_p_i0_inferred__2/i__carry 
       (.CI(1'b0),
        .CO({\pwm_a_p_i0_inferred__2/i__carry_n_0 ,\pwm_a_p_i0_inferred__2/i__carry_n_1 ,\pwm_a_p_i0_inferred__2/i__carry_n_2 ,\pwm_a_p_i0_inferred__2/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({counter_phase90_n_20,counter_phase90_n_21,counter_phase90_n_22,counter_phase90_n_23}),
        .O(\NLW_pwm_a_p_i0_inferred__2/i__carry_O_UNCONNECTED [3:0]),
        .S({pwm_x40_ip_v1_0_S_AXI_inst_n_60,pwm_x40_ip_v1_0_S_AXI_inst_n_61,pwm_x40_ip_v1_0_S_AXI_inst_n_62,pwm_x40_ip_v1_0_S_AXI_inst_n_63}));
  CARRY4 \pwm_a_p_i0_inferred__2/i__carry__0 
       (.CI(\pwm_a_p_i0_inferred__2/i__carry_n_0 ),
        .CO({\NLW_pwm_a_p_i0_inferred__2/i__carry__0_CO_UNCONNECTED [3],pwm_a_p_i[3],\pwm_a_p_i0_inferred__2/i__carry__0_n_2 ,\pwm_a_p_i0_inferred__2/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_79,counter_phase90_n_24,counter_phase90_n_25}),
        .O(\NLW_pwm_a_p_i0_inferred__2/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase90_n_45,pwm_x40_ip_v1_0_S_AXI_inst_n_77,pwm_x40_ip_v1_0_S_AXI_inst_n_78}));
  CARRY4 \pwm_a_p_i0_inferred__3/i__carry 
       (.CI(1'b0),
        .CO({\pwm_a_p_i0_inferred__3/i__carry_n_0 ,\pwm_a_p_i0_inferred__3/i__carry_n_1 ,\pwm_a_p_i0_inferred__3/i__carry_n_2 ,\pwm_a_p_i0_inferred__3/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({counter_phase0_n_26,counter_phase0_n_27,counter_phase0_n_28,counter_phase0_n_29}),
        .O(\NLW_pwm_a_p_i0_inferred__3/i__carry_O_UNCONNECTED [3:0]),
        .S({pwm_x40_ip_v1_0_S_AXI_inst_n_80,pwm_x40_ip_v1_0_S_AXI_inst_n_81,pwm_x40_ip_v1_0_S_AXI_inst_n_82,pwm_x40_ip_v1_0_S_AXI_inst_n_83}));
  CARRY4 \pwm_a_p_i0_inferred__3/i__carry__0 
       (.CI(\pwm_a_p_i0_inferred__3/i__carry_n_0 ),
        .CO({\NLW_pwm_a_p_i0_inferred__3/i__carry__0_CO_UNCONNECTED [3],pwm_a_p_i[4],\pwm_a_p_i0_inferred__3/i__carry__0_n_2 ,\pwm_a_p_i0_inferred__3/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_99,counter_phase0_n_30,counter_phase0_n_31}),
        .O(\NLW_pwm_a_p_i0_inferred__3/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase0_n_46,pwm_x40_ip_v1_0_S_AXI_inst_n_97,pwm_x40_ip_v1_0_S_AXI_inst_n_98}));
  CARRY4 \pwm_a_p_i0_inferred__4/i__carry 
       (.CI(1'b0),
        .CO({\pwm_a_p_i0_inferred__4/i__carry_n_0 ,\pwm_a_p_i0_inferred__4/i__carry_n_1 ,\pwm_a_p_i0_inferred__4/i__carry_n_2 ,\pwm_a_p_i0_inferred__4/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({counter_phase90_n_26,counter_phase90_n_27,counter_phase90_n_28,counter_phase90_n_29}),
        .O(\NLW_pwm_a_p_i0_inferred__4/i__carry_O_UNCONNECTED [3:0]),
        .S({pwm_x40_ip_v1_0_S_AXI_inst_n_100,pwm_x40_ip_v1_0_S_AXI_inst_n_101,pwm_x40_ip_v1_0_S_AXI_inst_n_102,pwm_x40_ip_v1_0_S_AXI_inst_n_103}));
  CARRY4 \pwm_a_p_i0_inferred__4/i__carry__0 
       (.CI(\pwm_a_p_i0_inferred__4/i__carry_n_0 ),
        .CO({\NLW_pwm_a_p_i0_inferred__4/i__carry__0_CO_UNCONNECTED [3],pwm_a_p_i[5],\pwm_a_p_i0_inferred__4/i__carry__0_n_2 ,\pwm_a_p_i0_inferred__4/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_119,counter_phase90_n_30,counter_phase90_n_31}),
        .O(\NLW_pwm_a_p_i0_inferred__4/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase90_n_46,pwm_x40_ip_v1_0_S_AXI_inst_n_117,pwm_x40_ip_v1_0_S_AXI_inst_n_118}));
  CARRY4 \pwm_a_p_i0_inferred__5/i__carry 
       (.CI(1'b0),
        .CO({\pwm_a_p_i0_inferred__5/i__carry_n_0 ,\pwm_a_p_i0_inferred__5/i__carry_n_1 ,\pwm_a_p_i0_inferred__5/i__carry_n_2 ,\pwm_a_p_i0_inferred__5/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({counter_phase0_n_32,counter_phase0_n_33,counter_phase0_n_34,counter_phase0_n_35}),
        .O(\NLW_pwm_a_p_i0_inferred__5/i__carry_O_UNCONNECTED [3:0]),
        .S({pwm_x40_ip_v1_0_S_AXI_inst_n_120,pwm_x40_ip_v1_0_S_AXI_inst_n_121,pwm_x40_ip_v1_0_S_AXI_inst_n_122,pwm_x40_ip_v1_0_S_AXI_inst_n_123}));
  CARRY4 \pwm_a_p_i0_inferred__5/i__carry__0 
       (.CI(\pwm_a_p_i0_inferred__5/i__carry_n_0 ),
        .CO({\NLW_pwm_a_p_i0_inferred__5/i__carry__0_CO_UNCONNECTED [3],pwm_a_p_i[6],\pwm_a_p_i0_inferred__5/i__carry__0_n_2 ,\pwm_a_p_i0_inferred__5/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_139,counter_phase0_n_36,counter_phase0_n_37}),
        .O(\NLW_pwm_a_p_i0_inferred__5/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase0_n_47,pwm_x40_ip_v1_0_S_AXI_inst_n_137,pwm_x40_ip_v1_0_S_AXI_inst_n_138}));
  CARRY4 \pwm_a_p_i0_inferred__6/i__carry 
       (.CI(1'b0),
        .CO({\pwm_a_p_i0_inferred__6/i__carry_n_0 ,\pwm_a_p_i0_inferred__6/i__carry_n_1 ,\pwm_a_p_i0_inferred__6/i__carry_n_2 ,\pwm_a_p_i0_inferred__6/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({counter_phase90_n_32,counter_phase90_n_33,counter_phase90_n_34,counter_phase90_n_35}),
        .O(\NLW_pwm_a_p_i0_inferred__6/i__carry_O_UNCONNECTED [3:0]),
        .S({pwm_x40_ip_v1_0_S_AXI_inst_n_140,pwm_x40_ip_v1_0_S_AXI_inst_n_141,pwm_x40_ip_v1_0_S_AXI_inst_n_142,pwm_x40_ip_v1_0_S_AXI_inst_n_143}));
  CARRY4 \pwm_a_p_i0_inferred__6/i__carry__0 
       (.CI(\pwm_a_p_i0_inferred__6/i__carry_n_0 ),
        .CO({\NLW_pwm_a_p_i0_inferred__6/i__carry__0_CO_UNCONNECTED [3],pwm_a_p_i[7],\pwm_a_p_i0_inferred__6/i__carry__0_n_2 ,\pwm_a_p_i0_inferred__6/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_159,counter_phase90_n_36,counter_phase90_n_37}),
        .O(\NLW_pwm_a_p_i0_inferred__6/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase90_n_47,pwm_x40_ip_v1_0_S_AXI_inst_n_157,pwm_x40_ip_v1_0_S_AXI_inst_n_158}));
  CARRY4 \pwm_a_p_i0_inferred__7/i__carry 
       (.CI(1'b0),
        .CO({\pwm_a_p_i0_inferred__7/i__carry_n_0 ,\pwm_a_p_i0_inferred__7/i__carry_n_1 ,\pwm_a_p_i0_inferred__7/i__carry_n_2 ,\pwm_a_p_i0_inferred__7/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({counter_phase0_n_38,counter_phase0_n_39,counter_phase0_n_40,counter_phase0_n_41}),
        .O(\NLW_pwm_a_p_i0_inferred__7/i__carry_O_UNCONNECTED [3:0]),
        .S({pwm_x40_ip_v1_0_S_AXI_inst_n_160,pwm_x40_ip_v1_0_S_AXI_inst_n_161,pwm_x40_ip_v1_0_S_AXI_inst_n_162,pwm_x40_ip_v1_0_S_AXI_inst_n_163}));
  CARRY4 \pwm_a_p_i0_inferred__7/i__carry__0 
       (.CI(\pwm_a_p_i0_inferred__7/i__carry_n_0 ),
        .CO({\NLW_pwm_a_p_i0_inferred__7/i__carry__0_CO_UNCONNECTED [3],pwm_a_p_i[8],\pwm_a_p_i0_inferred__7/i__carry__0_n_2 ,\pwm_a_p_i0_inferred__7/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_179,counter_phase0_n_42,counter_phase0_n_43}),
        .O(\NLW_pwm_a_p_i0_inferred__7/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase0_n_48,pwm_x40_ip_v1_0_S_AXI_inst_n_177,pwm_x40_ip_v1_0_S_AXI_inst_n_178}));
  CARRY4 \pwm_a_p_i0_inferred__8/i__carry 
       (.CI(1'b0),
        .CO({\pwm_a_p_i0_inferred__8/i__carry_n_0 ,\pwm_a_p_i0_inferred__8/i__carry_n_1 ,\pwm_a_p_i0_inferred__8/i__carry_n_2 ,\pwm_a_p_i0_inferred__8/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({counter_phase90_n_38,counter_phase90_n_39,counter_phase90_n_40,counter_phase90_n_41}),
        .O(\NLW_pwm_a_p_i0_inferred__8/i__carry_O_UNCONNECTED [3:0]),
        .S({pwm_x40_ip_v1_0_S_AXI_inst_n_180,pwm_x40_ip_v1_0_S_AXI_inst_n_181,pwm_x40_ip_v1_0_S_AXI_inst_n_182,pwm_x40_ip_v1_0_S_AXI_inst_n_183}));
  CARRY4 \pwm_a_p_i0_inferred__8/i__carry__0 
       (.CI(\pwm_a_p_i0_inferred__8/i__carry_n_0 ),
        .CO({\NLW_pwm_a_p_i0_inferred__8/i__carry__0_CO_UNCONNECTED [3],pwm_a_p_i[9],\pwm_a_p_i0_inferred__8/i__carry__0_n_2 ,\pwm_a_p_i0_inferred__8/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_199,counter_phase90_n_42,counter_phase90_n_43}),
        .O(\NLW_pwm_a_p_i0_inferred__8/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase90_n_48,pwm_x40_ip_v1_0_S_AXI_inst_n_197,pwm_x40_ip_v1_0_S_AXI_inst_n_198}));
  CARRY4 pwm_b_p_i0_carry
       (.CI(1'b0),
        .CO({pwm_b_p_i0_carry_n_0,pwm_b_p_i0_carry_n_1,pwm_b_p_i0_carry_n_2,pwm_b_p_i0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({pwm_x40_ip_v1_0_S_AXI_inst_n_200,pwm_x40_ip_v1_0_S_AXI_inst_n_201,pwm_x40_ip_v1_0_S_AXI_inst_n_202,pwm_x40_ip_v1_0_S_AXI_inst_n_203}),
        .O(NLW_pwm_b_p_i0_carry_O_UNCONNECTED[3:0]),
        .S({counter_phase180_n_13,counter_phase180_n_14,counter_phase180_n_15,counter_phase180_n_16}));
  CARRY4 pwm_b_p_i0_carry__0
       (.CI(pwm_b_p_i0_carry_n_0),
        .CO({NLW_pwm_b_p_i0_carry__0_CO_UNCONNECTED[3],pwm_b_p_i[0],pwm_b_p_i0_carry__0_n_2,pwm_b_p_i0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_204,pwm_x40_ip_v1_0_S_AXI_inst_n_205,pwm_x40_ip_v1_0_S_AXI_inst_n_206}),
        .O(NLW_pwm_b_p_i0_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,counter_phase180_n_17,counter_phase180_n_18,counter_phase180_n_19}));
  CARRY4 \pwm_b_p_i0_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\pwm_b_p_i0_inferred__0/i__carry_n_0 ,\pwm_b_p_i0_inferred__0/i__carry_n_1 ,\pwm_b_p_i0_inferred__0/i__carry_n_2 ,\pwm_b_p_i0_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({pwm_x40_ip_v1_0_S_AXI_inst_n_207,pwm_x40_ip_v1_0_S_AXI_inst_n_208,pwm_x40_ip_v1_0_S_AXI_inst_n_209,pwm_x40_ip_v1_0_S_AXI_inst_n_210}),
        .O(\NLW_pwm_b_p_i0_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({counter_phase270_n_13,counter_phase270_n_14,counter_phase270_n_15,counter_phase270_n_16}));
  CARRY4 \pwm_b_p_i0_inferred__0/i__carry__0 
       (.CI(\pwm_b_p_i0_inferred__0/i__carry_n_0 ),
        .CO({\NLW_pwm_b_p_i0_inferred__0/i__carry__0_CO_UNCONNECTED [3],pwm_b_p_i[1],\pwm_b_p_i0_inferred__0/i__carry__0_n_2 ,\pwm_b_p_i0_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_211,pwm_x40_ip_v1_0_S_AXI_inst_n_212,pwm_x40_ip_v1_0_S_AXI_inst_n_213}),
        .O(\NLW_pwm_b_p_i0_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase270_n_17,counter_phase270_n_18,counter_phase270_n_19}));
  CARRY4 \pwm_b_p_i0_inferred__1/i__carry 
       (.CI(1'b0),
        .CO({\pwm_b_p_i0_inferred__1/i__carry_n_0 ,\pwm_b_p_i0_inferred__1/i__carry_n_1 ,\pwm_b_p_i0_inferred__1/i__carry_n_2 ,\pwm_b_p_i0_inferred__1/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({pwm_x40_ip_v1_0_S_AXI_inst_n_214,pwm_x40_ip_v1_0_S_AXI_inst_n_215,pwm_x40_ip_v1_0_S_AXI_inst_n_216,pwm_x40_ip_v1_0_S_AXI_inst_n_217}),
        .O(\NLW_pwm_b_p_i0_inferred__1/i__carry_O_UNCONNECTED [3:0]),
        .S({counter_phase180_n_20,counter_phase180_n_21,counter_phase180_n_22,counter_phase180_n_23}));
  CARRY4 \pwm_b_p_i0_inferred__1/i__carry__0 
       (.CI(\pwm_b_p_i0_inferred__1/i__carry_n_0 ),
        .CO({\NLW_pwm_b_p_i0_inferred__1/i__carry__0_CO_UNCONNECTED [3],pwm_b_p_i[2],\pwm_b_p_i0_inferred__1/i__carry__0_n_2 ,\pwm_b_p_i0_inferred__1/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_218,pwm_x40_ip_v1_0_S_AXI_inst_n_219,pwm_x40_ip_v1_0_S_AXI_inst_n_220}),
        .O(\NLW_pwm_b_p_i0_inferred__1/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase180_n_24,counter_phase180_n_25,counter_phase180_n_26}));
  CARRY4 \pwm_b_p_i0_inferred__2/i__carry 
       (.CI(1'b0),
        .CO({\pwm_b_p_i0_inferred__2/i__carry_n_0 ,\pwm_b_p_i0_inferred__2/i__carry_n_1 ,\pwm_b_p_i0_inferred__2/i__carry_n_2 ,\pwm_b_p_i0_inferred__2/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({pwm_x40_ip_v1_0_S_AXI_inst_n_221,pwm_x40_ip_v1_0_S_AXI_inst_n_222,pwm_x40_ip_v1_0_S_AXI_inst_n_223,pwm_x40_ip_v1_0_S_AXI_inst_n_224}),
        .O(\NLW_pwm_b_p_i0_inferred__2/i__carry_O_UNCONNECTED [3:0]),
        .S({counter_phase270_n_20,counter_phase270_n_21,counter_phase270_n_22,counter_phase270_n_23}));
  CARRY4 \pwm_b_p_i0_inferred__2/i__carry__0 
       (.CI(\pwm_b_p_i0_inferred__2/i__carry_n_0 ),
        .CO({\NLW_pwm_b_p_i0_inferred__2/i__carry__0_CO_UNCONNECTED [3],pwm_b_p_i[3],\pwm_b_p_i0_inferred__2/i__carry__0_n_2 ,\pwm_b_p_i0_inferred__2/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_225,pwm_x40_ip_v1_0_S_AXI_inst_n_226,pwm_x40_ip_v1_0_S_AXI_inst_n_227}),
        .O(\NLW_pwm_b_p_i0_inferred__2/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase270_n_24,counter_phase270_n_25,counter_phase270_n_26}));
  CARRY4 \pwm_b_p_i0_inferred__3/i__carry 
       (.CI(1'b0),
        .CO({\pwm_b_p_i0_inferred__3/i__carry_n_0 ,\pwm_b_p_i0_inferred__3/i__carry_n_1 ,\pwm_b_p_i0_inferred__3/i__carry_n_2 ,\pwm_b_p_i0_inferred__3/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({pwm_x40_ip_v1_0_S_AXI_inst_n_228,pwm_x40_ip_v1_0_S_AXI_inst_n_229,pwm_x40_ip_v1_0_S_AXI_inst_n_230,pwm_x40_ip_v1_0_S_AXI_inst_n_231}),
        .O(\NLW_pwm_b_p_i0_inferred__3/i__carry_O_UNCONNECTED [3:0]),
        .S({counter_phase180_n_27,counter_phase180_n_28,counter_phase180_n_29,counter_phase180_n_30}));
  CARRY4 \pwm_b_p_i0_inferred__3/i__carry__0 
       (.CI(\pwm_b_p_i0_inferred__3/i__carry_n_0 ),
        .CO({\NLW_pwm_b_p_i0_inferred__3/i__carry__0_CO_UNCONNECTED [3],pwm_b_p_i[4],\pwm_b_p_i0_inferred__3/i__carry__0_n_2 ,\pwm_b_p_i0_inferred__3/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_232,pwm_x40_ip_v1_0_S_AXI_inst_n_233,pwm_x40_ip_v1_0_S_AXI_inst_n_234}),
        .O(\NLW_pwm_b_p_i0_inferred__3/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase180_n_31,counter_phase180_n_32,counter_phase180_n_33}));
  CARRY4 \pwm_b_p_i0_inferred__4/i__carry 
       (.CI(1'b0),
        .CO({\pwm_b_p_i0_inferred__4/i__carry_n_0 ,\pwm_b_p_i0_inferred__4/i__carry_n_1 ,\pwm_b_p_i0_inferred__4/i__carry_n_2 ,\pwm_b_p_i0_inferred__4/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({pwm_x40_ip_v1_0_S_AXI_inst_n_235,pwm_x40_ip_v1_0_S_AXI_inst_n_236,pwm_x40_ip_v1_0_S_AXI_inst_n_237,pwm_x40_ip_v1_0_S_AXI_inst_n_238}),
        .O(\NLW_pwm_b_p_i0_inferred__4/i__carry_O_UNCONNECTED [3:0]),
        .S({counter_phase270_n_27,counter_phase270_n_28,counter_phase270_n_29,counter_phase270_n_30}));
  CARRY4 \pwm_b_p_i0_inferred__4/i__carry__0 
       (.CI(\pwm_b_p_i0_inferred__4/i__carry_n_0 ),
        .CO({\NLW_pwm_b_p_i0_inferred__4/i__carry__0_CO_UNCONNECTED [3],pwm_b_p_i[5],\pwm_b_p_i0_inferred__4/i__carry__0_n_2 ,\pwm_b_p_i0_inferred__4/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_239,pwm_x40_ip_v1_0_S_AXI_inst_n_240,pwm_x40_ip_v1_0_S_AXI_inst_n_241}),
        .O(\NLW_pwm_b_p_i0_inferred__4/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase270_n_31,counter_phase270_n_32,counter_phase270_n_33}));
  CARRY4 \pwm_b_p_i0_inferred__5/i__carry 
       (.CI(1'b0),
        .CO({\pwm_b_p_i0_inferred__5/i__carry_n_0 ,\pwm_b_p_i0_inferred__5/i__carry_n_1 ,\pwm_b_p_i0_inferred__5/i__carry_n_2 ,\pwm_b_p_i0_inferred__5/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({pwm_x40_ip_v1_0_S_AXI_inst_n_242,pwm_x40_ip_v1_0_S_AXI_inst_n_243,pwm_x40_ip_v1_0_S_AXI_inst_n_244,pwm_x40_ip_v1_0_S_AXI_inst_n_245}),
        .O(\NLW_pwm_b_p_i0_inferred__5/i__carry_O_UNCONNECTED [3:0]),
        .S({counter_phase180_n_34,counter_phase180_n_35,counter_phase180_n_36,counter_phase180_n_37}));
  CARRY4 \pwm_b_p_i0_inferred__5/i__carry__0 
       (.CI(\pwm_b_p_i0_inferred__5/i__carry_n_0 ),
        .CO({\NLW_pwm_b_p_i0_inferred__5/i__carry__0_CO_UNCONNECTED [3],pwm_b_p_i[6],\pwm_b_p_i0_inferred__5/i__carry__0_n_2 ,\pwm_b_p_i0_inferred__5/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_246,pwm_x40_ip_v1_0_S_AXI_inst_n_247,pwm_x40_ip_v1_0_S_AXI_inst_n_248}),
        .O(\NLW_pwm_b_p_i0_inferred__5/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase180_n_38,counter_phase180_n_39,counter_phase180_n_40}));
  CARRY4 \pwm_b_p_i0_inferred__6/i__carry 
       (.CI(1'b0),
        .CO({\pwm_b_p_i0_inferred__6/i__carry_n_0 ,\pwm_b_p_i0_inferred__6/i__carry_n_1 ,\pwm_b_p_i0_inferred__6/i__carry_n_2 ,\pwm_b_p_i0_inferred__6/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({pwm_x40_ip_v1_0_S_AXI_inst_n_249,pwm_x40_ip_v1_0_S_AXI_inst_n_250,pwm_x40_ip_v1_0_S_AXI_inst_n_251,pwm_x40_ip_v1_0_S_AXI_inst_n_252}),
        .O(\NLW_pwm_b_p_i0_inferred__6/i__carry_O_UNCONNECTED [3:0]),
        .S({counter_phase270_n_34,counter_phase270_n_35,counter_phase270_n_36,counter_phase270_n_37}));
  CARRY4 \pwm_b_p_i0_inferred__6/i__carry__0 
       (.CI(\pwm_b_p_i0_inferred__6/i__carry_n_0 ),
        .CO({\NLW_pwm_b_p_i0_inferred__6/i__carry__0_CO_UNCONNECTED [3],pwm_b_p_i[7],\pwm_b_p_i0_inferred__6/i__carry__0_n_2 ,\pwm_b_p_i0_inferred__6/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_253,pwm_x40_ip_v1_0_S_AXI_inst_n_254,pwm_x40_ip_v1_0_S_AXI_inst_n_255}),
        .O(\NLW_pwm_b_p_i0_inferred__6/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase270_n_38,counter_phase270_n_39,counter_phase270_n_40}));
  CARRY4 \pwm_b_p_i0_inferred__7/i__carry 
       (.CI(1'b0),
        .CO({\pwm_b_p_i0_inferred__7/i__carry_n_0 ,\pwm_b_p_i0_inferred__7/i__carry_n_1 ,\pwm_b_p_i0_inferred__7/i__carry_n_2 ,\pwm_b_p_i0_inferred__7/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({pwm_x40_ip_v1_0_S_AXI_inst_n_256,pwm_x40_ip_v1_0_S_AXI_inst_n_257,pwm_x40_ip_v1_0_S_AXI_inst_n_258,pwm_x40_ip_v1_0_S_AXI_inst_n_259}),
        .O(\NLW_pwm_b_p_i0_inferred__7/i__carry_O_UNCONNECTED [3:0]),
        .S({counter_phase180_n_41,counter_phase180_n_42,counter_phase180_n_43,counter_phase180_n_44}));
  CARRY4 \pwm_b_p_i0_inferred__7/i__carry__0 
       (.CI(\pwm_b_p_i0_inferred__7/i__carry_n_0 ),
        .CO({\NLW_pwm_b_p_i0_inferred__7/i__carry__0_CO_UNCONNECTED [3],pwm_b_p_i[8],\pwm_b_p_i0_inferred__7/i__carry__0_n_2 ,\pwm_b_p_i0_inferred__7/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_260,pwm_x40_ip_v1_0_S_AXI_inst_n_261,pwm_x40_ip_v1_0_S_AXI_inst_n_262}),
        .O(\NLW_pwm_b_p_i0_inferred__7/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase180_n_45,counter_phase180_n_46,counter_phase180_n_47}));
  CARRY4 \pwm_b_p_i0_inferred__8/i__carry 
       (.CI(1'b0),
        .CO({\pwm_b_p_i0_inferred__8/i__carry_n_0 ,\pwm_b_p_i0_inferred__8/i__carry_n_1 ,\pwm_b_p_i0_inferred__8/i__carry_n_2 ,\pwm_b_p_i0_inferred__8/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({pwm_x40_ip_v1_0_S_AXI_inst_n_263,pwm_x40_ip_v1_0_S_AXI_inst_n_264,pwm_x40_ip_v1_0_S_AXI_inst_n_265,pwm_x40_ip_v1_0_S_AXI_inst_n_266}),
        .O(\NLW_pwm_b_p_i0_inferred__8/i__carry_O_UNCONNECTED [3:0]),
        .S({counter_phase270_n_41,counter_phase270_n_42,counter_phase270_n_43,counter_phase270_n_44}));
  CARRY4 \pwm_b_p_i0_inferred__8/i__carry__0 
       (.CI(\pwm_b_p_i0_inferred__8/i__carry_n_0 ),
        .CO({\NLW_pwm_b_p_i0_inferred__8/i__carry__0_CO_UNCONNECTED [3],pwm_b_p_i[9],\pwm_b_p_i0_inferred__8/i__carry__0_n_2 ,\pwm_b_p_i0_inferred__8/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,pwm_x40_ip_v1_0_S_AXI_inst_n_267,pwm_x40_ip_v1_0_S_AXI_inst_n_268,pwm_x40_ip_v1_0_S_AXI_inst_n_269}),
        .O(\NLW_pwm_b_p_i0_inferred__8/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,counter_phase270_n_45,counter_phase270_n_46,counter_phase270_n_47}));
  system_pwm_x40_ip_0_0_pwm_x40_ip_v1_0_S_AXI pwm_x40_ip_v1_0_S_AXI_inst
       (.DI({pwm_x40_ip_v1_0_S_AXI_inst_n_0,pwm_x40_ip_v1_0_S_AXI_inst_n_1,pwm_x40_ip_v1_0_S_AXI_inst_n_2,pwm_x40_ip_v1_0_S_AXI_inst_n_3}),
        .S({pwm_x40_ip_v1_0_S_AXI_inst_n_40,pwm_x40_ip_v1_0_S_AXI_inst_n_41,pwm_x40_ip_v1_0_S_AXI_inst_n_42,pwm_x40_ip_v1_0_S_AXI_inst_n_43}),
        .S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .X_MOD_0(X_MOD_0),
        .X_MOD_1(X_MOD_1),
        .X_MOD_2(X_MOD_2),
        .X_MOD_3(X_MOD_3),
        .X_MOD_4(X_MOD_4),
        .X_MOD_5(X_MOD_5),
        .X_MOD_6(X_MOD_6),
        .X_MOD_7(X_MOD_7),
        .X_MOD_8(X_MOD_8),
        .X_MOD_9(X_MOD_9),
        .counter_phase180_output(counter_phase180_output),
        .counter_phase270_output(counter_phase270_output),
        .counter_phase90_output(counter_phase90_output),
        .external_reset_i(external_reset_i),
        .q(q),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_araddr(s_axi_araddr),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata(s_axi_wdata),
        .s_axi_wstrb(s_axi_wstrb),
        .s_axi_wvalid(s_axi_wvalid),
        .\x_mod_0_i_reg[13]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_17,pwm_x40_ip_v1_0_S_AXI_inst_n_18,pwm_x40_ip_v1_0_S_AXI_inst_n_19}),
        .\x_mod_0_i_reg[13]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_204,pwm_x40_ip_v1_0_S_AXI_inst_n_205,pwm_x40_ip_v1_0_S_AXI_inst_n_206}),
        .\x_mod_0_i_reg[8]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_200,pwm_x40_ip_v1_0_S_AXI_inst_n_201,pwm_x40_ip_v1_0_S_AXI_inst_n_202,pwm_x40_ip_v1_0_S_AXI_inst_n_203}),
        .\x_mod_1_i_reg[13]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_37,pwm_x40_ip_v1_0_S_AXI_inst_n_38,pwm_x40_ip_v1_0_S_AXI_inst_n_39}),
        .\x_mod_1_i_reg[13]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_211,pwm_x40_ip_v1_0_S_AXI_inst_n_212,pwm_x40_ip_v1_0_S_AXI_inst_n_213}),
        .\x_mod_1_i_reg[8]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_20,pwm_x40_ip_v1_0_S_AXI_inst_n_21,pwm_x40_ip_v1_0_S_AXI_inst_n_22,pwm_x40_ip_v1_0_S_AXI_inst_n_23}),
        .\x_mod_1_i_reg[8]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_207,pwm_x40_ip_v1_0_S_AXI_inst_n_208,pwm_x40_ip_v1_0_S_AXI_inst_n_209,pwm_x40_ip_v1_0_S_AXI_inst_n_210}),
        .\x_mod_2_i_reg[12]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_57,pwm_x40_ip_v1_0_S_AXI_inst_n_58}),
        .\x_mod_2_i_reg[13]_0 (pwm_x40_ip_v1_0_S_AXI_inst_n_59),
        .\x_mod_2_i_reg[13]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_218,pwm_x40_ip_v1_0_S_AXI_inst_n_219,pwm_x40_ip_v1_0_S_AXI_inst_n_220}),
        .\x_mod_2_i_reg[8]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_214,pwm_x40_ip_v1_0_S_AXI_inst_n_215,pwm_x40_ip_v1_0_S_AXI_inst_n_216,pwm_x40_ip_v1_0_S_AXI_inst_n_217}),
        .\x_mod_3_i_reg[12]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_77,pwm_x40_ip_v1_0_S_AXI_inst_n_78}),
        .\x_mod_3_i_reg[13]_0 (pwm_x40_ip_v1_0_S_AXI_inst_n_79),
        .\x_mod_3_i_reg[13]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_225,pwm_x40_ip_v1_0_S_AXI_inst_n_226,pwm_x40_ip_v1_0_S_AXI_inst_n_227}),
        .\x_mod_3_i_reg[8]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_60,pwm_x40_ip_v1_0_S_AXI_inst_n_61,pwm_x40_ip_v1_0_S_AXI_inst_n_62,pwm_x40_ip_v1_0_S_AXI_inst_n_63}),
        .\x_mod_3_i_reg[8]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_221,pwm_x40_ip_v1_0_S_AXI_inst_n_222,pwm_x40_ip_v1_0_S_AXI_inst_n_223,pwm_x40_ip_v1_0_S_AXI_inst_n_224}),
        .\x_mod_4_i_reg[12]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_97,pwm_x40_ip_v1_0_S_AXI_inst_n_98}),
        .\x_mod_4_i_reg[13]_0 (pwm_x40_ip_v1_0_S_AXI_inst_n_99),
        .\x_mod_4_i_reg[13]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_232,pwm_x40_ip_v1_0_S_AXI_inst_n_233,pwm_x40_ip_v1_0_S_AXI_inst_n_234}),
        .\x_mod_4_i_reg[8]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_80,pwm_x40_ip_v1_0_S_AXI_inst_n_81,pwm_x40_ip_v1_0_S_AXI_inst_n_82,pwm_x40_ip_v1_0_S_AXI_inst_n_83}),
        .\x_mod_4_i_reg[8]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_228,pwm_x40_ip_v1_0_S_AXI_inst_n_229,pwm_x40_ip_v1_0_S_AXI_inst_n_230,pwm_x40_ip_v1_0_S_AXI_inst_n_231}),
        .\x_mod_5_i_reg[12]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_117,pwm_x40_ip_v1_0_S_AXI_inst_n_118}),
        .\x_mod_5_i_reg[13]_0 (pwm_x40_ip_v1_0_S_AXI_inst_n_119),
        .\x_mod_5_i_reg[13]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_239,pwm_x40_ip_v1_0_S_AXI_inst_n_240,pwm_x40_ip_v1_0_S_AXI_inst_n_241}),
        .\x_mod_5_i_reg[8]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_100,pwm_x40_ip_v1_0_S_AXI_inst_n_101,pwm_x40_ip_v1_0_S_AXI_inst_n_102,pwm_x40_ip_v1_0_S_AXI_inst_n_103}),
        .\x_mod_5_i_reg[8]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_235,pwm_x40_ip_v1_0_S_AXI_inst_n_236,pwm_x40_ip_v1_0_S_AXI_inst_n_237,pwm_x40_ip_v1_0_S_AXI_inst_n_238}),
        .\x_mod_6_i_reg[12]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_137,pwm_x40_ip_v1_0_S_AXI_inst_n_138}),
        .\x_mod_6_i_reg[13]_0 (pwm_x40_ip_v1_0_S_AXI_inst_n_139),
        .\x_mod_6_i_reg[13]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_246,pwm_x40_ip_v1_0_S_AXI_inst_n_247,pwm_x40_ip_v1_0_S_AXI_inst_n_248}),
        .\x_mod_6_i_reg[8]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_120,pwm_x40_ip_v1_0_S_AXI_inst_n_121,pwm_x40_ip_v1_0_S_AXI_inst_n_122,pwm_x40_ip_v1_0_S_AXI_inst_n_123}),
        .\x_mod_6_i_reg[8]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_242,pwm_x40_ip_v1_0_S_AXI_inst_n_243,pwm_x40_ip_v1_0_S_AXI_inst_n_244,pwm_x40_ip_v1_0_S_AXI_inst_n_245}),
        .\x_mod_7_i_reg[12]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_157,pwm_x40_ip_v1_0_S_AXI_inst_n_158}),
        .\x_mod_7_i_reg[13]_0 (pwm_x40_ip_v1_0_S_AXI_inst_n_159),
        .\x_mod_7_i_reg[13]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_253,pwm_x40_ip_v1_0_S_AXI_inst_n_254,pwm_x40_ip_v1_0_S_AXI_inst_n_255}),
        .\x_mod_7_i_reg[8]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_140,pwm_x40_ip_v1_0_S_AXI_inst_n_141,pwm_x40_ip_v1_0_S_AXI_inst_n_142,pwm_x40_ip_v1_0_S_AXI_inst_n_143}),
        .\x_mod_7_i_reg[8]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_249,pwm_x40_ip_v1_0_S_AXI_inst_n_250,pwm_x40_ip_v1_0_S_AXI_inst_n_251,pwm_x40_ip_v1_0_S_AXI_inst_n_252}),
        .\x_mod_8_i_reg[12]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_177,pwm_x40_ip_v1_0_S_AXI_inst_n_178}),
        .\x_mod_8_i_reg[13]_0 (pwm_x40_ip_v1_0_S_AXI_inst_n_179),
        .\x_mod_8_i_reg[13]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_260,pwm_x40_ip_v1_0_S_AXI_inst_n_261,pwm_x40_ip_v1_0_S_AXI_inst_n_262}),
        .\x_mod_8_i_reg[8]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_160,pwm_x40_ip_v1_0_S_AXI_inst_n_161,pwm_x40_ip_v1_0_S_AXI_inst_n_162,pwm_x40_ip_v1_0_S_AXI_inst_n_163}),
        .\x_mod_8_i_reg[8]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_256,pwm_x40_ip_v1_0_S_AXI_inst_n_257,pwm_x40_ip_v1_0_S_AXI_inst_n_258,pwm_x40_ip_v1_0_S_AXI_inst_n_259}),
        .\x_mod_9_i_reg[12]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_197,pwm_x40_ip_v1_0_S_AXI_inst_n_198}),
        .\x_mod_9_i_reg[13]_0 (pwm_x40_ip_v1_0_S_AXI_inst_n_199),
        .\x_mod_9_i_reg[13]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_267,pwm_x40_ip_v1_0_S_AXI_inst_n_268,pwm_x40_ip_v1_0_S_AXI_inst_n_269}),
        .\x_mod_9_i_reg[8]_0 ({pwm_x40_ip_v1_0_S_AXI_inst_n_180,pwm_x40_ip_v1_0_S_AXI_inst_n_181,pwm_x40_ip_v1_0_S_AXI_inst_n_182,pwm_x40_ip_v1_0_S_AXI_inst_n_183}),
        .\x_mod_9_i_reg[8]_1 ({pwm_x40_ip_v1_0_S_AXI_inst_n_263,pwm_x40_ip_v1_0_S_AXI_inst_n_264,pwm_x40_ip_v1_0_S_AXI_inst_n_265,pwm_x40_ip_v1_0_S_AXI_inst_n_266}));
endmodule

module system_pwm_x40_ip_0_0_pwm_x40_ip_v1_0_S_AXI
   (DI,
    X_MOD_0,
    \x_mod_0_i_reg[13]_0 ,
    \x_mod_1_i_reg[8]_0 ,
    X_MOD_1,
    \x_mod_1_i_reg[13]_0 ,
    S,
    X_MOD_2,
    \x_mod_2_i_reg[12]_0 ,
    \x_mod_2_i_reg[13]_0 ,
    \x_mod_3_i_reg[8]_0 ,
    X_MOD_3,
    \x_mod_3_i_reg[12]_0 ,
    \x_mod_3_i_reg[13]_0 ,
    \x_mod_4_i_reg[8]_0 ,
    X_MOD_4,
    \x_mod_4_i_reg[12]_0 ,
    \x_mod_4_i_reg[13]_0 ,
    \x_mod_5_i_reg[8]_0 ,
    X_MOD_5,
    \x_mod_5_i_reg[12]_0 ,
    \x_mod_5_i_reg[13]_0 ,
    \x_mod_6_i_reg[8]_0 ,
    X_MOD_6,
    \x_mod_6_i_reg[12]_0 ,
    \x_mod_6_i_reg[13]_0 ,
    \x_mod_7_i_reg[8]_0 ,
    X_MOD_7,
    \x_mod_7_i_reg[12]_0 ,
    \x_mod_7_i_reg[13]_0 ,
    \x_mod_8_i_reg[8]_0 ,
    X_MOD_8,
    \x_mod_8_i_reg[12]_0 ,
    \x_mod_8_i_reg[13]_0 ,
    \x_mod_9_i_reg[8]_0 ,
    X_MOD_9,
    \x_mod_9_i_reg[12]_0 ,
    \x_mod_9_i_reg[13]_0 ,
    \x_mod_0_i_reg[8]_0 ,
    \x_mod_0_i_reg[13]_1 ,
    \x_mod_1_i_reg[8]_1 ,
    \x_mod_1_i_reg[13]_1 ,
    \x_mod_2_i_reg[8]_0 ,
    \x_mod_2_i_reg[13]_1 ,
    \x_mod_3_i_reg[8]_1 ,
    \x_mod_3_i_reg[13]_1 ,
    \x_mod_4_i_reg[8]_1 ,
    \x_mod_4_i_reg[13]_1 ,
    \x_mod_5_i_reg[8]_1 ,
    \x_mod_5_i_reg[13]_1 ,
    \x_mod_6_i_reg[8]_1 ,
    \x_mod_6_i_reg[13]_1 ,
    \x_mod_7_i_reg[8]_1 ,
    \x_mod_7_i_reg[13]_1 ,
    \x_mod_8_i_reg[8]_1 ,
    \x_mod_8_i_reg[13]_1 ,
    \x_mod_9_i_reg[8]_1 ,
    \x_mod_9_i_reg[13]_1 ,
    S_AXI_AWREADY,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s_axi_rdata,
    s_axi_rvalid,
    s_axi_bvalid,
    q,
    counter_phase90_output,
    counter_phase180_output,
    counter_phase270_output,
    s_axi_aclk,
    s_axi_awaddr,
    s_axi_wvalid,
    s_axi_wdata,
    external_reset_i,
    s_axi_araddr,
    s_axi_awvalid,
    s_axi_wstrb,
    s_axi_arvalid,
    s_axi_aresetn,
    s_axi_bready,
    s_axi_rready);
  output [3:0]DI;
  output [12:0]X_MOD_0;
  output [2:0]\x_mod_0_i_reg[13]_0 ;
  output [3:0]\x_mod_1_i_reg[8]_0 ;
  output [12:0]X_MOD_1;
  output [2:0]\x_mod_1_i_reg[13]_0 ;
  output [3:0]S;
  output [12:0]X_MOD_2;
  output [1:0]\x_mod_2_i_reg[12]_0 ;
  output [0:0]\x_mod_2_i_reg[13]_0 ;
  output [3:0]\x_mod_3_i_reg[8]_0 ;
  output [12:0]X_MOD_3;
  output [1:0]\x_mod_3_i_reg[12]_0 ;
  output [0:0]\x_mod_3_i_reg[13]_0 ;
  output [3:0]\x_mod_4_i_reg[8]_0 ;
  output [12:0]X_MOD_4;
  output [1:0]\x_mod_4_i_reg[12]_0 ;
  output [0:0]\x_mod_4_i_reg[13]_0 ;
  output [3:0]\x_mod_5_i_reg[8]_0 ;
  output [12:0]X_MOD_5;
  output [1:0]\x_mod_5_i_reg[12]_0 ;
  output [0:0]\x_mod_5_i_reg[13]_0 ;
  output [3:0]\x_mod_6_i_reg[8]_0 ;
  output [12:0]X_MOD_6;
  output [1:0]\x_mod_6_i_reg[12]_0 ;
  output [0:0]\x_mod_6_i_reg[13]_0 ;
  output [3:0]\x_mod_7_i_reg[8]_0 ;
  output [12:0]X_MOD_7;
  output [1:0]\x_mod_7_i_reg[12]_0 ;
  output [0:0]\x_mod_7_i_reg[13]_0 ;
  output [3:0]\x_mod_8_i_reg[8]_0 ;
  output [12:0]X_MOD_8;
  output [1:0]\x_mod_8_i_reg[12]_0 ;
  output [0:0]\x_mod_8_i_reg[13]_0 ;
  output [3:0]\x_mod_9_i_reg[8]_0 ;
  output [12:0]X_MOD_9;
  output [1:0]\x_mod_9_i_reg[12]_0 ;
  output [0:0]\x_mod_9_i_reg[13]_0 ;
  output [3:0]\x_mod_0_i_reg[8]_0 ;
  output [2:0]\x_mod_0_i_reg[13]_1 ;
  output [3:0]\x_mod_1_i_reg[8]_1 ;
  output [2:0]\x_mod_1_i_reg[13]_1 ;
  output [3:0]\x_mod_2_i_reg[8]_0 ;
  output [2:0]\x_mod_2_i_reg[13]_1 ;
  output [3:0]\x_mod_3_i_reg[8]_1 ;
  output [2:0]\x_mod_3_i_reg[13]_1 ;
  output [3:0]\x_mod_4_i_reg[8]_1 ;
  output [2:0]\x_mod_4_i_reg[13]_1 ;
  output [3:0]\x_mod_5_i_reg[8]_1 ;
  output [2:0]\x_mod_5_i_reg[13]_1 ;
  output [3:0]\x_mod_6_i_reg[8]_1 ;
  output [2:0]\x_mod_6_i_reg[13]_1 ;
  output [3:0]\x_mod_7_i_reg[8]_1 ;
  output [2:0]\x_mod_7_i_reg[13]_1 ;
  output [3:0]\x_mod_8_i_reg[8]_1 ;
  output [2:0]\x_mod_8_i_reg[13]_1 ;
  output [3:0]\x_mod_9_i_reg[8]_1 ;
  output [2:0]\x_mod_9_i_reg[13]_1 ;
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]s_axi_rdata;
  output s_axi_rvalid;
  output s_axi_bvalid;
  input [12:0]q;
  input [12:0]counter_phase90_output;
  input [12:0]counter_phase180_output;
  input [12:0]counter_phase270_output;
  input s_axi_aclk;
  input [3:0]s_axi_awaddr;
  input s_axi_wvalid;
  input [31:0]s_axi_wdata;
  input external_reset_i;
  input [3:0]s_axi_araddr;
  input s_axi_awvalid;
  input [3:0]s_axi_wstrb;
  input s_axi_arvalid;
  input s_axi_aresetn;
  input s_axi_bready;
  input s_axi_rready;

  wire [3:0]DI;
  wire [3:0]S;
  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire [12:0]X_MOD_0;
  wire [12:0]X_MOD_1;
  wire [12:0]X_MOD_2;
  wire [12:0]X_MOD_3;
  wire [12:0]X_MOD_4;
  wire [12:0]X_MOD_5;
  wire [12:0]X_MOD_6;
  wire [12:0]X_MOD_7;
  wire [12:0]X_MOD_8;
  wire [12:0]X_MOD_9;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire axi_arready0;
  wire axi_awready0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[0]_i_4_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[10]_i_4_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[11]_i_4_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[12]_i_4_n_0 ;
  wire \axi_rdata[13]_i_3_n_0 ;
  wire \axi_rdata[13]_i_4_n_0 ;
  wire \axi_rdata[14]_i_3_n_0 ;
  wire \axi_rdata[14]_i_4_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[15]_i_4_n_0 ;
  wire \axi_rdata[16]_i_3_n_0 ;
  wire \axi_rdata[16]_i_4_n_0 ;
  wire \axi_rdata[17]_i_3_n_0 ;
  wire \axi_rdata[17]_i_4_n_0 ;
  wire \axi_rdata[18]_i_3_n_0 ;
  wire \axi_rdata[18]_i_4_n_0 ;
  wire \axi_rdata[19]_i_3_n_0 ;
  wire \axi_rdata[19]_i_4_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[1]_i_4_n_0 ;
  wire \axi_rdata[20]_i_3_n_0 ;
  wire \axi_rdata[20]_i_4_n_0 ;
  wire \axi_rdata[21]_i_3_n_0 ;
  wire \axi_rdata[21]_i_4_n_0 ;
  wire \axi_rdata[22]_i_3_n_0 ;
  wire \axi_rdata[22]_i_4_n_0 ;
  wire \axi_rdata[23]_i_3_n_0 ;
  wire \axi_rdata[23]_i_4_n_0 ;
  wire \axi_rdata[24]_i_3_n_0 ;
  wire \axi_rdata[24]_i_4_n_0 ;
  wire \axi_rdata[25]_i_3_n_0 ;
  wire \axi_rdata[25]_i_4_n_0 ;
  wire \axi_rdata[26]_i_3_n_0 ;
  wire \axi_rdata[26]_i_4_n_0 ;
  wire \axi_rdata[27]_i_3_n_0 ;
  wire \axi_rdata[27]_i_4_n_0 ;
  wire \axi_rdata[28]_i_3_n_0 ;
  wire \axi_rdata[28]_i_4_n_0 ;
  wire \axi_rdata[29]_i_3_n_0 ;
  wire \axi_rdata[29]_i_4_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[2]_i_4_n_0 ;
  wire \axi_rdata[30]_i_3_n_0 ;
  wire \axi_rdata[30]_i_4_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[31]_i_4_n_0 ;
  wire \axi_rdata[31]_i_6_n_0 ;
  wire \axi_rdata[31]_i_7_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[3]_i_4_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[4]_i_4_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[5]_i_4_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[6]_i_4_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[7]_i_4_n_0 ;
  wire \axi_rdata[8]_i_3_n_0 ;
  wire \axi_rdata[8]_i_4_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire \axi_rdata[9]_i_4_n_0 ;
  wire \axi_rdata_reg[0]_i_2_n_0 ;
  wire \axi_rdata_reg[10]_i_2_n_0 ;
  wire \axi_rdata_reg[11]_i_2_n_0 ;
  wire \axi_rdata_reg[12]_i_2_n_0 ;
  wire \axi_rdata_reg[13]_i_2_n_0 ;
  wire \axi_rdata_reg[14]_i_2_n_0 ;
  wire \axi_rdata_reg[15]_i_2_n_0 ;
  wire \axi_rdata_reg[16]_i_2_n_0 ;
  wire \axi_rdata_reg[17]_i_2_n_0 ;
  wire \axi_rdata_reg[18]_i_2_n_0 ;
  wire \axi_rdata_reg[19]_i_2_n_0 ;
  wire \axi_rdata_reg[1]_i_2_n_0 ;
  wire \axi_rdata_reg[20]_i_2_n_0 ;
  wire \axi_rdata_reg[21]_i_2_n_0 ;
  wire \axi_rdata_reg[22]_i_2_n_0 ;
  wire \axi_rdata_reg[23]_i_2_n_0 ;
  wire \axi_rdata_reg[24]_i_2_n_0 ;
  wire \axi_rdata_reg[25]_i_2_n_0 ;
  wire \axi_rdata_reg[26]_i_2_n_0 ;
  wire \axi_rdata_reg[27]_i_2_n_0 ;
  wire \axi_rdata_reg[28]_i_2_n_0 ;
  wire \axi_rdata_reg[29]_i_2_n_0 ;
  wire \axi_rdata_reg[2]_i_2_n_0 ;
  wire \axi_rdata_reg[30]_i_2_n_0 ;
  wire \axi_rdata_reg[31]_i_5_n_0 ;
  wire \axi_rdata_reg[3]_i_2_n_0 ;
  wire \axi_rdata_reg[4]_i_2_n_0 ;
  wire \axi_rdata_reg[5]_i_2_n_0 ;
  wire \axi_rdata_reg[6]_i_2_n_0 ;
  wire \axi_rdata_reg[7]_i_2_n_0 ;
  wire \axi_rdata_reg[8]_i_2_n_0 ;
  wire \axi_rdata_reg[9]_i_2_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire axi_wvalid_falling_edge;
  wire axi_wvalid_falling_edge0;
  wire [12:0]counter_phase180_output;
  wire [12:0]counter_phase270_output;
  wire [12:0]counter_phase90_output;
  wire external_reset_i;
  wire p_0_in;
  wire [3:0]p_0_in__0;
  wire [31:7]p_1_in;
  wire past_axi_wvalid;
  wire [12:0]q;
  wire [31:0]reg_data_out__0;
  wire s_axi_aclk;
  wire [3:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arvalid;
  wire [3:0]s_axi_awaddr;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire [3:0]s_axi_wstrb;
  wire s_axi_wvalid;
  wire [3:0]sel0;
  wire [31:0]slv_reg0;
  wire [31:0]slv_reg1;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [31:0]slv_reg2;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire [31:0]slv_reg3;
  wire \slv_reg3[15]_i_1_n_0 ;
  wire \slv_reg3[23]_i_1_n_0 ;
  wire \slv_reg3[31]_i_1_n_0 ;
  wire \slv_reg3[7]_i_1_n_0 ;
  wire [31:0]slv_reg4;
  wire \slv_reg4[15]_i_1_n_0 ;
  wire \slv_reg4[23]_i_1_n_0 ;
  wire \slv_reg4[31]_i_1_n_0 ;
  wire \slv_reg4[7]_i_1_n_0 ;
  wire [31:0]slv_reg5;
  wire \slv_reg5[15]_i_1_n_0 ;
  wire \slv_reg5[23]_i_1_n_0 ;
  wire \slv_reg5[31]_i_1_n_0 ;
  wire \slv_reg5[7]_i_1_n_0 ;
  wire [31:0]slv_reg6;
  wire \slv_reg6[15]_i_1_n_0 ;
  wire \slv_reg6[23]_i_1_n_0 ;
  wire \slv_reg6[31]_i_1_n_0 ;
  wire \slv_reg6[7]_i_1_n_0 ;
  wire [31:0]slv_reg7;
  wire \slv_reg7[15]_i_1_n_0 ;
  wire \slv_reg7[23]_i_1_n_0 ;
  wire \slv_reg7[31]_i_1_n_0 ;
  wire \slv_reg7[7]_i_1_n_0 ;
  wire [31:0]slv_reg8;
  wire \slv_reg8[15]_i_1_n_0 ;
  wire \slv_reg8[23]_i_1_n_0 ;
  wire \slv_reg8[31]_i_1_n_0 ;
  wire \slv_reg8[7]_i_1_n_0 ;
  wire [31:0]slv_reg9;
  wire \slv_reg9[15]_i_1_n_0 ;
  wire \slv_reg9[23]_i_1_n_0 ;
  wire \slv_reg9[31]_i_1_n_0 ;
  wire \slv_reg9[7]_i_1_n_0 ;
  wire slv_reg_rden;
  wire slv_reg_wren__2;
  wire x_mod_0_i0;
  wire [2:0]\x_mod_0_i_reg[13]_0 ;
  wire [2:0]\x_mod_0_i_reg[13]_1 ;
  wire [3:0]\x_mod_0_i_reg[8]_0 ;
  wire [2:0]\x_mod_1_i_reg[13]_0 ;
  wire [2:0]\x_mod_1_i_reg[13]_1 ;
  wire [3:0]\x_mod_1_i_reg[8]_0 ;
  wire [3:0]\x_mod_1_i_reg[8]_1 ;
  wire [1:0]\x_mod_2_i_reg[12]_0 ;
  wire [0:0]\x_mod_2_i_reg[13]_0 ;
  wire [2:0]\x_mod_2_i_reg[13]_1 ;
  wire [3:0]\x_mod_2_i_reg[8]_0 ;
  wire [1:0]\x_mod_3_i_reg[12]_0 ;
  wire [0:0]\x_mod_3_i_reg[13]_0 ;
  wire [2:0]\x_mod_3_i_reg[13]_1 ;
  wire [3:0]\x_mod_3_i_reg[8]_0 ;
  wire [3:0]\x_mod_3_i_reg[8]_1 ;
  wire [1:0]\x_mod_4_i_reg[12]_0 ;
  wire [0:0]\x_mod_4_i_reg[13]_0 ;
  wire [2:0]\x_mod_4_i_reg[13]_1 ;
  wire [3:0]\x_mod_4_i_reg[8]_0 ;
  wire [3:0]\x_mod_4_i_reg[8]_1 ;
  wire [1:0]\x_mod_5_i_reg[12]_0 ;
  wire [0:0]\x_mod_5_i_reg[13]_0 ;
  wire [2:0]\x_mod_5_i_reg[13]_1 ;
  wire [3:0]\x_mod_5_i_reg[8]_0 ;
  wire [3:0]\x_mod_5_i_reg[8]_1 ;
  wire [1:0]\x_mod_6_i_reg[12]_0 ;
  wire [0:0]\x_mod_6_i_reg[13]_0 ;
  wire [2:0]\x_mod_6_i_reg[13]_1 ;
  wire [3:0]\x_mod_6_i_reg[8]_0 ;
  wire [3:0]\x_mod_6_i_reg[8]_1 ;
  wire [1:0]\x_mod_7_i_reg[12]_0 ;
  wire [0:0]\x_mod_7_i_reg[13]_0 ;
  wire [2:0]\x_mod_7_i_reg[13]_1 ;
  wire [3:0]\x_mod_7_i_reg[8]_0 ;
  wire [3:0]\x_mod_7_i_reg[8]_1 ;
  wire [1:0]\x_mod_8_i_reg[12]_0 ;
  wire [0:0]\x_mod_8_i_reg[13]_0 ;
  wire [2:0]\x_mod_8_i_reg[13]_1 ;
  wire [3:0]\x_mod_8_i_reg[8]_0 ;
  wire [3:0]\x_mod_8_i_reg[8]_1 ;
  wire [1:0]\x_mod_9_i_reg[12]_0 ;
  wire [0:0]\x_mod_9_i_reg[13]_0 ;
  wire [2:0]\x_mod_9_i_reg[13]_1 ;
  wire [3:0]\x_mod_9_i_reg[8]_0 ;
  wire [3:0]\x_mod_9_i_reg[8]_1 ;

  LUT6 #(
    .INIT(64'hBFFF8CCC8CCC8CCC)) 
    aw_en_i_1
       (.I0(S_AXI_AWREADY),
        .I1(aw_en_reg_n_0),
        .I2(s_axi_wvalid),
        .I3(s_axi_awvalid),
        .I4(s_axi_bready),
        .I5(s_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(p_0_in));
  FDSE \axi_araddr_reg[2] 
       (.C(s_axi_aclk),
        .CE(axi_arready0),
        .D(s_axi_araddr[0]),
        .Q(sel0[0]),
        .S(p_0_in));
  FDSE \axi_araddr_reg[3] 
       (.C(s_axi_aclk),
        .CE(axi_arready0),
        .D(s_axi_araddr[1]),
        .Q(sel0[1]),
        .S(p_0_in));
  FDSE \axi_araddr_reg[4] 
       (.C(s_axi_aclk),
        .CE(axi_arready0),
        .D(s_axi_araddr[2]),
        .Q(sel0[2]),
        .S(p_0_in));
  FDSE \axi_araddr_reg[5] 
       (.C(s_axi_aclk),
        .CE(axi_arready0),
        .D(s_axi_araddr[3]),
        .Q(sel0[3]),
        .S(p_0_in));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[2] 
       (.C(s_axi_aclk),
        .CE(axi_awready0),
        .D(s_axi_awaddr[0]),
        .Q(p_0_in__0[0]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[3] 
       (.C(s_axi_aclk),
        .CE(axi_awready0),
        .D(s_axi_awaddr[1]),
        .Q(p_0_in__0[1]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[4] 
       (.C(s_axi_aclk),
        .CE(axi_awready0),
        .D(s_axi_awaddr[2]),
        .Q(p_0_in__0[2]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[5] 
       (.C(s_axi_aclk),
        .CE(axi_awready0),
        .D(s_axi_awaddr[3]),
        .Q(p_0_in__0[3]),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s_axi_aresetn),
        .O(p_0_in));
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(s_axi_awvalid),
        .I1(s_axi_wvalid),
        .I2(aw_en_reg_n_0),
        .I3(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s_axi_wvalid),
        .I4(s_axi_bready),
        .I5(s_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s_axi_bvalid),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[0]_i_1 
       (.I0(slv_reg8[0]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[0]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[0]_i_2_n_0 ),
        .O(reg_data_out__0[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_3 
       (.I0(slv_reg3[0]),
        .I1(slv_reg2[0]),
        .I2(sel0[1]),
        .I3(slv_reg1[0]),
        .I4(sel0[0]),
        .I5(slv_reg0[0]),
        .O(\axi_rdata[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_4 
       (.I0(slv_reg7[0]),
        .I1(slv_reg6[0]),
        .I2(sel0[1]),
        .I3(slv_reg5[0]),
        .I4(sel0[0]),
        .I5(slv_reg4[0]),
        .O(\axi_rdata[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[10]_i_1 
       (.I0(slv_reg8[10]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[10]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[10]_i_2_n_0 ),
        .O(reg_data_out__0[10]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_3 
       (.I0(slv_reg3[10]),
        .I1(slv_reg2[10]),
        .I2(sel0[1]),
        .I3(slv_reg1[10]),
        .I4(sel0[0]),
        .I5(slv_reg0[10]),
        .O(\axi_rdata[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_4 
       (.I0(slv_reg7[10]),
        .I1(slv_reg6[10]),
        .I2(sel0[1]),
        .I3(slv_reg5[10]),
        .I4(sel0[0]),
        .I5(slv_reg4[10]),
        .O(\axi_rdata[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[11]_i_1 
       (.I0(slv_reg8[11]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[11]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[11]_i_2_n_0 ),
        .O(reg_data_out__0[11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_3 
       (.I0(slv_reg3[11]),
        .I1(slv_reg2[11]),
        .I2(sel0[1]),
        .I3(slv_reg1[11]),
        .I4(sel0[0]),
        .I5(slv_reg0[11]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_4 
       (.I0(slv_reg7[11]),
        .I1(slv_reg6[11]),
        .I2(sel0[1]),
        .I3(slv_reg5[11]),
        .I4(sel0[0]),
        .I5(slv_reg4[11]),
        .O(\axi_rdata[11]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[12]_i_1 
       (.I0(slv_reg8[12]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[12]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[12]_i_2_n_0 ),
        .O(reg_data_out__0[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_3 
       (.I0(slv_reg3[12]),
        .I1(slv_reg2[12]),
        .I2(sel0[1]),
        .I3(slv_reg1[12]),
        .I4(sel0[0]),
        .I5(slv_reg0[12]),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_4 
       (.I0(slv_reg7[12]),
        .I1(slv_reg6[12]),
        .I2(sel0[1]),
        .I3(slv_reg5[12]),
        .I4(sel0[0]),
        .I5(slv_reg4[12]),
        .O(\axi_rdata[12]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[13]_i_1 
       (.I0(slv_reg8[13]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[13]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[13]_i_2_n_0 ),
        .O(reg_data_out__0[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_3 
       (.I0(slv_reg3[13]),
        .I1(slv_reg2[13]),
        .I2(sel0[1]),
        .I3(slv_reg1[13]),
        .I4(sel0[0]),
        .I5(slv_reg0[13]),
        .O(\axi_rdata[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_4 
       (.I0(slv_reg7[13]),
        .I1(slv_reg6[13]),
        .I2(sel0[1]),
        .I3(slv_reg5[13]),
        .I4(sel0[0]),
        .I5(slv_reg4[13]),
        .O(\axi_rdata[13]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[14]_i_1 
       (.I0(slv_reg8[14]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[14]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[14]_i_2_n_0 ),
        .O(reg_data_out__0[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_3 
       (.I0(slv_reg3[14]),
        .I1(slv_reg2[14]),
        .I2(sel0[1]),
        .I3(slv_reg1[14]),
        .I4(sel0[0]),
        .I5(slv_reg0[14]),
        .O(\axi_rdata[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_4 
       (.I0(slv_reg7[14]),
        .I1(slv_reg6[14]),
        .I2(sel0[1]),
        .I3(slv_reg5[14]),
        .I4(sel0[0]),
        .I5(slv_reg4[14]),
        .O(\axi_rdata[14]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[15]_i_1 
       (.I0(slv_reg8[15]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[15]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[15]_i_2_n_0 ),
        .O(reg_data_out__0[15]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_3 
       (.I0(slv_reg3[15]),
        .I1(slv_reg2[15]),
        .I2(sel0[1]),
        .I3(slv_reg1[15]),
        .I4(sel0[0]),
        .I5(slv_reg0[15]),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_4 
       (.I0(slv_reg7[15]),
        .I1(slv_reg6[15]),
        .I2(sel0[1]),
        .I3(slv_reg5[15]),
        .I4(sel0[0]),
        .I5(slv_reg4[15]),
        .O(\axi_rdata[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[16]_i_1 
       (.I0(slv_reg8[16]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[16]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[16]_i_2_n_0 ),
        .O(reg_data_out__0[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_3 
       (.I0(slv_reg3[16]),
        .I1(slv_reg2[16]),
        .I2(sel0[1]),
        .I3(slv_reg1[16]),
        .I4(sel0[0]),
        .I5(slv_reg0[16]),
        .O(\axi_rdata[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_4 
       (.I0(slv_reg7[16]),
        .I1(slv_reg6[16]),
        .I2(sel0[1]),
        .I3(slv_reg5[16]),
        .I4(sel0[0]),
        .I5(slv_reg4[16]),
        .O(\axi_rdata[16]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[17]_i_1 
       (.I0(slv_reg8[17]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[17]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[17]_i_2_n_0 ),
        .O(reg_data_out__0[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_3 
       (.I0(slv_reg3[17]),
        .I1(slv_reg2[17]),
        .I2(sel0[1]),
        .I3(slv_reg1[17]),
        .I4(sel0[0]),
        .I5(slv_reg0[17]),
        .O(\axi_rdata[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_4 
       (.I0(slv_reg7[17]),
        .I1(slv_reg6[17]),
        .I2(sel0[1]),
        .I3(slv_reg5[17]),
        .I4(sel0[0]),
        .I5(slv_reg4[17]),
        .O(\axi_rdata[17]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[18]_i_1 
       (.I0(slv_reg8[18]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[18]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[18]_i_2_n_0 ),
        .O(reg_data_out__0[18]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_3 
       (.I0(slv_reg3[18]),
        .I1(slv_reg2[18]),
        .I2(sel0[1]),
        .I3(slv_reg1[18]),
        .I4(sel0[0]),
        .I5(slv_reg0[18]),
        .O(\axi_rdata[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_4 
       (.I0(slv_reg7[18]),
        .I1(slv_reg6[18]),
        .I2(sel0[1]),
        .I3(slv_reg5[18]),
        .I4(sel0[0]),
        .I5(slv_reg4[18]),
        .O(\axi_rdata[18]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[19]_i_1 
       (.I0(slv_reg8[19]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[19]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[19]_i_2_n_0 ),
        .O(reg_data_out__0[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_3 
       (.I0(slv_reg3[19]),
        .I1(slv_reg2[19]),
        .I2(sel0[1]),
        .I3(slv_reg1[19]),
        .I4(sel0[0]),
        .I5(slv_reg0[19]),
        .O(\axi_rdata[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_4 
       (.I0(slv_reg7[19]),
        .I1(slv_reg6[19]),
        .I2(sel0[1]),
        .I3(slv_reg5[19]),
        .I4(sel0[0]),
        .I5(slv_reg4[19]),
        .O(\axi_rdata[19]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[1]_i_1 
       (.I0(slv_reg8[1]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[1]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[1]_i_2_n_0 ),
        .O(reg_data_out__0[1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_3 
       (.I0(slv_reg3[1]),
        .I1(slv_reg2[1]),
        .I2(sel0[1]),
        .I3(slv_reg1[1]),
        .I4(sel0[0]),
        .I5(slv_reg0[1]),
        .O(\axi_rdata[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_4 
       (.I0(slv_reg7[1]),
        .I1(slv_reg6[1]),
        .I2(sel0[1]),
        .I3(slv_reg5[1]),
        .I4(sel0[0]),
        .I5(slv_reg4[1]),
        .O(\axi_rdata[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[20]_i_1 
       (.I0(slv_reg8[20]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[20]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[20]_i_2_n_0 ),
        .O(reg_data_out__0[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_3 
       (.I0(slv_reg3[20]),
        .I1(slv_reg2[20]),
        .I2(sel0[1]),
        .I3(slv_reg1[20]),
        .I4(sel0[0]),
        .I5(slv_reg0[20]),
        .O(\axi_rdata[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_4 
       (.I0(slv_reg7[20]),
        .I1(slv_reg6[20]),
        .I2(sel0[1]),
        .I3(slv_reg5[20]),
        .I4(sel0[0]),
        .I5(slv_reg4[20]),
        .O(\axi_rdata[20]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[21]_i_1 
       (.I0(slv_reg8[21]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[21]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[21]_i_2_n_0 ),
        .O(reg_data_out__0[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_3 
       (.I0(slv_reg3[21]),
        .I1(slv_reg2[21]),
        .I2(sel0[1]),
        .I3(slv_reg1[21]),
        .I4(sel0[0]),
        .I5(slv_reg0[21]),
        .O(\axi_rdata[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_4 
       (.I0(slv_reg7[21]),
        .I1(slv_reg6[21]),
        .I2(sel0[1]),
        .I3(slv_reg5[21]),
        .I4(sel0[0]),
        .I5(slv_reg4[21]),
        .O(\axi_rdata[21]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[22]_i_1 
       (.I0(slv_reg8[22]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[22]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[22]_i_2_n_0 ),
        .O(reg_data_out__0[22]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_3 
       (.I0(slv_reg3[22]),
        .I1(slv_reg2[22]),
        .I2(sel0[1]),
        .I3(slv_reg1[22]),
        .I4(sel0[0]),
        .I5(slv_reg0[22]),
        .O(\axi_rdata[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_4 
       (.I0(slv_reg7[22]),
        .I1(slv_reg6[22]),
        .I2(sel0[1]),
        .I3(slv_reg5[22]),
        .I4(sel0[0]),
        .I5(slv_reg4[22]),
        .O(\axi_rdata[22]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[23]_i_1 
       (.I0(slv_reg8[23]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[23]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[23]_i_2_n_0 ),
        .O(reg_data_out__0[23]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_3 
       (.I0(slv_reg3[23]),
        .I1(slv_reg2[23]),
        .I2(sel0[1]),
        .I3(slv_reg1[23]),
        .I4(sel0[0]),
        .I5(slv_reg0[23]),
        .O(\axi_rdata[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_4 
       (.I0(slv_reg7[23]),
        .I1(slv_reg6[23]),
        .I2(sel0[1]),
        .I3(slv_reg5[23]),
        .I4(sel0[0]),
        .I5(slv_reg4[23]),
        .O(\axi_rdata[23]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[24]_i_1 
       (.I0(slv_reg8[24]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[24]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[24]_i_2_n_0 ),
        .O(reg_data_out__0[24]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_3 
       (.I0(slv_reg3[24]),
        .I1(slv_reg2[24]),
        .I2(sel0[1]),
        .I3(slv_reg1[24]),
        .I4(sel0[0]),
        .I5(slv_reg0[24]),
        .O(\axi_rdata[24]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_4 
       (.I0(slv_reg7[24]),
        .I1(slv_reg6[24]),
        .I2(sel0[1]),
        .I3(slv_reg5[24]),
        .I4(sel0[0]),
        .I5(slv_reg4[24]),
        .O(\axi_rdata[24]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[25]_i_1 
       (.I0(slv_reg8[25]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[25]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[25]_i_2_n_0 ),
        .O(reg_data_out__0[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_3 
       (.I0(slv_reg3[25]),
        .I1(slv_reg2[25]),
        .I2(sel0[1]),
        .I3(slv_reg1[25]),
        .I4(sel0[0]),
        .I5(slv_reg0[25]),
        .O(\axi_rdata[25]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_4 
       (.I0(slv_reg7[25]),
        .I1(slv_reg6[25]),
        .I2(sel0[1]),
        .I3(slv_reg5[25]),
        .I4(sel0[0]),
        .I5(slv_reg4[25]),
        .O(\axi_rdata[25]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[26]_i_1 
       (.I0(slv_reg8[26]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[26]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[26]_i_2_n_0 ),
        .O(reg_data_out__0[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_3 
       (.I0(slv_reg3[26]),
        .I1(slv_reg2[26]),
        .I2(sel0[1]),
        .I3(slv_reg1[26]),
        .I4(sel0[0]),
        .I5(slv_reg0[26]),
        .O(\axi_rdata[26]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_4 
       (.I0(slv_reg7[26]),
        .I1(slv_reg6[26]),
        .I2(sel0[1]),
        .I3(slv_reg5[26]),
        .I4(sel0[0]),
        .I5(slv_reg4[26]),
        .O(\axi_rdata[26]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[27]_i_1 
       (.I0(slv_reg8[27]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[27]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[27]_i_2_n_0 ),
        .O(reg_data_out__0[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_3 
       (.I0(slv_reg3[27]),
        .I1(slv_reg2[27]),
        .I2(sel0[1]),
        .I3(slv_reg1[27]),
        .I4(sel0[0]),
        .I5(slv_reg0[27]),
        .O(\axi_rdata[27]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_4 
       (.I0(slv_reg7[27]),
        .I1(slv_reg6[27]),
        .I2(sel0[1]),
        .I3(slv_reg5[27]),
        .I4(sel0[0]),
        .I5(slv_reg4[27]),
        .O(\axi_rdata[27]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[28]_i_1 
       (.I0(slv_reg8[28]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[28]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[28]_i_2_n_0 ),
        .O(reg_data_out__0[28]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_3 
       (.I0(slv_reg3[28]),
        .I1(slv_reg2[28]),
        .I2(sel0[1]),
        .I3(slv_reg1[28]),
        .I4(sel0[0]),
        .I5(slv_reg0[28]),
        .O(\axi_rdata[28]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_4 
       (.I0(slv_reg7[28]),
        .I1(slv_reg6[28]),
        .I2(sel0[1]),
        .I3(slv_reg5[28]),
        .I4(sel0[0]),
        .I5(slv_reg4[28]),
        .O(\axi_rdata[28]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[29]_i_1 
       (.I0(slv_reg8[29]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[29]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[29]_i_2_n_0 ),
        .O(reg_data_out__0[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_3 
       (.I0(slv_reg3[29]),
        .I1(slv_reg2[29]),
        .I2(sel0[1]),
        .I3(slv_reg1[29]),
        .I4(sel0[0]),
        .I5(slv_reg0[29]),
        .O(\axi_rdata[29]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_4 
       (.I0(slv_reg7[29]),
        .I1(slv_reg6[29]),
        .I2(sel0[1]),
        .I3(slv_reg5[29]),
        .I4(sel0[0]),
        .I5(slv_reg4[29]),
        .O(\axi_rdata[29]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[2]_i_1 
       (.I0(slv_reg8[2]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[2]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[2]_i_2_n_0 ),
        .O(reg_data_out__0[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_3 
       (.I0(slv_reg3[2]),
        .I1(slv_reg2[2]),
        .I2(sel0[1]),
        .I3(slv_reg1[2]),
        .I4(sel0[0]),
        .I5(slv_reg0[2]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_4 
       (.I0(slv_reg7[2]),
        .I1(slv_reg6[2]),
        .I2(sel0[1]),
        .I3(slv_reg5[2]),
        .I4(sel0[0]),
        .I5(slv_reg4[2]),
        .O(\axi_rdata[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[30]_i_1 
       (.I0(slv_reg8[30]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[30]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[30]_i_2_n_0 ),
        .O(reg_data_out__0[30]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_3 
       (.I0(slv_reg3[30]),
        .I1(slv_reg2[30]),
        .I2(sel0[1]),
        .I3(slv_reg1[30]),
        .I4(sel0[0]),
        .I5(slv_reg0[30]),
        .O(\axi_rdata[30]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_4 
       (.I0(slv_reg7[30]),
        .I1(slv_reg6[30]),
        .I2(sel0[1]),
        .I3(slv_reg5[30]),
        .I4(sel0[0]),
        .I5(slv_reg4[30]),
        .O(\axi_rdata[30]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(S_AXI_ARREADY),
        .I1(s_axi_arvalid),
        .I2(s_axi_rvalid),
        .O(slv_reg_rden));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[31]_i_2 
       (.I0(slv_reg8[31]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[31]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[31]_i_5_n_0 ),
        .O(reg_data_out__0[31]));
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[31]_i_3 
       (.I0(sel0[2]),
        .I1(sel0[1]),
        .I2(sel0[0]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \axi_rdata[31]_i_4 
       (.I0(sel0[1]),
        .I1(sel0[2]),
        .O(\axi_rdata[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_6 
       (.I0(slv_reg3[31]),
        .I1(slv_reg2[31]),
        .I2(sel0[1]),
        .I3(slv_reg1[31]),
        .I4(sel0[0]),
        .I5(slv_reg0[31]),
        .O(\axi_rdata[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_7 
       (.I0(slv_reg7[31]),
        .I1(slv_reg6[31]),
        .I2(sel0[1]),
        .I3(slv_reg5[31]),
        .I4(sel0[0]),
        .I5(slv_reg4[31]),
        .O(\axi_rdata[31]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[3]_i_1 
       (.I0(slv_reg8[3]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[3]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[3]_i_2_n_0 ),
        .O(reg_data_out__0[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_3 
       (.I0(slv_reg3[3]),
        .I1(slv_reg2[3]),
        .I2(sel0[1]),
        .I3(slv_reg1[3]),
        .I4(sel0[0]),
        .I5(slv_reg0[3]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_4 
       (.I0(slv_reg7[3]),
        .I1(slv_reg6[3]),
        .I2(sel0[1]),
        .I3(slv_reg5[3]),
        .I4(sel0[0]),
        .I5(slv_reg4[3]),
        .O(\axi_rdata[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[4]_i_1 
       (.I0(slv_reg8[4]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[4]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[4]_i_2_n_0 ),
        .O(reg_data_out__0[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_3 
       (.I0(slv_reg3[4]),
        .I1(slv_reg2[4]),
        .I2(sel0[1]),
        .I3(slv_reg1[4]),
        .I4(sel0[0]),
        .I5(slv_reg0[4]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_4 
       (.I0(slv_reg7[4]),
        .I1(slv_reg6[4]),
        .I2(sel0[1]),
        .I3(slv_reg5[4]),
        .I4(sel0[0]),
        .I5(slv_reg4[4]),
        .O(\axi_rdata[4]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[5]_i_1 
       (.I0(slv_reg8[5]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[5]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[5]_i_2_n_0 ),
        .O(reg_data_out__0[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_3 
       (.I0(slv_reg3[5]),
        .I1(slv_reg2[5]),
        .I2(sel0[1]),
        .I3(slv_reg1[5]),
        .I4(sel0[0]),
        .I5(slv_reg0[5]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_4 
       (.I0(slv_reg7[5]),
        .I1(slv_reg6[5]),
        .I2(sel0[1]),
        .I3(slv_reg5[5]),
        .I4(sel0[0]),
        .I5(slv_reg4[5]),
        .O(\axi_rdata[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[6]_i_1 
       (.I0(slv_reg8[6]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[6]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[6]_i_2_n_0 ),
        .O(reg_data_out__0[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_3 
       (.I0(slv_reg3[6]),
        .I1(slv_reg2[6]),
        .I2(sel0[1]),
        .I3(slv_reg1[6]),
        .I4(sel0[0]),
        .I5(slv_reg0[6]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_4 
       (.I0(slv_reg7[6]),
        .I1(slv_reg6[6]),
        .I2(sel0[1]),
        .I3(slv_reg5[6]),
        .I4(sel0[0]),
        .I5(slv_reg4[6]),
        .O(\axi_rdata[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[7]_i_1 
       (.I0(slv_reg8[7]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[7]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[7]_i_2_n_0 ),
        .O(reg_data_out__0[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_3 
       (.I0(slv_reg3[7]),
        .I1(slv_reg2[7]),
        .I2(sel0[1]),
        .I3(slv_reg1[7]),
        .I4(sel0[0]),
        .I5(slv_reg0[7]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_4 
       (.I0(slv_reg7[7]),
        .I1(slv_reg6[7]),
        .I2(sel0[1]),
        .I3(slv_reg5[7]),
        .I4(sel0[0]),
        .I5(slv_reg4[7]),
        .O(\axi_rdata[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[8]_i_1 
       (.I0(slv_reg8[8]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[8]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[8]_i_2_n_0 ),
        .O(reg_data_out__0[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_3 
       (.I0(slv_reg3[8]),
        .I1(slv_reg2[8]),
        .I2(sel0[1]),
        .I3(slv_reg1[8]),
        .I4(sel0[0]),
        .I5(slv_reg0[8]),
        .O(\axi_rdata[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_4 
       (.I0(slv_reg7[8]),
        .I1(slv_reg6[8]),
        .I2(sel0[1]),
        .I3(slv_reg5[8]),
        .I4(sel0[0]),
        .I5(slv_reg4[8]),
        .O(\axi_rdata[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[9]_i_1 
       (.I0(slv_reg8[9]),
        .I1(\axi_rdata[31]_i_3_n_0 ),
        .I2(slv_reg9[9]),
        .I3(\axi_rdata[31]_i_4_n_0 ),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[9]_i_2_n_0 ),
        .O(reg_data_out__0[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_3 
       (.I0(slv_reg3[9]),
        .I1(slv_reg2[9]),
        .I2(sel0[1]),
        .I3(slv_reg1[9]),
        .I4(sel0[0]),
        .I5(slv_reg0[9]),
        .O(\axi_rdata[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_4 
       (.I0(slv_reg7[9]),
        .I1(slv_reg6[9]),
        .I2(sel0[1]),
        .I3(slv_reg5[9]),
        .I4(sel0[0]),
        .I5(slv_reg4[9]),
        .O(\axi_rdata[9]_i_4_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[0]),
        .Q(s_axi_rdata[0]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[0]_i_2 
       (.I0(\axi_rdata[0]_i_3_n_0 ),
        .I1(\axi_rdata[0]_i_4_n_0 ),
        .O(\axi_rdata_reg[0]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[10] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[10]),
        .Q(s_axi_rdata[10]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[10]_i_2 
       (.I0(\axi_rdata[10]_i_3_n_0 ),
        .I1(\axi_rdata[10]_i_4_n_0 ),
        .O(\axi_rdata_reg[10]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[11] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[11]),
        .Q(s_axi_rdata[11]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[11]_i_2 
       (.I0(\axi_rdata[11]_i_3_n_0 ),
        .I1(\axi_rdata[11]_i_4_n_0 ),
        .O(\axi_rdata_reg[11]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[12] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[12]),
        .Q(s_axi_rdata[12]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[12]_i_2 
       (.I0(\axi_rdata[12]_i_3_n_0 ),
        .I1(\axi_rdata[12]_i_4_n_0 ),
        .O(\axi_rdata_reg[12]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[13] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[13]),
        .Q(s_axi_rdata[13]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[13]_i_2 
       (.I0(\axi_rdata[13]_i_3_n_0 ),
        .I1(\axi_rdata[13]_i_4_n_0 ),
        .O(\axi_rdata_reg[13]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[14] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[14]),
        .Q(s_axi_rdata[14]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[14]_i_2 
       (.I0(\axi_rdata[14]_i_3_n_0 ),
        .I1(\axi_rdata[14]_i_4_n_0 ),
        .O(\axi_rdata_reg[14]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[15] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[15]),
        .Q(s_axi_rdata[15]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[15]_i_2 
       (.I0(\axi_rdata[15]_i_3_n_0 ),
        .I1(\axi_rdata[15]_i_4_n_0 ),
        .O(\axi_rdata_reg[15]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[16] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[16]),
        .Q(s_axi_rdata[16]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[16]_i_2 
       (.I0(\axi_rdata[16]_i_3_n_0 ),
        .I1(\axi_rdata[16]_i_4_n_0 ),
        .O(\axi_rdata_reg[16]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[17] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[17]),
        .Q(s_axi_rdata[17]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[17]_i_2 
       (.I0(\axi_rdata[17]_i_3_n_0 ),
        .I1(\axi_rdata[17]_i_4_n_0 ),
        .O(\axi_rdata_reg[17]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[18] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[18]),
        .Q(s_axi_rdata[18]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[18]_i_2 
       (.I0(\axi_rdata[18]_i_3_n_0 ),
        .I1(\axi_rdata[18]_i_4_n_0 ),
        .O(\axi_rdata_reg[18]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[19] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[19]),
        .Q(s_axi_rdata[19]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[19]_i_2 
       (.I0(\axi_rdata[19]_i_3_n_0 ),
        .I1(\axi_rdata[19]_i_4_n_0 ),
        .O(\axi_rdata_reg[19]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[1] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[1]),
        .Q(s_axi_rdata[1]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[1]_i_2 
       (.I0(\axi_rdata[1]_i_3_n_0 ),
        .I1(\axi_rdata[1]_i_4_n_0 ),
        .O(\axi_rdata_reg[1]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[20] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[20]),
        .Q(s_axi_rdata[20]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[20]_i_2 
       (.I0(\axi_rdata[20]_i_3_n_0 ),
        .I1(\axi_rdata[20]_i_4_n_0 ),
        .O(\axi_rdata_reg[20]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[21] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[21]),
        .Q(s_axi_rdata[21]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[21]_i_2 
       (.I0(\axi_rdata[21]_i_3_n_0 ),
        .I1(\axi_rdata[21]_i_4_n_0 ),
        .O(\axi_rdata_reg[21]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[22] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[22]),
        .Q(s_axi_rdata[22]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[22]_i_2 
       (.I0(\axi_rdata[22]_i_3_n_0 ),
        .I1(\axi_rdata[22]_i_4_n_0 ),
        .O(\axi_rdata_reg[22]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[23] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[23]),
        .Q(s_axi_rdata[23]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[23]_i_2 
       (.I0(\axi_rdata[23]_i_3_n_0 ),
        .I1(\axi_rdata[23]_i_4_n_0 ),
        .O(\axi_rdata_reg[23]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[24] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[24]),
        .Q(s_axi_rdata[24]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[24]_i_2 
       (.I0(\axi_rdata[24]_i_3_n_0 ),
        .I1(\axi_rdata[24]_i_4_n_0 ),
        .O(\axi_rdata_reg[24]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[25] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[25]),
        .Q(s_axi_rdata[25]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[25]_i_2 
       (.I0(\axi_rdata[25]_i_3_n_0 ),
        .I1(\axi_rdata[25]_i_4_n_0 ),
        .O(\axi_rdata_reg[25]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[26] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[26]),
        .Q(s_axi_rdata[26]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[26]_i_2 
       (.I0(\axi_rdata[26]_i_3_n_0 ),
        .I1(\axi_rdata[26]_i_4_n_0 ),
        .O(\axi_rdata_reg[26]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[27] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[27]),
        .Q(s_axi_rdata[27]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[27]_i_2 
       (.I0(\axi_rdata[27]_i_3_n_0 ),
        .I1(\axi_rdata[27]_i_4_n_0 ),
        .O(\axi_rdata_reg[27]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[28] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[28]),
        .Q(s_axi_rdata[28]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[28]_i_2 
       (.I0(\axi_rdata[28]_i_3_n_0 ),
        .I1(\axi_rdata[28]_i_4_n_0 ),
        .O(\axi_rdata_reg[28]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[29] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[29]),
        .Q(s_axi_rdata[29]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[29]_i_2 
       (.I0(\axi_rdata[29]_i_3_n_0 ),
        .I1(\axi_rdata[29]_i_4_n_0 ),
        .O(\axi_rdata_reg[29]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[2] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[2]),
        .Q(s_axi_rdata[2]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[2]_i_2 
       (.I0(\axi_rdata[2]_i_3_n_0 ),
        .I1(\axi_rdata[2]_i_4_n_0 ),
        .O(\axi_rdata_reg[2]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[30] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[30]),
        .Q(s_axi_rdata[30]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[30]_i_2 
       (.I0(\axi_rdata[30]_i_3_n_0 ),
        .I1(\axi_rdata[30]_i_4_n_0 ),
        .O(\axi_rdata_reg[30]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[31] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[31]),
        .Q(s_axi_rdata[31]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[31]_i_5 
       (.I0(\axi_rdata[31]_i_6_n_0 ),
        .I1(\axi_rdata[31]_i_7_n_0 ),
        .O(\axi_rdata_reg[31]_i_5_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[3] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[3]),
        .Q(s_axi_rdata[3]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[3]_i_2 
       (.I0(\axi_rdata[3]_i_3_n_0 ),
        .I1(\axi_rdata[3]_i_4_n_0 ),
        .O(\axi_rdata_reg[3]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[4] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[4]),
        .Q(s_axi_rdata[4]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[4]_i_2 
       (.I0(\axi_rdata[4]_i_3_n_0 ),
        .I1(\axi_rdata[4]_i_4_n_0 ),
        .O(\axi_rdata_reg[4]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[5] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[5]),
        .Q(s_axi_rdata[5]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[5]_i_2 
       (.I0(\axi_rdata[5]_i_3_n_0 ),
        .I1(\axi_rdata[5]_i_4_n_0 ),
        .O(\axi_rdata_reg[5]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[6] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[6]),
        .Q(s_axi_rdata[6]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[6]_i_2 
       (.I0(\axi_rdata[6]_i_3_n_0 ),
        .I1(\axi_rdata[6]_i_4_n_0 ),
        .O(\axi_rdata_reg[6]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[7] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[7]),
        .Q(s_axi_rdata[7]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[7]_i_2 
       (.I0(\axi_rdata[7]_i_3_n_0 ),
        .I1(\axi_rdata[7]_i_4_n_0 ),
        .O(\axi_rdata_reg[7]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[8] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[8]),
        .Q(s_axi_rdata[8]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[8]_i_2 
       (.I0(\axi_rdata[8]_i_3_n_0 ),
        .I1(\axi_rdata[8]_i_4_n_0 ),
        .O(\axi_rdata_reg[8]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[9] 
       (.C(s_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out__0[9]),
        .Q(s_axi_rdata[9]),
        .R(p_0_in));
  MUXF7 \axi_rdata_reg[9]_i_2 
       (.I0(\axi_rdata[9]_i_3_n_0 ),
        .I1(\axi_rdata[9]_i_4_n_0 ),
        .O(\axi_rdata_reg[9]_i_2_n_0 ),
        .S(sel0[2]));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s_axi_rvalid),
        .I3(s_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s_axi_rvalid),
        .R(p_0_in));
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(s_axi_awvalid),
        .I1(s_axi_wvalid),
        .I2(aw_en_reg_n_0),
        .I3(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(p_0_in));
  LUT2 #(
    .INIT(4'h2)) 
    axi_wvalid_falling_edge_i_1
       (.I0(past_axi_wvalid),
        .I1(s_axi_wvalid),
        .O(axi_wvalid_falling_edge0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    axi_wvalid_falling_edge_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_wvalid_falling_edge0),
        .Q(axi_wvalid_falling_edge),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1
       (.I0(X_MOD_1[12]),
        .I1(counter_phase90_output[12]),
        .O(\x_mod_1_i_reg[13]_0 [2]));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__0
       (.I0(X_MOD_2[12]),
        .I1(q[12]),
        .O(\x_mod_2_i_reg[13]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__1
       (.I0(X_MOD_3[12]),
        .I1(counter_phase90_output[12]),
        .O(\x_mod_3_i_reg[13]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__10
       (.I0(X_MOD_3[12]),
        .I1(counter_phase270_output[12]),
        .O(\x_mod_3_i_reg[13]_1 [2]));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__11
       (.I0(X_MOD_4[12]),
        .I1(counter_phase180_output[12]),
        .O(\x_mod_4_i_reg[13]_1 [2]));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__12
       (.I0(X_MOD_5[12]),
        .I1(counter_phase270_output[12]),
        .O(\x_mod_5_i_reg[13]_1 [2]));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__13
       (.I0(X_MOD_6[12]),
        .I1(counter_phase180_output[12]),
        .O(\x_mod_6_i_reg[13]_1 [2]));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__14
       (.I0(X_MOD_7[12]),
        .I1(counter_phase270_output[12]),
        .O(\x_mod_7_i_reg[13]_1 [2]));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__15
       (.I0(X_MOD_8[12]),
        .I1(counter_phase180_output[12]),
        .O(\x_mod_8_i_reg[13]_1 [2]));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__16
       (.I0(X_MOD_9[12]),
        .I1(counter_phase270_output[12]),
        .O(\x_mod_9_i_reg[13]_1 [2]));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__2
       (.I0(X_MOD_4[12]),
        .I1(q[12]),
        .O(\x_mod_4_i_reg[13]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__3
       (.I0(X_MOD_5[12]),
        .I1(counter_phase90_output[12]),
        .O(\x_mod_5_i_reg[13]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__4
       (.I0(X_MOD_6[12]),
        .I1(q[12]),
        .O(\x_mod_6_i_reg[13]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__5
       (.I0(X_MOD_7[12]),
        .I1(counter_phase90_output[12]),
        .O(\x_mod_7_i_reg[13]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__6
       (.I0(X_MOD_8[12]),
        .I1(q[12]),
        .O(\x_mod_8_i_reg[13]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__7
       (.I0(X_MOD_9[12]),
        .I1(counter_phase90_output[12]),
        .O(\x_mod_9_i_reg[13]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__8
       (.I0(X_MOD_1[12]),
        .I1(counter_phase270_output[12]),
        .O(\x_mod_1_i_reg[13]_1 [2]));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry__0_i_1__9
       (.I0(X_MOD_2[12]),
        .I1(counter_phase180_output[12]),
        .O(\x_mod_2_i_reg[13]_1 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_2
       (.I0(X_MOD_1[11]),
        .I1(counter_phase90_output[11]),
        .I2(X_MOD_1[10]),
        .I3(counter_phase90_output[10]),
        .O(\x_mod_1_i_reg[13]_0 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_2__10
       (.I0(X_MOD_3[11]),
        .I1(counter_phase270_output[11]),
        .I2(X_MOD_3[10]),
        .I3(counter_phase270_output[10]),
        .O(\x_mod_3_i_reg[13]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_2__11
       (.I0(X_MOD_4[11]),
        .I1(counter_phase180_output[11]),
        .I2(X_MOD_4[10]),
        .I3(counter_phase180_output[10]),
        .O(\x_mod_4_i_reg[13]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_2__12
       (.I0(X_MOD_5[11]),
        .I1(counter_phase270_output[11]),
        .I2(X_MOD_5[10]),
        .I3(counter_phase270_output[10]),
        .O(\x_mod_5_i_reg[13]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_2__13
       (.I0(X_MOD_6[11]),
        .I1(counter_phase180_output[11]),
        .I2(X_MOD_6[10]),
        .I3(counter_phase180_output[10]),
        .O(\x_mod_6_i_reg[13]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_2__14
       (.I0(X_MOD_7[11]),
        .I1(counter_phase270_output[11]),
        .I2(X_MOD_7[10]),
        .I3(counter_phase270_output[10]),
        .O(\x_mod_7_i_reg[13]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_2__15
       (.I0(X_MOD_8[11]),
        .I1(counter_phase180_output[11]),
        .I2(X_MOD_8[10]),
        .I3(counter_phase180_output[10]),
        .O(\x_mod_8_i_reg[13]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_2__16
       (.I0(X_MOD_9[11]),
        .I1(counter_phase270_output[11]),
        .I2(X_MOD_9[10]),
        .I3(counter_phase270_output[10]),
        .O(\x_mod_9_i_reg[13]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_2__8
       (.I0(X_MOD_1[11]),
        .I1(counter_phase270_output[11]),
        .I2(X_MOD_1[10]),
        .I3(counter_phase270_output[10]),
        .O(\x_mod_1_i_reg[13]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_2__9
       (.I0(X_MOD_2[11]),
        .I1(counter_phase180_output[11]),
        .I2(X_MOD_2[10]),
        .I3(counter_phase180_output[10]),
        .O(\x_mod_2_i_reg[13]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_3
       (.I0(X_MOD_1[9]),
        .I1(counter_phase90_output[9]),
        .I2(X_MOD_1[8]),
        .I3(counter_phase90_output[8]),
        .O(\x_mod_1_i_reg[13]_0 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_3__10
       (.I0(X_MOD_3[9]),
        .I1(counter_phase270_output[9]),
        .I2(X_MOD_3[8]),
        .I3(counter_phase270_output[8]),
        .O(\x_mod_3_i_reg[13]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_3__11
       (.I0(X_MOD_4[9]),
        .I1(counter_phase180_output[9]),
        .I2(X_MOD_4[8]),
        .I3(counter_phase180_output[8]),
        .O(\x_mod_4_i_reg[13]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_3__12
       (.I0(X_MOD_5[9]),
        .I1(counter_phase270_output[9]),
        .I2(X_MOD_5[8]),
        .I3(counter_phase270_output[8]),
        .O(\x_mod_5_i_reg[13]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_3__13
       (.I0(X_MOD_6[9]),
        .I1(counter_phase180_output[9]),
        .I2(X_MOD_6[8]),
        .I3(counter_phase180_output[8]),
        .O(\x_mod_6_i_reg[13]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_3__14
       (.I0(X_MOD_7[9]),
        .I1(counter_phase270_output[9]),
        .I2(X_MOD_7[8]),
        .I3(counter_phase270_output[8]),
        .O(\x_mod_7_i_reg[13]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_3__15
       (.I0(X_MOD_8[9]),
        .I1(counter_phase180_output[9]),
        .I2(X_MOD_8[8]),
        .I3(counter_phase180_output[8]),
        .O(\x_mod_8_i_reg[13]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_3__16
       (.I0(X_MOD_9[9]),
        .I1(counter_phase270_output[9]),
        .I2(X_MOD_9[8]),
        .I3(counter_phase270_output[8]),
        .O(\x_mod_9_i_reg[13]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_3__8
       (.I0(X_MOD_1[9]),
        .I1(counter_phase270_output[9]),
        .I2(X_MOD_1[8]),
        .I3(counter_phase270_output[8]),
        .O(\x_mod_1_i_reg[13]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_3__9
       (.I0(X_MOD_2[9]),
        .I1(counter_phase180_output[9]),
        .I2(X_MOD_2[8]),
        .I3(counter_phase180_output[8]),
        .O(\x_mod_2_i_reg[13]_1 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__0
       (.I0(X_MOD_2[11]),
        .I1(q[11]),
        .I2(X_MOD_2[10]),
        .I3(q[10]),
        .O(\x_mod_2_i_reg[12]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__1
       (.I0(X_MOD_3[11]),
        .I1(counter_phase90_output[11]),
        .I2(X_MOD_3[10]),
        .I3(counter_phase90_output[10]),
        .O(\x_mod_3_i_reg[12]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__2
       (.I0(X_MOD_4[11]),
        .I1(q[11]),
        .I2(X_MOD_4[10]),
        .I3(q[10]),
        .O(\x_mod_4_i_reg[12]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__3
       (.I0(X_MOD_5[11]),
        .I1(counter_phase90_output[11]),
        .I2(X_MOD_5[10]),
        .I3(counter_phase90_output[10]),
        .O(\x_mod_5_i_reg[12]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__4
       (.I0(X_MOD_6[11]),
        .I1(q[11]),
        .I2(X_MOD_6[10]),
        .I3(q[10]),
        .O(\x_mod_6_i_reg[12]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__5
       (.I0(X_MOD_7[11]),
        .I1(counter_phase90_output[11]),
        .I2(X_MOD_7[10]),
        .I3(counter_phase90_output[10]),
        .O(\x_mod_7_i_reg[12]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__6
       (.I0(X_MOD_8[11]),
        .I1(q[11]),
        .I2(X_MOD_8[10]),
        .I3(q[10]),
        .O(\x_mod_8_i_reg[12]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5__7
       (.I0(X_MOD_9[11]),
        .I1(counter_phase90_output[11]),
        .I2(X_MOD_9[10]),
        .I3(counter_phase90_output[10]),
        .O(\x_mod_9_i_reg[12]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__0
       (.I0(X_MOD_2[9]),
        .I1(q[9]),
        .I2(X_MOD_2[8]),
        .I3(q[8]),
        .O(\x_mod_2_i_reg[12]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__1
       (.I0(X_MOD_3[9]),
        .I1(counter_phase90_output[9]),
        .I2(X_MOD_3[8]),
        .I3(counter_phase90_output[8]),
        .O(\x_mod_3_i_reg[12]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__2
       (.I0(X_MOD_4[9]),
        .I1(q[9]),
        .I2(X_MOD_4[8]),
        .I3(q[8]),
        .O(\x_mod_4_i_reg[12]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__3
       (.I0(X_MOD_5[9]),
        .I1(counter_phase90_output[9]),
        .I2(X_MOD_5[8]),
        .I3(counter_phase90_output[8]),
        .O(\x_mod_5_i_reg[12]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__4
       (.I0(X_MOD_6[9]),
        .I1(q[9]),
        .I2(X_MOD_6[8]),
        .I3(q[8]),
        .O(\x_mod_6_i_reg[12]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__5
       (.I0(X_MOD_7[9]),
        .I1(counter_phase90_output[9]),
        .I2(X_MOD_7[8]),
        .I3(counter_phase90_output[8]),
        .O(\x_mod_7_i_reg[12]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__6
       (.I0(X_MOD_8[9]),
        .I1(q[9]),
        .I2(X_MOD_8[8]),
        .I3(q[8]),
        .O(\x_mod_8_i_reg[12]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6__7
       (.I0(X_MOD_9[9]),
        .I1(counter_phase90_output[9]),
        .I2(X_MOD_9[8]),
        .I3(counter_phase90_output[8]),
        .O(\x_mod_9_i_reg[12]_0 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_1
       (.I0(X_MOD_1[7]),
        .I1(counter_phase90_output[7]),
        .I2(X_MOD_1[6]),
        .I3(counter_phase90_output[6]),
        .O(\x_mod_1_i_reg[8]_0 [3]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_1__10
       (.I0(X_MOD_3[7]),
        .I1(counter_phase270_output[7]),
        .I2(X_MOD_3[6]),
        .I3(counter_phase270_output[6]),
        .O(\x_mod_3_i_reg[8]_1 [3]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_1__11
       (.I0(X_MOD_4[7]),
        .I1(counter_phase180_output[7]),
        .I2(X_MOD_4[6]),
        .I3(counter_phase180_output[6]),
        .O(\x_mod_4_i_reg[8]_1 [3]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_1__12
       (.I0(X_MOD_5[7]),
        .I1(counter_phase270_output[7]),
        .I2(X_MOD_5[6]),
        .I3(counter_phase270_output[6]),
        .O(\x_mod_5_i_reg[8]_1 [3]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_1__13
       (.I0(X_MOD_6[7]),
        .I1(counter_phase180_output[7]),
        .I2(X_MOD_6[6]),
        .I3(counter_phase180_output[6]),
        .O(\x_mod_6_i_reg[8]_1 [3]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_1__14
       (.I0(X_MOD_7[7]),
        .I1(counter_phase270_output[7]),
        .I2(X_MOD_7[6]),
        .I3(counter_phase270_output[6]),
        .O(\x_mod_7_i_reg[8]_1 [3]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_1__15
       (.I0(X_MOD_8[7]),
        .I1(counter_phase180_output[7]),
        .I2(X_MOD_8[6]),
        .I3(counter_phase180_output[6]),
        .O(\x_mod_8_i_reg[8]_1 [3]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_1__16
       (.I0(X_MOD_9[7]),
        .I1(counter_phase270_output[7]),
        .I2(X_MOD_9[6]),
        .I3(counter_phase270_output[6]),
        .O(\x_mod_9_i_reg[8]_1 [3]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_1__8
       (.I0(X_MOD_1[7]),
        .I1(counter_phase270_output[7]),
        .I2(X_MOD_1[6]),
        .I3(counter_phase270_output[6]),
        .O(\x_mod_1_i_reg[8]_1 [3]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_1__9
       (.I0(X_MOD_2[7]),
        .I1(counter_phase180_output[7]),
        .I2(X_MOD_2[6]),
        .I3(counter_phase180_output[6]),
        .O(\x_mod_2_i_reg[8]_0 [3]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_2
       (.I0(X_MOD_1[5]),
        .I1(counter_phase90_output[5]),
        .I2(X_MOD_1[4]),
        .I3(counter_phase90_output[4]),
        .O(\x_mod_1_i_reg[8]_0 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_2__10
       (.I0(X_MOD_3[5]),
        .I1(counter_phase270_output[5]),
        .I2(X_MOD_3[4]),
        .I3(counter_phase270_output[4]),
        .O(\x_mod_3_i_reg[8]_1 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_2__11
       (.I0(X_MOD_4[5]),
        .I1(counter_phase180_output[5]),
        .I2(X_MOD_4[4]),
        .I3(counter_phase180_output[4]),
        .O(\x_mod_4_i_reg[8]_1 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_2__12
       (.I0(X_MOD_5[5]),
        .I1(counter_phase270_output[5]),
        .I2(X_MOD_5[4]),
        .I3(counter_phase270_output[4]),
        .O(\x_mod_5_i_reg[8]_1 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_2__13
       (.I0(X_MOD_6[5]),
        .I1(counter_phase180_output[5]),
        .I2(X_MOD_6[4]),
        .I3(counter_phase180_output[4]),
        .O(\x_mod_6_i_reg[8]_1 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_2__14
       (.I0(X_MOD_7[5]),
        .I1(counter_phase270_output[5]),
        .I2(X_MOD_7[4]),
        .I3(counter_phase270_output[4]),
        .O(\x_mod_7_i_reg[8]_1 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_2__15
       (.I0(X_MOD_8[5]),
        .I1(counter_phase180_output[5]),
        .I2(X_MOD_8[4]),
        .I3(counter_phase180_output[4]),
        .O(\x_mod_8_i_reg[8]_1 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_2__16
       (.I0(X_MOD_9[5]),
        .I1(counter_phase270_output[5]),
        .I2(X_MOD_9[4]),
        .I3(counter_phase270_output[4]),
        .O(\x_mod_9_i_reg[8]_1 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_2__8
       (.I0(X_MOD_1[5]),
        .I1(counter_phase270_output[5]),
        .I2(X_MOD_1[4]),
        .I3(counter_phase270_output[4]),
        .O(\x_mod_1_i_reg[8]_1 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_2__9
       (.I0(X_MOD_2[5]),
        .I1(counter_phase180_output[5]),
        .I2(X_MOD_2[4]),
        .I3(counter_phase180_output[4]),
        .O(\x_mod_2_i_reg[8]_0 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_3
       (.I0(X_MOD_1[3]),
        .I1(counter_phase90_output[3]),
        .I2(X_MOD_1[2]),
        .I3(counter_phase90_output[2]),
        .O(\x_mod_1_i_reg[8]_0 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_3__10
       (.I0(X_MOD_3[3]),
        .I1(counter_phase270_output[3]),
        .I2(X_MOD_3[2]),
        .I3(counter_phase270_output[2]),
        .O(\x_mod_3_i_reg[8]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_3__11
       (.I0(X_MOD_4[3]),
        .I1(counter_phase180_output[3]),
        .I2(X_MOD_4[2]),
        .I3(counter_phase180_output[2]),
        .O(\x_mod_4_i_reg[8]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_3__12
       (.I0(X_MOD_5[3]),
        .I1(counter_phase270_output[3]),
        .I2(X_MOD_5[2]),
        .I3(counter_phase270_output[2]),
        .O(\x_mod_5_i_reg[8]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_3__13
       (.I0(X_MOD_6[3]),
        .I1(counter_phase180_output[3]),
        .I2(X_MOD_6[2]),
        .I3(counter_phase180_output[2]),
        .O(\x_mod_6_i_reg[8]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_3__14
       (.I0(X_MOD_7[3]),
        .I1(counter_phase270_output[3]),
        .I2(X_MOD_7[2]),
        .I3(counter_phase270_output[2]),
        .O(\x_mod_7_i_reg[8]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_3__15
       (.I0(X_MOD_8[3]),
        .I1(counter_phase180_output[3]),
        .I2(X_MOD_8[2]),
        .I3(counter_phase180_output[2]),
        .O(\x_mod_8_i_reg[8]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_3__16
       (.I0(X_MOD_9[3]),
        .I1(counter_phase270_output[3]),
        .I2(X_MOD_9[2]),
        .I3(counter_phase270_output[2]),
        .O(\x_mod_9_i_reg[8]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_3__8
       (.I0(X_MOD_1[3]),
        .I1(counter_phase270_output[3]),
        .I2(X_MOD_1[2]),
        .I3(counter_phase270_output[2]),
        .O(\x_mod_1_i_reg[8]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_3__9
       (.I0(X_MOD_2[3]),
        .I1(counter_phase180_output[3]),
        .I2(X_MOD_2[2]),
        .I3(counter_phase180_output[2]),
        .O(\x_mod_2_i_reg[8]_0 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_4
       (.I0(X_MOD_1[1]),
        .I1(counter_phase90_output[1]),
        .I2(X_MOD_1[0]),
        .I3(counter_phase90_output[0]),
        .O(\x_mod_1_i_reg[8]_0 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_4__10
       (.I0(X_MOD_3[1]),
        .I1(counter_phase270_output[1]),
        .I2(X_MOD_3[0]),
        .I3(counter_phase270_output[0]),
        .O(\x_mod_3_i_reg[8]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_4__11
       (.I0(X_MOD_4[1]),
        .I1(counter_phase180_output[1]),
        .I2(X_MOD_4[0]),
        .I3(counter_phase180_output[0]),
        .O(\x_mod_4_i_reg[8]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_4__12
       (.I0(X_MOD_5[1]),
        .I1(counter_phase270_output[1]),
        .I2(X_MOD_5[0]),
        .I3(counter_phase270_output[0]),
        .O(\x_mod_5_i_reg[8]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_4__13
       (.I0(X_MOD_6[1]),
        .I1(counter_phase180_output[1]),
        .I2(X_MOD_6[0]),
        .I3(counter_phase180_output[0]),
        .O(\x_mod_6_i_reg[8]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_4__14
       (.I0(X_MOD_7[1]),
        .I1(counter_phase270_output[1]),
        .I2(X_MOD_7[0]),
        .I3(counter_phase270_output[0]),
        .O(\x_mod_7_i_reg[8]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_4__15
       (.I0(X_MOD_8[1]),
        .I1(counter_phase180_output[1]),
        .I2(X_MOD_8[0]),
        .I3(counter_phase180_output[0]),
        .O(\x_mod_8_i_reg[8]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_4__16
       (.I0(X_MOD_9[1]),
        .I1(counter_phase270_output[1]),
        .I2(X_MOD_9[0]),
        .I3(counter_phase270_output[0]),
        .O(\x_mod_9_i_reg[8]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_4__8
       (.I0(X_MOD_1[1]),
        .I1(counter_phase270_output[1]),
        .I2(X_MOD_1[0]),
        .I3(counter_phase270_output[0]),
        .O(\x_mod_1_i_reg[8]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_4__9
       (.I0(X_MOD_2[1]),
        .I1(counter_phase180_output[1]),
        .I2(X_MOD_2[0]),
        .I3(counter_phase180_output[0]),
        .O(\x_mod_2_i_reg[8]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__0
       (.I0(X_MOD_2[7]),
        .I1(q[7]),
        .I2(X_MOD_2[6]),
        .I3(q[6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__1
       (.I0(X_MOD_3[7]),
        .I1(counter_phase90_output[7]),
        .I2(X_MOD_3[6]),
        .I3(counter_phase90_output[6]),
        .O(\x_mod_3_i_reg[8]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__2
       (.I0(X_MOD_4[7]),
        .I1(q[7]),
        .I2(X_MOD_4[6]),
        .I3(q[6]),
        .O(\x_mod_4_i_reg[8]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__3
       (.I0(X_MOD_5[7]),
        .I1(counter_phase90_output[7]),
        .I2(X_MOD_5[6]),
        .I3(counter_phase90_output[6]),
        .O(\x_mod_5_i_reg[8]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__4
       (.I0(X_MOD_6[7]),
        .I1(q[7]),
        .I2(X_MOD_6[6]),
        .I3(q[6]),
        .O(\x_mod_6_i_reg[8]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__5
       (.I0(X_MOD_7[7]),
        .I1(counter_phase90_output[7]),
        .I2(X_MOD_7[6]),
        .I3(counter_phase90_output[6]),
        .O(\x_mod_7_i_reg[8]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__6
       (.I0(X_MOD_8[7]),
        .I1(q[7]),
        .I2(X_MOD_8[6]),
        .I3(q[6]),
        .O(\x_mod_8_i_reg[8]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5__7
       (.I0(X_MOD_9[7]),
        .I1(counter_phase90_output[7]),
        .I2(X_MOD_9[6]),
        .I3(counter_phase90_output[6]),
        .O(\x_mod_9_i_reg[8]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__0
       (.I0(X_MOD_2[5]),
        .I1(q[5]),
        .I2(X_MOD_2[4]),
        .I3(q[4]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__1
       (.I0(X_MOD_3[5]),
        .I1(counter_phase90_output[5]),
        .I2(X_MOD_3[4]),
        .I3(counter_phase90_output[4]),
        .O(\x_mod_3_i_reg[8]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__2
       (.I0(X_MOD_4[5]),
        .I1(q[5]),
        .I2(X_MOD_4[4]),
        .I3(q[4]),
        .O(\x_mod_4_i_reg[8]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__3
       (.I0(X_MOD_5[5]),
        .I1(counter_phase90_output[5]),
        .I2(X_MOD_5[4]),
        .I3(counter_phase90_output[4]),
        .O(\x_mod_5_i_reg[8]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__4
       (.I0(X_MOD_6[5]),
        .I1(q[5]),
        .I2(X_MOD_6[4]),
        .I3(q[4]),
        .O(\x_mod_6_i_reg[8]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__5
       (.I0(X_MOD_7[5]),
        .I1(counter_phase90_output[5]),
        .I2(X_MOD_7[4]),
        .I3(counter_phase90_output[4]),
        .O(\x_mod_7_i_reg[8]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__6
       (.I0(X_MOD_8[5]),
        .I1(q[5]),
        .I2(X_MOD_8[4]),
        .I3(q[4]),
        .O(\x_mod_8_i_reg[8]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6__7
       (.I0(X_MOD_9[5]),
        .I1(counter_phase90_output[5]),
        .I2(X_MOD_9[4]),
        .I3(counter_phase90_output[4]),
        .O(\x_mod_9_i_reg[8]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__0
       (.I0(X_MOD_2[3]),
        .I1(q[3]),
        .I2(X_MOD_2[2]),
        .I3(q[2]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__1
       (.I0(X_MOD_3[3]),
        .I1(counter_phase90_output[3]),
        .I2(X_MOD_3[2]),
        .I3(counter_phase90_output[2]),
        .O(\x_mod_3_i_reg[8]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__2
       (.I0(X_MOD_4[3]),
        .I1(q[3]),
        .I2(X_MOD_4[2]),
        .I3(q[2]),
        .O(\x_mod_4_i_reg[8]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__3
       (.I0(X_MOD_5[3]),
        .I1(counter_phase90_output[3]),
        .I2(X_MOD_5[2]),
        .I3(counter_phase90_output[2]),
        .O(\x_mod_5_i_reg[8]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__4
       (.I0(X_MOD_6[3]),
        .I1(q[3]),
        .I2(X_MOD_6[2]),
        .I3(q[2]),
        .O(\x_mod_6_i_reg[8]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__5
       (.I0(X_MOD_7[3]),
        .I1(counter_phase90_output[3]),
        .I2(X_MOD_7[2]),
        .I3(counter_phase90_output[2]),
        .O(\x_mod_7_i_reg[8]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__6
       (.I0(X_MOD_8[3]),
        .I1(q[3]),
        .I2(X_MOD_8[2]),
        .I3(q[2]),
        .O(\x_mod_8_i_reg[8]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7__7
       (.I0(X_MOD_9[3]),
        .I1(counter_phase90_output[3]),
        .I2(X_MOD_9[2]),
        .I3(counter_phase90_output[2]),
        .O(\x_mod_9_i_reg[8]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__0
       (.I0(X_MOD_2[1]),
        .I1(q[1]),
        .I2(X_MOD_2[0]),
        .I3(q[0]),
        .O(S[0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__1
       (.I0(X_MOD_3[1]),
        .I1(counter_phase90_output[1]),
        .I2(X_MOD_3[0]),
        .I3(counter_phase90_output[0]),
        .O(\x_mod_3_i_reg[8]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__2
       (.I0(X_MOD_4[1]),
        .I1(q[1]),
        .I2(X_MOD_4[0]),
        .I3(q[0]),
        .O(\x_mod_4_i_reg[8]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__3
       (.I0(X_MOD_5[1]),
        .I1(counter_phase90_output[1]),
        .I2(X_MOD_5[0]),
        .I3(counter_phase90_output[0]),
        .O(\x_mod_5_i_reg[8]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__4
       (.I0(X_MOD_6[1]),
        .I1(q[1]),
        .I2(X_MOD_6[0]),
        .I3(q[0]),
        .O(\x_mod_6_i_reg[8]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__5
       (.I0(X_MOD_7[1]),
        .I1(counter_phase90_output[1]),
        .I2(X_MOD_7[0]),
        .I3(counter_phase90_output[0]),
        .O(\x_mod_7_i_reg[8]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__6
       (.I0(X_MOD_8[1]),
        .I1(q[1]),
        .I2(X_MOD_8[0]),
        .I3(q[0]),
        .O(\x_mod_8_i_reg[8]_0 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8__7
       (.I0(X_MOD_9[1]),
        .I1(counter_phase90_output[1]),
        .I2(X_MOD_9[0]),
        .I3(counter_phase90_output[0]),
        .O(\x_mod_9_i_reg[8]_0 [0]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    past_axi_wvalid_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(s_axi_wvalid),
        .Q(past_axi_wvalid),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    pwm_a_p_i0_carry__0_i_1
       (.I0(X_MOD_0[12]),
        .I1(q[12]),
        .O(\x_mod_0_i_reg[13]_0 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    pwm_a_p_i0_carry__0_i_2
       (.I0(X_MOD_0[11]),
        .I1(q[11]),
        .I2(X_MOD_0[10]),
        .I3(q[10]),
        .O(\x_mod_0_i_reg[13]_0 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    pwm_a_p_i0_carry__0_i_3
       (.I0(X_MOD_0[9]),
        .I1(q[9]),
        .I2(X_MOD_0[8]),
        .I3(q[8]),
        .O(\x_mod_0_i_reg[13]_0 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    pwm_a_p_i0_carry_i_1
       (.I0(X_MOD_0[7]),
        .I1(q[7]),
        .I2(X_MOD_0[6]),
        .I3(q[6]),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h22B2)) 
    pwm_a_p_i0_carry_i_2
       (.I0(X_MOD_0[5]),
        .I1(q[5]),
        .I2(X_MOD_0[4]),
        .I3(q[4]),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    pwm_a_p_i0_carry_i_3
       (.I0(X_MOD_0[3]),
        .I1(q[3]),
        .I2(X_MOD_0[2]),
        .I3(q[2]),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    pwm_a_p_i0_carry_i_4
       (.I0(X_MOD_0[1]),
        .I1(q[1]),
        .I2(X_MOD_0[0]),
        .I3(q[0]),
        .O(DI[0]));
  LUT2 #(
    .INIT(4'h2)) 
    pwm_b_p_i0_carry__0_i_1
       (.I0(X_MOD_0[12]),
        .I1(counter_phase180_output[12]),
        .O(\x_mod_0_i_reg[13]_1 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    pwm_b_p_i0_carry__0_i_2
       (.I0(X_MOD_0[11]),
        .I1(counter_phase180_output[11]),
        .I2(X_MOD_0[10]),
        .I3(counter_phase180_output[10]),
        .O(\x_mod_0_i_reg[13]_1 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    pwm_b_p_i0_carry__0_i_3
       (.I0(X_MOD_0[9]),
        .I1(counter_phase180_output[9]),
        .I2(X_MOD_0[8]),
        .I3(counter_phase180_output[8]),
        .O(\x_mod_0_i_reg[13]_1 [0]));
  LUT4 #(
    .INIT(16'h22B2)) 
    pwm_b_p_i0_carry_i_1
       (.I0(X_MOD_0[7]),
        .I1(counter_phase180_output[7]),
        .I2(X_MOD_0[6]),
        .I3(counter_phase180_output[6]),
        .O(\x_mod_0_i_reg[8]_0 [3]));
  LUT4 #(
    .INIT(16'h22B2)) 
    pwm_b_p_i0_carry_i_2
       (.I0(X_MOD_0[5]),
        .I1(counter_phase180_output[5]),
        .I2(X_MOD_0[4]),
        .I3(counter_phase180_output[4]),
        .O(\x_mod_0_i_reg[8]_0 [2]));
  LUT4 #(
    .INIT(16'h22B2)) 
    pwm_b_p_i0_carry_i_3
       (.I0(X_MOD_0[3]),
        .I1(counter_phase180_output[3]),
        .I2(X_MOD_0[2]),
        .I3(counter_phase180_output[2]),
        .O(\x_mod_0_i_reg[8]_0 [1]));
  LUT4 #(
    .INIT(16'h22B2)) 
    pwm_b_p_i0_carry_i_4
       (.I0(X_MOD_0[1]),
        .I1(counter_phase180_output[1]),
        .I2(X_MOD_0[0]),
        .I3(counter_phase180_output[0]),
        .O(\x_mod_0_i_reg[8]_0 [0]));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_reg0[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[0]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[2]),
        .I5(s_axi_wstrb[1]),
        .O(p_1_in[13]));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_reg0[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[0]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[2]),
        .I5(s_axi_wstrb[2]),
        .O(p_1_in[23]));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_reg0[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[0]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[2]),
        .I5(s_axi_wstrb[3]),
        .O(p_1_in[31]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg0[31]_i_2 
       (.I0(s_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s_axi_wvalid),
        .O(slv_reg_wren__2));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_reg0[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[0]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[2]),
        .I5(s_axi_wstrb[0]),
        .O(p_1_in[7]));
  FDRE \slv_reg0_reg[0] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[0]),
        .Q(slv_reg0[0]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[10] 
       (.C(s_axi_aclk),
        .CE(p_1_in[13]),
        .D(s_axi_wdata[10]),
        .Q(slv_reg0[10]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[11] 
       (.C(s_axi_aclk),
        .CE(p_1_in[13]),
        .D(s_axi_wdata[11]),
        .Q(slv_reg0[11]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[12] 
       (.C(s_axi_aclk),
        .CE(p_1_in[13]),
        .D(s_axi_wdata[12]),
        .Q(slv_reg0[12]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[13] 
       (.C(s_axi_aclk),
        .CE(p_1_in[13]),
        .D(s_axi_wdata[13]),
        .Q(slv_reg0[13]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[14] 
       (.C(s_axi_aclk),
        .CE(p_1_in[13]),
        .D(s_axi_wdata[14]),
        .Q(slv_reg0[14]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[15] 
       (.C(s_axi_aclk),
        .CE(p_1_in[13]),
        .D(s_axi_wdata[15]),
        .Q(slv_reg0[15]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[16] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[16]),
        .Q(slv_reg0[16]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[17] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[17]),
        .Q(slv_reg0[17]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[18] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[18]),
        .Q(slv_reg0[18]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[19] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[19]),
        .Q(slv_reg0[19]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[1] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[1]),
        .Q(slv_reg0[1]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[20] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[20]),
        .Q(slv_reg0[20]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[21] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[21]),
        .Q(slv_reg0[21]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[22] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[22]),
        .Q(slv_reg0[22]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[23] 
       (.C(s_axi_aclk),
        .CE(p_1_in[23]),
        .D(s_axi_wdata[23]),
        .Q(slv_reg0[23]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[24] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[24]),
        .Q(slv_reg0[24]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[25] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[25]),
        .Q(slv_reg0[25]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[26] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[26]),
        .Q(slv_reg0[26]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[27] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[27]),
        .Q(slv_reg0[27]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[28] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[28]),
        .Q(slv_reg0[28]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[29] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[29]),
        .Q(slv_reg0[29]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[2] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[2]),
        .Q(slv_reg0[2]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[30] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[30]),
        .Q(slv_reg0[30]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[31] 
       (.C(s_axi_aclk),
        .CE(p_1_in[31]),
        .D(s_axi_wdata[31]),
        .Q(slv_reg0[31]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[3] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[3]),
        .Q(slv_reg0[3]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[4] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[4]),
        .Q(slv_reg0[4]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[5] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[5]),
        .Q(slv_reg0[5]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[6] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[6]),
        .Q(slv_reg0[6]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[7] 
       (.C(s_axi_aclk),
        .CE(p_1_in[7]),
        .D(s_axi_wdata[7]),
        .Q(slv_reg0[7]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[8] 
       (.C(s_axi_aclk),
        .CE(p_1_in[13]),
        .D(s_axi_wdata[8]),
        .Q(slv_reg0[8]),
        .R(p_0_in));
  FDRE \slv_reg0_reg[9] 
       (.C(s_axi_aclk),
        .CE(p_1_in[13]),
        .D(s_axi_wdata[9]),
        .Q(slv_reg0[9]),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[1]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[2]),
        .I5(p_0_in__0[0]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[2]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[2]),
        .I5(p_0_in__0[0]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[3]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[2]),
        .I5(p_0_in__0[0]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[0]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[2]),
        .I5(p_0_in__0[0]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[10] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[11] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[12] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[13] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[14] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[15] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[16] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[17] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[18] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[19] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[1] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[20] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[21] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[22] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[23] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[24] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[25] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[26] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[27] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[28] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[29] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[2] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[30] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[31] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[3] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[4] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[5] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[6] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[7] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[8] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(p_0_in));
  FDRE \slv_reg1_reg[9] 
       (.C(s_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[1]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[0]),
        .I4(p_0_in__0[2]),
        .I5(p_0_in__0[1]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[2]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[0]),
        .I4(p_0_in__0[2]),
        .I5(p_0_in__0[1]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[3]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[0]),
        .I4(p_0_in__0[2]),
        .I5(p_0_in__0[1]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[0]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[0]),
        .I4(p_0_in__0[2]),
        .I5(p_0_in__0[1]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[10] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[11] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[12] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[13] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[14] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[15] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[16] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(slv_reg2[16]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[17] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[18] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[19] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[1] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[20] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[21] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[22] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[23] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[24] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[25] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[26] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[27] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[28] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[29] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[2] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[30] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[31] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[3] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[4] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[5] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[6] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[7] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[8] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(p_0_in));
  FDRE \slv_reg2_reg[9] 
       (.C(s_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[1]),
        .I2(p_0_in__0[0]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[2]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg3[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[2]),
        .I2(p_0_in__0[0]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[2]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg3[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[3]),
        .I2(p_0_in__0[0]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[2]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg3[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[0]),
        .I2(p_0_in__0[0]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[2]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg3[7]_i_1_n_0 ));
  FDRE \slv_reg3_reg[0] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(slv_reg3[0]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[10] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(slv_reg3[10]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[11] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(slv_reg3[11]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[12] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(slv_reg3[12]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[13] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(slv_reg3[13]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[14] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(slv_reg3[14]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[15] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(slv_reg3[15]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[16] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(slv_reg3[16]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[17] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(slv_reg3[17]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[18] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(slv_reg3[18]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[19] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(slv_reg3[19]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[1] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(slv_reg3[1]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[20] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(slv_reg3[20]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[21] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(slv_reg3[21]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[22] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(slv_reg3[22]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[23] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(slv_reg3[23]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[24] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(slv_reg3[24]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[25] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(slv_reg3[25]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[26] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(slv_reg3[26]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[27] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(slv_reg3[27]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[28] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(slv_reg3[28]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[29] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(slv_reg3[29]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[2] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(slv_reg3[2]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[30] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(slv_reg3[30]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[31] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(slv_reg3[31]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[3] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(slv_reg3[3]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[4] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(slv_reg3[4]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[5] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(slv_reg3[5]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[6] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(slv_reg3[6]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[7] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(slv_reg3[7]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[8] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(slv_reg3[8]),
        .R(p_0_in));
  FDRE \slv_reg3_reg[9] 
       (.C(s_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(slv_reg3[9]),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[1]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[0]),
        .I5(p_0_in__0[2]),
        .O(\slv_reg4[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[2]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[0]),
        .I5(p_0_in__0[2]),
        .O(\slv_reg4[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[3]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[0]),
        .I5(p_0_in__0[2]),
        .O(\slv_reg4[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[0]),
        .I2(p_0_in__0[3]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[0]),
        .I5(p_0_in__0[2]),
        .O(\slv_reg4[7]_i_1_n_0 ));
  FDRE \slv_reg4_reg[0] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(slv_reg4[0]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[10] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(slv_reg4[10]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[11] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(slv_reg4[11]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[12] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(slv_reg4[12]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[13] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(slv_reg4[13]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[14] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(slv_reg4[14]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[15] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(slv_reg4[15]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[16] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(slv_reg4[16]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[17] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(slv_reg4[17]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[18] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(slv_reg4[18]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[19] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(slv_reg4[19]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[1] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(slv_reg4[1]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[20] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(slv_reg4[20]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[21] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(slv_reg4[21]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[22] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(slv_reg4[22]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[23] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(slv_reg4[23]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[24] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(slv_reg4[24]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[25] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(slv_reg4[25]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[26] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(slv_reg4[26]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[27] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(slv_reg4[27]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[28] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(slv_reg4[28]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[29] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(slv_reg4[29]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[2] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(slv_reg4[2]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[30] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(slv_reg4[30]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[31] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(slv_reg4[31]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[3] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(slv_reg4[3]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[4] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(slv_reg4[4]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[5] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(slv_reg4[5]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[6] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(slv_reg4[6]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[7] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(slv_reg4[7]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[8] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(slv_reg4[8]),
        .R(p_0_in));
  FDRE \slv_reg4_reg[9] 
       (.C(s_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(slv_reg4[9]),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[1]),
        .I2(p_0_in__0[0]),
        .I3(p_0_in__0[2]),
        .I4(p_0_in__0[1]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg5[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[2]),
        .I2(p_0_in__0[0]),
        .I3(p_0_in__0[2]),
        .I4(p_0_in__0[1]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg5[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[3]),
        .I2(p_0_in__0[0]),
        .I3(p_0_in__0[2]),
        .I4(p_0_in__0[1]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg5[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[0]),
        .I2(p_0_in__0[0]),
        .I3(p_0_in__0[2]),
        .I4(p_0_in__0[1]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg5[7]_i_1_n_0 ));
  FDRE \slv_reg5_reg[0] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(slv_reg5[0]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[10] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(slv_reg5[10]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[11] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(slv_reg5[11]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[12] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(slv_reg5[12]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[13] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(slv_reg5[13]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[14] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(slv_reg5[14]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[15] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(slv_reg5[15]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[16] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(slv_reg5[16]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[17] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(slv_reg5[17]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[18] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(slv_reg5[18]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[19] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(slv_reg5[19]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[1] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(slv_reg5[1]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[20] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(slv_reg5[20]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[21] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(slv_reg5[21]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[22] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(slv_reg5[22]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[23] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(slv_reg5[23]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[24] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(slv_reg5[24]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[25] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(slv_reg5[25]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[26] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(slv_reg5[26]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[27] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(slv_reg5[27]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[28] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(slv_reg5[28]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[29] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(slv_reg5[29]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[2] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(slv_reg5[2]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[30] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(slv_reg5[30]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[31] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(slv_reg5[31]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[3] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(slv_reg5[3]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[4] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(slv_reg5[4]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[5] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(slv_reg5[5]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[6] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(slv_reg5[6]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[7] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(slv_reg5[7]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[8] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(slv_reg5[8]),
        .R(p_0_in));
  FDRE \slv_reg5_reg[9] 
       (.C(s_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(slv_reg5[9]),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[1]),
        .I2(p_0_in__0[2]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[0]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg6[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[2]),
        .I2(p_0_in__0[2]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[0]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg6[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[3]),
        .I2(p_0_in__0[2]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[0]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg6[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s_axi_wstrb[0]),
        .I2(p_0_in__0[2]),
        .I3(p_0_in__0[1]),
        .I4(p_0_in__0[0]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg6[7]_i_1_n_0 ));
  FDRE \slv_reg6_reg[0] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(slv_reg6[0]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[10] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(slv_reg6[10]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[11] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(slv_reg6[11]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[12] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(slv_reg6[12]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[13] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(slv_reg6[13]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[14] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(slv_reg6[14]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[15] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(slv_reg6[15]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[16] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(slv_reg6[16]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[17] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(slv_reg6[17]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[18] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(slv_reg6[18]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[19] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(slv_reg6[19]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[1] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(slv_reg6[1]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[20] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(slv_reg6[20]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[21] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(slv_reg6[21]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[22] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(slv_reg6[22]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[23] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(slv_reg6[23]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[24] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(slv_reg6[24]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[25] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(slv_reg6[25]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[26] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(slv_reg6[26]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[27] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(slv_reg6[27]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[28] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(slv_reg6[28]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[29] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(slv_reg6[29]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[2] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(slv_reg6[2]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[30] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(slv_reg6[30]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[31] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(slv_reg6[31]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[3] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(slv_reg6[3]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[4] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(slv_reg6[4]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[5] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(slv_reg6[5]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[6] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(slv_reg6[6]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[7] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(slv_reg6[7]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[8] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(slv_reg6[8]),
        .R(p_0_in));
  FDRE \slv_reg6_reg[9] 
       (.C(s_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(slv_reg6[9]),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[2]),
        .I2(s_axi_wstrb[1]),
        .I3(p_0_in__0[0]),
        .I4(p_0_in__0[1]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg7[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[2]),
        .I2(s_axi_wstrb[2]),
        .I3(p_0_in__0[0]),
        .I4(p_0_in__0[1]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg7[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[2]),
        .I2(s_axi_wstrb[3]),
        .I3(p_0_in__0[0]),
        .I4(p_0_in__0[1]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg7[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[2]),
        .I2(s_axi_wstrb[0]),
        .I3(p_0_in__0[0]),
        .I4(p_0_in__0[1]),
        .I5(p_0_in__0[3]),
        .O(\slv_reg7[7]_i_1_n_0 ));
  FDRE \slv_reg7_reg[0] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(slv_reg7[0]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[10] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(slv_reg7[10]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[11] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(slv_reg7[11]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[12] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(slv_reg7[12]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[13] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(slv_reg7[13]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[14] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(slv_reg7[14]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[15] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(slv_reg7[15]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[16] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(slv_reg7[16]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[17] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(slv_reg7[17]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[18] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(slv_reg7[18]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[19] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(slv_reg7[19]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[1] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(slv_reg7[1]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[20] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(slv_reg7[20]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[21] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(slv_reg7[21]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[22] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(slv_reg7[22]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[23] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(slv_reg7[23]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[24] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(slv_reg7[24]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[25] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(slv_reg7[25]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[26] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(slv_reg7[26]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[27] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(slv_reg7[27]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[28] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(slv_reg7[28]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[29] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(slv_reg7[29]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[2] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(slv_reg7[2]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[30] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(slv_reg7[30]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[31] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(slv_reg7[31]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[3] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(slv_reg7[3]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[4] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(slv_reg7[4]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[5] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(slv_reg7[5]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[6] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(slv_reg7[6]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[7] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(slv_reg7[7]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[8] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(slv_reg7[8]),
        .R(p_0_in));
  FDRE \slv_reg7_reg[9] 
       (.C(s_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(slv_reg7[9]),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg8[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[3]),
        .I2(p_0_in__0[1]),
        .I3(p_0_in__0[0]),
        .I4(p_0_in__0[2]),
        .I5(s_axi_wstrb[1]),
        .O(\slv_reg8[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg8[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[3]),
        .I2(p_0_in__0[1]),
        .I3(p_0_in__0[0]),
        .I4(p_0_in__0[2]),
        .I5(s_axi_wstrb[2]),
        .O(\slv_reg8[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg8[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[3]),
        .I2(p_0_in__0[1]),
        .I3(p_0_in__0[0]),
        .I4(p_0_in__0[2]),
        .I5(s_axi_wstrb[3]),
        .O(\slv_reg8[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg8[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[3]),
        .I2(p_0_in__0[1]),
        .I3(p_0_in__0[0]),
        .I4(p_0_in__0[2]),
        .I5(s_axi_wstrb[0]),
        .O(\slv_reg8[7]_i_1_n_0 ));
  FDRE \slv_reg8_reg[0] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(slv_reg8[0]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[10] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(slv_reg8[10]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[11] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(slv_reg8[11]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[12] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(slv_reg8[12]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[13] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(slv_reg8[13]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[14] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(slv_reg8[14]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[15] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(slv_reg8[15]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[16] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(slv_reg8[16]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[17] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(slv_reg8[17]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[18] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(slv_reg8[18]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[19] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(slv_reg8[19]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[1] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(slv_reg8[1]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[20] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(slv_reg8[20]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[21] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(slv_reg8[21]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[22] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(slv_reg8[22]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[23] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(slv_reg8[23]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[24] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(slv_reg8[24]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[25] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(slv_reg8[25]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[26] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(slv_reg8[26]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[27] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(slv_reg8[27]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[28] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(slv_reg8[28]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[29] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(slv_reg8[29]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[2] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(slv_reg8[2]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[30] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(slv_reg8[30]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[31] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(slv_reg8[31]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[3] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(slv_reg8[3]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[4] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(slv_reg8[4]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[5] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(slv_reg8[5]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[6] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(slv_reg8[6]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[7] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(slv_reg8[7]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[8] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(slv_reg8[8]),
        .R(p_0_in));
  FDRE \slv_reg8_reg[9] 
       (.C(s_axi_aclk),
        .CE(\slv_reg8[15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(slv_reg8[9]),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg9[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[3]),
        .I2(p_0_in__0[0]),
        .I3(s_axi_wstrb[1]),
        .I4(p_0_in__0[1]),
        .I5(p_0_in__0[2]),
        .O(\slv_reg9[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg9[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[3]),
        .I2(p_0_in__0[0]),
        .I3(s_axi_wstrb[2]),
        .I4(p_0_in__0[1]),
        .I5(p_0_in__0[2]),
        .O(\slv_reg9[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg9[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[3]),
        .I2(p_0_in__0[0]),
        .I3(s_axi_wstrb[3]),
        .I4(p_0_in__0[1]),
        .I5(p_0_in__0[2]),
        .O(\slv_reg9[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg9[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in__0[3]),
        .I2(p_0_in__0[0]),
        .I3(s_axi_wstrb[0]),
        .I4(p_0_in__0[1]),
        .I5(p_0_in__0[2]),
        .O(\slv_reg9[7]_i_1_n_0 ));
  FDRE \slv_reg9_reg[0] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s_axi_wdata[0]),
        .Q(slv_reg9[0]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[10] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s_axi_wdata[10]),
        .Q(slv_reg9[10]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[11] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s_axi_wdata[11]),
        .Q(slv_reg9[11]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[12] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s_axi_wdata[12]),
        .Q(slv_reg9[12]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[13] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s_axi_wdata[13]),
        .Q(slv_reg9[13]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[14] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s_axi_wdata[14]),
        .Q(slv_reg9[14]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[15] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s_axi_wdata[15]),
        .Q(slv_reg9[15]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[16] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s_axi_wdata[16]),
        .Q(slv_reg9[16]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[17] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s_axi_wdata[17]),
        .Q(slv_reg9[17]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[18] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s_axi_wdata[18]),
        .Q(slv_reg9[18]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[19] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s_axi_wdata[19]),
        .Q(slv_reg9[19]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[1] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s_axi_wdata[1]),
        .Q(slv_reg9[1]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[20] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s_axi_wdata[20]),
        .Q(slv_reg9[20]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[21] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s_axi_wdata[21]),
        .Q(slv_reg9[21]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[22] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s_axi_wdata[22]),
        .Q(slv_reg9[22]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[23] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[23]_i_1_n_0 ),
        .D(s_axi_wdata[23]),
        .Q(slv_reg9[23]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[24] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s_axi_wdata[24]),
        .Q(slv_reg9[24]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[25] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s_axi_wdata[25]),
        .Q(slv_reg9[25]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[26] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s_axi_wdata[26]),
        .Q(slv_reg9[26]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[27] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s_axi_wdata[27]),
        .Q(slv_reg9[27]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[28] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s_axi_wdata[28]),
        .Q(slv_reg9[28]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[29] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s_axi_wdata[29]),
        .Q(slv_reg9[29]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[2] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s_axi_wdata[2]),
        .Q(slv_reg9[2]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[30] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s_axi_wdata[30]),
        .Q(slv_reg9[30]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[31] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[31]_i_1_n_0 ),
        .D(s_axi_wdata[31]),
        .Q(slv_reg9[31]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[3] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s_axi_wdata[3]),
        .Q(slv_reg9[3]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[4] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s_axi_wdata[4]),
        .Q(slv_reg9[4]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[5] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s_axi_wdata[5]),
        .Q(slv_reg9[5]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[6] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s_axi_wdata[6]),
        .Q(slv_reg9[6]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[7] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[7]_i_1_n_0 ),
        .D(s_axi_wdata[7]),
        .Q(slv_reg9[7]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[8] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s_axi_wdata[8]),
        .Q(slv_reg9[8]),
        .R(p_0_in));
  FDRE \slv_reg9_reg[9] 
       (.C(s_axi_aclk),
        .CE(\slv_reg9[15]_i_1_n_0 ),
        .D(s_axi_wdata[9]),
        .Q(slv_reg9[9]),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'h00000080)) 
    \x_mod_0_i[13]_i_1 
       (.I0(axi_wvalid_falling_edge),
        .I1(p_0_in__0[3]),
        .I2(p_0_in__0[0]),
        .I3(p_0_in__0[2]),
        .I4(p_0_in__0[1]),
        .O(x_mod_0_i0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_0_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg0[10]),
        .Q(X_MOD_0[9]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_0_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg0[11]),
        .Q(X_MOD_0[10]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_0_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg0[12]),
        .Q(X_MOD_0[11]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_0_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg0[13]),
        .Q(X_MOD_0[12]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_0_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg0[1]),
        .Q(X_MOD_0[0]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_0_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg0[2]),
        .Q(X_MOD_0[1]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_0_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg0[3]),
        .Q(X_MOD_0[2]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_0_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg0[4]),
        .Q(X_MOD_0[3]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_0_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg0[5]),
        .Q(X_MOD_0[4]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_0_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg0[6]),
        .Q(X_MOD_0[5]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_0_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg0[7]),
        .Q(X_MOD_0[6]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_0_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg0[8]),
        .Q(X_MOD_0[7]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_0_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg0[9]),
        .Q(X_MOD_0[8]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_1_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg1[10]),
        .Q(X_MOD_1[9]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_1_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg1[11]),
        .Q(X_MOD_1[10]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_1_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg1[12]),
        .Q(X_MOD_1[11]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_1_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg1[13]),
        .Q(X_MOD_1[12]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_1_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg1[1]),
        .Q(X_MOD_1[0]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_1_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg1[2]),
        .Q(X_MOD_1[1]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_1_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg1[3]),
        .Q(X_MOD_1[2]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_1_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg1[4]),
        .Q(X_MOD_1[3]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_1_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg1[5]),
        .Q(X_MOD_1[4]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_1_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg1[6]),
        .Q(X_MOD_1[5]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_1_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg1[7]),
        .Q(X_MOD_1[6]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_1_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg1[8]),
        .Q(X_MOD_1[7]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_1_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg1[9]),
        .Q(X_MOD_1[8]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_2_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg2[10]),
        .Q(X_MOD_2[9]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_2_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg2[11]),
        .Q(X_MOD_2[10]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_2_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg2[12]),
        .Q(X_MOD_2[11]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_2_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg2[13]),
        .Q(X_MOD_2[12]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_2_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg2[1]),
        .Q(X_MOD_2[0]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_2_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg2[2]),
        .Q(X_MOD_2[1]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_2_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg2[3]),
        .Q(X_MOD_2[2]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_2_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg2[4]),
        .Q(X_MOD_2[3]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_2_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg2[5]),
        .Q(X_MOD_2[4]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_2_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg2[6]),
        .Q(X_MOD_2[5]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_2_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg2[7]),
        .Q(X_MOD_2[6]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_2_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg2[8]),
        .Q(X_MOD_2[7]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_2_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg2[9]),
        .Q(X_MOD_2[8]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_3_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg3[10]),
        .Q(X_MOD_3[9]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_3_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg3[11]),
        .Q(X_MOD_3[10]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_3_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg3[12]),
        .Q(X_MOD_3[11]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_3_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg3[13]),
        .Q(X_MOD_3[12]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_3_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg3[1]),
        .Q(X_MOD_3[0]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_3_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg3[2]),
        .Q(X_MOD_3[1]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_3_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg3[3]),
        .Q(X_MOD_3[2]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_3_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg3[4]),
        .Q(X_MOD_3[3]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_3_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg3[5]),
        .Q(X_MOD_3[4]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_3_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg3[6]),
        .Q(X_MOD_3[5]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_3_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg3[7]),
        .Q(X_MOD_3[6]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_3_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg3[8]),
        .Q(X_MOD_3[7]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_3_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg3[9]),
        .Q(X_MOD_3[8]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_4_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg4[10]),
        .Q(X_MOD_4[9]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_4_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg4[11]),
        .Q(X_MOD_4[10]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_4_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg4[12]),
        .Q(X_MOD_4[11]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_4_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg4[13]),
        .Q(X_MOD_4[12]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_4_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg4[1]),
        .Q(X_MOD_4[0]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_4_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg4[2]),
        .Q(X_MOD_4[1]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_4_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg4[3]),
        .Q(X_MOD_4[2]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_4_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg4[4]),
        .Q(X_MOD_4[3]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_4_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg4[5]),
        .Q(X_MOD_4[4]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_4_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg4[6]),
        .Q(X_MOD_4[5]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_4_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg4[7]),
        .Q(X_MOD_4[6]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_4_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg4[8]),
        .Q(X_MOD_4[7]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_4_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg4[9]),
        .Q(X_MOD_4[8]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_5_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg5[10]),
        .Q(X_MOD_5[9]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_5_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg5[11]),
        .Q(X_MOD_5[10]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_5_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg5[12]),
        .Q(X_MOD_5[11]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_5_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg5[13]),
        .Q(X_MOD_5[12]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_5_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg5[1]),
        .Q(X_MOD_5[0]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_5_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg5[2]),
        .Q(X_MOD_5[1]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_5_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg5[3]),
        .Q(X_MOD_5[2]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_5_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg5[4]),
        .Q(X_MOD_5[3]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_5_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg5[5]),
        .Q(X_MOD_5[4]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_5_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg5[6]),
        .Q(X_MOD_5[5]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_5_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg5[7]),
        .Q(X_MOD_5[6]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_5_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg5[8]),
        .Q(X_MOD_5[7]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_5_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg5[9]),
        .Q(X_MOD_5[8]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_6_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg6[10]),
        .Q(X_MOD_6[9]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_6_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg6[11]),
        .Q(X_MOD_6[10]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_6_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg6[12]),
        .Q(X_MOD_6[11]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_6_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg6[13]),
        .Q(X_MOD_6[12]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_6_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg6[1]),
        .Q(X_MOD_6[0]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_6_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg6[2]),
        .Q(X_MOD_6[1]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_6_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg6[3]),
        .Q(X_MOD_6[2]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_6_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg6[4]),
        .Q(X_MOD_6[3]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_6_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg6[5]),
        .Q(X_MOD_6[4]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_6_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg6[6]),
        .Q(X_MOD_6[5]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_6_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg6[7]),
        .Q(X_MOD_6[6]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_6_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg6[8]),
        .Q(X_MOD_6[7]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_6_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg6[9]),
        .Q(X_MOD_6[8]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_7_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg7[10]),
        .Q(X_MOD_7[9]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_7_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg7[11]),
        .Q(X_MOD_7[10]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_7_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg7[12]),
        .Q(X_MOD_7[11]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_7_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg7[13]),
        .Q(X_MOD_7[12]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_7_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg7[1]),
        .Q(X_MOD_7[0]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_7_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg7[2]),
        .Q(X_MOD_7[1]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_7_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg7[3]),
        .Q(X_MOD_7[2]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_7_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg7[4]),
        .Q(X_MOD_7[3]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_7_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg7[5]),
        .Q(X_MOD_7[4]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_7_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg7[6]),
        .Q(X_MOD_7[5]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_7_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg7[7]),
        .Q(X_MOD_7[6]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_7_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg7[8]),
        .Q(X_MOD_7[7]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_7_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg7[9]),
        .Q(X_MOD_7[8]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_8_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg8[10]),
        .Q(X_MOD_8[9]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_8_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg8[11]),
        .Q(X_MOD_8[10]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_8_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg8[12]),
        .Q(X_MOD_8[11]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_8_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg8[13]),
        .Q(X_MOD_8[12]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_8_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg8[1]),
        .Q(X_MOD_8[0]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_8_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg8[2]),
        .Q(X_MOD_8[1]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_8_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg8[3]),
        .Q(X_MOD_8[2]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_8_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg8[4]),
        .Q(X_MOD_8[3]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_8_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg8[5]),
        .Q(X_MOD_8[4]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_8_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg8[6]),
        .Q(X_MOD_8[5]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_8_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg8[7]),
        .Q(X_MOD_8[6]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_8_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg8[8]),
        .Q(X_MOD_8[7]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_8_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg8[9]),
        .Q(X_MOD_8[8]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_9_i_reg[10] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg9[10]),
        .Q(X_MOD_9[9]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_9_i_reg[11] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg9[11]),
        .Q(X_MOD_9[10]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_9_i_reg[12] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg9[12]),
        .Q(X_MOD_9[11]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_9_i_reg[13] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg9[13]),
        .Q(X_MOD_9[12]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_9_i_reg[1] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg9[1]),
        .Q(X_MOD_9[0]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_9_i_reg[2] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg9[2]),
        .Q(X_MOD_9[1]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_9_i_reg[3] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg9[3]),
        .Q(X_MOD_9[2]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_9_i_reg[4] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg9[4]),
        .Q(X_MOD_9[3]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_9_i_reg[5] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg9[5]),
        .Q(X_MOD_9[4]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_9_i_reg[6] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg9[6]),
        .Q(X_MOD_9[5]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_9_i_reg[7] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg9[7]),
        .Q(X_MOD_9[6]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_9_i_reg[8] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg9[8]),
        .Q(X_MOD_9[7]),
        .R(external_reset_i));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \x_mod_9_i_reg[9] 
       (.C(s_axi_aclk),
        .CE(x_mod_0_i0),
        .D(slv_reg9[9]),
        .Q(X_MOD_9[8]),
        .R(external_reset_i));
endmodule

(* CHECK_LICENSE_TYPE = "system_pwm_x40_ip_0_0,pwm_x40_ip_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "pwm_x40_ip_v1_0,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module system_pwm_x40_ip_0_0
   (PWM_A_P,
    PWM_B_P,
    PWM_A_N,
    PWM_B_N,
    CLK_100MHZ,
    EXTERNAL_RESET,
    s_axi_awaddr,
    s_axi_awprot,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_araddr,
    s_axi_arprot,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_rready,
    s_axi_aclk,
    s_axi_aresetn);
  output [9:0]PWM_A_P;
  output [9:0]PWM_B_P;
  output [9:0]PWM_A_N;
  output [9:0]PWM_B_N;
  input CLK_100MHZ;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 COUNTERS_RESET RST" *) (* x_interface_parameter = "XIL_INTERFACENAME COUNTERS_RESET, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input EXTERNAL_RESET;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 10, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [5:0]s_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWPROT" *) input [2:0]s_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWVALID" *) input s_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWREADY" *) output s_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WDATA" *) input [31:0]s_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WSTRB" *) input [3:0]s_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WVALID" *) input s_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WREADY" *) output s_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI BRESP" *) output [1:0]s_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI BVALID" *) output s_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI BREADY" *) input s_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARADDR" *) input [5:0]s_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARPROT" *) input [2:0]s_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARVALID" *) input s_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARREADY" *) output s_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RDATA" *) output [31:0]s_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RRESP" *) output [1:0]s_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RVALID" *) output s_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RREADY" *) input s_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S_AXI_CLK, ASSOCIATED_BUSIF S_AXI, ASSOCIATED_RESET s_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s_axi_aresetn;

  wire \<const0> ;
  wire CLK_100MHZ;
  wire EXTERNAL_RESET;
  wire [9:0]PWM_A_N;
  wire [9:0]PWM_A_P;
  wire [9:0]PWM_B_N;
  wire [9:0]PWM_B_P;
  wire s_axi_aclk;
  wire [5:0]s_axi_araddr;
  wire s_axi_aresetn;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [5:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire [3:0]s_axi_wstrb;
  wire s_axi_wvalid;

  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_rresp[1] = \<const0> ;
  assign s_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  system_pwm_x40_ip_0_0_pwm_x40_ip_v1_0 U0
       (.CLK_100MHZ(CLK_100MHZ),
        .EXTERNAL_RESET(EXTERNAL_RESET),
        .PWM_A_N(PWM_A_N),
        .PWM_A_P(PWM_A_P),
        .PWM_B_N(PWM_B_N),
        .PWM_B_P(PWM_B_P),
        .S_AXI_ARREADY(s_axi_arready),
        .S_AXI_AWREADY(s_axi_awready),
        .S_AXI_WREADY(s_axi_wready),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_araddr(s_axi_araddr[5:2]),
        .s_axi_aresetn(s_axi_aresetn),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr[5:2]),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata(s_axi_wdata),
        .s_axi_wstrb(s_axi_wstrb),
        .s_axi_wvalid(s_axi_wvalid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

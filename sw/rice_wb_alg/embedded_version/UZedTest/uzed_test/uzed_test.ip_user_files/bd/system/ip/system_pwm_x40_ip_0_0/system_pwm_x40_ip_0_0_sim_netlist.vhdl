-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
-- Date        : Mon Jun 14 21:18:39 2021
-- Host        : FloresToWin running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top system_pwm_x40_ip_0_0 -prefix
--               system_pwm_x40_ip_0_0_ system_pwm_x40_ip_0_0_sim_netlist.vhdl
-- Design      : system_pwm_x40_ip_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_pwm_x40_ip_0_0_N_bits_counter is
  port (
    \^q\ : out STD_LOGIC_VECTOR ( 12 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[12]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[11]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \ctr_state_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[11]_1\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \ctr_state_reg[7]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[11]_2\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \ctr_state_reg[7]_2\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[11]_3\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    tc : out STD_LOGIC;
    \ctr_state_reg[12]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \ctr_state_reg[12]_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \ctr_state_reg[12]_3\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \ctr_state_reg[12]_4\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    external_reset_i : in STD_LOGIC;
    dir : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_a_p_i0_inferred__1/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_a_p_i0_inferred__3/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_a_p_i0_inferred__5/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_a_p_i0_inferred__7/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    CLK_100MHZ : in STD_LOGIC
  );
end system_pwm_x40_ip_0_0_N_bits_counter;

architecture STRUCTURE of system_pwm_x40_ip_0_0_N_bits_counter is
  signal \ctr_state[10]_i_1_n_0\ : STD_LOGIC;
  signal \ctr_state[11]_i_1__1_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_10_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_11_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_12_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_1__0_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_2_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_5_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_6_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_7_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_8_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_9_n_0\ : STD_LOGIC;
  signal \ctr_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \ctr_state[2]_i_1__1_n_0\ : STD_LOGIC;
  signal \ctr_state[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_1_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_4_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_5_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_6_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_7_n_0\ : STD_LOGIC;
  signal \ctr_state[5]_i_1_n_0\ : STD_LOGIC;
  signal \ctr_state[6]_i_1__1_n_0\ : STD_LOGIC;
  signal \ctr_state[7]_i_1__2_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_1__2_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_4_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_5_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_6_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_7_n_0\ : STD_LOGIC;
  signal \ctr_state[9]_i_1__0_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_3_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_3_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_3_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4_n_7\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3_n_7\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3_n_7\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 12 downto 1 );
  signal p_1_in : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^q_1\ : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \NLW_ctr_state_reg[12]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ctr_state_reg[12]_i_4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of counter_phase0_dir_i_2 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \ctr_state[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \ctr_state[12]_i_12\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \ctr_state[12]_i_7\ : label is "soft_lutpair1";
begin
  \^q\(12 downto 0) <= \^q_1\(12 downto 0);
counter_phase0_dir_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \ctr_state[12]_i_5_n_0\,
      I1 => dir,
      I2 => \ctr_state[12]_i_2_n_0\,
      O => tc
    );
\ctr_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00011101"
    )
        port map (
      I0 => external_reset_i,
      I1 => \^q_1\(0),
      I2 => \ctr_state[12]_i_2_n_0\,
      I3 => dir,
      I4 => \ctr_state[12]_i_5_n_0\,
      O => p_1_in(0)
    );
\ctr_state[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(10),
      I2 => \ctr_state[12]_i_2_n_0\,
      I3 => dir,
      I4 => \ctr_state_reg[12]_i_4_n_6\,
      I5 => \ctr_state[12]_i_5_n_0\,
      O => \ctr_state[10]_i_1_n_0\
    );
\ctr_state[11]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(11),
      I2 => \ctr_state[12]_i_2_n_0\,
      I3 => dir,
      I4 => \ctr_state_reg[12]_i_4_n_5\,
      I5 => \ctr_state[12]_i_5_n_0\,
      O => \ctr_state[11]_i_1__1_n_0\
    );
\ctr_state[12]_i_10\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q_1\(10),
      O => \ctr_state[12]_i_10_n_0\
    );
\ctr_state[12]_i_11\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q_1\(9),
      O => \ctr_state[12]_i_11_n_0\
    );
\ctr_state[12]_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => \^q_1\(9),
      I1 => \^q_1\(8),
      I2 => \^q_1\(3),
      I3 => \^q_1\(7),
      I4 => \^q_1\(12),
      O => \ctr_state[12]_i_12_n_0\
    );
\ctr_state[12]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0054005455540054"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2_n_0\,
      I2 => data0(12),
      I3 => dir,
      I4 => \ctr_state_reg[12]_i_4_n_4\,
      I5 => \ctr_state[12]_i_5_n_0\,
      O => \ctr_state[12]_i_1__0_n_0\
    );
\ctr_state[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \ctr_state[12]_i_6_n_0\,
      I1 => \^q_1\(1),
      I2 => \^q_1\(0),
      I3 => \^q_1\(10),
      I4 => \^q_1\(2),
      I5 => \ctr_state[12]_i_7_n_0\,
      O => \ctr_state[12]_i_2_n_0\
    );
\ctr_state[12]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \ctr_state[12]_i_6_n_0\,
      I1 => \^q_1\(1),
      I2 => \^q_1\(0),
      I3 => \^q_1\(10),
      I4 => \^q_1\(2),
      I5 => \ctr_state[12]_i_12_n_0\,
      O => \ctr_state[12]_i_5_n_0\
    );
\ctr_state[12]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^q_1\(5),
      I1 => \^q_1\(4),
      I2 => \^q_1\(11),
      I3 => \^q_1\(6),
      O => \ctr_state[12]_i_6_n_0\
    );
\ctr_state[12]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^q_1\(3),
      I1 => \^q_1\(7),
      I2 => \^q_1\(12),
      I3 => \^q_1\(9),
      I4 => \^q_1\(8),
      O => \ctr_state[12]_i_7_n_0\
    );
\ctr_state[12]_i_8\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q_1\(12),
      O => \ctr_state[12]_i_8_n_0\
    );
\ctr_state[12]_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q_1\(11),
      O => \ctr_state[12]_i_9_n_0\
    );
\ctr_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(1),
      I2 => \ctr_state[12]_i_2_n_0\,
      I3 => dir,
      I4 => \ctr_state_reg[4]_i_3_n_7\,
      I5 => \ctr_state[12]_i_5_n_0\,
      O => \ctr_state[1]_i_1_n_0\
    );
\ctr_state[2]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(2),
      I2 => \ctr_state[12]_i_2_n_0\,
      I3 => dir,
      I4 => \ctr_state_reg[4]_i_3_n_6\,
      I5 => \ctr_state[12]_i_5_n_0\,
      O => \ctr_state[2]_i_1__1_n_0\
    );
\ctr_state[3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0054005455540054"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2_n_0\,
      I2 => data0(3),
      I3 => dir,
      I4 => \ctr_state_reg[4]_i_3_n_5\,
      I5 => \ctr_state[12]_i_5_n_0\,
      O => \ctr_state[3]_i_1__0_n_0\
    );
\ctr_state[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(4),
      I2 => \ctr_state[12]_i_2_n_0\,
      I3 => dir,
      I4 => \ctr_state_reg[4]_i_3_n_4\,
      I5 => \ctr_state[12]_i_5_n_0\,
      O => \ctr_state[4]_i_1_n_0\
    );
\ctr_state[4]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q_1\(4),
      O => \ctr_state[4]_i_4_n_0\
    );
\ctr_state[4]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q_1\(3),
      O => \ctr_state[4]_i_5_n_0\
    );
\ctr_state[4]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q_1\(2),
      O => \ctr_state[4]_i_6_n_0\
    );
\ctr_state[4]_i_7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q_1\(1),
      O => \ctr_state[4]_i_7_n_0\
    );
\ctr_state[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(5),
      I2 => \ctr_state[12]_i_2_n_0\,
      I3 => dir,
      I4 => \ctr_state_reg[8]_i_3_n_7\,
      I5 => \ctr_state[12]_i_5_n_0\,
      O => \ctr_state[5]_i_1_n_0\
    );
\ctr_state[6]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(6),
      I2 => \ctr_state[12]_i_2_n_0\,
      I3 => dir,
      I4 => \ctr_state_reg[8]_i_3_n_6\,
      I5 => \ctr_state[12]_i_5_n_0\,
      O => \ctr_state[6]_i_1__1_n_0\
    );
\ctr_state[7]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0054005455540054"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2_n_0\,
      I2 => data0(7),
      I3 => dir,
      I4 => \ctr_state_reg[8]_i_3_n_5\,
      I5 => \ctr_state[12]_i_5_n_0\,
      O => \ctr_state[7]_i_1__2_n_0\
    );
\ctr_state[8]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0054005455540054"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2_n_0\,
      I2 => data0(8),
      I3 => dir,
      I4 => \ctr_state_reg[8]_i_3_n_4\,
      I5 => \ctr_state[12]_i_5_n_0\,
      O => \ctr_state[8]_i_1__2_n_0\
    );
\ctr_state[8]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q_1\(8),
      O => \ctr_state[8]_i_4_n_0\
    );
\ctr_state[8]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q_1\(7),
      O => \ctr_state[8]_i_5_n_0\
    );
\ctr_state[8]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q_1\(6),
      O => \ctr_state[8]_i_6_n_0\
    );
\ctr_state[8]_i_7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q_1\(5),
      O => \ctr_state[8]_i_7_n_0\
    );
\ctr_state[9]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0054005455540054"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2_n_0\,
      I2 => data0(9),
      I3 => dir,
      I4 => \ctr_state_reg[12]_i_4_n_7\,
      I5 => \ctr_state[12]_i_5_n_0\,
      O => \ctr_state[9]_i_1__0_n_0\
    );
\ctr_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(0),
      Q => \^q_1\(0),
      R => '0'
    );
\ctr_state_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[10]_i_1_n_0\,
      Q => \^q_1\(10),
      R => '0'
    );
\ctr_state_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[11]_i_1__1_n_0\,
      Q => \^q_1\(11),
      R => '0'
    );
\ctr_state_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[12]_i_1__0_n_0\,
      Q => \^q_1\(12),
      R => '0'
    );
\ctr_state_reg[12]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[8]_i_2_n_0\,
      CO(3) => \NLW_ctr_state_reg[12]_i_3_CO_UNCONNECTED\(3),
      CO(2) => \ctr_state_reg[12]_i_3_n_1\,
      CO(1) => \ctr_state_reg[12]_i_3_n_2\,
      CO(0) => \ctr_state_reg[12]_i_3_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \^q_1\(11 downto 9),
      O(3 downto 0) => data0(12 downto 9),
      S(3) => \ctr_state[12]_i_8_n_0\,
      S(2) => \ctr_state[12]_i_9_n_0\,
      S(1) => \ctr_state[12]_i_10_n_0\,
      S(0) => \ctr_state[12]_i_11_n_0\
    );
\ctr_state_reg[12]_i_4\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[8]_i_3_n_0\,
      CO(3) => \NLW_ctr_state_reg[12]_i_4_CO_UNCONNECTED\(3),
      CO(2) => \ctr_state_reg[12]_i_4_n_1\,
      CO(1) => \ctr_state_reg[12]_i_4_n_2\,
      CO(0) => \ctr_state_reg[12]_i_4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[12]_i_4_n_4\,
      O(2) => \ctr_state_reg[12]_i_4_n_5\,
      O(1) => \ctr_state_reg[12]_i_4_n_6\,
      O(0) => \ctr_state_reg[12]_i_4_n_7\,
      S(3 downto 0) => \^q_1\(12 downto 9)
    );
\ctr_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[1]_i_1_n_0\,
      Q => \^q_1\(1),
      R => '0'
    );
\ctr_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[2]_i_1__1_n_0\,
      Q => \^q_1\(2),
      R => '0'
    );
\ctr_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[3]_i_1__0_n_0\,
      Q => \^q_1\(3),
      R => '0'
    );
\ctr_state_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[4]_i_1_n_0\,
      Q => \^q_1\(4),
      R => '0'
    );
\ctr_state_reg[4]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ctr_state_reg[4]_i_2_n_0\,
      CO(2) => \ctr_state_reg[4]_i_2_n_1\,
      CO(1) => \ctr_state_reg[4]_i_2_n_2\,
      CO(0) => \ctr_state_reg[4]_i_2_n_3\,
      CYINIT => \^q_1\(0),
      DI(3 downto 0) => \^q_1\(4 downto 1),
      O(3 downto 0) => data0(4 downto 1),
      S(3) => \ctr_state[4]_i_4_n_0\,
      S(2) => \ctr_state[4]_i_5_n_0\,
      S(1) => \ctr_state[4]_i_6_n_0\,
      S(0) => \ctr_state[4]_i_7_n_0\
    );
\ctr_state_reg[4]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ctr_state_reg[4]_i_3_n_0\,
      CO(2) => \ctr_state_reg[4]_i_3_n_1\,
      CO(1) => \ctr_state_reg[4]_i_3_n_2\,
      CO(0) => \ctr_state_reg[4]_i_3_n_3\,
      CYINIT => \^q_1\(0),
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[4]_i_3_n_4\,
      O(2) => \ctr_state_reg[4]_i_3_n_5\,
      O(1) => \ctr_state_reg[4]_i_3_n_6\,
      O(0) => \ctr_state_reg[4]_i_3_n_7\,
      S(3 downto 0) => \^q_1\(4 downto 1)
    );
\ctr_state_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[5]_i_1_n_0\,
      Q => \^q_1\(5),
      R => '0'
    );
\ctr_state_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[6]_i_1__1_n_0\,
      Q => \^q_1\(6),
      R => '0'
    );
\ctr_state_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[7]_i_1__2_n_0\,
      Q => \^q_1\(7),
      R => '0'
    );
\ctr_state_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[8]_i_1__2_n_0\,
      Q => \^q_1\(8),
      R => '0'
    );
\ctr_state_reg[8]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[4]_i_2_n_0\,
      CO(3) => \ctr_state_reg[8]_i_2_n_0\,
      CO(2) => \ctr_state_reg[8]_i_2_n_1\,
      CO(1) => \ctr_state_reg[8]_i_2_n_2\,
      CO(0) => \ctr_state_reg[8]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^q_1\(8 downto 5),
      O(3 downto 0) => data0(8 downto 5),
      S(3) => \ctr_state[8]_i_4_n_0\,
      S(2) => \ctr_state[8]_i_5_n_0\,
      S(1) => \ctr_state[8]_i_6_n_0\,
      S(0) => \ctr_state[8]_i_7_n_0\
    );
\ctr_state_reg[8]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[4]_i_3_n_0\,
      CO(3) => \ctr_state_reg[8]_i_3_n_0\,
      CO(2) => \ctr_state_reg[8]_i_3_n_1\,
      CO(1) => \ctr_state_reg[8]_i_3_n_2\,
      CO(0) => \ctr_state_reg[8]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[8]_i_3_n_4\,
      O(2) => \ctr_state_reg[8]_i_3_n_5\,
      O(1) => \ctr_state_reg[8]_i_3_n_6\,
      O(0) => \ctr_state_reg[8]_i_3_n_7\,
      S(3 downto 0) => \^q_1\(8 downto 5)
    );
\ctr_state_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[9]_i_1__0_n_0\,
      Q => \^q_1\(9),
      R => '0'
    );
\i__carry__0_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(11),
      I1 => \pwm_a_p_i0_inferred__1/i__carry__0\(11),
      I2 => \pwm_a_p_i0_inferred__1/i__carry__0\(10),
      I3 => \^q_1\(10),
      O => \ctr_state_reg[11]_0\(1)
    );
\i__carry__0_i_2__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(11),
      I1 => \pwm_a_p_i0_inferred__3/i__carry__0\(11),
      I2 => \pwm_a_p_i0_inferred__3/i__carry__0\(10),
      I3 => \^q_1\(10),
      O => \ctr_state_reg[11]_1\(1)
    );
\i__carry__0_i_2__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(11),
      I1 => \pwm_a_p_i0_inferred__5/i__carry__0\(11),
      I2 => \pwm_a_p_i0_inferred__5/i__carry__0\(10),
      I3 => \^q_1\(10),
      O => \ctr_state_reg[11]_2\(1)
    );
\i__carry__0_i_2__6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(11),
      I1 => \pwm_a_p_i0_inferred__7/i__carry__0\(11),
      I2 => \pwm_a_p_i0_inferred__7/i__carry__0\(10),
      I3 => \^q_1\(10),
      O => \ctr_state_reg[11]_3\(1)
    );
\i__carry__0_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(9),
      I1 => \pwm_a_p_i0_inferred__1/i__carry__0\(9),
      I2 => \pwm_a_p_i0_inferred__1/i__carry__0\(8),
      I3 => \^q_1\(8),
      O => \ctr_state_reg[11]_0\(0)
    );
\i__carry__0_i_3__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(9),
      I1 => \pwm_a_p_i0_inferred__3/i__carry__0\(9),
      I2 => \pwm_a_p_i0_inferred__3/i__carry__0\(8),
      I3 => \^q_1\(8),
      O => \ctr_state_reg[11]_1\(0)
    );
\i__carry__0_i_3__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(9),
      I1 => \pwm_a_p_i0_inferred__5/i__carry__0\(9),
      I2 => \pwm_a_p_i0_inferred__5/i__carry__0\(8),
      I3 => \^q_1\(8),
      O => \ctr_state_reg[11]_2\(0)
    );
\i__carry__0_i_3__6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(9),
      I1 => \pwm_a_p_i0_inferred__7/i__carry__0\(9),
      I2 => \pwm_a_p_i0_inferred__7/i__carry__0\(8),
      I3 => \^q_1\(8),
      O => \ctr_state_reg[11]_3\(0)
    );
\i__carry__0_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q_1\(12),
      I1 => \pwm_a_p_i0_inferred__1/i__carry__0\(12),
      O => \ctr_state_reg[12]_1\(0)
    );
\i__carry__0_i_4__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q_1\(12),
      I1 => \pwm_a_p_i0_inferred__3/i__carry__0\(12),
      O => \ctr_state_reg[12]_2\(0)
    );
\i__carry__0_i_4__4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q_1\(12),
      I1 => \pwm_a_p_i0_inferred__5/i__carry__0\(12),
      O => \ctr_state_reg[12]_3\(0)
    );
\i__carry__0_i_4__6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q_1\(12),
      I1 => \pwm_a_p_i0_inferred__7/i__carry__0\(12),
      O => \ctr_state_reg[12]_4\(0)
    );
\i__carry_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(7),
      I1 => \pwm_a_p_i0_inferred__1/i__carry__0\(7),
      I2 => \pwm_a_p_i0_inferred__1/i__carry__0\(6),
      I3 => \^q_1\(6),
      O => DI(3)
    );
\i__carry_i_1__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(7),
      I1 => \pwm_a_p_i0_inferred__3/i__carry__0\(7),
      I2 => \pwm_a_p_i0_inferred__3/i__carry__0\(6),
      I3 => \^q_1\(6),
      O => \ctr_state_reg[7]_0\(3)
    );
\i__carry_i_1__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(7),
      I1 => \pwm_a_p_i0_inferred__5/i__carry__0\(7),
      I2 => \pwm_a_p_i0_inferred__5/i__carry__0\(6),
      I3 => \^q_1\(6),
      O => \ctr_state_reg[7]_1\(3)
    );
\i__carry_i_1__6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(7),
      I1 => \pwm_a_p_i0_inferred__7/i__carry__0\(7),
      I2 => \pwm_a_p_i0_inferred__7/i__carry__0\(6),
      I3 => \^q_1\(6),
      O => \ctr_state_reg[7]_2\(3)
    );
\i__carry_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(5),
      I1 => \pwm_a_p_i0_inferred__1/i__carry__0\(5),
      I2 => \pwm_a_p_i0_inferred__1/i__carry__0\(4),
      I3 => \^q_1\(4),
      O => DI(2)
    );
\i__carry_i_2__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(5),
      I1 => \pwm_a_p_i0_inferred__3/i__carry__0\(5),
      I2 => \pwm_a_p_i0_inferred__3/i__carry__0\(4),
      I3 => \^q_1\(4),
      O => \ctr_state_reg[7]_0\(2)
    );
\i__carry_i_2__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(5),
      I1 => \pwm_a_p_i0_inferred__5/i__carry__0\(5),
      I2 => \pwm_a_p_i0_inferred__5/i__carry__0\(4),
      I3 => \^q_1\(4),
      O => \ctr_state_reg[7]_1\(2)
    );
\i__carry_i_2__6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(5),
      I1 => \pwm_a_p_i0_inferred__7/i__carry__0\(5),
      I2 => \pwm_a_p_i0_inferred__7/i__carry__0\(4),
      I3 => \^q_1\(4),
      O => \ctr_state_reg[7]_2\(2)
    );
\i__carry_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(3),
      I1 => \pwm_a_p_i0_inferred__1/i__carry__0\(3),
      I2 => \pwm_a_p_i0_inferred__1/i__carry__0\(2),
      I3 => \^q_1\(2),
      O => DI(1)
    );
\i__carry_i_3__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(3),
      I1 => \pwm_a_p_i0_inferred__3/i__carry__0\(3),
      I2 => \pwm_a_p_i0_inferred__3/i__carry__0\(2),
      I3 => \^q_1\(2),
      O => \ctr_state_reg[7]_0\(1)
    );
\i__carry_i_3__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(3),
      I1 => \pwm_a_p_i0_inferred__5/i__carry__0\(3),
      I2 => \pwm_a_p_i0_inferred__5/i__carry__0\(2),
      I3 => \^q_1\(2),
      O => \ctr_state_reg[7]_1\(1)
    );
\i__carry_i_3__6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(3),
      I1 => \pwm_a_p_i0_inferred__7/i__carry__0\(3),
      I2 => \pwm_a_p_i0_inferred__7/i__carry__0\(2),
      I3 => \^q_1\(2),
      O => \ctr_state_reg[7]_2\(1)
    );
\i__carry_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(1),
      I1 => \pwm_a_p_i0_inferred__1/i__carry__0\(1),
      I2 => \pwm_a_p_i0_inferred__1/i__carry__0\(0),
      I3 => \^q_1\(0),
      O => DI(0)
    );
\i__carry_i_4__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(1),
      I1 => \pwm_a_p_i0_inferred__3/i__carry__0\(1),
      I2 => \pwm_a_p_i0_inferred__3/i__carry__0\(0),
      I3 => \^q_1\(0),
      O => \ctr_state_reg[7]_0\(0)
    );
\i__carry_i_4__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(1),
      I1 => \pwm_a_p_i0_inferred__5/i__carry__0\(1),
      I2 => \pwm_a_p_i0_inferred__5/i__carry__0\(0),
      I3 => \^q_1\(0),
      O => \ctr_state_reg[7]_1\(0)
    );
\i__carry_i_4__6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^q_1\(1),
      I1 => \pwm_a_p_i0_inferred__7/i__carry__0\(1),
      I2 => \pwm_a_p_i0_inferred__7/i__carry__0\(0),
      I3 => \^q_1\(0),
      O => \ctr_state_reg[7]_2\(0)
    );
\pwm_a_p_i0_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q_1\(12),
      I1 => Q(12),
      O => \ctr_state_reg[12]_0\(2)
    );
\pwm_a_p_i0_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^q_1\(11),
      I1 => Q(11),
      I2 => \^q_1\(10),
      I3 => Q(10),
      O => \ctr_state_reg[12]_0\(1)
    );
\pwm_a_p_i0_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^q_1\(9),
      I1 => Q(9),
      I2 => \^q_1\(8),
      I3 => Q(8),
      O => \ctr_state_reg[12]_0\(0)
    );
pwm_a_p_i0_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^q_1\(7),
      I1 => Q(7),
      I2 => \^q_1\(6),
      I3 => Q(6),
      O => S(3)
    );
pwm_a_p_i0_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^q_1\(5),
      I1 => Q(5),
      I2 => \^q_1\(4),
      I3 => Q(4),
      O => S(2)
    );
pwm_a_p_i0_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^q_1\(3),
      I1 => Q(3),
      I2 => \^q_1\(2),
      I3 => Q(2),
      O => S(1)
    );
pwm_a_p_i0_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^q_1\(1),
      I1 => Q(1),
      I2 => \^q_1\(0),
      I3 => Q(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_pwm_x40_ip_0_0_N_bits_counter_0 is
  port (
    counter_phase180_output : out STD_LOGIC_VECTOR ( 12 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[12]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \ctr_state_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[12]_1\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \ctr_state_reg[7]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[12]_2\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \ctr_state_reg[7]_2\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[12]_3\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \ctr_state_reg[7]_3\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[12]_4\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    counter_phase180_tc : out STD_LOGIC;
    external_reset_i : in STD_LOGIC;
    counter_phase180_dir_reg : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_b_p_i0_inferred__1/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_b_p_i0_inferred__3/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_b_p_i0_inferred__5/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_b_p_i0_inferred__7/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    CLK_100MHZ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_pwm_x40_ip_0_0_N_bits_counter_0 : entity is "N_bits_counter";
end system_pwm_x40_ip_0_0_N_bits_counter_0;

architecture STRUCTURE of system_pwm_x40_ip_0_0_N_bits_counter_0 is
  signal \^counter_phase180_output\ : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \ctr_state[10]_i_1__1_n_0\ : STD_LOGIC;
  signal \ctr_state[11]_i_1__2_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_10__1_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_11__1_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_12__1_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_13__0_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_2__1_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_4_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_6__1_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_7__1_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_8__1_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_9__1_n_0\ : STD_LOGIC;
  signal \ctr_state[1]_i_1__1_n_0\ : STD_LOGIC;
  signal \ctr_state[2]_i_1__2_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_1__1_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_4__1_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_5__1_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_6__1_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_7__1_n_0\ : STD_LOGIC;
  signal \ctr_state[5]_i_1__1_n_0\ : STD_LOGIC;
  signal \ctr_state[6]_i_1__2_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_4__1_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_5__1_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_6__1_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_7__1_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_3__1_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_3__1_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_3__1_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_5_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_5_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_5_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_5_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_5_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_5_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_5_n_7\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2__1_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2__1_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2__1_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2__1_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__1_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__1_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__1_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__1_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__1_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__1_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__1_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__1_n_7\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2__1_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2__1_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2__1_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2__1_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__1_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__1_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__1_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__1_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__1_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__1_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__1_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__1_n_7\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 12 downto 1 );
  signal p_1_in : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \NLW_ctr_state_reg[12]_i_3__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ctr_state_reg[12]_i_5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of counter_phase180_dir_i_2 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \ctr_state[0]_i_1__0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \ctr_state[12]_i_12__1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \ctr_state[12]_i_6__1\ : label is "soft_lutpair3";
begin
  counter_phase180_output(12 downto 0) <= \^counter_phase180_output\(12 downto 0);
counter_phase180_dir_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => \ctr_state[12]_i_2__1_n_0\,
      I1 => counter_phase180_dir_reg,
      I2 => \ctr_state[12]_i_4_n_0\,
      O => counter_phase180_tc
    );
\ctr_state[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00001103"
    )
        port map (
      I0 => \ctr_state[12]_i_4_n_0\,
      I1 => \^counter_phase180_output\(0),
      I2 => \ctr_state[12]_i_2__1_n_0\,
      I3 => counter_phase180_dir_reg,
      I4 => external_reset_i,
      O => p_1_in(0)
    );
\ctr_state[10]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(10),
      I2 => \ctr_state[12]_i_2__1_n_0\,
      I3 => counter_phase180_dir_reg,
      I4 => \ctr_state_reg[12]_i_5_n_6\,
      I5 => \ctr_state[12]_i_4_n_0\,
      O => \ctr_state[10]_i_1__1_n_0\
    );
\ctr_state[11]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(11),
      I2 => \ctr_state[12]_i_2__1_n_0\,
      I3 => counter_phase180_dir_reg,
      I4 => \ctr_state_reg[12]_i_5_n_5\,
      I5 => \ctr_state[12]_i_4_n_0\,
      O => \ctr_state[11]_i_1__2_n_0\
    );
\ctr_state[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFEFFFEAAFEAAFE"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__1_n_0\,
      I2 => data0(12),
      I3 => counter_phase180_dir_reg,
      I4 => \ctr_state[12]_i_4_n_0\,
      I5 => \ctr_state_reg[12]_i_5_n_4\,
      O => p_1_in(12)
    );
\ctr_state[12]_i_10__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase180_output\(10),
      O => \ctr_state[12]_i_10__1_n_0\
    );
\ctr_state[12]_i_11__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase180_output\(9),
      O => \ctr_state[12]_i_11__1_n_0\
    );
\ctr_state[12]_i_12__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^counter_phase180_output\(5),
      I1 => \^counter_phase180_output\(4),
      I2 => \^counter_phase180_output\(11),
      I3 => \^counter_phase180_output\(6),
      O => \ctr_state[12]_i_12__1_n_0\
    );
\ctr_state[12]_i_13__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => \^counter_phase180_output\(3),
      I1 => \^counter_phase180_output\(12),
      I2 => \^counter_phase180_output\(8),
      I3 => \^counter_phase180_output\(9),
      I4 => \^counter_phase180_output\(7),
      O => \ctr_state[12]_i_13__0_n_0\
    );
\ctr_state[12]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \ctr_state[12]_i_6__1_n_0\,
      I1 => \^counter_phase180_output\(3),
      I2 => \^counter_phase180_output\(2),
      I3 => \^counter_phase180_output\(1),
      I4 => \^counter_phase180_output\(0),
      I5 => \ctr_state[12]_i_7__1_n_0\,
      O => \ctr_state[12]_i_2__1_n_0\
    );
\ctr_state[12]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \ctr_state[12]_i_12__1_n_0\,
      I1 => \^counter_phase180_output\(1),
      I2 => \^counter_phase180_output\(0),
      I3 => \^counter_phase180_output\(10),
      I4 => \^counter_phase180_output\(2),
      I5 => \ctr_state[12]_i_13__0_n_0\,
      O => \ctr_state[12]_i_4_n_0\
    );
\ctr_state[12]_i_6__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^counter_phase180_output\(5),
      I1 => \^counter_phase180_output\(4),
      I2 => \^counter_phase180_output\(7),
      I3 => \^counter_phase180_output\(6),
      O => \ctr_state[12]_i_6__1_n_0\
    );
\ctr_state[12]_i_7__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^counter_phase180_output\(11),
      I1 => \^counter_phase180_output\(8),
      I2 => \^counter_phase180_output\(9),
      I3 => \^counter_phase180_output\(10),
      I4 => \^counter_phase180_output\(12),
      O => \ctr_state[12]_i_7__1_n_0\
    );
\ctr_state[12]_i_8__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase180_output\(12),
      O => \ctr_state[12]_i_8__1_n_0\
    );
\ctr_state[12]_i_9__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase180_output\(11),
      O => \ctr_state[12]_i_9__1_n_0\
    );
\ctr_state[1]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(1),
      I2 => \ctr_state[12]_i_2__1_n_0\,
      I3 => counter_phase180_dir_reg,
      I4 => \ctr_state_reg[4]_i_3__1_n_7\,
      I5 => \ctr_state[12]_i_4_n_0\,
      O => \ctr_state[1]_i_1__1_n_0\
    );
\ctr_state[2]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(2),
      I2 => \ctr_state[12]_i_2__1_n_0\,
      I3 => counter_phase180_dir_reg,
      I4 => \ctr_state_reg[4]_i_3__1_n_6\,
      I5 => \ctr_state[12]_i_4_n_0\,
      O => \ctr_state[2]_i_1__2_n_0\
    );
\ctr_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFEFFFECCFECCFE"
    )
        port map (
      I0 => \ctr_state[12]_i_2__1_n_0\,
      I1 => external_reset_i,
      I2 => data0(3),
      I3 => counter_phase180_dir_reg,
      I4 => \ctr_state[12]_i_4_n_0\,
      I5 => \ctr_state_reg[4]_i_3__1_n_5\,
      O => p_1_in(3)
    );
\ctr_state[4]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(4),
      I2 => \ctr_state[12]_i_2__1_n_0\,
      I3 => counter_phase180_dir_reg,
      I4 => \ctr_state_reg[4]_i_3__1_n_4\,
      I5 => \ctr_state[12]_i_4_n_0\,
      O => \ctr_state[4]_i_1__1_n_0\
    );
\ctr_state[4]_i_4__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase180_output\(4),
      O => \ctr_state[4]_i_4__1_n_0\
    );
\ctr_state[4]_i_5__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase180_output\(3),
      O => \ctr_state[4]_i_5__1_n_0\
    );
\ctr_state[4]_i_6__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase180_output\(2),
      O => \ctr_state[4]_i_6__1_n_0\
    );
\ctr_state[4]_i_7__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase180_output\(1),
      O => \ctr_state[4]_i_7__1_n_0\
    );
\ctr_state[5]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(5),
      I2 => \ctr_state[12]_i_2__1_n_0\,
      I3 => counter_phase180_dir_reg,
      I4 => \ctr_state_reg[8]_i_3__1_n_7\,
      I5 => \ctr_state[12]_i_4_n_0\,
      O => \ctr_state[5]_i_1__1_n_0\
    );
\ctr_state[6]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(6),
      I2 => \ctr_state[12]_i_2__1_n_0\,
      I3 => counter_phase180_dir_reg,
      I4 => \ctr_state_reg[8]_i_3__1_n_6\,
      I5 => \ctr_state[12]_i_4_n_0\,
      O => \ctr_state[6]_i_1__2_n_0\
    );
\ctr_state[7]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFEFFFECCFECCFE"
    )
        port map (
      I0 => \ctr_state[12]_i_2__1_n_0\,
      I1 => external_reset_i,
      I2 => data0(7),
      I3 => counter_phase180_dir_reg,
      I4 => \ctr_state[12]_i_4_n_0\,
      I5 => \ctr_state_reg[8]_i_3__1_n_5\,
      O => p_1_in(7)
    );
\ctr_state[8]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFEFFFECCFECCFE"
    )
        port map (
      I0 => \ctr_state[12]_i_2__1_n_0\,
      I1 => external_reset_i,
      I2 => data0(8),
      I3 => counter_phase180_dir_reg,
      I4 => \ctr_state[12]_i_4_n_0\,
      I5 => \ctr_state_reg[8]_i_3__1_n_4\,
      O => p_1_in(8)
    );
\ctr_state[8]_i_4__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase180_output\(8),
      O => \ctr_state[8]_i_4__1_n_0\
    );
\ctr_state[8]_i_5__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase180_output\(7),
      O => \ctr_state[8]_i_5__1_n_0\
    );
\ctr_state[8]_i_6__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase180_output\(6),
      O => \ctr_state[8]_i_6__1_n_0\
    );
\ctr_state[8]_i_7__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase180_output\(5),
      O => \ctr_state[8]_i_7__1_n_0\
    );
\ctr_state[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFEFFFECCFECCFE"
    )
        port map (
      I0 => \ctr_state[12]_i_2__1_n_0\,
      I1 => external_reset_i,
      I2 => data0(9),
      I3 => counter_phase180_dir_reg,
      I4 => \ctr_state[12]_i_4_n_0\,
      I5 => \ctr_state_reg[12]_i_5_n_7\,
      O => p_1_in(9)
    );
\ctr_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(0),
      Q => \^counter_phase180_output\(0),
      R => '0'
    );
\ctr_state_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[10]_i_1__1_n_0\,
      Q => \^counter_phase180_output\(10),
      R => '0'
    );
\ctr_state_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[11]_i_1__2_n_0\,
      Q => \^counter_phase180_output\(11),
      R => '0'
    );
\ctr_state_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(12),
      Q => \^counter_phase180_output\(12),
      R => '0'
    );
\ctr_state_reg[12]_i_3__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[8]_i_2__1_n_0\,
      CO(3) => \NLW_ctr_state_reg[12]_i_3__1_CO_UNCONNECTED\(3),
      CO(2) => \ctr_state_reg[12]_i_3__1_n_1\,
      CO(1) => \ctr_state_reg[12]_i_3__1_n_2\,
      CO(0) => \ctr_state_reg[12]_i_3__1_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \^counter_phase180_output\(11 downto 9),
      O(3 downto 0) => data0(12 downto 9),
      S(3) => \ctr_state[12]_i_8__1_n_0\,
      S(2) => \ctr_state[12]_i_9__1_n_0\,
      S(1) => \ctr_state[12]_i_10__1_n_0\,
      S(0) => \ctr_state[12]_i_11__1_n_0\
    );
\ctr_state_reg[12]_i_5\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[8]_i_3__1_n_0\,
      CO(3) => \NLW_ctr_state_reg[12]_i_5_CO_UNCONNECTED\(3),
      CO(2) => \ctr_state_reg[12]_i_5_n_1\,
      CO(1) => \ctr_state_reg[12]_i_5_n_2\,
      CO(0) => \ctr_state_reg[12]_i_5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[12]_i_5_n_4\,
      O(2) => \ctr_state_reg[12]_i_5_n_5\,
      O(1) => \ctr_state_reg[12]_i_5_n_6\,
      O(0) => \ctr_state_reg[12]_i_5_n_7\,
      S(3 downto 0) => \^counter_phase180_output\(12 downto 9)
    );
\ctr_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[1]_i_1__1_n_0\,
      Q => \^counter_phase180_output\(1),
      R => '0'
    );
\ctr_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[2]_i_1__2_n_0\,
      Q => \^counter_phase180_output\(2),
      R => '0'
    );
\ctr_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(3),
      Q => \^counter_phase180_output\(3),
      R => '0'
    );
\ctr_state_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[4]_i_1__1_n_0\,
      Q => \^counter_phase180_output\(4),
      R => '0'
    );
\ctr_state_reg[4]_i_2__1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ctr_state_reg[4]_i_2__1_n_0\,
      CO(2) => \ctr_state_reg[4]_i_2__1_n_1\,
      CO(1) => \ctr_state_reg[4]_i_2__1_n_2\,
      CO(0) => \ctr_state_reg[4]_i_2__1_n_3\,
      CYINIT => \^counter_phase180_output\(0),
      DI(3 downto 0) => \^counter_phase180_output\(4 downto 1),
      O(3 downto 0) => data0(4 downto 1),
      S(3) => \ctr_state[4]_i_4__1_n_0\,
      S(2) => \ctr_state[4]_i_5__1_n_0\,
      S(1) => \ctr_state[4]_i_6__1_n_0\,
      S(0) => \ctr_state[4]_i_7__1_n_0\
    );
\ctr_state_reg[4]_i_3__1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ctr_state_reg[4]_i_3__1_n_0\,
      CO(2) => \ctr_state_reg[4]_i_3__1_n_1\,
      CO(1) => \ctr_state_reg[4]_i_3__1_n_2\,
      CO(0) => \ctr_state_reg[4]_i_3__1_n_3\,
      CYINIT => \^counter_phase180_output\(0),
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[4]_i_3__1_n_4\,
      O(2) => \ctr_state_reg[4]_i_3__1_n_5\,
      O(1) => \ctr_state_reg[4]_i_3__1_n_6\,
      O(0) => \ctr_state_reg[4]_i_3__1_n_7\,
      S(3 downto 0) => \^counter_phase180_output\(4 downto 1)
    );
\ctr_state_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[5]_i_1__1_n_0\,
      Q => \^counter_phase180_output\(5),
      R => '0'
    );
\ctr_state_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[6]_i_1__2_n_0\,
      Q => \^counter_phase180_output\(6),
      R => '0'
    );
\ctr_state_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(7),
      Q => \^counter_phase180_output\(7),
      R => '0'
    );
\ctr_state_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(8),
      Q => \^counter_phase180_output\(8),
      R => '0'
    );
\ctr_state_reg[8]_i_2__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[4]_i_2__1_n_0\,
      CO(3) => \ctr_state_reg[8]_i_2__1_n_0\,
      CO(2) => \ctr_state_reg[8]_i_2__1_n_1\,
      CO(1) => \ctr_state_reg[8]_i_2__1_n_2\,
      CO(0) => \ctr_state_reg[8]_i_2__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^counter_phase180_output\(8 downto 5),
      O(3 downto 0) => data0(8 downto 5),
      S(3) => \ctr_state[8]_i_4__1_n_0\,
      S(2) => \ctr_state[8]_i_5__1_n_0\,
      S(1) => \ctr_state[8]_i_6__1_n_0\,
      S(0) => \ctr_state[8]_i_7__1_n_0\
    );
\ctr_state_reg[8]_i_3__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[4]_i_3__1_n_0\,
      CO(3) => \ctr_state_reg[8]_i_3__1_n_0\,
      CO(2) => \ctr_state_reg[8]_i_3__1_n_1\,
      CO(1) => \ctr_state_reg[8]_i_3__1_n_2\,
      CO(0) => \ctr_state_reg[8]_i_3__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[8]_i_3__1_n_4\,
      O(2) => \ctr_state_reg[8]_i_3__1_n_5\,
      O(1) => \ctr_state_reg[8]_i_3__1_n_6\,
      O(0) => \ctr_state_reg[8]_i_3__1_n_7\,
      S(3 downto 0) => \^counter_phase180_output\(8 downto 5)
    );
\ctr_state_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(9),
      Q => \^counter_phase180_output\(9),
      R => '0'
    );
\i__carry__0_i_4__11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase180_output\(12),
      I1 => \pwm_b_p_i0_inferred__3/i__carry__0\(12),
      O => \ctr_state_reg[12]_2\(2)
    );
\i__carry__0_i_4__13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase180_output\(12),
      I1 => \pwm_b_p_i0_inferred__5/i__carry__0\(12),
      O => \ctr_state_reg[12]_3\(2)
    );
\i__carry__0_i_4__15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase180_output\(12),
      I1 => \pwm_b_p_i0_inferred__7/i__carry__0\(12),
      O => \ctr_state_reg[12]_4\(2)
    );
\i__carry__0_i_4__9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase180_output\(12),
      I1 => \pwm_b_p_i0_inferred__1/i__carry__0\(12),
      O => \ctr_state_reg[12]_1\(2)
    );
\i__carry__0_i_5__11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(11),
      I1 => \pwm_b_p_i0_inferred__3/i__carry__0\(11),
      I2 => \^counter_phase180_output\(10),
      I3 => \pwm_b_p_i0_inferred__3/i__carry__0\(10),
      O => \ctr_state_reg[12]_2\(1)
    );
\i__carry__0_i_5__13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(11),
      I1 => \pwm_b_p_i0_inferred__5/i__carry__0\(11),
      I2 => \^counter_phase180_output\(10),
      I3 => \pwm_b_p_i0_inferred__5/i__carry__0\(10),
      O => \ctr_state_reg[12]_3\(1)
    );
\i__carry__0_i_5__15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(11),
      I1 => \pwm_b_p_i0_inferred__7/i__carry__0\(11),
      I2 => \^counter_phase180_output\(10),
      I3 => \pwm_b_p_i0_inferred__7/i__carry__0\(10),
      O => \ctr_state_reg[12]_4\(1)
    );
\i__carry__0_i_5__9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(11),
      I1 => \pwm_b_p_i0_inferred__1/i__carry__0\(11),
      I2 => \^counter_phase180_output\(10),
      I3 => \pwm_b_p_i0_inferred__1/i__carry__0\(10),
      O => \ctr_state_reg[12]_1\(1)
    );
\i__carry__0_i_6__11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(9),
      I1 => \pwm_b_p_i0_inferred__3/i__carry__0\(9),
      I2 => \^counter_phase180_output\(8),
      I3 => \pwm_b_p_i0_inferred__3/i__carry__0\(8),
      O => \ctr_state_reg[12]_2\(0)
    );
\i__carry__0_i_6__13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(9),
      I1 => \pwm_b_p_i0_inferred__5/i__carry__0\(9),
      I2 => \^counter_phase180_output\(8),
      I3 => \pwm_b_p_i0_inferred__5/i__carry__0\(8),
      O => \ctr_state_reg[12]_3\(0)
    );
\i__carry__0_i_6__15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(9),
      I1 => \pwm_b_p_i0_inferred__7/i__carry__0\(9),
      I2 => \^counter_phase180_output\(8),
      I3 => \pwm_b_p_i0_inferred__7/i__carry__0\(8),
      O => \ctr_state_reg[12]_4\(0)
    );
\i__carry__0_i_6__9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(9),
      I1 => \pwm_b_p_i0_inferred__1/i__carry__0\(9),
      I2 => \^counter_phase180_output\(8),
      I3 => \pwm_b_p_i0_inferred__1/i__carry__0\(8),
      O => \ctr_state_reg[12]_1\(0)
    );
\i__carry_i_5__11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(7),
      I1 => \pwm_b_p_i0_inferred__3/i__carry__0\(7),
      I2 => \^counter_phase180_output\(6),
      I3 => \pwm_b_p_i0_inferred__3/i__carry__0\(6),
      O => \ctr_state_reg[7]_1\(3)
    );
\i__carry_i_5__13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(7),
      I1 => \pwm_b_p_i0_inferred__5/i__carry__0\(7),
      I2 => \^counter_phase180_output\(6),
      I3 => \pwm_b_p_i0_inferred__5/i__carry__0\(6),
      O => \ctr_state_reg[7]_2\(3)
    );
\i__carry_i_5__15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(7),
      I1 => \pwm_b_p_i0_inferred__7/i__carry__0\(7),
      I2 => \^counter_phase180_output\(6),
      I3 => \pwm_b_p_i0_inferred__7/i__carry__0\(6),
      O => \ctr_state_reg[7]_3\(3)
    );
\i__carry_i_5__9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(7),
      I1 => \pwm_b_p_i0_inferred__1/i__carry__0\(7),
      I2 => \^counter_phase180_output\(6),
      I3 => \pwm_b_p_i0_inferred__1/i__carry__0\(6),
      O => \ctr_state_reg[7]_0\(3)
    );
\i__carry_i_6__11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(5),
      I1 => \pwm_b_p_i0_inferred__3/i__carry__0\(5),
      I2 => \^counter_phase180_output\(4),
      I3 => \pwm_b_p_i0_inferred__3/i__carry__0\(4),
      O => \ctr_state_reg[7]_1\(2)
    );
\i__carry_i_6__13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(5),
      I1 => \pwm_b_p_i0_inferred__5/i__carry__0\(5),
      I2 => \^counter_phase180_output\(4),
      I3 => \pwm_b_p_i0_inferred__5/i__carry__0\(4),
      O => \ctr_state_reg[7]_2\(2)
    );
\i__carry_i_6__15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(5),
      I1 => \pwm_b_p_i0_inferred__7/i__carry__0\(5),
      I2 => \^counter_phase180_output\(4),
      I3 => \pwm_b_p_i0_inferred__7/i__carry__0\(4),
      O => \ctr_state_reg[7]_3\(2)
    );
\i__carry_i_6__9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(5),
      I1 => \pwm_b_p_i0_inferred__1/i__carry__0\(5),
      I2 => \^counter_phase180_output\(4),
      I3 => \pwm_b_p_i0_inferred__1/i__carry__0\(4),
      O => \ctr_state_reg[7]_0\(2)
    );
\i__carry_i_7__11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(3),
      I1 => \pwm_b_p_i0_inferred__3/i__carry__0\(3),
      I2 => \^counter_phase180_output\(2),
      I3 => \pwm_b_p_i0_inferred__3/i__carry__0\(2),
      O => \ctr_state_reg[7]_1\(1)
    );
\i__carry_i_7__13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(3),
      I1 => \pwm_b_p_i0_inferred__5/i__carry__0\(3),
      I2 => \^counter_phase180_output\(2),
      I3 => \pwm_b_p_i0_inferred__5/i__carry__0\(2),
      O => \ctr_state_reg[7]_2\(1)
    );
\i__carry_i_7__15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(3),
      I1 => \pwm_b_p_i0_inferred__7/i__carry__0\(3),
      I2 => \^counter_phase180_output\(2),
      I3 => \pwm_b_p_i0_inferred__7/i__carry__0\(2),
      O => \ctr_state_reg[7]_3\(1)
    );
\i__carry_i_7__9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(3),
      I1 => \pwm_b_p_i0_inferred__1/i__carry__0\(3),
      I2 => \^counter_phase180_output\(2),
      I3 => \pwm_b_p_i0_inferred__1/i__carry__0\(2),
      O => \ctr_state_reg[7]_0\(1)
    );
\i__carry_i_8__11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(1),
      I1 => \pwm_b_p_i0_inferred__3/i__carry__0\(1),
      I2 => \^counter_phase180_output\(0),
      I3 => \pwm_b_p_i0_inferred__3/i__carry__0\(0),
      O => \ctr_state_reg[7]_1\(0)
    );
\i__carry_i_8__13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(1),
      I1 => \pwm_b_p_i0_inferred__5/i__carry__0\(1),
      I2 => \^counter_phase180_output\(0),
      I3 => \pwm_b_p_i0_inferred__5/i__carry__0\(0),
      O => \ctr_state_reg[7]_2\(0)
    );
\i__carry_i_8__15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(1),
      I1 => \pwm_b_p_i0_inferred__7/i__carry__0\(1),
      I2 => \^counter_phase180_output\(0),
      I3 => \pwm_b_p_i0_inferred__7/i__carry__0\(0),
      O => \ctr_state_reg[7]_3\(0)
    );
\i__carry_i_8__9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(1),
      I1 => \pwm_b_p_i0_inferred__1/i__carry__0\(1),
      I2 => \^counter_phase180_output\(0),
      I3 => \pwm_b_p_i0_inferred__1/i__carry__0\(0),
      O => \ctr_state_reg[7]_0\(0)
    );
\pwm_b_p_i0_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase180_output\(12),
      I1 => Q(12),
      O => \ctr_state_reg[12]_0\(2)
    );
\pwm_b_p_i0_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(11),
      I1 => Q(11),
      I2 => \^counter_phase180_output\(10),
      I3 => Q(10),
      O => \ctr_state_reg[12]_0\(1)
    );
\pwm_b_p_i0_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(9),
      I1 => Q(9),
      I2 => \^counter_phase180_output\(8),
      I3 => Q(8),
      O => \ctr_state_reg[12]_0\(0)
    );
pwm_b_p_i0_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(7),
      I1 => Q(7),
      I2 => \^counter_phase180_output\(6),
      I3 => Q(6),
      O => S(3)
    );
pwm_b_p_i0_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(5),
      I1 => Q(5),
      I2 => \^counter_phase180_output\(4),
      I3 => Q(4),
      O => S(2)
    );
pwm_b_p_i0_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(3),
      I1 => Q(3),
      I2 => \^counter_phase180_output\(2),
      I3 => Q(2),
      O => S(1)
    );
pwm_b_p_i0_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase180_output\(1),
      I1 => Q(1),
      I2 => \^counter_phase180_output\(0),
      I3 => Q(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_pwm_x40_ip_0_0_N_bits_counter_1 is
  port (
    counter_phase270_output : out STD_LOGIC_VECTOR ( 12 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[12]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \ctr_state_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[12]_1\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \ctr_state_reg[7]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[12]_2\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \ctr_state_reg[7]_2\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[12]_3\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \ctr_state_reg[7]_3\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[12]_4\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    counter_phase270_tc : out STD_LOGIC;
    external_reset_i : in STD_LOGIC;
    \ctr_state_reg[0]_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_b_p_i0_inferred__2/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_b_p_i0_inferred__4/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_b_p_i0_inferred__6/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_b_p_i0_inferred__8/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    CLK_100MHZ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_pwm_x40_ip_0_0_N_bits_counter_1 : entity is "N_bits_counter";
end system_pwm_x40_ip_0_0_N_bits_counter_1;

architecture STRUCTURE of system_pwm_x40_ip_0_0_N_bits_counter_1 is
  signal \^counter_phase270_output\ : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \ctr_state[10]_i_1__2_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_10__2_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_11__2_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_12__2_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_1__2_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_2__2_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_5__1_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_6__2_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_7__2_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_8__2_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_9__2_n_0\ : STD_LOGIC;
  signal \ctr_state[1]_i_1__2_n_0\ : STD_LOGIC;
  signal \ctr_state[3]_i_1__2_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_1__2_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_4__2_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_5__2_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_6__2_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_7__2_n_0\ : STD_LOGIC;
  signal \ctr_state[5]_i_1__2_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_4__2_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_5__2_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_6__2_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_7__2_n_0\ : STD_LOGIC;
  signal \ctr_state[9]_i_1__2_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_3__2_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_3__2_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_3__2_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4__1_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4__1_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4__1_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4__1_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4__1_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4__1_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4__1_n_7\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2__2_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2__2_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2__2_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2__2_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__2_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__2_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__2_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__2_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__2_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__2_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__2_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__2_n_7\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2__2_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2__2_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2__2_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2__2_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__2_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__2_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__2_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__2_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__2_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__2_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__2_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__2_n_7\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 12 downto 1 );
  signal p_1_in : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \NLW_ctr_state_reg[12]_i_3__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ctr_state_reg[12]_i_4__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of counter_phase270_dir_i_2 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \ctr_state[0]_i_1__1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \ctr_state[12]_i_12__2\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \ctr_state[12]_i_6__2\ : label is "soft_lutpair5";
begin
  counter_phase270_output(12 downto 0) <= \^counter_phase270_output\(12 downto 0);
counter_phase270_dir_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \ctr_state[12]_i_5__1_n_0\,
      I1 => \ctr_state_reg[0]_0\,
      I2 => \ctr_state[12]_i_2__2_n_0\,
      O => counter_phase270_tc
    );
\ctr_state[0]_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00011101"
    )
        port map (
      I0 => external_reset_i,
      I1 => \^counter_phase270_output\(0),
      I2 => \ctr_state[12]_i_2__2_n_0\,
      I3 => \ctr_state_reg[0]_0\,
      I4 => \ctr_state[12]_i_5__1_n_0\,
      O => p_1_in(0)
    );
\ctr_state[10]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(10),
      I2 => \ctr_state[12]_i_2__2_n_0\,
      I3 => \ctr_state_reg[0]_0\,
      I4 => \ctr_state_reg[12]_i_4__1_n_6\,
      I5 => \ctr_state[12]_i_5__1_n_0\,
      O => \ctr_state[10]_i_1__2_n_0\
    );
\ctr_state[11]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABAFFBAAABAAABA"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__2_n_0\,
      I2 => data0(11),
      I3 => \ctr_state_reg[0]_0\,
      I4 => \ctr_state[12]_i_5__1_n_0\,
      I5 => \ctr_state_reg[12]_i_4__1_n_5\,
      O => p_1_in(11)
    );
\ctr_state[12]_i_10__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase270_output\(10),
      O => \ctr_state[12]_i_10__2_n_0\
    );
\ctr_state[12]_i_11__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase270_output\(9),
      O => \ctr_state[12]_i_11__2_n_0\
    );
\ctr_state[12]_i_12__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => \^counter_phase270_output\(9),
      I1 => \^counter_phase270_output\(8),
      I2 => \^counter_phase270_output\(3),
      I3 => \^counter_phase270_output\(7),
      I4 => \^counter_phase270_output\(12),
      O => \ctr_state[12]_i_12__2_n_0\
    );
\ctr_state[12]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0054005455540054"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__2_n_0\,
      I2 => data0(12),
      I3 => \ctr_state_reg[0]_0\,
      I4 => \ctr_state_reg[12]_i_4__1_n_4\,
      I5 => \ctr_state[12]_i_5__1_n_0\,
      O => \ctr_state[12]_i_1__2_n_0\
    );
\ctr_state[12]_i_2__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \ctr_state[12]_i_6__2_n_0\,
      I1 => \ctr_state[12]_i_7__2_n_0\,
      I2 => \^counter_phase270_output\(1),
      I3 => \^counter_phase270_output\(0),
      I4 => \^counter_phase270_output\(10),
      I5 => \^counter_phase270_output\(2),
      O => \ctr_state[12]_i_2__2_n_0\
    );
\ctr_state[12]_i_5__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \ctr_state[12]_i_7__2_n_0\,
      I1 => \^counter_phase270_output\(1),
      I2 => \^counter_phase270_output\(0),
      I3 => \^counter_phase270_output\(10),
      I4 => \^counter_phase270_output\(2),
      I5 => \ctr_state[12]_i_12__2_n_0\,
      O => \ctr_state[12]_i_5__1_n_0\
    );
\ctr_state[12]_i_6__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \^counter_phase270_output\(9),
      I1 => \^counter_phase270_output\(8),
      I2 => \^counter_phase270_output\(3),
      I3 => \^counter_phase270_output\(7),
      I4 => \^counter_phase270_output\(12),
      O => \ctr_state[12]_i_6__2_n_0\
    );
\ctr_state[12]_i_7__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^counter_phase270_output\(5),
      I1 => \^counter_phase270_output\(4),
      I2 => \^counter_phase270_output\(11),
      I3 => \^counter_phase270_output\(6),
      O => \ctr_state[12]_i_7__2_n_0\
    );
\ctr_state[12]_i_8__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase270_output\(12),
      O => \ctr_state[12]_i_8__2_n_0\
    );
\ctr_state[12]_i_9__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase270_output\(11),
      O => \ctr_state[12]_i_9__2_n_0\
    );
\ctr_state[1]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(1),
      I2 => \ctr_state[12]_i_2__2_n_0\,
      I3 => \ctr_state_reg[0]_0\,
      I4 => \ctr_state_reg[4]_i_3__2_n_7\,
      I5 => \ctr_state[12]_i_5__1_n_0\,
      O => \ctr_state[1]_i_1__2_n_0\
    );
\ctr_state[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABAFFBAAABAAABA"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__2_n_0\,
      I2 => data0(2),
      I3 => \ctr_state_reg[0]_0\,
      I4 => \ctr_state[12]_i_5__1_n_0\,
      I5 => \ctr_state_reg[4]_i_3__2_n_6\,
      O => p_1_in(2)
    );
\ctr_state[3]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0054005455540054"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__2_n_0\,
      I2 => data0(3),
      I3 => \ctr_state_reg[0]_0\,
      I4 => \ctr_state_reg[4]_i_3__2_n_5\,
      I5 => \ctr_state[12]_i_5__1_n_0\,
      O => \ctr_state[3]_i_1__2_n_0\
    );
\ctr_state[4]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(4),
      I2 => \ctr_state[12]_i_2__2_n_0\,
      I3 => \ctr_state_reg[0]_0\,
      I4 => \ctr_state_reg[4]_i_3__2_n_4\,
      I5 => \ctr_state[12]_i_5__1_n_0\,
      O => \ctr_state[4]_i_1__2_n_0\
    );
\ctr_state[4]_i_4__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase270_output\(4),
      O => \ctr_state[4]_i_4__2_n_0\
    );
\ctr_state[4]_i_5__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase270_output\(3),
      O => \ctr_state[4]_i_5__2_n_0\
    );
\ctr_state[4]_i_6__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase270_output\(2),
      O => \ctr_state[4]_i_6__2_n_0\
    );
\ctr_state[4]_i_7__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase270_output\(1),
      O => \ctr_state[4]_i_7__2_n_0\
    );
\ctr_state[5]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(5),
      I2 => \ctr_state[12]_i_2__2_n_0\,
      I3 => \ctr_state_reg[0]_0\,
      I4 => \ctr_state_reg[8]_i_3__2_n_7\,
      I5 => \ctr_state[12]_i_5__1_n_0\,
      O => \ctr_state[5]_i_1__2_n_0\
    );
\ctr_state[6]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABAFFBAAABAAABA"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__2_n_0\,
      I2 => data0(6),
      I3 => \ctr_state_reg[0]_0\,
      I4 => \ctr_state[12]_i_5__1_n_0\,
      I5 => \ctr_state_reg[8]_i_3__2_n_6\,
      O => p_1_in(6)
    );
\ctr_state[7]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFEFFFEAAFEAAFE"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__2_n_0\,
      I2 => data0(7),
      I3 => \ctr_state_reg[0]_0\,
      I4 => \ctr_state[12]_i_5__1_n_0\,
      I5 => \ctr_state_reg[8]_i_3__2_n_5\,
      O => p_1_in(7)
    );
\ctr_state[8]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFEFFFEAAFEAAFE"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__2_n_0\,
      I2 => data0(8),
      I3 => \ctr_state_reg[0]_0\,
      I4 => \ctr_state[12]_i_5__1_n_0\,
      I5 => \ctr_state_reg[8]_i_3__2_n_4\,
      O => p_1_in(8)
    );
\ctr_state[8]_i_4__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase270_output\(8),
      O => \ctr_state[8]_i_4__2_n_0\
    );
\ctr_state[8]_i_5__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase270_output\(7),
      O => \ctr_state[8]_i_5__2_n_0\
    );
\ctr_state[8]_i_6__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase270_output\(6),
      O => \ctr_state[8]_i_6__2_n_0\
    );
\ctr_state[8]_i_7__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase270_output\(5),
      O => \ctr_state[8]_i_7__2_n_0\
    );
\ctr_state[9]_i_1__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0054005455540054"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__2_n_0\,
      I2 => data0(9),
      I3 => \ctr_state_reg[0]_0\,
      I4 => \ctr_state_reg[12]_i_4__1_n_7\,
      I5 => \ctr_state[12]_i_5__1_n_0\,
      O => \ctr_state[9]_i_1__2_n_0\
    );
\ctr_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(0),
      Q => \^counter_phase270_output\(0),
      R => '0'
    );
\ctr_state_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[10]_i_1__2_n_0\,
      Q => \^counter_phase270_output\(10),
      R => '0'
    );
\ctr_state_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(11),
      Q => \^counter_phase270_output\(11),
      R => '0'
    );
\ctr_state_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[12]_i_1__2_n_0\,
      Q => \^counter_phase270_output\(12),
      R => '0'
    );
\ctr_state_reg[12]_i_3__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[8]_i_2__2_n_0\,
      CO(3) => \NLW_ctr_state_reg[12]_i_3__2_CO_UNCONNECTED\(3),
      CO(2) => \ctr_state_reg[12]_i_3__2_n_1\,
      CO(1) => \ctr_state_reg[12]_i_3__2_n_2\,
      CO(0) => \ctr_state_reg[12]_i_3__2_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \^counter_phase270_output\(11 downto 9),
      O(3 downto 0) => data0(12 downto 9),
      S(3) => \ctr_state[12]_i_8__2_n_0\,
      S(2) => \ctr_state[12]_i_9__2_n_0\,
      S(1) => \ctr_state[12]_i_10__2_n_0\,
      S(0) => \ctr_state[12]_i_11__2_n_0\
    );
\ctr_state_reg[12]_i_4__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[8]_i_3__2_n_0\,
      CO(3) => \NLW_ctr_state_reg[12]_i_4__1_CO_UNCONNECTED\(3),
      CO(2) => \ctr_state_reg[12]_i_4__1_n_1\,
      CO(1) => \ctr_state_reg[12]_i_4__1_n_2\,
      CO(0) => \ctr_state_reg[12]_i_4__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[12]_i_4__1_n_4\,
      O(2) => \ctr_state_reg[12]_i_4__1_n_5\,
      O(1) => \ctr_state_reg[12]_i_4__1_n_6\,
      O(0) => \ctr_state_reg[12]_i_4__1_n_7\,
      S(3 downto 0) => \^counter_phase270_output\(12 downto 9)
    );
\ctr_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[1]_i_1__2_n_0\,
      Q => \^counter_phase270_output\(1),
      R => '0'
    );
\ctr_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(2),
      Q => \^counter_phase270_output\(2),
      R => '0'
    );
\ctr_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[3]_i_1__2_n_0\,
      Q => \^counter_phase270_output\(3),
      R => '0'
    );
\ctr_state_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[4]_i_1__2_n_0\,
      Q => \^counter_phase270_output\(4),
      R => '0'
    );
\ctr_state_reg[4]_i_2__2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ctr_state_reg[4]_i_2__2_n_0\,
      CO(2) => \ctr_state_reg[4]_i_2__2_n_1\,
      CO(1) => \ctr_state_reg[4]_i_2__2_n_2\,
      CO(0) => \ctr_state_reg[4]_i_2__2_n_3\,
      CYINIT => \^counter_phase270_output\(0),
      DI(3 downto 0) => \^counter_phase270_output\(4 downto 1),
      O(3 downto 0) => data0(4 downto 1),
      S(3) => \ctr_state[4]_i_4__2_n_0\,
      S(2) => \ctr_state[4]_i_5__2_n_0\,
      S(1) => \ctr_state[4]_i_6__2_n_0\,
      S(0) => \ctr_state[4]_i_7__2_n_0\
    );
\ctr_state_reg[4]_i_3__2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ctr_state_reg[4]_i_3__2_n_0\,
      CO(2) => \ctr_state_reg[4]_i_3__2_n_1\,
      CO(1) => \ctr_state_reg[4]_i_3__2_n_2\,
      CO(0) => \ctr_state_reg[4]_i_3__2_n_3\,
      CYINIT => \^counter_phase270_output\(0),
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[4]_i_3__2_n_4\,
      O(2) => \ctr_state_reg[4]_i_3__2_n_5\,
      O(1) => \ctr_state_reg[4]_i_3__2_n_6\,
      O(0) => \ctr_state_reg[4]_i_3__2_n_7\,
      S(3 downto 0) => \^counter_phase270_output\(4 downto 1)
    );
\ctr_state_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[5]_i_1__2_n_0\,
      Q => \^counter_phase270_output\(5),
      R => '0'
    );
\ctr_state_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(6),
      Q => \^counter_phase270_output\(6),
      R => '0'
    );
\ctr_state_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(7),
      Q => \^counter_phase270_output\(7),
      R => '0'
    );
\ctr_state_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(8),
      Q => \^counter_phase270_output\(8),
      R => '0'
    );
\ctr_state_reg[8]_i_2__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[4]_i_2__2_n_0\,
      CO(3) => \ctr_state_reg[8]_i_2__2_n_0\,
      CO(2) => \ctr_state_reg[8]_i_2__2_n_1\,
      CO(1) => \ctr_state_reg[8]_i_2__2_n_2\,
      CO(0) => \ctr_state_reg[8]_i_2__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^counter_phase270_output\(8 downto 5),
      O(3 downto 0) => data0(8 downto 5),
      S(3) => \ctr_state[8]_i_4__2_n_0\,
      S(2) => \ctr_state[8]_i_5__2_n_0\,
      S(1) => \ctr_state[8]_i_6__2_n_0\,
      S(0) => \ctr_state[8]_i_7__2_n_0\
    );
\ctr_state_reg[8]_i_3__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[4]_i_3__2_n_0\,
      CO(3) => \ctr_state_reg[8]_i_3__2_n_0\,
      CO(2) => \ctr_state_reg[8]_i_3__2_n_1\,
      CO(1) => \ctr_state_reg[8]_i_3__2_n_2\,
      CO(0) => \ctr_state_reg[8]_i_3__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[8]_i_3__2_n_4\,
      O(2) => \ctr_state_reg[8]_i_3__2_n_5\,
      O(1) => \ctr_state_reg[8]_i_3__2_n_6\,
      O(0) => \ctr_state_reg[8]_i_3__2_n_7\,
      S(3 downto 0) => \^counter_phase270_output\(8 downto 5)
    );
\ctr_state_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[9]_i_1__2_n_0\,
      Q => \^counter_phase270_output\(9),
      R => '0'
    );
\i__carry__0_i_4__10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase270_output\(12),
      I1 => \pwm_b_p_i0_inferred__2/i__carry__0\(12),
      O => \ctr_state_reg[12]_1\(2)
    );
\i__carry__0_i_4__12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase270_output\(12),
      I1 => \pwm_b_p_i0_inferred__4/i__carry__0\(12),
      O => \ctr_state_reg[12]_2\(2)
    );
\i__carry__0_i_4__14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase270_output\(12),
      I1 => \pwm_b_p_i0_inferred__6/i__carry__0\(12),
      O => \ctr_state_reg[12]_3\(2)
    );
\i__carry__0_i_4__16\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase270_output\(12),
      I1 => \pwm_b_p_i0_inferred__8/i__carry__0\(12),
      O => \ctr_state_reg[12]_4\(2)
    );
\i__carry__0_i_4__8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase270_output\(12),
      I1 => Q(12),
      O => \ctr_state_reg[12]_0\(2)
    );
\i__carry__0_i_5__10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(11),
      I1 => \pwm_b_p_i0_inferred__2/i__carry__0\(11),
      I2 => \^counter_phase270_output\(10),
      I3 => \pwm_b_p_i0_inferred__2/i__carry__0\(10),
      O => \ctr_state_reg[12]_1\(1)
    );
\i__carry__0_i_5__12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(11),
      I1 => \pwm_b_p_i0_inferred__4/i__carry__0\(11),
      I2 => \^counter_phase270_output\(10),
      I3 => \pwm_b_p_i0_inferred__4/i__carry__0\(10),
      O => \ctr_state_reg[12]_2\(1)
    );
\i__carry__0_i_5__14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(11),
      I1 => \pwm_b_p_i0_inferred__6/i__carry__0\(11),
      I2 => \^counter_phase270_output\(10),
      I3 => \pwm_b_p_i0_inferred__6/i__carry__0\(10),
      O => \ctr_state_reg[12]_3\(1)
    );
\i__carry__0_i_5__16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(11),
      I1 => \pwm_b_p_i0_inferred__8/i__carry__0\(11),
      I2 => \^counter_phase270_output\(10),
      I3 => \pwm_b_p_i0_inferred__8/i__carry__0\(10),
      O => \ctr_state_reg[12]_4\(1)
    );
\i__carry__0_i_5__8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(11),
      I1 => Q(11),
      I2 => \^counter_phase270_output\(10),
      I3 => Q(10),
      O => \ctr_state_reg[12]_0\(1)
    );
\i__carry__0_i_6__10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(9),
      I1 => \pwm_b_p_i0_inferred__2/i__carry__0\(9),
      I2 => \^counter_phase270_output\(8),
      I3 => \pwm_b_p_i0_inferred__2/i__carry__0\(8),
      O => \ctr_state_reg[12]_1\(0)
    );
\i__carry__0_i_6__12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(9),
      I1 => \pwm_b_p_i0_inferred__4/i__carry__0\(9),
      I2 => \^counter_phase270_output\(8),
      I3 => \pwm_b_p_i0_inferred__4/i__carry__0\(8),
      O => \ctr_state_reg[12]_2\(0)
    );
\i__carry__0_i_6__14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(9),
      I1 => \pwm_b_p_i0_inferred__6/i__carry__0\(9),
      I2 => \^counter_phase270_output\(8),
      I3 => \pwm_b_p_i0_inferred__6/i__carry__0\(8),
      O => \ctr_state_reg[12]_3\(0)
    );
\i__carry__0_i_6__16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(9),
      I1 => \pwm_b_p_i0_inferred__8/i__carry__0\(9),
      I2 => \^counter_phase270_output\(8),
      I3 => \pwm_b_p_i0_inferred__8/i__carry__0\(8),
      O => \ctr_state_reg[12]_4\(0)
    );
\i__carry__0_i_6__8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(9),
      I1 => Q(9),
      I2 => \^counter_phase270_output\(8),
      I3 => Q(8),
      O => \ctr_state_reg[12]_0\(0)
    );
\i__carry_i_5__10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(7),
      I1 => \pwm_b_p_i0_inferred__2/i__carry__0\(7),
      I2 => \^counter_phase270_output\(6),
      I3 => \pwm_b_p_i0_inferred__2/i__carry__0\(6),
      O => \ctr_state_reg[7]_0\(3)
    );
\i__carry_i_5__12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(7),
      I1 => \pwm_b_p_i0_inferred__4/i__carry__0\(7),
      I2 => \^counter_phase270_output\(6),
      I3 => \pwm_b_p_i0_inferred__4/i__carry__0\(6),
      O => \ctr_state_reg[7]_1\(3)
    );
\i__carry_i_5__14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(7),
      I1 => \pwm_b_p_i0_inferred__6/i__carry__0\(7),
      I2 => \^counter_phase270_output\(6),
      I3 => \pwm_b_p_i0_inferred__6/i__carry__0\(6),
      O => \ctr_state_reg[7]_2\(3)
    );
\i__carry_i_5__16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(7),
      I1 => \pwm_b_p_i0_inferred__8/i__carry__0\(7),
      I2 => \^counter_phase270_output\(6),
      I3 => \pwm_b_p_i0_inferred__8/i__carry__0\(6),
      O => \ctr_state_reg[7]_3\(3)
    );
\i__carry_i_5__8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(7),
      I1 => Q(7),
      I2 => \^counter_phase270_output\(6),
      I3 => Q(6),
      O => S(3)
    );
\i__carry_i_6__10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(5),
      I1 => \pwm_b_p_i0_inferred__2/i__carry__0\(5),
      I2 => \^counter_phase270_output\(4),
      I3 => \pwm_b_p_i0_inferred__2/i__carry__0\(4),
      O => \ctr_state_reg[7]_0\(2)
    );
\i__carry_i_6__12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(5),
      I1 => \pwm_b_p_i0_inferred__4/i__carry__0\(5),
      I2 => \^counter_phase270_output\(4),
      I3 => \pwm_b_p_i0_inferred__4/i__carry__0\(4),
      O => \ctr_state_reg[7]_1\(2)
    );
\i__carry_i_6__14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(5),
      I1 => \pwm_b_p_i0_inferred__6/i__carry__0\(5),
      I2 => \^counter_phase270_output\(4),
      I3 => \pwm_b_p_i0_inferred__6/i__carry__0\(4),
      O => \ctr_state_reg[7]_2\(2)
    );
\i__carry_i_6__16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(5),
      I1 => \pwm_b_p_i0_inferred__8/i__carry__0\(5),
      I2 => \^counter_phase270_output\(4),
      I3 => \pwm_b_p_i0_inferred__8/i__carry__0\(4),
      O => \ctr_state_reg[7]_3\(2)
    );
\i__carry_i_6__8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(5),
      I1 => Q(5),
      I2 => \^counter_phase270_output\(4),
      I3 => Q(4),
      O => S(2)
    );
\i__carry_i_7__10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(3),
      I1 => \pwm_b_p_i0_inferred__2/i__carry__0\(3),
      I2 => \^counter_phase270_output\(2),
      I3 => \pwm_b_p_i0_inferred__2/i__carry__0\(2),
      O => \ctr_state_reg[7]_0\(1)
    );
\i__carry_i_7__12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(3),
      I1 => \pwm_b_p_i0_inferred__4/i__carry__0\(3),
      I2 => \^counter_phase270_output\(2),
      I3 => \pwm_b_p_i0_inferred__4/i__carry__0\(2),
      O => \ctr_state_reg[7]_1\(1)
    );
\i__carry_i_7__14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(3),
      I1 => \pwm_b_p_i0_inferred__6/i__carry__0\(3),
      I2 => \^counter_phase270_output\(2),
      I3 => \pwm_b_p_i0_inferred__6/i__carry__0\(2),
      O => \ctr_state_reg[7]_2\(1)
    );
\i__carry_i_7__16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(3),
      I1 => \pwm_b_p_i0_inferred__8/i__carry__0\(3),
      I2 => \^counter_phase270_output\(2),
      I3 => \pwm_b_p_i0_inferred__8/i__carry__0\(2),
      O => \ctr_state_reg[7]_3\(1)
    );
\i__carry_i_7__8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(3),
      I1 => Q(3),
      I2 => \^counter_phase270_output\(2),
      I3 => Q(2),
      O => S(1)
    );
\i__carry_i_8__10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(1),
      I1 => \pwm_b_p_i0_inferred__2/i__carry__0\(1),
      I2 => \^counter_phase270_output\(0),
      I3 => \pwm_b_p_i0_inferred__2/i__carry__0\(0),
      O => \ctr_state_reg[7]_0\(0)
    );
\i__carry_i_8__12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(1),
      I1 => \pwm_b_p_i0_inferred__4/i__carry__0\(1),
      I2 => \^counter_phase270_output\(0),
      I3 => \pwm_b_p_i0_inferred__4/i__carry__0\(0),
      O => \ctr_state_reg[7]_1\(0)
    );
\i__carry_i_8__14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(1),
      I1 => \pwm_b_p_i0_inferred__6/i__carry__0\(1),
      I2 => \^counter_phase270_output\(0),
      I3 => \pwm_b_p_i0_inferred__6/i__carry__0\(0),
      O => \ctr_state_reg[7]_2\(0)
    );
\i__carry_i_8__16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(1),
      I1 => \pwm_b_p_i0_inferred__8/i__carry__0\(1),
      I2 => \^counter_phase270_output\(0),
      I3 => \pwm_b_p_i0_inferred__8/i__carry__0\(0),
      O => \ctr_state_reg[7]_3\(0)
    );
\i__carry_i_8__8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase270_output\(1),
      I1 => Q(1),
      I2 => \^counter_phase270_output\(0),
      I3 => Q(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_pwm_x40_ip_0_0_N_bits_counter_2 is
  port (
    counter_phase90_output : out STD_LOGIC_VECTOR ( 12 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[12]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[11]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \ctr_state_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[11]_1\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \ctr_state_reg[7]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[11]_2\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \ctr_state_reg[7]_2\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \ctr_state_reg[11]_3\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    counter_phase90_tc : out STD_LOGIC;
    \ctr_state_reg[12]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \ctr_state_reg[12]_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \ctr_state_reg[12]_3\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \ctr_state_reg[12]_4\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    external_reset_i : in STD_LOGIC;
    counter_phase90_dir_reg : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_a_p_i0_inferred__2/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_a_p_i0_inferred__4/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_a_p_i0_inferred__6/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \pwm_a_p_i0_inferred__8/i__carry__0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    CLK_100MHZ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_pwm_x40_ip_0_0_N_bits_counter_2 : entity is "N_bits_counter";
end system_pwm_x40_ip_0_0_N_bits_counter_2;

architecture STRUCTURE of system_pwm_x40_ip_0_0_N_bits_counter_2 is
  signal \^counter_phase90_output\ : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \ctr_state[0]_i_1__2_n_0\ : STD_LOGIC;
  signal \ctr_state[10]_i_1__0_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_10__0_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_11__0_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_12__0_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_13_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_1__1_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_2__0_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_5__0_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_6__0_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_7__0_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_8__0_n_0\ : STD_LOGIC;
  signal \ctr_state[12]_i_9__0_n_0\ : STD_LOGIC;
  signal \ctr_state[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \ctr_state[3]_i_1__1_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_4__0_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_5__0_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_6__0_n_0\ : STD_LOGIC;
  signal \ctr_state[4]_i_7__0_n_0\ : STD_LOGIC;
  signal \ctr_state[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_4__0_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_5__0_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_6__0_n_0\ : STD_LOGIC;
  signal \ctr_state[8]_i_7__0_n_0\ : STD_LOGIC;
  signal \ctr_state[9]_i_1__1_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_3__0_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_3__0_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_3__0_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4__0_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4__0_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4__0_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4__0_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4__0_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4__0_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[12]_i_4__0_n_7\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2__0_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2__0_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2__0_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_2__0_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__0_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__0_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__0_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__0_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__0_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__0_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__0_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[4]_i_3__0_n_7\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2__0_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2__0_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2__0_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_2__0_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__0_n_0\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__0_n_1\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__0_n_2\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__0_n_3\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__0_n_4\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__0_n_5\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__0_n_6\ : STD_LOGIC;
  signal \ctr_state_reg[8]_i_3__0_n_7\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 12 downto 1 );
  signal p_1_in : STD_LOGIC_VECTOR ( 11 downto 2 );
  signal \NLW_ctr_state_reg[12]_i_3__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ctr_state_reg[12]_i_4__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of counter_phase90_dir_i_2 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \ctr_state[0]_i_1__2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \ctr_state[12]_i_12__0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \ctr_state[12]_i_6__0\ : label is "soft_lutpair7";
begin
  counter_phase90_output(12 downto 0) <= \^counter_phase90_output\(12 downto 0);
counter_phase90_dir_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \ctr_state[12]_i_5__0_n_0\,
      I1 => counter_phase90_dir_reg,
      I2 => \ctr_state[12]_i_2__0_n_0\,
      O => counter_phase90_tc
    );
\ctr_state[0]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010501"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__0_n_0\,
      I2 => \^counter_phase90_output\(0),
      I3 => counter_phase90_dir_reg,
      I4 => \ctr_state[12]_i_5__0_n_0\,
      O => \ctr_state[0]_i_1__2_n_0\
    );
\ctr_state[10]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(10),
      I2 => \ctr_state[12]_i_2__0_n_0\,
      I3 => counter_phase90_dir_reg,
      I4 => \ctr_state_reg[12]_i_4__0_n_6\,
      I5 => \ctr_state[12]_i_5__0_n_0\,
      O => \ctr_state[10]_i_1__0_n_0\
    );
\ctr_state[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABAFFBAAABAAABA"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__0_n_0\,
      I2 => data0(11),
      I3 => counter_phase90_dir_reg,
      I4 => \ctr_state[12]_i_5__0_n_0\,
      I5 => \ctr_state_reg[12]_i_4__0_n_5\,
      O => p_1_in(11)
    );
\ctr_state[12]_i_10__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase90_output\(10),
      O => \ctr_state[12]_i_10__0_n_0\
    );
\ctr_state[12]_i_11__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase90_output\(9),
      O => \ctr_state[12]_i_11__0_n_0\
    );
\ctr_state[12]_i_12__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^counter_phase90_output\(5),
      I1 => \^counter_phase90_output\(4),
      I2 => \^counter_phase90_output\(11),
      I3 => \^counter_phase90_output\(6),
      O => \ctr_state[12]_i_12__0_n_0\
    );
\ctr_state[12]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => \^counter_phase90_output\(3),
      I1 => \^counter_phase90_output\(12),
      I2 => \^counter_phase90_output\(8),
      I3 => \^counter_phase90_output\(9),
      I4 => \^counter_phase90_output\(7),
      O => \ctr_state[12]_i_13_n_0\
    );
\ctr_state[12]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0054005455540054"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__0_n_0\,
      I2 => data0(12),
      I3 => counter_phase90_dir_reg,
      I4 => \ctr_state_reg[12]_i_4__0_n_4\,
      I5 => \ctr_state[12]_i_5__0_n_0\,
      O => \ctr_state[12]_i_1__1_n_0\
    );
\ctr_state[12]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \ctr_state[12]_i_6__0_n_0\,
      I1 => \^counter_phase90_output\(1),
      I2 => \^counter_phase90_output\(0),
      I3 => \^counter_phase90_output\(3),
      I4 => \^counter_phase90_output\(2),
      I5 => \ctr_state[12]_i_7__0_n_0\,
      O => \ctr_state[12]_i_2__0_n_0\
    );
\ctr_state[12]_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \ctr_state[12]_i_12__0_n_0\,
      I1 => \^counter_phase90_output\(1),
      I2 => \^counter_phase90_output\(0),
      I3 => \^counter_phase90_output\(10),
      I4 => \^counter_phase90_output\(2),
      I5 => \ctr_state[12]_i_13_n_0\,
      O => \ctr_state[12]_i_5__0_n_0\
    );
\ctr_state[12]_i_6__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^counter_phase90_output\(5),
      I1 => \^counter_phase90_output\(4),
      I2 => \^counter_phase90_output\(7),
      I3 => \^counter_phase90_output\(6),
      O => \ctr_state[12]_i_6__0_n_0\
    );
\ctr_state[12]_i_7__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^counter_phase90_output\(11),
      I1 => \^counter_phase90_output\(8),
      I2 => \^counter_phase90_output\(9),
      I3 => \^counter_phase90_output\(10),
      I4 => \^counter_phase90_output\(12),
      O => \ctr_state[12]_i_7__0_n_0\
    );
\ctr_state[12]_i_8__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase90_output\(12),
      O => \ctr_state[12]_i_8__0_n_0\
    );
\ctr_state[12]_i_9__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase90_output\(11),
      O => \ctr_state[12]_i_9__0_n_0\
    );
\ctr_state[1]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(1),
      I2 => \ctr_state[12]_i_2__0_n_0\,
      I3 => counter_phase90_dir_reg,
      I4 => \ctr_state_reg[4]_i_3__0_n_7\,
      I5 => \ctr_state[12]_i_5__0_n_0\,
      O => \ctr_state[1]_i_1__0_n_0\
    );
\ctr_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABAFFBAAABAAABA"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__0_n_0\,
      I2 => data0(2),
      I3 => counter_phase90_dir_reg,
      I4 => \ctr_state[12]_i_5__0_n_0\,
      I5 => \ctr_state_reg[4]_i_3__0_n_6\,
      O => p_1_in(2)
    );
\ctr_state[3]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0054005455540054"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__0_n_0\,
      I2 => data0(3),
      I3 => counter_phase90_dir_reg,
      I4 => \ctr_state_reg[4]_i_3__0_n_5\,
      I5 => \ctr_state[12]_i_5__0_n_0\,
      O => \ctr_state[3]_i_1__1_n_0\
    );
\ctr_state[4]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(4),
      I2 => \ctr_state[12]_i_2__0_n_0\,
      I3 => counter_phase90_dir_reg,
      I4 => \ctr_state_reg[4]_i_3__0_n_4\,
      I5 => \ctr_state[12]_i_5__0_n_0\,
      O => \ctr_state[4]_i_1__0_n_0\
    );
\ctr_state[4]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase90_output\(4),
      O => \ctr_state[4]_i_4__0_n_0\
    );
\ctr_state[4]_i_5__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase90_output\(3),
      O => \ctr_state[4]_i_5__0_n_0\
    );
\ctr_state[4]_i_6__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase90_output\(2),
      O => \ctr_state[4]_i_6__0_n_0\
    );
\ctr_state[4]_i_7__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase90_output\(1),
      O => \ctr_state[4]_i_7__0_n_0\
    );
\ctr_state[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000455040004"
    )
        port map (
      I0 => external_reset_i,
      I1 => data0(5),
      I2 => \ctr_state[12]_i_2__0_n_0\,
      I3 => counter_phase90_dir_reg,
      I4 => \ctr_state_reg[8]_i_3__0_n_7\,
      I5 => \ctr_state[12]_i_5__0_n_0\,
      O => \ctr_state[5]_i_1__0_n_0\
    );
\ctr_state[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABAFFBAAABAAABA"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__0_n_0\,
      I2 => data0(6),
      I3 => counter_phase90_dir_reg,
      I4 => \ctr_state[12]_i_5__0_n_0\,
      I5 => \ctr_state_reg[8]_i_3__0_n_6\,
      O => p_1_in(6)
    );
\ctr_state[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFEFFFEAAFEAAFE"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__0_n_0\,
      I2 => data0(7),
      I3 => counter_phase90_dir_reg,
      I4 => \ctr_state[12]_i_5__0_n_0\,
      I5 => \ctr_state_reg[8]_i_3__0_n_5\,
      O => p_1_in(7)
    );
\ctr_state[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFEFFFEAAFEAAFE"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__0_n_0\,
      I2 => data0(8),
      I3 => counter_phase90_dir_reg,
      I4 => \ctr_state[12]_i_5__0_n_0\,
      I5 => \ctr_state_reg[8]_i_3__0_n_4\,
      O => p_1_in(8)
    );
\ctr_state[8]_i_4__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase90_output\(8),
      O => \ctr_state[8]_i_4__0_n_0\
    );
\ctr_state[8]_i_5__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase90_output\(7),
      O => \ctr_state[8]_i_5__0_n_0\
    );
\ctr_state[8]_i_6__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase90_output\(6),
      O => \ctr_state[8]_i_6__0_n_0\
    );
\ctr_state[8]_i_7__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^counter_phase90_output\(5),
      O => \ctr_state[8]_i_7__0_n_0\
    );
\ctr_state[9]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0054005455540054"
    )
        port map (
      I0 => external_reset_i,
      I1 => \ctr_state[12]_i_2__0_n_0\,
      I2 => data0(9),
      I3 => counter_phase90_dir_reg,
      I4 => \ctr_state_reg[12]_i_4__0_n_7\,
      I5 => \ctr_state[12]_i_5__0_n_0\,
      O => \ctr_state[9]_i_1__1_n_0\
    );
\ctr_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[0]_i_1__2_n_0\,
      Q => \^counter_phase90_output\(0),
      R => '0'
    );
\ctr_state_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[10]_i_1__0_n_0\,
      Q => \^counter_phase90_output\(10),
      R => '0'
    );
\ctr_state_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(11),
      Q => \^counter_phase90_output\(11),
      R => '0'
    );
\ctr_state_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[12]_i_1__1_n_0\,
      Q => \^counter_phase90_output\(12),
      R => '0'
    );
\ctr_state_reg[12]_i_3__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[8]_i_2__0_n_0\,
      CO(3) => \NLW_ctr_state_reg[12]_i_3__0_CO_UNCONNECTED\(3),
      CO(2) => \ctr_state_reg[12]_i_3__0_n_1\,
      CO(1) => \ctr_state_reg[12]_i_3__0_n_2\,
      CO(0) => \ctr_state_reg[12]_i_3__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \^counter_phase90_output\(11 downto 9),
      O(3 downto 0) => data0(12 downto 9),
      S(3) => \ctr_state[12]_i_8__0_n_0\,
      S(2) => \ctr_state[12]_i_9__0_n_0\,
      S(1) => \ctr_state[12]_i_10__0_n_0\,
      S(0) => \ctr_state[12]_i_11__0_n_0\
    );
\ctr_state_reg[12]_i_4__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[8]_i_3__0_n_0\,
      CO(3) => \NLW_ctr_state_reg[12]_i_4__0_CO_UNCONNECTED\(3),
      CO(2) => \ctr_state_reg[12]_i_4__0_n_1\,
      CO(1) => \ctr_state_reg[12]_i_4__0_n_2\,
      CO(0) => \ctr_state_reg[12]_i_4__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[12]_i_4__0_n_4\,
      O(2) => \ctr_state_reg[12]_i_4__0_n_5\,
      O(1) => \ctr_state_reg[12]_i_4__0_n_6\,
      O(0) => \ctr_state_reg[12]_i_4__0_n_7\,
      S(3 downto 0) => \^counter_phase90_output\(12 downto 9)
    );
\ctr_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[1]_i_1__0_n_0\,
      Q => \^counter_phase90_output\(1),
      R => '0'
    );
\ctr_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(2),
      Q => \^counter_phase90_output\(2),
      R => '0'
    );
\ctr_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[3]_i_1__1_n_0\,
      Q => \^counter_phase90_output\(3),
      R => '0'
    );
\ctr_state_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[4]_i_1__0_n_0\,
      Q => \^counter_phase90_output\(4),
      R => '0'
    );
\ctr_state_reg[4]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ctr_state_reg[4]_i_2__0_n_0\,
      CO(2) => \ctr_state_reg[4]_i_2__0_n_1\,
      CO(1) => \ctr_state_reg[4]_i_2__0_n_2\,
      CO(0) => \ctr_state_reg[4]_i_2__0_n_3\,
      CYINIT => \^counter_phase90_output\(0),
      DI(3 downto 0) => \^counter_phase90_output\(4 downto 1),
      O(3 downto 0) => data0(4 downto 1),
      S(3) => \ctr_state[4]_i_4__0_n_0\,
      S(2) => \ctr_state[4]_i_5__0_n_0\,
      S(1) => \ctr_state[4]_i_6__0_n_0\,
      S(0) => \ctr_state[4]_i_7__0_n_0\
    );
\ctr_state_reg[4]_i_3__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ctr_state_reg[4]_i_3__0_n_0\,
      CO(2) => \ctr_state_reg[4]_i_3__0_n_1\,
      CO(1) => \ctr_state_reg[4]_i_3__0_n_2\,
      CO(0) => \ctr_state_reg[4]_i_3__0_n_3\,
      CYINIT => \^counter_phase90_output\(0),
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[4]_i_3__0_n_4\,
      O(2) => \ctr_state_reg[4]_i_3__0_n_5\,
      O(1) => \ctr_state_reg[4]_i_3__0_n_6\,
      O(0) => \ctr_state_reg[4]_i_3__0_n_7\,
      S(3 downto 0) => \^counter_phase90_output\(4 downto 1)
    );
\ctr_state_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[5]_i_1__0_n_0\,
      Q => \^counter_phase90_output\(5),
      R => '0'
    );
\ctr_state_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(6),
      Q => \^counter_phase90_output\(6),
      R => '0'
    );
\ctr_state_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(7),
      Q => \^counter_phase90_output\(7),
      R => '0'
    );
\ctr_state_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => p_1_in(8),
      Q => \^counter_phase90_output\(8),
      R => '0'
    );
\ctr_state_reg[8]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[4]_i_2__0_n_0\,
      CO(3) => \ctr_state_reg[8]_i_2__0_n_0\,
      CO(2) => \ctr_state_reg[8]_i_2__0_n_1\,
      CO(1) => \ctr_state_reg[8]_i_2__0_n_2\,
      CO(0) => \ctr_state_reg[8]_i_2__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^counter_phase90_output\(8 downto 5),
      O(3 downto 0) => data0(8 downto 5),
      S(3) => \ctr_state[8]_i_4__0_n_0\,
      S(2) => \ctr_state[8]_i_5__0_n_0\,
      S(1) => \ctr_state[8]_i_6__0_n_0\,
      S(0) => \ctr_state[8]_i_7__0_n_0\
    );
\ctr_state_reg[8]_i_3__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \ctr_state_reg[4]_i_3__0_n_0\,
      CO(3) => \ctr_state_reg[8]_i_3__0_n_0\,
      CO(2) => \ctr_state_reg[8]_i_3__0_n_1\,
      CO(1) => \ctr_state_reg[8]_i_3__0_n_2\,
      CO(0) => \ctr_state_reg[8]_i_3__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \ctr_state_reg[8]_i_3__0_n_4\,
      O(2) => \ctr_state_reg[8]_i_3__0_n_5\,
      O(1) => \ctr_state_reg[8]_i_3__0_n_6\,
      O(0) => \ctr_state_reg[8]_i_3__0_n_7\,
      S(3 downto 0) => \^counter_phase90_output\(8 downto 5)
    );
\ctr_state_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => \ctr_state[9]_i_1__1_n_0\,
      Q => \^counter_phase90_output\(9),
      R => '0'
    );
\i__carry__0_i_2__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(11),
      I1 => \pwm_a_p_i0_inferred__2/i__carry__0\(11),
      I2 => \pwm_a_p_i0_inferred__2/i__carry__0\(10),
      I3 => \^counter_phase90_output\(10),
      O => \ctr_state_reg[11]_0\(1)
    );
\i__carry__0_i_2__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(11),
      I1 => \pwm_a_p_i0_inferred__4/i__carry__0\(11),
      I2 => \pwm_a_p_i0_inferred__4/i__carry__0\(10),
      I3 => \^counter_phase90_output\(10),
      O => \ctr_state_reg[11]_1\(1)
    );
\i__carry__0_i_2__5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(11),
      I1 => \pwm_a_p_i0_inferred__6/i__carry__0\(11),
      I2 => \pwm_a_p_i0_inferred__6/i__carry__0\(10),
      I3 => \^counter_phase90_output\(10),
      O => \ctr_state_reg[11]_2\(1)
    );
\i__carry__0_i_2__7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(11),
      I1 => \pwm_a_p_i0_inferred__8/i__carry__0\(11),
      I2 => \pwm_a_p_i0_inferred__8/i__carry__0\(10),
      I3 => \^counter_phase90_output\(10),
      O => \ctr_state_reg[11]_3\(1)
    );
\i__carry__0_i_3__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(9),
      I1 => \pwm_a_p_i0_inferred__2/i__carry__0\(9),
      I2 => \pwm_a_p_i0_inferred__2/i__carry__0\(8),
      I3 => \^counter_phase90_output\(8),
      O => \ctr_state_reg[11]_0\(0)
    );
\i__carry__0_i_3__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(9),
      I1 => \pwm_a_p_i0_inferred__4/i__carry__0\(9),
      I2 => \pwm_a_p_i0_inferred__4/i__carry__0\(8),
      I3 => \^counter_phase90_output\(8),
      O => \ctr_state_reg[11]_1\(0)
    );
\i__carry__0_i_3__5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(9),
      I1 => \pwm_a_p_i0_inferred__6/i__carry__0\(9),
      I2 => \pwm_a_p_i0_inferred__6/i__carry__0\(8),
      I3 => \^counter_phase90_output\(8),
      O => \ctr_state_reg[11]_2\(0)
    );
\i__carry__0_i_3__7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(9),
      I1 => \pwm_a_p_i0_inferred__8/i__carry__0\(9),
      I2 => \pwm_a_p_i0_inferred__8/i__carry__0\(8),
      I3 => \^counter_phase90_output\(8),
      O => \ctr_state_reg[11]_3\(0)
    );
\i__carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase90_output\(12),
      I1 => Q(12),
      O => \ctr_state_reg[12]_0\(2)
    );
\i__carry__0_i_4__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase90_output\(12),
      I1 => \pwm_a_p_i0_inferred__2/i__carry__0\(12),
      O => \ctr_state_reg[12]_1\(0)
    );
\i__carry__0_i_4__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase90_output\(12),
      I1 => \pwm_a_p_i0_inferred__4/i__carry__0\(12),
      O => \ctr_state_reg[12]_2\(0)
    );
\i__carry__0_i_4__5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase90_output\(12),
      I1 => \pwm_a_p_i0_inferred__6/i__carry__0\(12),
      O => \ctr_state_reg[12]_3\(0)
    );
\i__carry__0_i_4__7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^counter_phase90_output\(12),
      I1 => \pwm_a_p_i0_inferred__8/i__carry__0\(12),
      O => \ctr_state_reg[12]_4\(0)
    );
\i__carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase90_output\(11),
      I1 => Q(11),
      I2 => \^counter_phase90_output\(10),
      I3 => Q(10),
      O => \ctr_state_reg[12]_0\(1)
    );
\i__carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase90_output\(9),
      I1 => Q(9),
      I2 => \^counter_phase90_output\(8),
      I3 => Q(8),
      O => \ctr_state_reg[12]_0\(0)
    );
\i__carry_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(7),
      I1 => \pwm_a_p_i0_inferred__2/i__carry__0\(7),
      I2 => \pwm_a_p_i0_inferred__2/i__carry__0\(6),
      I3 => \^counter_phase90_output\(6),
      O => DI(3)
    );
\i__carry_i_1__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(7),
      I1 => \pwm_a_p_i0_inferred__4/i__carry__0\(7),
      I2 => \pwm_a_p_i0_inferred__4/i__carry__0\(6),
      I3 => \^counter_phase90_output\(6),
      O => \ctr_state_reg[7]_0\(3)
    );
\i__carry_i_1__5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(7),
      I1 => \pwm_a_p_i0_inferred__6/i__carry__0\(7),
      I2 => \pwm_a_p_i0_inferred__6/i__carry__0\(6),
      I3 => \^counter_phase90_output\(6),
      O => \ctr_state_reg[7]_1\(3)
    );
\i__carry_i_1__7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(7),
      I1 => \pwm_a_p_i0_inferred__8/i__carry__0\(7),
      I2 => \pwm_a_p_i0_inferred__8/i__carry__0\(6),
      I3 => \^counter_phase90_output\(6),
      O => \ctr_state_reg[7]_2\(3)
    );
\i__carry_i_2__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(5),
      I1 => \pwm_a_p_i0_inferred__2/i__carry__0\(5),
      I2 => \pwm_a_p_i0_inferred__2/i__carry__0\(4),
      I3 => \^counter_phase90_output\(4),
      O => DI(2)
    );
\i__carry_i_2__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(5),
      I1 => \pwm_a_p_i0_inferred__4/i__carry__0\(5),
      I2 => \pwm_a_p_i0_inferred__4/i__carry__0\(4),
      I3 => \^counter_phase90_output\(4),
      O => \ctr_state_reg[7]_0\(2)
    );
\i__carry_i_2__5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(5),
      I1 => \pwm_a_p_i0_inferred__6/i__carry__0\(5),
      I2 => \pwm_a_p_i0_inferred__6/i__carry__0\(4),
      I3 => \^counter_phase90_output\(4),
      O => \ctr_state_reg[7]_1\(2)
    );
\i__carry_i_2__7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(5),
      I1 => \pwm_a_p_i0_inferred__8/i__carry__0\(5),
      I2 => \pwm_a_p_i0_inferred__8/i__carry__0\(4),
      I3 => \^counter_phase90_output\(4),
      O => \ctr_state_reg[7]_2\(2)
    );
\i__carry_i_3__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(3),
      I1 => \pwm_a_p_i0_inferred__2/i__carry__0\(3),
      I2 => \pwm_a_p_i0_inferred__2/i__carry__0\(2),
      I3 => \^counter_phase90_output\(2),
      O => DI(1)
    );
\i__carry_i_3__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(3),
      I1 => \pwm_a_p_i0_inferred__4/i__carry__0\(3),
      I2 => \pwm_a_p_i0_inferred__4/i__carry__0\(2),
      I3 => \^counter_phase90_output\(2),
      O => \ctr_state_reg[7]_0\(1)
    );
\i__carry_i_3__5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(3),
      I1 => \pwm_a_p_i0_inferred__6/i__carry__0\(3),
      I2 => \pwm_a_p_i0_inferred__6/i__carry__0\(2),
      I3 => \^counter_phase90_output\(2),
      O => \ctr_state_reg[7]_1\(1)
    );
\i__carry_i_3__7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(3),
      I1 => \pwm_a_p_i0_inferred__8/i__carry__0\(3),
      I2 => \pwm_a_p_i0_inferred__8/i__carry__0\(2),
      I3 => \^counter_phase90_output\(2),
      O => \ctr_state_reg[7]_2\(1)
    );
\i__carry_i_4__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(1),
      I1 => \pwm_a_p_i0_inferred__2/i__carry__0\(1),
      I2 => \pwm_a_p_i0_inferred__2/i__carry__0\(0),
      I3 => \^counter_phase90_output\(0),
      O => DI(0)
    );
\i__carry_i_4__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(1),
      I1 => \pwm_a_p_i0_inferred__4/i__carry__0\(1),
      I2 => \pwm_a_p_i0_inferred__4/i__carry__0\(0),
      I3 => \^counter_phase90_output\(0),
      O => \ctr_state_reg[7]_0\(0)
    );
\i__carry_i_4__5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(1),
      I1 => \pwm_a_p_i0_inferred__6/i__carry__0\(1),
      I2 => \pwm_a_p_i0_inferred__6/i__carry__0\(0),
      I3 => \^counter_phase90_output\(0),
      O => \ctr_state_reg[7]_1\(0)
    );
\i__carry_i_4__7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => \^counter_phase90_output\(1),
      I1 => \pwm_a_p_i0_inferred__8/i__carry__0\(1),
      I2 => \pwm_a_p_i0_inferred__8/i__carry__0\(0),
      I3 => \^counter_phase90_output\(0),
      O => \ctr_state_reg[7]_2\(0)
    );
\i__carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase90_output\(7),
      I1 => Q(7),
      I2 => \^counter_phase90_output\(6),
      I3 => Q(6),
      O => S(3)
    );
\i__carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase90_output\(5),
      I1 => Q(5),
      I2 => \^counter_phase90_output\(4),
      I3 => Q(4),
      O => S(2)
    );
\i__carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase90_output\(3),
      I1 => Q(3),
      I2 => \^counter_phase90_output\(2),
      I3 => Q(2),
      O => S(1)
    );
\i__carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^counter_phase90_output\(1),
      I1 => Q(1),
      I2 => \^counter_phase90_output\(0),
      I3 => Q(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_pwm_x40_ip_0_0_pwm_x40_ip_v1_0_S_AXI is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    X_MOD_0 : out STD_LOGIC_VECTOR ( 12 downto 0 );
    \x_mod_0_i_reg[13]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \x_mod_1_i_reg[8]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    X_MOD_1 : out STD_LOGIC_VECTOR ( 12 downto 0 );
    \x_mod_1_i_reg[13]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    X_MOD_2 : out STD_LOGIC_VECTOR ( 12 downto 0 );
    \x_mod_2_i_reg[12]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \x_mod_2_i_reg[13]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \x_mod_3_i_reg[8]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    X_MOD_3 : out STD_LOGIC_VECTOR ( 12 downto 0 );
    \x_mod_3_i_reg[12]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \x_mod_3_i_reg[13]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \x_mod_4_i_reg[8]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    X_MOD_4 : out STD_LOGIC_VECTOR ( 12 downto 0 );
    \x_mod_4_i_reg[12]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \x_mod_4_i_reg[13]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \x_mod_5_i_reg[8]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    X_MOD_5 : out STD_LOGIC_VECTOR ( 12 downto 0 );
    \x_mod_5_i_reg[12]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \x_mod_5_i_reg[13]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \x_mod_6_i_reg[8]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    X_MOD_6 : out STD_LOGIC_VECTOR ( 12 downto 0 );
    \x_mod_6_i_reg[12]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \x_mod_6_i_reg[13]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \x_mod_7_i_reg[8]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    X_MOD_7 : out STD_LOGIC_VECTOR ( 12 downto 0 );
    \x_mod_7_i_reg[12]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \x_mod_7_i_reg[13]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \x_mod_8_i_reg[8]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    X_MOD_8 : out STD_LOGIC_VECTOR ( 12 downto 0 );
    \x_mod_8_i_reg[12]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \x_mod_8_i_reg[13]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \x_mod_9_i_reg[8]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    X_MOD_9 : out STD_LOGIC_VECTOR ( 12 downto 0 );
    \x_mod_9_i_reg[12]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \x_mod_9_i_reg[13]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \x_mod_0_i_reg[8]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x_mod_0_i_reg[13]_1\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \x_mod_1_i_reg[8]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x_mod_1_i_reg[13]_1\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \x_mod_2_i_reg[8]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x_mod_2_i_reg[13]_1\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \x_mod_3_i_reg[8]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x_mod_3_i_reg[13]_1\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \x_mod_4_i_reg[8]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x_mod_4_i_reg[13]_1\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \x_mod_5_i_reg[8]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x_mod_5_i_reg[13]_1\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \x_mod_6_i_reg[8]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x_mod_6_i_reg[13]_1\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \x_mod_7_i_reg[8]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x_mod_7_i_reg[13]_1\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \x_mod_8_i_reg[8]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x_mod_8_i_reg[13]_1\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \x_mod_9_i_reg[8]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \x_mod_9_i_reg[13]_1\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    q : in STD_LOGIC_VECTOR ( 12 downto 0 );
    counter_phase90_output : in STD_LOGIC_VECTOR ( 12 downto 0 );
    counter_phase180_output : in STD_LOGIC_VECTOR ( 12 downto 0 );
    counter_phase270_output : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_aclk : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    external_reset_i : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_rready : in STD_LOGIC
  );
end system_pwm_x40_ip_0_0_pwm_x40_ip_v1_0_S_AXI;

architecture STRUCTURE of system_pwm_x40_ip_0_0_pwm_x40_ip_v1_0_S_AXI is
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal \^x_mod_0\ : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \^x_mod_1\ : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \^x_mod_2\ : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \^x_mod_3\ : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \^x_mod_4\ : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \^x_mod_5\ : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \^x_mod_6\ : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \^x_mod_7\ : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \^x_mod_8\ : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \^x_mod_9\ : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_6_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_7_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[31]_i_5_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[9]_i_2_n_0\ : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal axi_wvalid_falling_edge : STD_LOGIC;
  signal axi_wvalid_falling_edge0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 7 );
  signal past_axi_wvalid : STD_LOGIC;
  signal \reg_data_out__0\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s_axi_bvalid\ : STD_LOGIC;
  signal \^s_axi_rvalid\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal slv_reg0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal slv_reg1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg1[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg2 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg2[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg3 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg3[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg4 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg4[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg5 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg5[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg6 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg6[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg6[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg6[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg6[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg7 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg7[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg8 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg8[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg8[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg8[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg8[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg9 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg9[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg9[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg9[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg9[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg_rden : STD_LOGIC;
  signal \slv_reg_wren__2\ : STD_LOGIC;
  signal x_mod_0_i0 : STD_LOGIC;
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  X_MOD_0(12 downto 0) <= \^x_mod_0\(12 downto 0);
  X_MOD_1(12 downto 0) <= \^x_mod_1\(12 downto 0);
  X_MOD_2(12 downto 0) <= \^x_mod_2\(12 downto 0);
  X_MOD_3(12 downto 0) <= \^x_mod_3\(12 downto 0);
  X_MOD_4(12 downto 0) <= \^x_mod_4\(12 downto 0);
  X_MOD_5(12 downto 0) <= \^x_mod_5\(12 downto 0);
  X_MOD_6(12 downto 0) <= \^x_mod_6\(12 downto 0);
  X_MOD_7(12 downto 0) <= \^x_mod_7\(12 downto 0);
  X_MOD_8(12 downto 0) <= \^x_mod_8\(12 downto 0);
  X_MOD_9(12 downto 0) <= \^x_mod_9\(12 downto 0);
  s_axi_bvalid <= \^s_axi_bvalid\;
  s_axi_rvalid <= \^s_axi_rvalid\;
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFF8CCC8CCC8CCC"
    )
        port map (
      I0 => \^s_axi_awready\,
      I1 => aw_en_reg_n_0,
      I2 => s_axi_wvalid,
      I3 => s_axi_awvalid,
      I4 => s_axi_bready,
      I5 => \^s_axi_bvalid\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => p_0_in
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => axi_arready0,
      D => s_axi_araddr(0),
      Q => sel0(0),
      S => p_0_in
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => axi_arready0,
      D => s_axi_araddr(1),
      Q => sel0(1),
      S => p_0_in
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => axi_arready0,
      D => s_axi_araddr(2),
      Q => sel0(2),
      S => p_0_in
    );
\axi_araddr_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => axi_arready0,
      D => s_axi_araddr(3),
      Q => sel0(3),
      S => p_0_in
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s_axi_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s_axi_arready\,
      R => p_0_in
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => axi_awready0,
      D => s_axi_awaddr(0),
      Q => \p_0_in__0\(0),
      R => p_0_in
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => axi_awready0,
      D => s_axi_awaddr(1),
      Q => \p_0_in__0\(1),
      R => p_0_in
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => axi_awready0,
      D => s_axi_awaddr(2),
      Q => \p_0_in__0\(2),
      R => p_0_in
    );
\axi_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => axi_awready0,
      D => s_axi_awaddr(3),
      Q => \p_0_in__0\(3),
      R => p_0_in
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s_axi_aresetn,
      O => p_0_in
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => s_axi_awvalid,
      I1 => s_axi_wvalid,
      I2 => aw_en_reg_n_0,
      I3 => \^s_axi_awready\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s_axi_awready\,
      R => p_0_in
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s_axi_wvalid,
      I4 => s_axi_bready,
      I5 => \^s_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s_axi_bvalid\,
      R => p_0_in
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(0),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(0),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[0]_i_2_n_0\,
      O => \reg_data_out__0\(0)
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(0),
      I1 => slv_reg2(0),
      I2 => sel0(1),
      I3 => slv_reg1(0),
      I4 => sel0(0),
      I5 => slv_reg0(0),
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(0),
      I1 => slv_reg6(0),
      I2 => sel0(1),
      I3 => slv_reg5(0),
      I4 => sel0(0),
      I5 => slv_reg4(0),
      O => \axi_rdata[0]_i_4_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(10),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(10),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[10]_i_2_n_0\,
      O => \reg_data_out__0\(10)
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(10),
      I1 => slv_reg2(10),
      I2 => sel0(1),
      I3 => slv_reg1(10),
      I4 => sel0(0),
      I5 => slv_reg0(10),
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[10]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(10),
      I1 => slv_reg6(10),
      I2 => sel0(1),
      I3 => slv_reg5(10),
      I4 => sel0(0),
      I5 => slv_reg4(10),
      O => \axi_rdata[10]_i_4_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(11),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(11),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[11]_i_2_n_0\,
      O => \reg_data_out__0\(11)
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(11),
      I1 => slv_reg2(11),
      I2 => sel0(1),
      I3 => slv_reg1(11),
      I4 => sel0(0),
      I5 => slv_reg0(11),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[11]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(11),
      I1 => slv_reg6(11),
      I2 => sel0(1),
      I3 => slv_reg5(11),
      I4 => sel0(0),
      I5 => slv_reg4(11),
      O => \axi_rdata[11]_i_4_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(12),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(12),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[12]_i_2_n_0\,
      O => \reg_data_out__0\(12)
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(12),
      I1 => slv_reg2(12),
      I2 => sel0(1),
      I3 => slv_reg1(12),
      I4 => sel0(0),
      I5 => slv_reg0(12),
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[12]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(12),
      I1 => slv_reg6(12),
      I2 => sel0(1),
      I3 => slv_reg5(12),
      I4 => sel0(0),
      I5 => slv_reg4(12),
      O => \axi_rdata[12]_i_4_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(13),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(13),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[13]_i_2_n_0\,
      O => \reg_data_out__0\(13)
    );
\axi_rdata[13]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(13),
      I1 => slv_reg2(13),
      I2 => sel0(1),
      I3 => slv_reg1(13),
      I4 => sel0(0),
      I5 => slv_reg0(13),
      O => \axi_rdata[13]_i_3_n_0\
    );
\axi_rdata[13]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(13),
      I1 => slv_reg6(13),
      I2 => sel0(1),
      I3 => slv_reg5(13),
      I4 => sel0(0),
      I5 => slv_reg4(13),
      O => \axi_rdata[13]_i_4_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(14),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(14),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[14]_i_2_n_0\,
      O => \reg_data_out__0\(14)
    );
\axi_rdata[14]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(14),
      I1 => slv_reg2(14),
      I2 => sel0(1),
      I3 => slv_reg1(14),
      I4 => sel0(0),
      I5 => slv_reg0(14),
      O => \axi_rdata[14]_i_3_n_0\
    );
\axi_rdata[14]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(14),
      I1 => slv_reg6(14),
      I2 => sel0(1),
      I3 => slv_reg5(14),
      I4 => sel0(0),
      I5 => slv_reg4(14),
      O => \axi_rdata[14]_i_4_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(15),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(15),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[15]_i_2_n_0\,
      O => \reg_data_out__0\(15)
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(15),
      I1 => slv_reg2(15),
      I2 => sel0(1),
      I3 => slv_reg1(15),
      I4 => sel0(0),
      I5 => slv_reg0(15),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(15),
      I1 => slv_reg6(15),
      I2 => sel0(1),
      I3 => slv_reg5(15),
      I4 => sel0(0),
      I5 => slv_reg4(15),
      O => \axi_rdata[15]_i_4_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(16),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(16),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[16]_i_2_n_0\,
      O => \reg_data_out__0\(16)
    );
\axi_rdata[16]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(16),
      I1 => slv_reg2(16),
      I2 => sel0(1),
      I3 => slv_reg1(16),
      I4 => sel0(0),
      I5 => slv_reg0(16),
      O => \axi_rdata[16]_i_3_n_0\
    );
\axi_rdata[16]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(16),
      I1 => slv_reg6(16),
      I2 => sel0(1),
      I3 => slv_reg5(16),
      I4 => sel0(0),
      I5 => slv_reg4(16),
      O => \axi_rdata[16]_i_4_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(17),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(17),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[17]_i_2_n_0\,
      O => \reg_data_out__0\(17)
    );
\axi_rdata[17]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(17),
      I1 => slv_reg2(17),
      I2 => sel0(1),
      I3 => slv_reg1(17),
      I4 => sel0(0),
      I5 => slv_reg0(17),
      O => \axi_rdata[17]_i_3_n_0\
    );
\axi_rdata[17]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(17),
      I1 => slv_reg6(17),
      I2 => sel0(1),
      I3 => slv_reg5(17),
      I4 => sel0(0),
      I5 => slv_reg4(17),
      O => \axi_rdata[17]_i_4_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(18),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(18),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[18]_i_2_n_0\,
      O => \reg_data_out__0\(18)
    );
\axi_rdata[18]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(18),
      I1 => slv_reg2(18),
      I2 => sel0(1),
      I3 => slv_reg1(18),
      I4 => sel0(0),
      I5 => slv_reg0(18),
      O => \axi_rdata[18]_i_3_n_0\
    );
\axi_rdata[18]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(18),
      I1 => slv_reg6(18),
      I2 => sel0(1),
      I3 => slv_reg5(18),
      I4 => sel0(0),
      I5 => slv_reg4(18),
      O => \axi_rdata[18]_i_4_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(19),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(19),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[19]_i_2_n_0\,
      O => \reg_data_out__0\(19)
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(19),
      I1 => slv_reg2(19),
      I2 => sel0(1),
      I3 => slv_reg1(19),
      I4 => sel0(0),
      I5 => slv_reg0(19),
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[19]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(19),
      I1 => slv_reg6(19),
      I2 => sel0(1),
      I3 => slv_reg5(19),
      I4 => sel0(0),
      I5 => slv_reg4(19),
      O => \axi_rdata[19]_i_4_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(1),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(1),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[1]_i_2_n_0\,
      O => \reg_data_out__0\(1)
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(1),
      I1 => slv_reg2(1),
      I2 => sel0(1),
      I3 => slv_reg1(1),
      I4 => sel0(0),
      I5 => slv_reg0(1),
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(1),
      I1 => slv_reg6(1),
      I2 => sel0(1),
      I3 => slv_reg5(1),
      I4 => sel0(0),
      I5 => slv_reg4(1),
      O => \axi_rdata[1]_i_4_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(20),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(20),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[20]_i_2_n_0\,
      O => \reg_data_out__0\(20)
    );
\axi_rdata[20]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(20),
      I1 => slv_reg2(20),
      I2 => sel0(1),
      I3 => slv_reg1(20),
      I4 => sel0(0),
      I5 => slv_reg0(20),
      O => \axi_rdata[20]_i_3_n_0\
    );
\axi_rdata[20]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(20),
      I1 => slv_reg6(20),
      I2 => sel0(1),
      I3 => slv_reg5(20),
      I4 => sel0(0),
      I5 => slv_reg4(20),
      O => \axi_rdata[20]_i_4_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(21),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(21),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[21]_i_2_n_0\,
      O => \reg_data_out__0\(21)
    );
\axi_rdata[21]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(21),
      I1 => slv_reg2(21),
      I2 => sel0(1),
      I3 => slv_reg1(21),
      I4 => sel0(0),
      I5 => slv_reg0(21),
      O => \axi_rdata[21]_i_3_n_0\
    );
\axi_rdata[21]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(21),
      I1 => slv_reg6(21),
      I2 => sel0(1),
      I3 => slv_reg5(21),
      I4 => sel0(0),
      I5 => slv_reg4(21),
      O => \axi_rdata[21]_i_4_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(22),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(22),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[22]_i_2_n_0\,
      O => \reg_data_out__0\(22)
    );
\axi_rdata[22]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(22),
      I1 => slv_reg2(22),
      I2 => sel0(1),
      I3 => slv_reg1(22),
      I4 => sel0(0),
      I5 => slv_reg0(22),
      O => \axi_rdata[22]_i_3_n_0\
    );
\axi_rdata[22]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(22),
      I1 => slv_reg6(22),
      I2 => sel0(1),
      I3 => slv_reg5(22),
      I4 => sel0(0),
      I5 => slv_reg4(22),
      O => \axi_rdata[22]_i_4_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(23),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(23),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[23]_i_2_n_0\,
      O => \reg_data_out__0\(23)
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(23),
      I1 => slv_reg2(23),
      I2 => sel0(1),
      I3 => slv_reg1(23),
      I4 => sel0(0),
      I5 => slv_reg0(23),
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[23]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(23),
      I1 => slv_reg6(23),
      I2 => sel0(1),
      I3 => slv_reg5(23),
      I4 => sel0(0),
      I5 => slv_reg4(23),
      O => \axi_rdata[23]_i_4_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(24),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(24),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[24]_i_2_n_0\,
      O => \reg_data_out__0\(24)
    );
\axi_rdata[24]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(24),
      I1 => slv_reg2(24),
      I2 => sel0(1),
      I3 => slv_reg1(24),
      I4 => sel0(0),
      I5 => slv_reg0(24),
      O => \axi_rdata[24]_i_3_n_0\
    );
\axi_rdata[24]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(24),
      I1 => slv_reg6(24),
      I2 => sel0(1),
      I3 => slv_reg5(24),
      I4 => sel0(0),
      I5 => slv_reg4(24),
      O => \axi_rdata[24]_i_4_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(25),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(25),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[25]_i_2_n_0\,
      O => \reg_data_out__0\(25)
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(25),
      I1 => slv_reg2(25),
      I2 => sel0(1),
      I3 => slv_reg1(25),
      I4 => sel0(0),
      I5 => slv_reg0(25),
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[25]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(25),
      I1 => slv_reg6(25),
      I2 => sel0(1),
      I3 => slv_reg5(25),
      I4 => sel0(0),
      I5 => slv_reg4(25),
      O => \axi_rdata[25]_i_4_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(26),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(26),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[26]_i_2_n_0\,
      O => \reg_data_out__0\(26)
    );
\axi_rdata[26]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(26),
      I1 => slv_reg2(26),
      I2 => sel0(1),
      I3 => slv_reg1(26),
      I4 => sel0(0),
      I5 => slv_reg0(26),
      O => \axi_rdata[26]_i_3_n_0\
    );
\axi_rdata[26]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(26),
      I1 => slv_reg6(26),
      I2 => sel0(1),
      I3 => slv_reg5(26),
      I4 => sel0(0),
      I5 => slv_reg4(26),
      O => \axi_rdata[26]_i_4_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(27),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(27),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[27]_i_2_n_0\,
      O => \reg_data_out__0\(27)
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(27),
      I1 => slv_reg2(27),
      I2 => sel0(1),
      I3 => slv_reg1(27),
      I4 => sel0(0),
      I5 => slv_reg0(27),
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[27]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(27),
      I1 => slv_reg6(27),
      I2 => sel0(1),
      I3 => slv_reg5(27),
      I4 => sel0(0),
      I5 => slv_reg4(27),
      O => \axi_rdata[27]_i_4_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(28),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(28),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[28]_i_2_n_0\,
      O => \reg_data_out__0\(28)
    );
\axi_rdata[28]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(28),
      I1 => slv_reg2(28),
      I2 => sel0(1),
      I3 => slv_reg1(28),
      I4 => sel0(0),
      I5 => slv_reg0(28),
      O => \axi_rdata[28]_i_3_n_0\
    );
\axi_rdata[28]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(28),
      I1 => slv_reg6(28),
      I2 => sel0(1),
      I3 => slv_reg5(28),
      I4 => sel0(0),
      I5 => slv_reg4(28),
      O => \axi_rdata[28]_i_4_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(29),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(29),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[29]_i_2_n_0\,
      O => \reg_data_out__0\(29)
    );
\axi_rdata[29]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(29),
      I1 => slv_reg2(29),
      I2 => sel0(1),
      I3 => slv_reg1(29),
      I4 => sel0(0),
      I5 => slv_reg0(29),
      O => \axi_rdata[29]_i_3_n_0\
    );
\axi_rdata[29]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(29),
      I1 => slv_reg6(29),
      I2 => sel0(1),
      I3 => slv_reg5(29),
      I4 => sel0(0),
      I5 => slv_reg4(29),
      O => \axi_rdata[29]_i_4_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(2),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(2),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[2]_i_2_n_0\,
      O => \reg_data_out__0\(2)
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(2),
      I1 => slv_reg2(2),
      I2 => sel0(1),
      I3 => slv_reg1(2),
      I4 => sel0(0),
      I5 => slv_reg0(2),
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(2),
      I1 => slv_reg6(2),
      I2 => sel0(1),
      I3 => slv_reg5(2),
      I4 => sel0(0),
      I5 => slv_reg4(2),
      O => \axi_rdata[2]_i_4_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(30),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(30),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[30]_i_2_n_0\,
      O => \reg_data_out__0\(30)
    );
\axi_rdata[30]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(30),
      I1 => slv_reg2(30),
      I2 => sel0(1),
      I3 => slv_reg1(30),
      I4 => sel0(0),
      I5 => slv_reg0(30),
      O => \axi_rdata[30]_i_3_n_0\
    );
\axi_rdata[30]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(30),
      I1 => slv_reg6(30),
      I2 => sel0(1),
      I3 => slv_reg5(30),
      I4 => sel0(0),
      I5 => slv_reg4(30),
      O => \axi_rdata[30]_i_4_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s_axi_arvalid,
      I2 => \^s_axi_rvalid\,
      O => slv_reg_rden
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(31),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(31),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[31]_i_5_n_0\,
      O => \reg_data_out__0\(31)
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => sel0(2),
      I1 => sel0(1),
      I2 => sel0(0),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(2),
      O => \axi_rdata[31]_i_4_n_0\
    );
\axi_rdata[31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(31),
      I1 => slv_reg2(31),
      I2 => sel0(1),
      I3 => slv_reg1(31),
      I4 => sel0(0),
      I5 => slv_reg0(31),
      O => \axi_rdata[31]_i_6_n_0\
    );
\axi_rdata[31]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(31),
      I1 => slv_reg6(31),
      I2 => sel0(1),
      I3 => slv_reg5(31),
      I4 => sel0(0),
      I5 => slv_reg4(31),
      O => \axi_rdata[31]_i_7_n_0\
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(3),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(3),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[3]_i_2_n_0\,
      O => \reg_data_out__0\(3)
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(3),
      I1 => slv_reg2(3),
      I2 => sel0(1),
      I3 => slv_reg1(3),
      I4 => sel0(0),
      I5 => slv_reg0(3),
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(3),
      I1 => slv_reg6(3),
      I2 => sel0(1),
      I3 => slv_reg5(3),
      I4 => sel0(0),
      I5 => slv_reg4(3),
      O => \axi_rdata[3]_i_4_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(4),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(4),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[4]_i_2_n_0\,
      O => \reg_data_out__0\(4)
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(4),
      I1 => slv_reg2(4),
      I2 => sel0(1),
      I3 => slv_reg1(4),
      I4 => sel0(0),
      I5 => slv_reg0(4),
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[4]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(4),
      I1 => slv_reg6(4),
      I2 => sel0(1),
      I3 => slv_reg5(4),
      I4 => sel0(0),
      I5 => slv_reg4(4),
      O => \axi_rdata[4]_i_4_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(5),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(5),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[5]_i_2_n_0\,
      O => \reg_data_out__0\(5)
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(5),
      I1 => slv_reg2(5),
      I2 => sel0(1),
      I3 => slv_reg1(5),
      I4 => sel0(0),
      I5 => slv_reg0(5),
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(5),
      I1 => slv_reg6(5),
      I2 => sel0(1),
      I3 => slv_reg5(5),
      I4 => sel0(0),
      I5 => slv_reg4(5),
      O => \axi_rdata[5]_i_4_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(6),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(6),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[6]_i_2_n_0\,
      O => \reg_data_out__0\(6)
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(6),
      I1 => slv_reg2(6),
      I2 => sel0(1),
      I3 => slv_reg1(6),
      I4 => sel0(0),
      I5 => slv_reg0(6),
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(6),
      I1 => slv_reg6(6),
      I2 => sel0(1),
      I3 => slv_reg5(6),
      I4 => sel0(0),
      I5 => slv_reg4(6),
      O => \axi_rdata[6]_i_4_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(7),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(7),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[7]_i_2_n_0\,
      O => \reg_data_out__0\(7)
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(7),
      I1 => slv_reg2(7),
      I2 => sel0(1),
      I3 => slv_reg1(7),
      I4 => sel0(0),
      I5 => slv_reg0(7),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(7),
      I1 => slv_reg6(7),
      I2 => sel0(1),
      I3 => slv_reg5(7),
      I4 => sel0(0),
      I5 => slv_reg4(7),
      O => \axi_rdata[7]_i_4_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(8),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(8),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[8]_i_2_n_0\,
      O => \reg_data_out__0\(8)
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(8),
      I1 => slv_reg2(8),
      I2 => sel0(1),
      I3 => slv_reg1(8),
      I4 => sel0(0),
      I5 => slv_reg0(8),
      O => \axi_rdata[8]_i_3_n_0\
    );
\axi_rdata[8]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(8),
      I1 => slv_reg6(8),
      I2 => sel0(1),
      I3 => slv_reg5(8),
      I4 => sel0(0),
      I5 => slv_reg4(8),
      O => \axi_rdata[8]_i_4_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg8(9),
      I1 => \axi_rdata[31]_i_3_n_0\,
      I2 => slv_reg9(9),
      I3 => \axi_rdata[31]_i_4_n_0\,
      I4 => sel0(3),
      I5 => \axi_rdata_reg[9]_i_2_n_0\,
      O => \reg_data_out__0\(9)
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(9),
      I1 => slv_reg2(9),
      I2 => sel0(1),
      I3 => slv_reg1(9),
      I4 => sel0(0),
      I5 => slv_reg0(9),
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(9),
      I1 => slv_reg6(9),
      I2 => sel0(1),
      I3 => slv_reg5(9),
      I4 => sel0(0),
      I5 => slv_reg4(9),
      O => \axi_rdata[9]_i_4_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(0),
      Q => s_axi_rdata(0),
      R => p_0_in
    );
\axi_rdata_reg[0]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[0]_i_3_n_0\,
      I1 => \axi_rdata[0]_i_4_n_0\,
      O => \axi_rdata_reg[0]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(10),
      Q => s_axi_rdata(10),
      R => p_0_in
    );
\axi_rdata_reg[10]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[10]_i_3_n_0\,
      I1 => \axi_rdata[10]_i_4_n_0\,
      O => \axi_rdata_reg[10]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(11),
      Q => s_axi_rdata(11),
      R => p_0_in
    );
\axi_rdata_reg[11]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[11]_i_3_n_0\,
      I1 => \axi_rdata[11]_i_4_n_0\,
      O => \axi_rdata_reg[11]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(12),
      Q => s_axi_rdata(12),
      R => p_0_in
    );
\axi_rdata_reg[12]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[12]_i_3_n_0\,
      I1 => \axi_rdata[12]_i_4_n_0\,
      O => \axi_rdata_reg[12]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(13),
      Q => s_axi_rdata(13),
      R => p_0_in
    );
\axi_rdata_reg[13]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[13]_i_3_n_0\,
      I1 => \axi_rdata[13]_i_4_n_0\,
      O => \axi_rdata_reg[13]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(14),
      Q => s_axi_rdata(14),
      R => p_0_in
    );
\axi_rdata_reg[14]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[14]_i_3_n_0\,
      I1 => \axi_rdata[14]_i_4_n_0\,
      O => \axi_rdata_reg[14]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(15),
      Q => s_axi_rdata(15),
      R => p_0_in
    );
\axi_rdata_reg[15]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[15]_i_3_n_0\,
      I1 => \axi_rdata[15]_i_4_n_0\,
      O => \axi_rdata_reg[15]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(16),
      Q => s_axi_rdata(16),
      R => p_0_in
    );
\axi_rdata_reg[16]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[16]_i_3_n_0\,
      I1 => \axi_rdata[16]_i_4_n_0\,
      O => \axi_rdata_reg[16]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(17),
      Q => s_axi_rdata(17),
      R => p_0_in
    );
\axi_rdata_reg[17]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[17]_i_3_n_0\,
      I1 => \axi_rdata[17]_i_4_n_0\,
      O => \axi_rdata_reg[17]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(18),
      Q => s_axi_rdata(18),
      R => p_0_in
    );
\axi_rdata_reg[18]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[18]_i_3_n_0\,
      I1 => \axi_rdata[18]_i_4_n_0\,
      O => \axi_rdata_reg[18]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(19),
      Q => s_axi_rdata(19),
      R => p_0_in
    );
\axi_rdata_reg[19]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[19]_i_3_n_0\,
      I1 => \axi_rdata[19]_i_4_n_0\,
      O => \axi_rdata_reg[19]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(1),
      Q => s_axi_rdata(1),
      R => p_0_in
    );
\axi_rdata_reg[1]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[1]_i_3_n_0\,
      I1 => \axi_rdata[1]_i_4_n_0\,
      O => \axi_rdata_reg[1]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(20),
      Q => s_axi_rdata(20),
      R => p_0_in
    );
\axi_rdata_reg[20]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[20]_i_3_n_0\,
      I1 => \axi_rdata[20]_i_4_n_0\,
      O => \axi_rdata_reg[20]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(21),
      Q => s_axi_rdata(21),
      R => p_0_in
    );
\axi_rdata_reg[21]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[21]_i_3_n_0\,
      I1 => \axi_rdata[21]_i_4_n_0\,
      O => \axi_rdata_reg[21]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(22),
      Q => s_axi_rdata(22),
      R => p_0_in
    );
\axi_rdata_reg[22]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[22]_i_3_n_0\,
      I1 => \axi_rdata[22]_i_4_n_0\,
      O => \axi_rdata_reg[22]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(23),
      Q => s_axi_rdata(23),
      R => p_0_in
    );
\axi_rdata_reg[23]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[23]_i_3_n_0\,
      I1 => \axi_rdata[23]_i_4_n_0\,
      O => \axi_rdata_reg[23]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(24),
      Q => s_axi_rdata(24),
      R => p_0_in
    );
\axi_rdata_reg[24]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[24]_i_3_n_0\,
      I1 => \axi_rdata[24]_i_4_n_0\,
      O => \axi_rdata_reg[24]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(25),
      Q => s_axi_rdata(25),
      R => p_0_in
    );
\axi_rdata_reg[25]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[25]_i_3_n_0\,
      I1 => \axi_rdata[25]_i_4_n_0\,
      O => \axi_rdata_reg[25]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(26),
      Q => s_axi_rdata(26),
      R => p_0_in
    );
\axi_rdata_reg[26]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[26]_i_3_n_0\,
      I1 => \axi_rdata[26]_i_4_n_0\,
      O => \axi_rdata_reg[26]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(27),
      Q => s_axi_rdata(27),
      R => p_0_in
    );
\axi_rdata_reg[27]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[27]_i_3_n_0\,
      I1 => \axi_rdata[27]_i_4_n_0\,
      O => \axi_rdata_reg[27]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(28),
      Q => s_axi_rdata(28),
      R => p_0_in
    );
\axi_rdata_reg[28]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[28]_i_3_n_0\,
      I1 => \axi_rdata[28]_i_4_n_0\,
      O => \axi_rdata_reg[28]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(29),
      Q => s_axi_rdata(29),
      R => p_0_in
    );
\axi_rdata_reg[29]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[29]_i_3_n_0\,
      I1 => \axi_rdata[29]_i_4_n_0\,
      O => \axi_rdata_reg[29]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(2),
      Q => s_axi_rdata(2),
      R => p_0_in
    );
\axi_rdata_reg[2]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_3_n_0\,
      I1 => \axi_rdata[2]_i_4_n_0\,
      O => \axi_rdata_reg[2]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(30),
      Q => s_axi_rdata(30),
      R => p_0_in
    );
\axi_rdata_reg[30]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[30]_i_3_n_0\,
      I1 => \axi_rdata[30]_i_4_n_0\,
      O => \axi_rdata_reg[30]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(31),
      Q => s_axi_rdata(31),
      R => p_0_in
    );
\axi_rdata_reg[31]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[31]_i_6_n_0\,
      I1 => \axi_rdata[31]_i_7_n_0\,
      O => \axi_rdata_reg[31]_i_5_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(3),
      Q => s_axi_rdata(3),
      R => p_0_in
    );
\axi_rdata_reg[3]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[3]_i_3_n_0\,
      I1 => \axi_rdata[3]_i_4_n_0\,
      O => \axi_rdata_reg[3]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(4),
      Q => s_axi_rdata(4),
      R => p_0_in
    );
\axi_rdata_reg[4]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[4]_i_3_n_0\,
      I1 => \axi_rdata[4]_i_4_n_0\,
      O => \axi_rdata_reg[4]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(5),
      Q => s_axi_rdata(5),
      R => p_0_in
    );
\axi_rdata_reg[5]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[5]_i_3_n_0\,
      I1 => \axi_rdata[5]_i_4_n_0\,
      O => \axi_rdata_reg[5]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(6),
      Q => s_axi_rdata(6),
      R => p_0_in
    );
\axi_rdata_reg[6]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[6]_i_3_n_0\,
      I1 => \axi_rdata[6]_i_4_n_0\,
      O => \axi_rdata_reg[6]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(7),
      Q => s_axi_rdata(7),
      R => p_0_in
    );
\axi_rdata_reg[7]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[7]_i_3_n_0\,
      I1 => \axi_rdata[7]_i_4_n_0\,
      O => \axi_rdata_reg[7]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(8),
      Q => s_axi_rdata(8),
      R => p_0_in
    );
\axi_rdata_reg[8]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[8]_i_3_n_0\,
      I1 => \axi_rdata[8]_i_4_n_0\,
      O => \axi_rdata_reg[8]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => slv_reg_rden,
      D => \reg_data_out__0\(9),
      Q => s_axi_rdata(9),
      R => p_0_in
    );
\axi_rdata_reg[9]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[9]_i_3_n_0\,
      I1 => \axi_rdata[9]_i_4_n_0\,
      O => \axi_rdata_reg[9]_i_2_n_0\,
      S => sel0(2)
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => s_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \^s_axi_rvalid\,
      I3 => s_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s_axi_rvalid\,
      R => p_0_in
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => s_axi_awvalid,
      I1 => s_axi_wvalid,
      I2 => aw_en_reg_n_0,
      I3 => \^s_axi_wready\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s_axi_wready\,
      R => p_0_in
    );
axi_wvalid_falling_edge_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => past_axi_wvalid,
      I1 => s_axi_wvalid,
      O => axi_wvalid_falling_edge0
    );
axi_wvalid_falling_edge_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_wvalid_falling_edge0,
      Q => axi_wvalid_falling_edge,
      R => '0'
    );
\i__carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_1\(12),
      I1 => counter_phase90_output(12),
      O => \x_mod_1_i_reg[13]_0\(2)
    );
\i__carry__0_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_2\(12),
      I1 => q(12),
      O => \x_mod_2_i_reg[13]_0\(0)
    );
\i__carry__0_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_3\(12),
      I1 => counter_phase90_output(12),
      O => \x_mod_3_i_reg[13]_0\(0)
    );
\i__carry__0_i_1__10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_3\(12),
      I1 => counter_phase270_output(12),
      O => \x_mod_3_i_reg[13]_1\(2)
    );
\i__carry__0_i_1__11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_4\(12),
      I1 => counter_phase180_output(12),
      O => \x_mod_4_i_reg[13]_1\(2)
    );
\i__carry__0_i_1__12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_5\(12),
      I1 => counter_phase270_output(12),
      O => \x_mod_5_i_reg[13]_1\(2)
    );
\i__carry__0_i_1__13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_6\(12),
      I1 => counter_phase180_output(12),
      O => \x_mod_6_i_reg[13]_1\(2)
    );
\i__carry__0_i_1__14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_7\(12),
      I1 => counter_phase270_output(12),
      O => \x_mod_7_i_reg[13]_1\(2)
    );
\i__carry__0_i_1__15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_8\(12),
      I1 => counter_phase180_output(12),
      O => \x_mod_8_i_reg[13]_1\(2)
    );
\i__carry__0_i_1__16\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_9\(12),
      I1 => counter_phase270_output(12),
      O => \x_mod_9_i_reg[13]_1\(2)
    );
\i__carry__0_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_4\(12),
      I1 => q(12),
      O => \x_mod_4_i_reg[13]_0\(0)
    );
\i__carry__0_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_5\(12),
      I1 => counter_phase90_output(12),
      O => \x_mod_5_i_reg[13]_0\(0)
    );
\i__carry__0_i_1__4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_6\(12),
      I1 => q(12),
      O => \x_mod_6_i_reg[13]_0\(0)
    );
\i__carry__0_i_1__5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_7\(12),
      I1 => counter_phase90_output(12),
      O => \x_mod_7_i_reg[13]_0\(0)
    );
\i__carry__0_i_1__6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_8\(12),
      I1 => q(12),
      O => \x_mod_8_i_reg[13]_0\(0)
    );
\i__carry__0_i_1__7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_9\(12),
      I1 => counter_phase90_output(12),
      O => \x_mod_9_i_reg[13]_0\(0)
    );
\i__carry__0_i_1__8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_1\(12),
      I1 => counter_phase270_output(12),
      O => \x_mod_1_i_reg[13]_1\(2)
    );
\i__carry__0_i_1__9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_2\(12),
      I1 => counter_phase180_output(12),
      O => \x_mod_2_i_reg[13]_1\(2)
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_1\(11),
      I1 => counter_phase90_output(11),
      I2 => \^x_mod_1\(10),
      I3 => counter_phase90_output(10),
      O => \x_mod_1_i_reg[13]_0\(1)
    );
\i__carry__0_i_2__10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_3\(11),
      I1 => counter_phase270_output(11),
      I2 => \^x_mod_3\(10),
      I3 => counter_phase270_output(10),
      O => \x_mod_3_i_reg[13]_1\(1)
    );
\i__carry__0_i_2__11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_4\(11),
      I1 => counter_phase180_output(11),
      I2 => \^x_mod_4\(10),
      I3 => counter_phase180_output(10),
      O => \x_mod_4_i_reg[13]_1\(1)
    );
\i__carry__0_i_2__12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_5\(11),
      I1 => counter_phase270_output(11),
      I2 => \^x_mod_5\(10),
      I3 => counter_phase270_output(10),
      O => \x_mod_5_i_reg[13]_1\(1)
    );
\i__carry__0_i_2__13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_6\(11),
      I1 => counter_phase180_output(11),
      I2 => \^x_mod_6\(10),
      I3 => counter_phase180_output(10),
      O => \x_mod_6_i_reg[13]_1\(1)
    );
\i__carry__0_i_2__14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_7\(11),
      I1 => counter_phase270_output(11),
      I2 => \^x_mod_7\(10),
      I3 => counter_phase270_output(10),
      O => \x_mod_7_i_reg[13]_1\(1)
    );
\i__carry__0_i_2__15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_8\(11),
      I1 => counter_phase180_output(11),
      I2 => \^x_mod_8\(10),
      I3 => counter_phase180_output(10),
      O => \x_mod_8_i_reg[13]_1\(1)
    );
\i__carry__0_i_2__16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_9\(11),
      I1 => counter_phase270_output(11),
      I2 => \^x_mod_9\(10),
      I3 => counter_phase270_output(10),
      O => \x_mod_9_i_reg[13]_1\(1)
    );
\i__carry__0_i_2__8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_1\(11),
      I1 => counter_phase270_output(11),
      I2 => \^x_mod_1\(10),
      I3 => counter_phase270_output(10),
      O => \x_mod_1_i_reg[13]_1\(1)
    );
\i__carry__0_i_2__9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_2\(11),
      I1 => counter_phase180_output(11),
      I2 => \^x_mod_2\(10),
      I3 => counter_phase180_output(10),
      O => \x_mod_2_i_reg[13]_1\(1)
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_1\(9),
      I1 => counter_phase90_output(9),
      I2 => \^x_mod_1\(8),
      I3 => counter_phase90_output(8),
      O => \x_mod_1_i_reg[13]_0\(0)
    );
\i__carry__0_i_3__10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_3\(9),
      I1 => counter_phase270_output(9),
      I2 => \^x_mod_3\(8),
      I3 => counter_phase270_output(8),
      O => \x_mod_3_i_reg[13]_1\(0)
    );
\i__carry__0_i_3__11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_4\(9),
      I1 => counter_phase180_output(9),
      I2 => \^x_mod_4\(8),
      I3 => counter_phase180_output(8),
      O => \x_mod_4_i_reg[13]_1\(0)
    );
\i__carry__0_i_3__12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_5\(9),
      I1 => counter_phase270_output(9),
      I2 => \^x_mod_5\(8),
      I3 => counter_phase270_output(8),
      O => \x_mod_5_i_reg[13]_1\(0)
    );
\i__carry__0_i_3__13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_6\(9),
      I1 => counter_phase180_output(9),
      I2 => \^x_mod_6\(8),
      I3 => counter_phase180_output(8),
      O => \x_mod_6_i_reg[13]_1\(0)
    );
\i__carry__0_i_3__14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_7\(9),
      I1 => counter_phase270_output(9),
      I2 => \^x_mod_7\(8),
      I3 => counter_phase270_output(8),
      O => \x_mod_7_i_reg[13]_1\(0)
    );
\i__carry__0_i_3__15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_8\(9),
      I1 => counter_phase180_output(9),
      I2 => \^x_mod_8\(8),
      I3 => counter_phase180_output(8),
      O => \x_mod_8_i_reg[13]_1\(0)
    );
\i__carry__0_i_3__16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_9\(9),
      I1 => counter_phase270_output(9),
      I2 => \^x_mod_9\(8),
      I3 => counter_phase270_output(8),
      O => \x_mod_9_i_reg[13]_1\(0)
    );
\i__carry__0_i_3__8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_1\(9),
      I1 => counter_phase270_output(9),
      I2 => \^x_mod_1\(8),
      I3 => counter_phase270_output(8),
      O => \x_mod_1_i_reg[13]_1\(0)
    );
\i__carry__0_i_3__9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_2\(9),
      I1 => counter_phase180_output(9),
      I2 => \^x_mod_2\(8),
      I3 => counter_phase180_output(8),
      O => \x_mod_2_i_reg[13]_1\(0)
    );
\i__carry__0_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_2\(11),
      I1 => q(11),
      I2 => \^x_mod_2\(10),
      I3 => q(10),
      O => \x_mod_2_i_reg[12]_0\(1)
    );
\i__carry__0_i_5__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_3\(11),
      I1 => counter_phase90_output(11),
      I2 => \^x_mod_3\(10),
      I3 => counter_phase90_output(10),
      O => \x_mod_3_i_reg[12]_0\(1)
    );
\i__carry__0_i_5__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_4\(11),
      I1 => q(11),
      I2 => \^x_mod_4\(10),
      I3 => q(10),
      O => \x_mod_4_i_reg[12]_0\(1)
    );
\i__carry__0_i_5__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_5\(11),
      I1 => counter_phase90_output(11),
      I2 => \^x_mod_5\(10),
      I3 => counter_phase90_output(10),
      O => \x_mod_5_i_reg[12]_0\(1)
    );
\i__carry__0_i_5__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_6\(11),
      I1 => q(11),
      I2 => \^x_mod_6\(10),
      I3 => q(10),
      O => \x_mod_6_i_reg[12]_0\(1)
    );
\i__carry__0_i_5__5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_7\(11),
      I1 => counter_phase90_output(11),
      I2 => \^x_mod_7\(10),
      I3 => counter_phase90_output(10),
      O => \x_mod_7_i_reg[12]_0\(1)
    );
\i__carry__0_i_5__6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_8\(11),
      I1 => q(11),
      I2 => \^x_mod_8\(10),
      I3 => q(10),
      O => \x_mod_8_i_reg[12]_0\(1)
    );
\i__carry__0_i_5__7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_9\(11),
      I1 => counter_phase90_output(11),
      I2 => \^x_mod_9\(10),
      I3 => counter_phase90_output(10),
      O => \x_mod_9_i_reg[12]_0\(1)
    );
\i__carry__0_i_6__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_2\(9),
      I1 => q(9),
      I2 => \^x_mod_2\(8),
      I3 => q(8),
      O => \x_mod_2_i_reg[12]_0\(0)
    );
\i__carry__0_i_6__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_3\(9),
      I1 => counter_phase90_output(9),
      I2 => \^x_mod_3\(8),
      I3 => counter_phase90_output(8),
      O => \x_mod_3_i_reg[12]_0\(0)
    );
\i__carry__0_i_6__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_4\(9),
      I1 => q(9),
      I2 => \^x_mod_4\(8),
      I3 => q(8),
      O => \x_mod_4_i_reg[12]_0\(0)
    );
\i__carry__0_i_6__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_5\(9),
      I1 => counter_phase90_output(9),
      I2 => \^x_mod_5\(8),
      I3 => counter_phase90_output(8),
      O => \x_mod_5_i_reg[12]_0\(0)
    );
\i__carry__0_i_6__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_6\(9),
      I1 => q(9),
      I2 => \^x_mod_6\(8),
      I3 => q(8),
      O => \x_mod_6_i_reg[12]_0\(0)
    );
\i__carry__0_i_6__5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_7\(9),
      I1 => counter_phase90_output(9),
      I2 => \^x_mod_7\(8),
      I3 => counter_phase90_output(8),
      O => \x_mod_7_i_reg[12]_0\(0)
    );
\i__carry__0_i_6__6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_8\(9),
      I1 => q(9),
      I2 => \^x_mod_8\(8),
      I3 => q(8),
      O => \x_mod_8_i_reg[12]_0\(0)
    );
\i__carry__0_i_6__7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_9\(9),
      I1 => counter_phase90_output(9),
      I2 => \^x_mod_9\(8),
      I3 => counter_phase90_output(8),
      O => \x_mod_9_i_reg[12]_0\(0)
    );
\i__carry_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_1\(7),
      I1 => counter_phase90_output(7),
      I2 => \^x_mod_1\(6),
      I3 => counter_phase90_output(6),
      O => \x_mod_1_i_reg[8]_0\(3)
    );
\i__carry_i_1__10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_3\(7),
      I1 => counter_phase270_output(7),
      I2 => \^x_mod_3\(6),
      I3 => counter_phase270_output(6),
      O => \x_mod_3_i_reg[8]_1\(3)
    );
\i__carry_i_1__11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_4\(7),
      I1 => counter_phase180_output(7),
      I2 => \^x_mod_4\(6),
      I3 => counter_phase180_output(6),
      O => \x_mod_4_i_reg[8]_1\(3)
    );
\i__carry_i_1__12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_5\(7),
      I1 => counter_phase270_output(7),
      I2 => \^x_mod_5\(6),
      I3 => counter_phase270_output(6),
      O => \x_mod_5_i_reg[8]_1\(3)
    );
\i__carry_i_1__13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_6\(7),
      I1 => counter_phase180_output(7),
      I2 => \^x_mod_6\(6),
      I3 => counter_phase180_output(6),
      O => \x_mod_6_i_reg[8]_1\(3)
    );
\i__carry_i_1__14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_7\(7),
      I1 => counter_phase270_output(7),
      I2 => \^x_mod_7\(6),
      I3 => counter_phase270_output(6),
      O => \x_mod_7_i_reg[8]_1\(3)
    );
\i__carry_i_1__15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_8\(7),
      I1 => counter_phase180_output(7),
      I2 => \^x_mod_8\(6),
      I3 => counter_phase180_output(6),
      O => \x_mod_8_i_reg[8]_1\(3)
    );
\i__carry_i_1__16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_9\(7),
      I1 => counter_phase270_output(7),
      I2 => \^x_mod_9\(6),
      I3 => counter_phase270_output(6),
      O => \x_mod_9_i_reg[8]_1\(3)
    );
\i__carry_i_1__8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_1\(7),
      I1 => counter_phase270_output(7),
      I2 => \^x_mod_1\(6),
      I3 => counter_phase270_output(6),
      O => \x_mod_1_i_reg[8]_1\(3)
    );
\i__carry_i_1__9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_2\(7),
      I1 => counter_phase180_output(7),
      I2 => \^x_mod_2\(6),
      I3 => counter_phase180_output(6),
      O => \x_mod_2_i_reg[8]_0\(3)
    );
\i__carry_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_1\(5),
      I1 => counter_phase90_output(5),
      I2 => \^x_mod_1\(4),
      I3 => counter_phase90_output(4),
      O => \x_mod_1_i_reg[8]_0\(2)
    );
\i__carry_i_2__10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_3\(5),
      I1 => counter_phase270_output(5),
      I2 => \^x_mod_3\(4),
      I3 => counter_phase270_output(4),
      O => \x_mod_3_i_reg[8]_1\(2)
    );
\i__carry_i_2__11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_4\(5),
      I1 => counter_phase180_output(5),
      I2 => \^x_mod_4\(4),
      I3 => counter_phase180_output(4),
      O => \x_mod_4_i_reg[8]_1\(2)
    );
\i__carry_i_2__12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_5\(5),
      I1 => counter_phase270_output(5),
      I2 => \^x_mod_5\(4),
      I3 => counter_phase270_output(4),
      O => \x_mod_5_i_reg[8]_1\(2)
    );
\i__carry_i_2__13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_6\(5),
      I1 => counter_phase180_output(5),
      I2 => \^x_mod_6\(4),
      I3 => counter_phase180_output(4),
      O => \x_mod_6_i_reg[8]_1\(2)
    );
\i__carry_i_2__14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_7\(5),
      I1 => counter_phase270_output(5),
      I2 => \^x_mod_7\(4),
      I3 => counter_phase270_output(4),
      O => \x_mod_7_i_reg[8]_1\(2)
    );
\i__carry_i_2__15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_8\(5),
      I1 => counter_phase180_output(5),
      I2 => \^x_mod_8\(4),
      I3 => counter_phase180_output(4),
      O => \x_mod_8_i_reg[8]_1\(2)
    );
\i__carry_i_2__16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_9\(5),
      I1 => counter_phase270_output(5),
      I2 => \^x_mod_9\(4),
      I3 => counter_phase270_output(4),
      O => \x_mod_9_i_reg[8]_1\(2)
    );
\i__carry_i_2__8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_1\(5),
      I1 => counter_phase270_output(5),
      I2 => \^x_mod_1\(4),
      I3 => counter_phase270_output(4),
      O => \x_mod_1_i_reg[8]_1\(2)
    );
\i__carry_i_2__9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_2\(5),
      I1 => counter_phase180_output(5),
      I2 => \^x_mod_2\(4),
      I3 => counter_phase180_output(4),
      O => \x_mod_2_i_reg[8]_0\(2)
    );
\i__carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_1\(3),
      I1 => counter_phase90_output(3),
      I2 => \^x_mod_1\(2),
      I3 => counter_phase90_output(2),
      O => \x_mod_1_i_reg[8]_0\(1)
    );
\i__carry_i_3__10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_3\(3),
      I1 => counter_phase270_output(3),
      I2 => \^x_mod_3\(2),
      I3 => counter_phase270_output(2),
      O => \x_mod_3_i_reg[8]_1\(1)
    );
\i__carry_i_3__11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_4\(3),
      I1 => counter_phase180_output(3),
      I2 => \^x_mod_4\(2),
      I3 => counter_phase180_output(2),
      O => \x_mod_4_i_reg[8]_1\(1)
    );
\i__carry_i_3__12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_5\(3),
      I1 => counter_phase270_output(3),
      I2 => \^x_mod_5\(2),
      I3 => counter_phase270_output(2),
      O => \x_mod_5_i_reg[8]_1\(1)
    );
\i__carry_i_3__13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_6\(3),
      I1 => counter_phase180_output(3),
      I2 => \^x_mod_6\(2),
      I3 => counter_phase180_output(2),
      O => \x_mod_6_i_reg[8]_1\(1)
    );
\i__carry_i_3__14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_7\(3),
      I1 => counter_phase270_output(3),
      I2 => \^x_mod_7\(2),
      I3 => counter_phase270_output(2),
      O => \x_mod_7_i_reg[8]_1\(1)
    );
\i__carry_i_3__15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_8\(3),
      I1 => counter_phase180_output(3),
      I2 => \^x_mod_8\(2),
      I3 => counter_phase180_output(2),
      O => \x_mod_8_i_reg[8]_1\(1)
    );
\i__carry_i_3__16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_9\(3),
      I1 => counter_phase270_output(3),
      I2 => \^x_mod_9\(2),
      I3 => counter_phase270_output(2),
      O => \x_mod_9_i_reg[8]_1\(1)
    );
\i__carry_i_3__8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_1\(3),
      I1 => counter_phase270_output(3),
      I2 => \^x_mod_1\(2),
      I3 => counter_phase270_output(2),
      O => \x_mod_1_i_reg[8]_1\(1)
    );
\i__carry_i_3__9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_2\(3),
      I1 => counter_phase180_output(3),
      I2 => \^x_mod_2\(2),
      I3 => counter_phase180_output(2),
      O => \x_mod_2_i_reg[8]_0\(1)
    );
\i__carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_1\(1),
      I1 => counter_phase90_output(1),
      I2 => \^x_mod_1\(0),
      I3 => counter_phase90_output(0),
      O => \x_mod_1_i_reg[8]_0\(0)
    );
\i__carry_i_4__10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_3\(1),
      I1 => counter_phase270_output(1),
      I2 => \^x_mod_3\(0),
      I3 => counter_phase270_output(0),
      O => \x_mod_3_i_reg[8]_1\(0)
    );
\i__carry_i_4__11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_4\(1),
      I1 => counter_phase180_output(1),
      I2 => \^x_mod_4\(0),
      I3 => counter_phase180_output(0),
      O => \x_mod_4_i_reg[8]_1\(0)
    );
\i__carry_i_4__12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_5\(1),
      I1 => counter_phase270_output(1),
      I2 => \^x_mod_5\(0),
      I3 => counter_phase270_output(0),
      O => \x_mod_5_i_reg[8]_1\(0)
    );
\i__carry_i_4__13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_6\(1),
      I1 => counter_phase180_output(1),
      I2 => \^x_mod_6\(0),
      I3 => counter_phase180_output(0),
      O => \x_mod_6_i_reg[8]_1\(0)
    );
\i__carry_i_4__14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_7\(1),
      I1 => counter_phase270_output(1),
      I2 => \^x_mod_7\(0),
      I3 => counter_phase270_output(0),
      O => \x_mod_7_i_reg[8]_1\(0)
    );
\i__carry_i_4__15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_8\(1),
      I1 => counter_phase180_output(1),
      I2 => \^x_mod_8\(0),
      I3 => counter_phase180_output(0),
      O => \x_mod_8_i_reg[8]_1\(0)
    );
\i__carry_i_4__16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_9\(1),
      I1 => counter_phase270_output(1),
      I2 => \^x_mod_9\(0),
      I3 => counter_phase270_output(0),
      O => \x_mod_9_i_reg[8]_1\(0)
    );
\i__carry_i_4__8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_1\(1),
      I1 => counter_phase270_output(1),
      I2 => \^x_mod_1\(0),
      I3 => counter_phase270_output(0),
      O => \x_mod_1_i_reg[8]_1\(0)
    );
\i__carry_i_4__9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_2\(1),
      I1 => counter_phase180_output(1),
      I2 => \^x_mod_2\(0),
      I3 => counter_phase180_output(0),
      O => \x_mod_2_i_reg[8]_0\(0)
    );
\i__carry_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_2\(7),
      I1 => q(7),
      I2 => \^x_mod_2\(6),
      I3 => q(6),
      O => S(3)
    );
\i__carry_i_5__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_3\(7),
      I1 => counter_phase90_output(7),
      I2 => \^x_mod_3\(6),
      I3 => counter_phase90_output(6),
      O => \x_mod_3_i_reg[8]_0\(3)
    );
\i__carry_i_5__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_4\(7),
      I1 => q(7),
      I2 => \^x_mod_4\(6),
      I3 => q(6),
      O => \x_mod_4_i_reg[8]_0\(3)
    );
\i__carry_i_5__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_5\(7),
      I1 => counter_phase90_output(7),
      I2 => \^x_mod_5\(6),
      I3 => counter_phase90_output(6),
      O => \x_mod_5_i_reg[8]_0\(3)
    );
\i__carry_i_5__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_6\(7),
      I1 => q(7),
      I2 => \^x_mod_6\(6),
      I3 => q(6),
      O => \x_mod_6_i_reg[8]_0\(3)
    );
\i__carry_i_5__5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_7\(7),
      I1 => counter_phase90_output(7),
      I2 => \^x_mod_7\(6),
      I3 => counter_phase90_output(6),
      O => \x_mod_7_i_reg[8]_0\(3)
    );
\i__carry_i_5__6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_8\(7),
      I1 => q(7),
      I2 => \^x_mod_8\(6),
      I3 => q(6),
      O => \x_mod_8_i_reg[8]_0\(3)
    );
\i__carry_i_5__7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_9\(7),
      I1 => counter_phase90_output(7),
      I2 => \^x_mod_9\(6),
      I3 => counter_phase90_output(6),
      O => \x_mod_9_i_reg[8]_0\(3)
    );
\i__carry_i_6__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_2\(5),
      I1 => q(5),
      I2 => \^x_mod_2\(4),
      I3 => q(4),
      O => S(2)
    );
\i__carry_i_6__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_3\(5),
      I1 => counter_phase90_output(5),
      I2 => \^x_mod_3\(4),
      I3 => counter_phase90_output(4),
      O => \x_mod_3_i_reg[8]_0\(2)
    );
\i__carry_i_6__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_4\(5),
      I1 => q(5),
      I2 => \^x_mod_4\(4),
      I3 => q(4),
      O => \x_mod_4_i_reg[8]_0\(2)
    );
\i__carry_i_6__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_5\(5),
      I1 => counter_phase90_output(5),
      I2 => \^x_mod_5\(4),
      I3 => counter_phase90_output(4),
      O => \x_mod_5_i_reg[8]_0\(2)
    );
\i__carry_i_6__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_6\(5),
      I1 => q(5),
      I2 => \^x_mod_6\(4),
      I3 => q(4),
      O => \x_mod_6_i_reg[8]_0\(2)
    );
\i__carry_i_6__5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_7\(5),
      I1 => counter_phase90_output(5),
      I2 => \^x_mod_7\(4),
      I3 => counter_phase90_output(4),
      O => \x_mod_7_i_reg[8]_0\(2)
    );
\i__carry_i_6__6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_8\(5),
      I1 => q(5),
      I2 => \^x_mod_8\(4),
      I3 => q(4),
      O => \x_mod_8_i_reg[8]_0\(2)
    );
\i__carry_i_6__7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_9\(5),
      I1 => counter_phase90_output(5),
      I2 => \^x_mod_9\(4),
      I3 => counter_phase90_output(4),
      O => \x_mod_9_i_reg[8]_0\(2)
    );
\i__carry_i_7__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_2\(3),
      I1 => q(3),
      I2 => \^x_mod_2\(2),
      I3 => q(2),
      O => S(1)
    );
\i__carry_i_7__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_3\(3),
      I1 => counter_phase90_output(3),
      I2 => \^x_mod_3\(2),
      I3 => counter_phase90_output(2),
      O => \x_mod_3_i_reg[8]_0\(1)
    );
\i__carry_i_7__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_4\(3),
      I1 => q(3),
      I2 => \^x_mod_4\(2),
      I3 => q(2),
      O => \x_mod_4_i_reg[8]_0\(1)
    );
\i__carry_i_7__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_5\(3),
      I1 => counter_phase90_output(3),
      I2 => \^x_mod_5\(2),
      I3 => counter_phase90_output(2),
      O => \x_mod_5_i_reg[8]_0\(1)
    );
\i__carry_i_7__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_6\(3),
      I1 => q(3),
      I2 => \^x_mod_6\(2),
      I3 => q(2),
      O => \x_mod_6_i_reg[8]_0\(1)
    );
\i__carry_i_7__5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_7\(3),
      I1 => counter_phase90_output(3),
      I2 => \^x_mod_7\(2),
      I3 => counter_phase90_output(2),
      O => \x_mod_7_i_reg[8]_0\(1)
    );
\i__carry_i_7__6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_8\(3),
      I1 => q(3),
      I2 => \^x_mod_8\(2),
      I3 => q(2),
      O => \x_mod_8_i_reg[8]_0\(1)
    );
\i__carry_i_7__7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_9\(3),
      I1 => counter_phase90_output(3),
      I2 => \^x_mod_9\(2),
      I3 => counter_phase90_output(2),
      O => \x_mod_9_i_reg[8]_0\(1)
    );
\i__carry_i_8__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_2\(1),
      I1 => q(1),
      I2 => \^x_mod_2\(0),
      I3 => q(0),
      O => S(0)
    );
\i__carry_i_8__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_3\(1),
      I1 => counter_phase90_output(1),
      I2 => \^x_mod_3\(0),
      I3 => counter_phase90_output(0),
      O => \x_mod_3_i_reg[8]_0\(0)
    );
\i__carry_i_8__2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_4\(1),
      I1 => q(1),
      I2 => \^x_mod_4\(0),
      I3 => q(0),
      O => \x_mod_4_i_reg[8]_0\(0)
    );
\i__carry_i_8__3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_5\(1),
      I1 => counter_phase90_output(1),
      I2 => \^x_mod_5\(0),
      I3 => counter_phase90_output(0),
      O => \x_mod_5_i_reg[8]_0\(0)
    );
\i__carry_i_8__4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_6\(1),
      I1 => q(1),
      I2 => \^x_mod_6\(0),
      I3 => q(0),
      O => \x_mod_6_i_reg[8]_0\(0)
    );
\i__carry_i_8__5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_7\(1),
      I1 => counter_phase90_output(1),
      I2 => \^x_mod_7\(0),
      I3 => counter_phase90_output(0),
      O => \x_mod_7_i_reg[8]_0\(0)
    );
\i__carry_i_8__6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_8\(1),
      I1 => q(1),
      I2 => \^x_mod_8\(0),
      I3 => q(0),
      O => \x_mod_8_i_reg[8]_0\(0)
    );
\i__carry_i_8__7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^x_mod_9\(1),
      I1 => counter_phase90_output(1),
      I2 => \^x_mod_9\(0),
      I3 => counter_phase90_output(0),
      O => \x_mod_9_i_reg[8]_0\(0)
    );
past_axi_wvalid_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => '1',
      D => s_axi_wvalid,
      Q => past_axi_wvalid,
      R => '0'
    );
\pwm_a_p_i0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_0\(12),
      I1 => q(12),
      O => \x_mod_0_i_reg[13]_0\(2)
    );
\pwm_a_p_i0_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_0\(11),
      I1 => q(11),
      I2 => \^x_mod_0\(10),
      I3 => q(10),
      O => \x_mod_0_i_reg[13]_0\(1)
    );
\pwm_a_p_i0_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_0\(9),
      I1 => q(9),
      I2 => \^x_mod_0\(8),
      I3 => q(8),
      O => \x_mod_0_i_reg[13]_0\(0)
    );
pwm_a_p_i0_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_0\(7),
      I1 => q(7),
      I2 => \^x_mod_0\(6),
      I3 => q(6),
      O => DI(3)
    );
pwm_a_p_i0_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_0\(5),
      I1 => q(5),
      I2 => \^x_mod_0\(4),
      I3 => q(4),
      O => DI(2)
    );
pwm_a_p_i0_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_0\(3),
      I1 => q(3),
      I2 => \^x_mod_0\(2),
      I3 => q(2),
      O => DI(1)
    );
pwm_a_p_i0_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_0\(1),
      I1 => q(1),
      I2 => \^x_mod_0\(0),
      I3 => q(0),
      O => DI(0)
    );
\pwm_b_p_i0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^x_mod_0\(12),
      I1 => counter_phase180_output(12),
      O => \x_mod_0_i_reg[13]_1\(2)
    );
\pwm_b_p_i0_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_0\(11),
      I1 => counter_phase180_output(11),
      I2 => \^x_mod_0\(10),
      I3 => counter_phase180_output(10),
      O => \x_mod_0_i_reg[13]_1\(1)
    );
\pwm_b_p_i0_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_0\(9),
      I1 => counter_phase180_output(9),
      I2 => \^x_mod_0\(8),
      I3 => counter_phase180_output(8),
      O => \x_mod_0_i_reg[13]_1\(0)
    );
pwm_b_p_i0_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_0\(7),
      I1 => counter_phase180_output(7),
      I2 => \^x_mod_0\(6),
      I3 => counter_phase180_output(6),
      O => \x_mod_0_i_reg[8]_0\(3)
    );
pwm_b_p_i0_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_0\(5),
      I1 => counter_phase180_output(5),
      I2 => \^x_mod_0\(4),
      I3 => counter_phase180_output(4),
      O => \x_mod_0_i_reg[8]_0\(2)
    );
pwm_b_p_i0_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_0\(3),
      I1 => counter_phase180_output(3),
      I2 => \^x_mod_0\(2),
      I3 => counter_phase180_output(2),
      O => \x_mod_0_i_reg[8]_0\(1)
    );
pwm_b_p_i0_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \^x_mod_0\(1),
      I1 => counter_phase180_output(1),
      I2 => \^x_mod_0\(0),
      I3 => counter_phase180_output(0),
      O => \x_mod_0_i_reg[8]_0\(0)
    );
\slv_reg0[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(0),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(2),
      I5 => s_axi_wstrb(1),
      O => p_1_in(13)
    );
\slv_reg0[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(0),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(2),
      I5 => s_axi_wstrb(2),
      O => p_1_in(23)
    );
\slv_reg0[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(0),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(2),
      I5 => s_axi_wstrb(3),
      O => p_1_in(31)
    );
\slv_reg0[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s_axi_wvalid,
      O => \slv_reg_wren__2\
    );
\slv_reg0[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(0),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(2),
      I5 => s_axi_wstrb(0),
      O => p_1_in(7)
    );
\slv_reg0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(0),
      Q => slv_reg0(0),
      R => p_0_in
    );
\slv_reg0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(13),
      D => s_axi_wdata(10),
      Q => slv_reg0(10),
      R => p_0_in
    );
\slv_reg0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(13),
      D => s_axi_wdata(11),
      Q => slv_reg0(11),
      R => p_0_in
    );
\slv_reg0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(13),
      D => s_axi_wdata(12),
      Q => slv_reg0(12),
      R => p_0_in
    );
\slv_reg0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(13),
      D => s_axi_wdata(13),
      Q => slv_reg0(13),
      R => p_0_in
    );
\slv_reg0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(13),
      D => s_axi_wdata(14),
      Q => slv_reg0(14),
      R => p_0_in
    );
\slv_reg0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(13),
      D => s_axi_wdata(15),
      Q => slv_reg0(15),
      R => p_0_in
    );
\slv_reg0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(16),
      Q => slv_reg0(16),
      R => p_0_in
    );
\slv_reg0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(17),
      Q => slv_reg0(17),
      R => p_0_in
    );
\slv_reg0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(18),
      Q => slv_reg0(18),
      R => p_0_in
    );
\slv_reg0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(19),
      Q => slv_reg0(19),
      R => p_0_in
    );
\slv_reg0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(1),
      Q => slv_reg0(1),
      R => p_0_in
    );
\slv_reg0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(20),
      Q => slv_reg0(20),
      R => p_0_in
    );
\slv_reg0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(21),
      Q => slv_reg0(21),
      R => p_0_in
    );
\slv_reg0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(22),
      Q => slv_reg0(22),
      R => p_0_in
    );
\slv_reg0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(23),
      D => s_axi_wdata(23),
      Q => slv_reg0(23),
      R => p_0_in
    );
\slv_reg0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(24),
      Q => slv_reg0(24),
      R => p_0_in
    );
\slv_reg0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(25),
      Q => slv_reg0(25),
      R => p_0_in
    );
\slv_reg0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(26),
      Q => slv_reg0(26),
      R => p_0_in
    );
\slv_reg0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(27),
      Q => slv_reg0(27),
      R => p_0_in
    );
\slv_reg0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(28),
      Q => slv_reg0(28),
      R => p_0_in
    );
\slv_reg0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(29),
      Q => slv_reg0(29),
      R => p_0_in
    );
\slv_reg0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(2),
      Q => slv_reg0(2),
      R => p_0_in
    );
\slv_reg0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(30),
      Q => slv_reg0(30),
      R => p_0_in
    );
\slv_reg0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(31),
      D => s_axi_wdata(31),
      Q => slv_reg0(31),
      R => p_0_in
    );
\slv_reg0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(3),
      Q => slv_reg0(3),
      R => p_0_in
    );
\slv_reg0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(4),
      Q => slv_reg0(4),
      R => p_0_in
    );
\slv_reg0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(5),
      Q => slv_reg0(5),
      R => p_0_in
    );
\slv_reg0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(6),
      Q => slv_reg0(6),
      R => p_0_in
    );
\slv_reg0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(7),
      D => s_axi_wdata(7),
      Q => slv_reg0(7),
      R => p_0_in
    );
\slv_reg0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(13),
      D => s_axi_wdata(8),
      Q => slv_reg0(8),
      R => p_0_in
    );
\slv_reg0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => p_1_in(13),
      D => s_axi_wdata(9),
      Q => slv_reg0(9),
      R => p_0_in
    );
\slv_reg1[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(1),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(2),
      I5 => \p_0_in__0\(0),
      O => \slv_reg1[15]_i_1_n_0\
    );
\slv_reg1[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(2),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(2),
      I5 => \p_0_in__0\(0),
      O => \slv_reg1[23]_i_1_n_0\
    );
\slv_reg1[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(3),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(2),
      I5 => \p_0_in__0\(0),
      O => \slv_reg1[31]_i_1_n_0\
    );
\slv_reg1[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(0),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(2),
      I5 => \p_0_in__0\(0),
      O => \slv_reg1[7]_i_1_n_0\
    );
\slv_reg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => slv_reg1(0),
      R => p_0_in
    );
\slv_reg1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => slv_reg1(10),
      R => p_0_in
    );
\slv_reg1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => slv_reg1(11),
      R => p_0_in
    );
\slv_reg1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => slv_reg1(12),
      R => p_0_in
    );
\slv_reg1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => slv_reg1(13),
      R => p_0_in
    );
\slv_reg1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => slv_reg1(14),
      R => p_0_in
    );
\slv_reg1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => slv_reg1(15),
      R => p_0_in
    );
\slv_reg1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => slv_reg1(16),
      R => p_0_in
    );
\slv_reg1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => slv_reg1(17),
      R => p_0_in
    );
\slv_reg1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => slv_reg1(18),
      R => p_0_in
    );
\slv_reg1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => slv_reg1(19),
      R => p_0_in
    );
\slv_reg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => slv_reg1(1),
      R => p_0_in
    );
\slv_reg1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => slv_reg1(20),
      R => p_0_in
    );
\slv_reg1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => slv_reg1(21),
      R => p_0_in
    );
\slv_reg1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => slv_reg1(22),
      R => p_0_in
    );
\slv_reg1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => slv_reg1(23),
      R => p_0_in
    );
\slv_reg1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => slv_reg1(24),
      R => p_0_in
    );
\slv_reg1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => slv_reg1(25),
      R => p_0_in
    );
\slv_reg1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => slv_reg1(26),
      R => p_0_in
    );
\slv_reg1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => slv_reg1(27),
      R => p_0_in
    );
\slv_reg1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => slv_reg1(28),
      R => p_0_in
    );
\slv_reg1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => slv_reg1(29),
      R => p_0_in
    );
\slv_reg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => slv_reg1(2),
      R => p_0_in
    );
\slv_reg1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => slv_reg1(30),
      R => p_0_in
    );
\slv_reg1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => slv_reg1(31),
      R => p_0_in
    );
\slv_reg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => slv_reg1(3),
      R => p_0_in
    );
\slv_reg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => slv_reg1(4),
      R => p_0_in
    );
\slv_reg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => slv_reg1(5),
      R => p_0_in
    );
\slv_reg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => slv_reg1(6),
      R => p_0_in
    );
\slv_reg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => slv_reg1(7),
      R => p_0_in
    );
\slv_reg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => slv_reg1(8),
      R => p_0_in
    );
\slv_reg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => slv_reg1(9),
      R => p_0_in
    );
\slv_reg2[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(1),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(0),
      I4 => \p_0_in__0\(2),
      I5 => \p_0_in__0\(1),
      O => \slv_reg2[15]_i_1_n_0\
    );
\slv_reg2[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(2),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(0),
      I4 => \p_0_in__0\(2),
      I5 => \p_0_in__0\(1),
      O => \slv_reg2[23]_i_1_n_0\
    );
\slv_reg2[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(3),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(0),
      I4 => \p_0_in__0\(2),
      I5 => \p_0_in__0\(1),
      O => \slv_reg2[31]_i_1_n_0\
    );
\slv_reg2[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(0),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(0),
      I4 => \p_0_in__0\(2),
      I5 => \p_0_in__0\(1),
      O => \slv_reg2[7]_i_1_n_0\
    );
\slv_reg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => slv_reg2(0),
      R => p_0_in
    );
\slv_reg2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => slv_reg2(10),
      R => p_0_in
    );
\slv_reg2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => slv_reg2(11),
      R => p_0_in
    );
\slv_reg2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => slv_reg2(12),
      R => p_0_in
    );
\slv_reg2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => slv_reg2(13),
      R => p_0_in
    );
\slv_reg2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => slv_reg2(14),
      R => p_0_in
    );
\slv_reg2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => slv_reg2(15),
      R => p_0_in
    );
\slv_reg2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => slv_reg2(16),
      R => p_0_in
    );
\slv_reg2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => slv_reg2(17),
      R => p_0_in
    );
\slv_reg2_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => slv_reg2(18),
      R => p_0_in
    );
\slv_reg2_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => slv_reg2(19),
      R => p_0_in
    );
\slv_reg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => slv_reg2(1),
      R => p_0_in
    );
\slv_reg2_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => slv_reg2(20),
      R => p_0_in
    );
\slv_reg2_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => slv_reg2(21),
      R => p_0_in
    );
\slv_reg2_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => slv_reg2(22),
      R => p_0_in
    );
\slv_reg2_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => slv_reg2(23),
      R => p_0_in
    );
\slv_reg2_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => slv_reg2(24),
      R => p_0_in
    );
\slv_reg2_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => slv_reg2(25),
      R => p_0_in
    );
\slv_reg2_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => slv_reg2(26),
      R => p_0_in
    );
\slv_reg2_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => slv_reg2(27),
      R => p_0_in
    );
\slv_reg2_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => slv_reg2(28),
      R => p_0_in
    );
\slv_reg2_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => slv_reg2(29),
      R => p_0_in
    );
\slv_reg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => slv_reg2(2),
      R => p_0_in
    );
\slv_reg2_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => slv_reg2(30),
      R => p_0_in
    );
\slv_reg2_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => slv_reg2(31),
      R => p_0_in
    );
\slv_reg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => slv_reg2(3),
      R => p_0_in
    );
\slv_reg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => slv_reg2(4),
      R => p_0_in
    );
\slv_reg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => slv_reg2(5),
      R => p_0_in
    );
\slv_reg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => slv_reg2(6),
      R => p_0_in
    );
\slv_reg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => slv_reg2(7),
      R => p_0_in
    );
\slv_reg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => slv_reg2(8),
      R => p_0_in
    );
\slv_reg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => slv_reg2(9),
      R => p_0_in
    );
\slv_reg3[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(1),
      I2 => \p_0_in__0\(0),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(2),
      I5 => \p_0_in__0\(3),
      O => \slv_reg3[15]_i_1_n_0\
    );
\slv_reg3[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(2),
      I2 => \p_0_in__0\(0),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(2),
      I5 => \p_0_in__0\(3),
      O => \slv_reg3[23]_i_1_n_0\
    );
\slv_reg3[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(3),
      I2 => \p_0_in__0\(0),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(2),
      I5 => \p_0_in__0\(3),
      O => \slv_reg3[31]_i_1_n_0\
    );
\slv_reg3[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(0),
      I2 => \p_0_in__0\(0),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(2),
      I5 => \p_0_in__0\(3),
      O => \slv_reg3[7]_i_1_n_0\
    );
\slv_reg3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => slv_reg3(0),
      R => p_0_in
    );
\slv_reg3_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => slv_reg3(10),
      R => p_0_in
    );
\slv_reg3_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => slv_reg3(11),
      R => p_0_in
    );
\slv_reg3_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => slv_reg3(12),
      R => p_0_in
    );
\slv_reg3_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => slv_reg3(13),
      R => p_0_in
    );
\slv_reg3_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => slv_reg3(14),
      R => p_0_in
    );
\slv_reg3_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => slv_reg3(15),
      R => p_0_in
    );
\slv_reg3_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => slv_reg3(16),
      R => p_0_in
    );
\slv_reg3_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => slv_reg3(17),
      R => p_0_in
    );
\slv_reg3_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => slv_reg3(18),
      R => p_0_in
    );
\slv_reg3_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => slv_reg3(19),
      R => p_0_in
    );
\slv_reg3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => slv_reg3(1),
      R => p_0_in
    );
\slv_reg3_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => slv_reg3(20),
      R => p_0_in
    );
\slv_reg3_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => slv_reg3(21),
      R => p_0_in
    );
\slv_reg3_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => slv_reg3(22),
      R => p_0_in
    );
\slv_reg3_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => slv_reg3(23),
      R => p_0_in
    );
\slv_reg3_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => slv_reg3(24),
      R => p_0_in
    );
\slv_reg3_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => slv_reg3(25),
      R => p_0_in
    );
\slv_reg3_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => slv_reg3(26),
      R => p_0_in
    );
\slv_reg3_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => slv_reg3(27),
      R => p_0_in
    );
\slv_reg3_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => slv_reg3(28),
      R => p_0_in
    );
\slv_reg3_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => slv_reg3(29),
      R => p_0_in
    );
\slv_reg3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => slv_reg3(2),
      R => p_0_in
    );
\slv_reg3_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => slv_reg3(30),
      R => p_0_in
    );
\slv_reg3_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => slv_reg3(31),
      R => p_0_in
    );
\slv_reg3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => slv_reg3(3),
      R => p_0_in
    );
\slv_reg3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => slv_reg3(4),
      R => p_0_in
    );
\slv_reg3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => slv_reg3(5),
      R => p_0_in
    );
\slv_reg3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => slv_reg3(6),
      R => p_0_in
    );
\slv_reg3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => slv_reg3(7),
      R => p_0_in
    );
\slv_reg3_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => slv_reg3(8),
      R => p_0_in
    );
\slv_reg3_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => slv_reg3(9),
      R => p_0_in
    );
\slv_reg4[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(1),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(0),
      I5 => \p_0_in__0\(2),
      O => \slv_reg4[15]_i_1_n_0\
    );
\slv_reg4[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(2),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(0),
      I5 => \p_0_in__0\(2),
      O => \slv_reg4[23]_i_1_n_0\
    );
\slv_reg4[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(3),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(0),
      I5 => \p_0_in__0\(2),
      O => \slv_reg4[31]_i_1_n_0\
    );
\slv_reg4[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(0),
      I2 => \p_0_in__0\(3),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(0),
      I5 => \p_0_in__0\(2),
      O => \slv_reg4[7]_i_1_n_0\
    );
\slv_reg4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => slv_reg4(0),
      R => p_0_in
    );
\slv_reg4_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => slv_reg4(10),
      R => p_0_in
    );
\slv_reg4_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => slv_reg4(11),
      R => p_0_in
    );
\slv_reg4_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => slv_reg4(12),
      R => p_0_in
    );
\slv_reg4_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => slv_reg4(13),
      R => p_0_in
    );
\slv_reg4_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => slv_reg4(14),
      R => p_0_in
    );
\slv_reg4_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => slv_reg4(15),
      R => p_0_in
    );
\slv_reg4_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => slv_reg4(16),
      R => p_0_in
    );
\slv_reg4_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => slv_reg4(17),
      R => p_0_in
    );
\slv_reg4_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => slv_reg4(18),
      R => p_0_in
    );
\slv_reg4_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => slv_reg4(19),
      R => p_0_in
    );
\slv_reg4_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => slv_reg4(1),
      R => p_0_in
    );
\slv_reg4_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => slv_reg4(20),
      R => p_0_in
    );
\slv_reg4_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => slv_reg4(21),
      R => p_0_in
    );
\slv_reg4_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => slv_reg4(22),
      R => p_0_in
    );
\slv_reg4_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => slv_reg4(23),
      R => p_0_in
    );
\slv_reg4_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => slv_reg4(24),
      R => p_0_in
    );
\slv_reg4_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => slv_reg4(25),
      R => p_0_in
    );
\slv_reg4_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => slv_reg4(26),
      R => p_0_in
    );
\slv_reg4_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => slv_reg4(27),
      R => p_0_in
    );
\slv_reg4_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => slv_reg4(28),
      R => p_0_in
    );
\slv_reg4_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => slv_reg4(29),
      R => p_0_in
    );
\slv_reg4_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => slv_reg4(2),
      R => p_0_in
    );
\slv_reg4_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => slv_reg4(30),
      R => p_0_in
    );
\slv_reg4_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => slv_reg4(31),
      R => p_0_in
    );
\slv_reg4_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => slv_reg4(3),
      R => p_0_in
    );
\slv_reg4_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => slv_reg4(4),
      R => p_0_in
    );
\slv_reg4_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => slv_reg4(5),
      R => p_0_in
    );
\slv_reg4_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => slv_reg4(6),
      R => p_0_in
    );
\slv_reg4_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => slv_reg4(7),
      R => p_0_in
    );
\slv_reg4_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => slv_reg4(8),
      R => p_0_in
    );
\slv_reg4_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => slv_reg4(9),
      R => p_0_in
    );
\slv_reg5[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(1),
      I2 => \p_0_in__0\(0),
      I3 => \p_0_in__0\(2),
      I4 => \p_0_in__0\(1),
      I5 => \p_0_in__0\(3),
      O => \slv_reg5[15]_i_1_n_0\
    );
\slv_reg5[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(2),
      I2 => \p_0_in__0\(0),
      I3 => \p_0_in__0\(2),
      I4 => \p_0_in__0\(1),
      I5 => \p_0_in__0\(3),
      O => \slv_reg5[23]_i_1_n_0\
    );
\slv_reg5[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(3),
      I2 => \p_0_in__0\(0),
      I3 => \p_0_in__0\(2),
      I4 => \p_0_in__0\(1),
      I5 => \p_0_in__0\(3),
      O => \slv_reg5[31]_i_1_n_0\
    );
\slv_reg5[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(0),
      I2 => \p_0_in__0\(0),
      I3 => \p_0_in__0\(2),
      I4 => \p_0_in__0\(1),
      I5 => \p_0_in__0\(3),
      O => \slv_reg5[7]_i_1_n_0\
    );
\slv_reg5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => slv_reg5(0),
      R => p_0_in
    );
\slv_reg5_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => slv_reg5(10),
      R => p_0_in
    );
\slv_reg5_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => slv_reg5(11),
      R => p_0_in
    );
\slv_reg5_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => slv_reg5(12),
      R => p_0_in
    );
\slv_reg5_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => slv_reg5(13),
      R => p_0_in
    );
\slv_reg5_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => slv_reg5(14),
      R => p_0_in
    );
\slv_reg5_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => slv_reg5(15),
      R => p_0_in
    );
\slv_reg5_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => slv_reg5(16),
      R => p_0_in
    );
\slv_reg5_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => slv_reg5(17),
      R => p_0_in
    );
\slv_reg5_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => slv_reg5(18),
      R => p_0_in
    );
\slv_reg5_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => slv_reg5(19),
      R => p_0_in
    );
\slv_reg5_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => slv_reg5(1),
      R => p_0_in
    );
\slv_reg5_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => slv_reg5(20),
      R => p_0_in
    );
\slv_reg5_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => slv_reg5(21),
      R => p_0_in
    );
\slv_reg5_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => slv_reg5(22),
      R => p_0_in
    );
\slv_reg5_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => slv_reg5(23),
      R => p_0_in
    );
\slv_reg5_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => slv_reg5(24),
      R => p_0_in
    );
\slv_reg5_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => slv_reg5(25),
      R => p_0_in
    );
\slv_reg5_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => slv_reg5(26),
      R => p_0_in
    );
\slv_reg5_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => slv_reg5(27),
      R => p_0_in
    );
\slv_reg5_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => slv_reg5(28),
      R => p_0_in
    );
\slv_reg5_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => slv_reg5(29),
      R => p_0_in
    );
\slv_reg5_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => slv_reg5(2),
      R => p_0_in
    );
\slv_reg5_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => slv_reg5(30),
      R => p_0_in
    );
\slv_reg5_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => slv_reg5(31),
      R => p_0_in
    );
\slv_reg5_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => slv_reg5(3),
      R => p_0_in
    );
\slv_reg5_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => slv_reg5(4),
      R => p_0_in
    );
\slv_reg5_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => slv_reg5(5),
      R => p_0_in
    );
\slv_reg5_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => slv_reg5(6),
      R => p_0_in
    );
\slv_reg5_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => slv_reg5(7),
      R => p_0_in
    );
\slv_reg5_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => slv_reg5(8),
      R => p_0_in
    );
\slv_reg5_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => slv_reg5(9),
      R => p_0_in
    );
\slv_reg6[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(1),
      I2 => \p_0_in__0\(2),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(0),
      I5 => \p_0_in__0\(3),
      O => \slv_reg6[15]_i_1_n_0\
    );
\slv_reg6[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(2),
      I2 => \p_0_in__0\(2),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(0),
      I5 => \p_0_in__0\(3),
      O => \slv_reg6[23]_i_1_n_0\
    );
\slv_reg6[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(3),
      I2 => \p_0_in__0\(2),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(0),
      I5 => \p_0_in__0\(3),
      O => \slv_reg6[31]_i_1_n_0\
    );
\slv_reg6[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => s_axi_wstrb(0),
      I2 => \p_0_in__0\(2),
      I3 => \p_0_in__0\(1),
      I4 => \p_0_in__0\(0),
      I5 => \p_0_in__0\(3),
      O => \slv_reg6[7]_i_1_n_0\
    );
\slv_reg6_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => slv_reg6(0),
      R => p_0_in
    );
\slv_reg6_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => slv_reg6(10),
      R => p_0_in
    );
\slv_reg6_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => slv_reg6(11),
      R => p_0_in
    );
\slv_reg6_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => slv_reg6(12),
      R => p_0_in
    );
\slv_reg6_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => slv_reg6(13),
      R => p_0_in
    );
\slv_reg6_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => slv_reg6(14),
      R => p_0_in
    );
\slv_reg6_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => slv_reg6(15),
      R => p_0_in
    );
\slv_reg6_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => slv_reg6(16),
      R => p_0_in
    );
\slv_reg6_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => slv_reg6(17),
      R => p_0_in
    );
\slv_reg6_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => slv_reg6(18),
      R => p_0_in
    );
\slv_reg6_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => slv_reg6(19),
      R => p_0_in
    );
\slv_reg6_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => slv_reg6(1),
      R => p_0_in
    );
\slv_reg6_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => slv_reg6(20),
      R => p_0_in
    );
\slv_reg6_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => slv_reg6(21),
      R => p_0_in
    );
\slv_reg6_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => slv_reg6(22),
      R => p_0_in
    );
\slv_reg6_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => slv_reg6(23),
      R => p_0_in
    );
\slv_reg6_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => slv_reg6(24),
      R => p_0_in
    );
\slv_reg6_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => slv_reg6(25),
      R => p_0_in
    );
\slv_reg6_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => slv_reg6(26),
      R => p_0_in
    );
\slv_reg6_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => slv_reg6(27),
      R => p_0_in
    );
\slv_reg6_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => slv_reg6(28),
      R => p_0_in
    );
\slv_reg6_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => slv_reg6(29),
      R => p_0_in
    );
\slv_reg6_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => slv_reg6(2),
      R => p_0_in
    );
\slv_reg6_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => slv_reg6(30),
      R => p_0_in
    );
\slv_reg6_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => slv_reg6(31),
      R => p_0_in
    );
\slv_reg6_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => slv_reg6(3),
      R => p_0_in
    );
\slv_reg6_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => slv_reg6(4),
      R => p_0_in
    );
\slv_reg6_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => slv_reg6(5),
      R => p_0_in
    );
\slv_reg6_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => slv_reg6(6),
      R => p_0_in
    );
\slv_reg6_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => slv_reg6(7),
      R => p_0_in
    );
\slv_reg6_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => slv_reg6(8),
      R => p_0_in
    );
\slv_reg6_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => slv_reg6(9),
      R => p_0_in
    );
\slv_reg7[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(2),
      I2 => s_axi_wstrb(1),
      I3 => \p_0_in__0\(0),
      I4 => \p_0_in__0\(1),
      I5 => \p_0_in__0\(3),
      O => \slv_reg7[15]_i_1_n_0\
    );
\slv_reg7[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(2),
      I2 => s_axi_wstrb(2),
      I3 => \p_0_in__0\(0),
      I4 => \p_0_in__0\(1),
      I5 => \p_0_in__0\(3),
      O => \slv_reg7[23]_i_1_n_0\
    );
\slv_reg7[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(2),
      I2 => s_axi_wstrb(3),
      I3 => \p_0_in__0\(0),
      I4 => \p_0_in__0\(1),
      I5 => \p_0_in__0\(3),
      O => \slv_reg7[31]_i_1_n_0\
    );
\slv_reg7[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(2),
      I2 => s_axi_wstrb(0),
      I3 => \p_0_in__0\(0),
      I4 => \p_0_in__0\(1),
      I5 => \p_0_in__0\(3),
      O => \slv_reg7[7]_i_1_n_0\
    );
\slv_reg7_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => slv_reg7(0),
      R => p_0_in
    );
\slv_reg7_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => slv_reg7(10),
      R => p_0_in
    );
\slv_reg7_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => slv_reg7(11),
      R => p_0_in
    );
\slv_reg7_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => slv_reg7(12),
      R => p_0_in
    );
\slv_reg7_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => slv_reg7(13),
      R => p_0_in
    );
\slv_reg7_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => slv_reg7(14),
      R => p_0_in
    );
\slv_reg7_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => slv_reg7(15),
      R => p_0_in
    );
\slv_reg7_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => slv_reg7(16),
      R => p_0_in
    );
\slv_reg7_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => slv_reg7(17),
      R => p_0_in
    );
\slv_reg7_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => slv_reg7(18),
      R => p_0_in
    );
\slv_reg7_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => slv_reg7(19),
      R => p_0_in
    );
\slv_reg7_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => slv_reg7(1),
      R => p_0_in
    );
\slv_reg7_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => slv_reg7(20),
      R => p_0_in
    );
\slv_reg7_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => slv_reg7(21),
      R => p_0_in
    );
\slv_reg7_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => slv_reg7(22),
      R => p_0_in
    );
\slv_reg7_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => slv_reg7(23),
      R => p_0_in
    );
\slv_reg7_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => slv_reg7(24),
      R => p_0_in
    );
\slv_reg7_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => slv_reg7(25),
      R => p_0_in
    );
\slv_reg7_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => slv_reg7(26),
      R => p_0_in
    );
\slv_reg7_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => slv_reg7(27),
      R => p_0_in
    );
\slv_reg7_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => slv_reg7(28),
      R => p_0_in
    );
\slv_reg7_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => slv_reg7(29),
      R => p_0_in
    );
\slv_reg7_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => slv_reg7(2),
      R => p_0_in
    );
\slv_reg7_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => slv_reg7(30),
      R => p_0_in
    );
\slv_reg7_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => slv_reg7(31),
      R => p_0_in
    );
\slv_reg7_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => slv_reg7(3),
      R => p_0_in
    );
\slv_reg7_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => slv_reg7(4),
      R => p_0_in
    );
\slv_reg7_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => slv_reg7(5),
      R => p_0_in
    );
\slv_reg7_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => slv_reg7(6),
      R => p_0_in
    );
\slv_reg7_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => slv_reg7(7),
      R => p_0_in
    );
\slv_reg7_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => slv_reg7(8),
      R => p_0_in
    );
\slv_reg7_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => slv_reg7(9),
      R => p_0_in
    );
\slv_reg8[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(3),
      I2 => \p_0_in__0\(1),
      I3 => \p_0_in__0\(0),
      I4 => \p_0_in__0\(2),
      I5 => s_axi_wstrb(1),
      O => \slv_reg8[15]_i_1_n_0\
    );
\slv_reg8[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(3),
      I2 => \p_0_in__0\(1),
      I3 => \p_0_in__0\(0),
      I4 => \p_0_in__0\(2),
      I5 => s_axi_wstrb(2),
      O => \slv_reg8[23]_i_1_n_0\
    );
\slv_reg8[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(3),
      I2 => \p_0_in__0\(1),
      I3 => \p_0_in__0\(0),
      I4 => \p_0_in__0\(2),
      I5 => s_axi_wstrb(3),
      O => \slv_reg8[31]_i_1_n_0\
    );
\slv_reg8[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(3),
      I2 => \p_0_in__0\(1),
      I3 => \p_0_in__0\(0),
      I4 => \p_0_in__0\(2),
      I5 => s_axi_wstrb(0),
      O => \slv_reg8[7]_i_1_n_0\
    );
\slv_reg8_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => slv_reg8(0),
      R => p_0_in
    );
\slv_reg8_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => slv_reg8(10),
      R => p_0_in
    );
\slv_reg8_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => slv_reg8(11),
      R => p_0_in
    );
\slv_reg8_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => slv_reg8(12),
      R => p_0_in
    );
\slv_reg8_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => slv_reg8(13),
      R => p_0_in
    );
\slv_reg8_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => slv_reg8(14),
      R => p_0_in
    );
\slv_reg8_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => slv_reg8(15),
      R => p_0_in
    );
\slv_reg8_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => slv_reg8(16),
      R => p_0_in
    );
\slv_reg8_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => slv_reg8(17),
      R => p_0_in
    );
\slv_reg8_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => slv_reg8(18),
      R => p_0_in
    );
\slv_reg8_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => slv_reg8(19),
      R => p_0_in
    );
\slv_reg8_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => slv_reg8(1),
      R => p_0_in
    );
\slv_reg8_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => slv_reg8(20),
      R => p_0_in
    );
\slv_reg8_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => slv_reg8(21),
      R => p_0_in
    );
\slv_reg8_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => slv_reg8(22),
      R => p_0_in
    );
\slv_reg8_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => slv_reg8(23),
      R => p_0_in
    );
\slv_reg8_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => slv_reg8(24),
      R => p_0_in
    );
\slv_reg8_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => slv_reg8(25),
      R => p_0_in
    );
\slv_reg8_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => slv_reg8(26),
      R => p_0_in
    );
\slv_reg8_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => slv_reg8(27),
      R => p_0_in
    );
\slv_reg8_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => slv_reg8(28),
      R => p_0_in
    );
\slv_reg8_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => slv_reg8(29),
      R => p_0_in
    );
\slv_reg8_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => slv_reg8(2),
      R => p_0_in
    );
\slv_reg8_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => slv_reg8(30),
      R => p_0_in
    );
\slv_reg8_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => slv_reg8(31),
      R => p_0_in
    );
\slv_reg8_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => slv_reg8(3),
      R => p_0_in
    );
\slv_reg8_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => slv_reg8(4),
      R => p_0_in
    );
\slv_reg8_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => slv_reg8(5),
      R => p_0_in
    );
\slv_reg8_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => slv_reg8(6),
      R => p_0_in
    );
\slv_reg8_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => slv_reg8(7),
      R => p_0_in
    );
\slv_reg8_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => slv_reg8(8),
      R => p_0_in
    );
\slv_reg8_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg8[15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => slv_reg8(9),
      R => p_0_in
    );
\slv_reg9[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(3),
      I2 => \p_0_in__0\(0),
      I3 => s_axi_wstrb(1),
      I4 => \p_0_in__0\(1),
      I5 => \p_0_in__0\(2),
      O => \slv_reg9[15]_i_1_n_0\
    );
\slv_reg9[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(3),
      I2 => \p_0_in__0\(0),
      I3 => s_axi_wstrb(2),
      I4 => \p_0_in__0\(1),
      I5 => \p_0_in__0\(2),
      O => \slv_reg9[23]_i_1_n_0\
    );
\slv_reg9[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(3),
      I2 => \p_0_in__0\(0),
      I3 => s_axi_wstrb(3),
      I4 => \p_0_in__0\(1),
      I5 => \p_0_in__0\(2),
      O => \slv_reg9[31]_i_1_n_0\
    );
\slv_reg9[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => \p_0_in__0\(3),
      I2 => \p_0_in__0\(0),
      I3 => s_axi_wstrb(0),
      I4 => \p_0_in__0\(1),
      I5 => \p_0_in__0\(2),
      O => \slv_reg9[7]_i_1_n_0\
    );
\slv_reg9_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s_axi_wdata(0),
      Q => slv_reg9(0),
      R => p_0_in
    );
\slv_reg9_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s_axi_wdata(10),
      Q => slv_reg9(10),
      R => p_0_in
    );
\slv_reg9_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s_axi_wdata(11),
      Q => slv_reg9(11),
      R => p_0_in
    );
\slv_reg9_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s_axi_wdata(12),
      Q => slv_reg9(12),
      R => p_0_in
    );
\slv_reg9_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s_axi_wdata(13),
      Q => slv_reg9(13),
      R => p_0_in
    );
\slv_reg9_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s_axi_wdata(14),
      Q => slv_reg9(14),
      R => p_0_in
    );
\slv_reg9_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s_axi_wdata(15),
      Q => slv_reg9(15),
      R => p_0_in
    );
\slv_reg9_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s_axi_wdata(16),
      Q => slv_reg9(16),
      R => p_0_in
    );
\slv_reg9_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s_axi_wdata(17),
      Q => slv_reg9(17),
      R => p_0_in
    );
\slv_reg9_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s_axi_wdata(18),
      Q => slv_reg9(18),
      R => p_0_in
    );
\slv_reg9_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s_axi_wdata(19),
      Q => slv_reg9(19),
      R => p_0_in
    );
\slv_reg9_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s_axi_wdata(1),
      Q => slv_reg9(1),
      R => p_0_in
    );
\slv_reg9_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s_axi_wdata(20),
      Q => slv_reg9(20),
      R => p_0_in
    );
\slv_reg9_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s_axi_wdata(21),
      Q => slv_reg9(21),
      R => p_0_in
    );
\slv_reg9_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s_axi_wdata(22),
      Q => slv_reg9(22),
      R => p_0_in
    );
\slv_reg9_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[23]_i_1_n_0\,
      D => s_axi_wdata(23),
      Q => slv_reg9(23),
      R => p_0_in
    );
\slv_reg9_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s_axi_wdata(24),
      Q => slv_reg9(24),
      R => p_0_in
    );
\slv_reg9_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s_axi_wdata(25),
      Q => slv_reg9(25),
      R => p_0_in
    );
\slv_reg9_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s_axi_wdata(26),
      Q => slv_reg9(26),
      R => p_0_in
    );
\slv_reg9_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s_axi_wdata(27),
      Q => slv_reg9(27),
      R => p_0_in
    );
\slv_reg9_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s_axi_wdata(28),
      Q => slv_reg9(28),
      R => p_0_in
    );
\slv_reg9_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s_axi_wdata(29),
      Q => slv_reg9(29),
      R => p_0_in
    );
\slv_reg9_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s_axi_wdata(2),
      Q => slv_reg9(2),
      R => p_0_in
    );
\slv_reg9_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s_axi_wdata(30),
      Q => slv_reg9(30),
      R => p_0_in
    );
\slv_reg9_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[31]_i_1_n_0\,
      D => s_axi_wdata(31),
      Q => slv_reg9(31),
      R => p_0_in
    );
\slv_reg9_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s_axi_wdata(3),
      Q => slv_reg9(3),
      R => p_0_in
    );
\slv_reg9_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s_axi_wdata(4),
      Q => slv_reg9(4),
      R => p_0_in
    );
\slv_reg9_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s_axi_wdata(5),
      Q => slv_reg9(5),
      R => p_0_in
    );
\slv_reg9_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s_axi_wdata(6),
      Q => slv_reg9(6),
      R => p_0_in
    );
\slv_reg9_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[7]_i_1_n_0\,
      D => s_axi_wdata(7),
      Q => slv_reg9(7),
      R => p_0_in
    );
\slv_reg9_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s_axi_wdata(8),
      Q => slv_reg9(8),
      R => p_0_in
    );
\slv_reg9_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \slv_reg9[15]_i_1_n_0\,
      D => s_axi_wdata(9),
      Q => slv_reg9(9),
      R => p_0_in
    );
\x_mod_0_i[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000080"
    )
        port map (
      I0 => axi_wvalid_falling_edge,
      I1 => \p_0_in__0\(3),
      I2 => \p_0_in__0\(0),
      I3 => \p_0_in__0\(2),
      I4 => \p_0_in__0\(1),
      O => x_mod_0_i0
    );
\x_mod_0_i_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg0(10),
      Q => \^x_mod_0\(9),
      R => external_reset_i
    );
\x_mod_0_i_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg0(11),
      Q => \^x_mod_0\(10),
      R => external_reset_i
    );
\x_mod_0_i_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg0(12),
      Q => \^x_mod_0\(11),
      R => external_reset_i
    );
\x_mod_0_i_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg0(13),
      Q => \^x_mod_0\(12),
      R => external_reset_i
    );
\x_mod_0_i_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg0(1),
      Q => \^x_mod_0\(0),
      R => external_reset_i
    );
\x_mod_0_i_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg0(2),
      Q => \^x_mod_0\(1),
      R => external_reset_i
    );
\x_mod_0_i_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg0(3),
      Q => \^x_mod_0\(2),
      R => external_reset_i
    );
\x_mod_0_i_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg0(4),
      Q => \^x_mod_0\(3),
      R => external_reset_i
    );
\x_mod_0_i_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg0(5),
      Q => \^x_mod_0\(4),
      R => external_reset_i
    );
\x_mod_0_i_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg0(6),
      Q => \^x_mod_0\(5),
      R => external_reset_i
    );
\x_mod_0_i_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg0(7),
      Q => \^x_mod_0\(6),
      R => external_reset_i
    );
\x_mod_0_i_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg0(8),
      Q => \^x_mod_0\(7),
      R => external_reset_i
    );
\x_mod_0_i_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg0(9),
      Q => \^x_mod_0\(8),
      R => external_reset_i
    );
\x_mod_1_i_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg1(10),
      Q => \^x_mod_1\(9),
      R => external_reset_i
    );
\x_mod_1_i_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg1(11),
      Q => \^x_mod_1\(10),
      R => external_reset_i
    );
\x_mod_1_i_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg1(12),
      Q => \^x_mod_1\(11),
      R => external_reset_i
    );
\x_mod_1_i_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg1(13),
      Q => \^x_mod_1\(12),
      R => external_reset_i
    );
\x_mod_1_i_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg1(1),
      Q => \^x_mod_1\(0),
      R => external_reset_i
    );
\x_mod_1_i_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg1(2),
      Q => \^x_mod_1\(1),
      R => external_reset_i
    );
\x_mod_1_i_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg1(3),
      Q => \^x_mod_1\(2),
      R => external_reset_i
    );
\x_mod_1_i_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg1(4),
      Q => \^x_mod_1\(3),
      R => external_reset_i
    );
\x_mod_1_i_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg1(5),
      Q => \^x_mod_1\(4),
      R => external_reset_i
    );
\x_mod_1_i_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg1(6),
      Q => \^x_mod_1\(5),
      R => external_reset_i
    );
\x_mod_1_i_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg1(7),
      Q => \^x_mod_1\(6),
      R => external_reset_i
    );
\x_mod_1_i_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg1(8),
      Q => \^x_mod_1\(7),
      R => external_reset_i
    );
\x_mod_1_i_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg1(9),
      Q => \^x_mod_1\(8),
      R => external_reset_i
    );
\x_mod_2_i_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg2(10),
      Q => \^x_mod_2\(9),
      R => external_reset_i
    );
\x_mod_2_i_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg2(11),
      Q => \^x_mod_2\(10),
      R => external_reset_i
    );
\x_mod_2_i_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg2(12),
      Q => \^x_mod_2\(11),
      R => external_reset_i
    );
\x_mod_2_i_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg2(13),
      Q => \^x_mod_2\(12),
      R => external_reset_i
    );
\x_mod_2_i_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg2(1),
      Q => \^x_mod_2\(0),
      R => external_reset_i
    );
\x_mod_2_i_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg2(2),
      Q => \^x_mod_2\(1),
      R => external_reset_i
    );
\x_mod_2_i_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg2(3),
      Q => \^x_mod_2\(2),
      R => external_reset_i
    );
\x_mod_2_i_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg2(4),
      Q => \^x_mod_2\(3),
      R => external_reset_i
    );
\x_mod_2_i_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg2(5),
      Q => \^x_mod_2\(4),
      R => external_reset_i
    );
\x_mod_2_i_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg2(6),
      Q => \^x_mod_2\(5),
      R => external_reset_i
    );
\x_mod_2_i_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg2(7),
      Q => \^x_mod_2\(6),
      R => external_reset_i
    );
\x_mod_2_i_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg2(8),
      Q => \^x_mod_2\(7),
      R => external_reset_i
    );
\x_mod_2_i_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg2(9),
      Q => \^x_mod_2\(8),
      R => external_reset_i
    );
\x_mod_3_i_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg3(10),
      Q => \^x_mod_3\(9),
      R => external_reset_i
    );
\x_mod_3_i_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg3(11),
      Q => \^x_mod_3\(10),
      R => external_reset_i
    );
\x_mod_3_i_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg3(12),
      Q => \^x_mod_3\(11),
      R => external_reset_i
    );
\x_mod_3_i_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg3(13),
      Q => \^x_mod_3\(12),
      R => external_reset_i
    );
\x_mod_3_i_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg3(1),
      Q => \^x_mod_3\(0),
      R => external_reset_i
    );
\x_mod_3_i_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg3(2),
      Q => \^x_mod_3\(1),
      R => external_reset_i
    );
\x_mod_3_i_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg3(3),
      Q => \^x_mod_3\(2),
      R => external_reset_i
    );
\x_mod_3_i_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg3(4),
      Q => \^x_mod_3\(3),
      R => external_reset_i
    );
\x_mod_3_i_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg3(5),
      Q => \^x_mod_3\(4),
      R => external_reset_i
    );
\x_mod_3_i_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg3(6),
      Q => \^x_mod_3\(5),
      R => external_reset_i
    );
\x_mod_3_i_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg3(7),
      Q => \^x_mod_3\(6),
      R => external_reset_i
    );
\x_mod_3_i_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg3(8),
      Q => \^x_mod_3\(7),
      R => external_reset_i
    );
\x_mod_3_i_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg3(9),
      Q => \^x_mod_3\(8),
      R => external_reset_i
    );
\x_mod_4_i_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg4(10),
      Q => \^x_mod_4\(9),
      R => external_reset_i
    );
\x_mod_4_i_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg4(11),
      Q => \^x_mod_4\(10),
      R => external_reset_i
    );
\x_mod_4_i_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg4(12),
      Q => \^x_mod_4\(11),
      R => external_reset_i
    );
\x_mod_4_i_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg4(13),
      Q => \^x_mod_4\(12),
      R => external_reset_i
    );
\x_mod_4_i_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg4(1),
      Q => \^x_mod_4\(0),
      R => external_reset_i
    );
\x_mod_4_i_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg4(2),
      Q => \^x_mod_4\(1),
      R => external_reset_i
    );
\x_mod_4_i_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg4(3),
      Q => \^x_mod_4\(2),
      R => external_reset_i
    );
\x_mod_4_i_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg4(4),
      Q => \^x_mod_4\(3),
      R => external_reset_i
    );
\x_mod_4_i_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg4(5),
      Q => \^x_mod_4\(4),
      R => external_reset_i
    );
\x_mod_4_i_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg4(6),
      Q => \^x_mod_4\(5),
      R => external_reset_i
    );
\x_mod_4_i_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg4(7),
      Q => \^x_mod_4\(6),
      R => external_reset_i
    );
\x_mod_4_i_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg4(8),
      Q => \^x_mod_4\(7),
      R => external_reset_i
    );
\x_mod_4_i_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg4(9),
      Q => \^x_mod_4\(8),
      R => external_reset_i
    );
\x_mod_5_i_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg5(10),
      Q => \^x_mod_5\(9),
      R => external_reset_i
    );
\x_mod_5_i_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg5(11),
      Q => \^x_mod_5\(10),
      R => external_reset_i
    );
\x_mod_5_i_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg5(12),
      Q => \^x_mod_5\(11),
      R => external_reset_i
    );
\x_mod_5_i_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg5(13),
      Q => \^x_mod_5\(12),
      R => external_reset_i
    );
\x_mod_5_i_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg5(1),
      Q => \^x_mod_5\(0),
      R => external_reset_i
    );
\x_mod_5_i_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg5(2),
      Q => \^x_mod_5\(1),
      R => external_reset_i
    );
\x_mod_5_i_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg5(3),
      Q => \^x_mod_5\(2),
      R => external_reset_i
    );
\x_mod_5_i_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg5(4),
      Q => \^x_mod_5\(3),
      R => external_reset_i
    );
\x_mod_5_i_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg5(5),
      Q => \^x_mod_5\(4),
      R => external_reset_i
    );
\x_mod_5_i_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg5(6),
      Q => \^x_mod_5\(5),
      R => external_reset_i
    );
\x_mod_5_i_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg5(7),
      Q => \^x_mod_5\(6),
      R => external_reset_i
    );
\x_mod_5_i_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg5(8),
      Q => \^x_mod_5\(7),
      R => external_reset_i
    );
\x_mod_5_i_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg5(9),
      Q => \^x_mod_5\(8),
      R => external_reset_i
    );
\x_mod_6_i_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg6(10),
      Q => \^x_mod_6\(9),
      R => external_reset_i
    );
\x_mod_6_i_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg6(11),
      Q => \^x_mod_6\(10),
      R => external_reset_i
    );
\x_mod_6_i_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg6(12),
      Q => \^x_mod_6\(11),
      R => external_reset_i
    );
\x_mod_6_i_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg6(13),
      Q => \^x_mod_6\(12),
      R => external_reset_i
    );
\x_mod_6_i_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg6(1),
      Q => \^x_mod_6\(0),
      R => external_reset_i
    );
\x_mod_6_i_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg6(2),
      Q => \^x_mod_6\(1),
      R => external_reset_i
    );
\x_mod_6_i_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg6(3),
      Q => \^x_mod_6\(2),
      R => external_reset_i
    );
\x_mod_6_i_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg6(4),
      Q => \^x_mod_6\(3),
      R => external_reset_i
    );
\x_mod_6_i_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg6(5),
      Q => \^x_mod_6\(4),
      R => external_reset_i
    );
\x_mod_6_i_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg6(6),
      Q => \^x_mod_6\(5),
      R => external_reset_i
    );
\x_mod_6_i_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg6(7),
      Q => \^x_mod_6\(6),
      R => external_reset_i
    );
\x_mod_6_i_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg6(8),
      Q => \^x_mod_6\(7),
      R => external_reset_i
    );
\x_mod_6_i_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg6(9),
      Q => \^x_mod_6\(8),
      R => external_reset_i
    );
\x_mod_7_i_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg7(10),
      Q => \^x_mod_7\(9),
      R => external_reset_i
    );
\x_mod_7_i_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg7(11),
      Q => \^x_mod_7\(10),
      R => external_reset_i
    );
\x_mod_7_i_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg7(12),
      Q => \^x_mod_7\(11),
      R => external_reset_i
    );
\x_mod_7_i_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg7(13),
      Q => \^x_mod_7\(12),
      R => external_reset_i
    );
\x_mod_7_i_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg7(1),
      Q => \^x_mod_7\(0),
      R => external_reset_i
    );
\x_mod_7_i_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg7(2),
      Q => \^x_mod_7\(1),
      R => external_reset_i
    );
\x_mod_7_i_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg7(3),
      Q => \^x_mod_7\(2),
      R => external_reset_i
    );
\x_mod_7_i_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg7(4),
      Q => \^x_mod_7\(3),
      R => external_reset_i
    );
\x_mod_7_i_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg7(5),
      Q => \^x_mod_7\(4),
      R => external_reset_i
    );
\x_mod_7_i_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg7(6),
      Q => \^x_mod_7\(5),
      R => external_reset_i
    );
\x_mod_7_i_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg7(7),
      Q => \^x_mod_7\(6),
      R => external_reset_i
    );
\x_mod_7_i_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg7(8),
      Q => \^x_mod_7\(7),
      R => external_reset_i
    );
\x_mod_7_i_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg7(9),
      Q => \^x_mod_7\(8),
      R => external_reset_i
    );
\x_mod_8_i_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg8(10),
      Q => \^x_mod_8\(9),
      R => external_reset_i
    );
\x_mod_8_i_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg8(11),
      Q => \^x_mod_8\(10),
      R => external_reset_i
    );
\x_mod_8_i_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg8(12),
      Q => \^x_mod_8\(11),
      R => external_reset_i
    );
\x_mod_8_i_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg8(13),
      Q => \^x_mod_8\(12),
      R => external_reset_i
    );
\x_mod_8_i_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg8(1),
      Q => \^x_mod_8\(0),
      R => external_reset_i
    );
\x_mod_8_i_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg8(2),
      Q => \^x_mod_8\(1),
      R => external_reset_i
    );
\x_mod_8_i_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg8(3),
      Q => \^x_mod_8\(2),
      R => external_reset_i
    );
\x_mod_8_i_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg8(4),
      Q => \^x_mod_8\(3),
      R => external_reset_i
    );
\x_mod_8_i_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg8(5),
      Q => \^x_mod_8\(4),
      R => external_reset_i
    );
\x_mod_8_i_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg8(6),
      Q => \^x_mod_8\(5),
      R => external_reset_i
    );
\x_mod_8_i_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg8(7),
      Q => \^x_mod_8\(6),
      R => external_reset_i
    );
\x_mod_8_i_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg8(8),
      Q => \^x_mod_8\(7),
      R => external_reset_i
    );
\x_mod_8_i_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg8(9),
      Q => \^x_mod_8\(8),
      R => external_reset_i
    );
\x_mod_9_i_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg9(10),
      Q => \^x_mod_9\(9),
      R => external_reset_i
    );
\x_mod_9_i_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg9(11),
      Q => \^x_mod_9\(10),
      R => external_reset_i
    );
\x_mod_9_i_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg9(12),
      Q => \^x_mod_9\(11),
      R => external_reset_i
    );
\x_mod_9_i_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg9(13),
      Q => \^x_mod_9\(12),
      R => external_reset_i
    );
\x_mod_9_i_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg9(1),
      Q => \^x_mod_9\(0),
      R => external_reset_i
    );
\x_mod_9_i_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg9(2),
      Q => \^x_mod_9\(1),
      R => external_reset_i
    );
\x_mod_9_i_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg9(3),
      Q => \^x_mod_9\(2),
      R => external_reset_i
    );
\x_mod_9_i_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg9(4),
      Q => \^x_mod_9\(3),
      R => external_reset_i
    );
\x_mod_9_i_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg9(5),
      Q => \^x_mod_9\(4),
      R => external_reset_i
    );
\x_mod_9_i_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg9(6),
      Q => \^x_mod_9\(5),
      R => external_reset_i
    );
\x_mod_9_i_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg9(7),
      Q => \^x_mod_9\(6),
      R => external_reset_i
    );
\x_mod_9_i_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg9(8),
      Q => \^x_mod_9\(7),
      R => external_reset_i
    );
\x_mod_9_i_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => s_axi_aclk,
      CE => x_mod_0_i0,
      D => slv_reg9(9),
      Q => \^x_mod_9\(8),
      R => external_reset_i
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_pwm_x40_ip_0_0_pwm_x40_ip_v1_0 is
  port (
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    PWM_A_P : out STD_LOGIC_VECTOR ( 9 downto 0 );
    PWM_B_P : out STD_LOGIC_VECTOR ( 9 downto 0 );
    PWM_A_N : out STD_LOGIC_VECTOR ( 9 downto 0 );
    PWM_B_N : out STD_LOGIC_VECTOR ( 9 downto 0 );
    s_axi_aclk : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    CLK_100MHZ : in STD_LOGIC;
    EXTERNAL_RESET : in STD_LOGIC
  );
end system_pwm_x40_ip_0_0_pwm_x40_ip_v1_0;

architecture STRUCTURE of system_pwm_x40_ip_0_0_pwm_x40_ip_v1_0 is
  signal X_MOD_0 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal X_MOD_1 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal X_MOD_2 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal X_MOD_3 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal X_MOD_4 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal X_MOD_5 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal X_MOD_6 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal X_MOD_7 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal X_MOD_8 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal X_MOD_9 : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal counter_phase0_n_13 : STD_LOGIC;
  signal counter_phase0_n_14 : STD_LOGIC;
  signal counter_phase0_n_15 : STD_LOGIC;
  signal counter_phase0_n_16 : STD_LOGIC;
  signal counter_phase0_n_17 : STD_LOGIC;
  signal counter_phase0_n_18 : STD_LOGIC;
  signal counter_phase0_n_19 : STD_LOGIC;
  signal counter_phase0_n_20 : STD_LOGIC;
  signal counter_phase0_n_21 : STD_LOGIC;
  signal counter_phase0_n_22 : STD_LOGIC;
  signal counter_phase0_n_23 : STD_LOGIC;
  signal counter_phase0_n_24 : STD_LOGIC;
  signal counter_phase0_n_25 : STD_LOGIC;
  signal counter_phase0_n_26 : STD_LOGIC;
  signal counter_phase0_n_27 : STD_LOGIC;
  signal counter_phase0_n_28 : STD_LOGIC;
  signal counter_phase0_n_29 : STD_LOGIC;
  signal counter_phase0_n_30 : STD_LOGIC;
  signal counter_phase0_n_31 : STD_LOGIC;
  signal counter_phase0_n_32 : STD_LOGIC;
  signal counter_phase0_n_33 : STD_LOGIC;
  signal counter_phase0_n_34 : STD_LOGIC;
  signal counter_phase0_n_35 : STD_LOGIC;
  signal counter_phase0_n_36 : STD_LOGIC;
  signal counter_phase0_n_37 : STD_LOGIC;
  signal counter_phase0_n_38 : STD_LOGIC;
  signal counter_phase0_n_39 : STD_LOGIC;
  signal counter_phase0_n_40 : STD_LOGIC;
  signal counter_phase0_n_41 : STD_LOGIC;
  signal counter_phase0_n_42 : STD_LOGIC;
  signal counter_phase0_n_43 : STD_LOGIC;
  signal counter_phase0_n_45 : STD_LOGIC;
  signal counter_phase0_n_46 : STD_LOGIC;
  signal counter_phase0_n_47 : STD_LOGIC;
  signal counter_phase0_n_48 : STD_LOGIC;
  signal counter_phase180_dir_i_1_n_0 : STD_LOGIC;
  signal counter_phase180_dir_reg_n_0 : STD_LOGIC;
  signal counter_phase180_n_13 : STD_LOGIC;
  signal counter_phase180_n_14 : STD_LOGIC;
  signal counter_phase180_n_15 : STD_LOGIC;
  signal counter_phase180_n_16 : STD_LOGIC;
  signal counter_phase180_n_17 : STD_LOGIC;
  signal counter_phase180_n_18 : STD_LOGIC;
  signal counter_phase180_n_19 : STD_LOGIC;
  signal counter_phase180_n_20 : STD_LOGIC;
  signal counter_phase180_n_21 : STD_LOGIC;
  signal counter_phase180_n_22 : STD_LOGIC;
  signal counter_phase180_n_23 : STD_LOGIC;
  signal counter_phase180_n_24 : STD_LOGIC;
  signal counter_phase180_n_25 : STD_LOGIC;
  signal counter_phase180_n_26 : STD_LOGIC;
  signal counter_phase180_n_27 : STD_LOGIC;
  signal counter_phase180_n_28 : STD_LOGIC;
  signal counter_phase180_n_29 : STD_LOGIC;
  signal counter_phase180_n_30 : STD_LOGIC;
  signal counter_phase180_n_31 : STD_LOGIC;
  signal counter_phase180_n_32 : STD_LOGIC;
  signal counter_phase180_n_33 : STD_LOGIC;
  signal counter_phase180_n_34 : STD_LOGIC;
  signal counter_phase180_n_35 : STD_LOGIC;
  signal counter_phase180_n_36 : STD_LOGIC;
  signal counter_phase180_n_37 : STD_LOGIC;
  signal counter_phase180_n_38 : STD_LOGIC;
  signal counter_phase180_n_39 : STD_LOGIC;
  signal counter_phase180_n_40 : STD_LOGIC;
  signal counter_phase180_n_41 : STD_LOGIC;
  signal counter_phase180_n_42 : STD_LOGIC;
  signal counter_phase180_n_43 : STD_LOGIC;
  signal counter_phase180_n_44 : STD_LOGIC;
  signal counter_phase180_n_45 : STD_LOGIC;
  signal counter_phase180_n_46 : STD_LOGIC;
  signal counter_phase180_n_47 : STD_LOGIC;
  signal counter_phase180_output : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal counter_phase180_tc : STD_LOGIC;
  signal counter_phase270_dir_i_1_n_0 : STD_LOGIC;
  signal counter_phase270_dir_reg_n_0 : STD_LOGIC;
  signal counter_phase270_n_13 : STD_LOGIC;
  signal counter_phase270_n_14 : STD_LOGIC;
  signal counter_phase270_n_15 : STD_LOGIC;
  signal counter_phase270_n_16 : STD_LOGIC;
  signal counter_phase270_n_17 : STD_LOGIC;
  signal counter_phase270_n_18 : STD_LOGIC;
  signal counter_phase270_n_19 : STD_LOGIC;
  signal counter_phase270_n_20 : STD_LOGIC;
  signal counter_phase270_n_21 : STD_LOGIC;
  signal counter_phase270_n_22 : STD_LOGIC;
  signal counter_phase270_n_23 : STD_LOGIC;
  signal counter_phase270_n_24 : STD_LOGIC;
  signal counter_phase270_n_25 : STD_LOGIC;
  signal counter_phase270_n_26 : STD_LOGIC;
  signal counter_phase270_n_27 : STD_LOGIC;
  signal counter_phase270_n_28 : STD_LOGIC;
  signal counter_phase270_n_29 : STD_LOGIC;
  signal counter_phase270_n_30 : STD_LOGIC;
  signal counter_phase270_n_31 : STD_LOGIC;
  signal counter_phase270_n_32 : STD_LOGIC;
  signal counter_phase270_n_33 : STD_LOGIC;
  signal counter_phase270_n_34 : STD_LOGIC;
  signal counter_phase270_n_35 : STD_LOGIC;
  signal counter_phase270_n_36 : STD_LOGIC;
  signal counter_phase270_n_37 : STD_LOGIC;
  signal counter_phase270_n_38 : STD_LOGIC;
  signal counter_phase270_n_39 : STD_LOGIC;
  signal counter_phase270_n_40 : STD_LOGIC;
  signal counter_phase270_n_41 : STD_LOGIC;
  signal counter_phase270_n_42 : STD_LOGIC;
  signal counter_phase270_n_43 : STD_LOGIC;
  signal counter_phase270_n_44 : STD_LOGIC;
  signal counter_phase270_n_45 : STD_LOGIC;
  signal counter_phase270_n_46 : STD_LOGIC;
  signal counter_phase270_n_47 : STD_LOGIC;
  signal counter_phase270_output : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal counter_phase270_tc : STD_LOGIC;
  signal counter_phase90_dir_i_1_n_0 : STD_LOGIC;
  signal counter_phase90_dir_reg_n_0 : STD_LOGIC;
  signal counter_phase90_n_13 : STD_LOGIC;
  signal counter_phase90_n_14 : STD_LOGIC;
  signal counter_phase90_n_15 : STD_LOGIC;
  signal counter_phase90_n_16 : STD_LOGIC;
  signal counter_phase90_n_17 : STD_LOGIC;
  signal counter_phase90_n_18 : STD_LOGIC;
  signal counter_phase90_n_19 : STD_LOGIC;
  signal counter_phase90_n_20 : STD_LOGIC;
  signal counter_phase90_n_21 : STD_LOGIC;
  signal counter_phase90_n_22 : STD_LOGIC;
  signal counter_phase90_n_23 : STD_LOGIC;
  signal counter_phase90_n_24 : STD_LOGIC;
  signal counter_phase90_n_25 : STD_LOGIC;
  signal counter_phase90_n_26 : STD_LOGIC;
  signal counter_phase90_n_27 : STD_LOGIC;
  signal counter_phase90_n_28 : STD_LOGIC;
  signal counter_phase90_n_29 : STD_LOGIC;
  signal counter_phase90_n_30 : STD_LOGIC;
  signal counter_phase90_n_31 : STD_LOGIC;
  signal counter_phase90_n_32 : STD_LOGIC;
  signal counter_phase90_n_33 : STD_LOGIC;
  signal counter_phase90_n_34 : STD_LOGIC;
  signal counter_phase90_n_35 : STD_LOGIC;
  signal counter_phase90_n_36 : STD_LOGIC;
  signal counter_phase90_n_37 : STD_LOGIC;
  signal counter_phase90_n_38 : STD_LOGIC;
  signal counter_phase90_n_39 : STD_LOGIC;
  signal counter_phase90_n_40 : STD_LOGIC;
  signal counter_phase90_n_41 : STD_LOGIC;
  signal counter_phase90_n_42 : STD_LOGIC;
  signal counter_phase90_n_43 : STD_LOGIC;
  signal counter_phase90_n_45 : STD_LOGIC;
  signal counter_phase90_n_46 : STD_LOGIC;
  signal counter_phase90_n_47 : STD_LOGIC;
  signal counter_phase90_n_48 : STD_LOGIC;
  signal counter_phase90_output : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal counter_phase90_tc : STD_LOGIC;
  signal dir : STD_LOGIC;
  signal external_reset_i : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal pwm_a_p_i : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \pwm_a_p_i0_carry__0_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_carry__0_n_3\ : STD_LOGIC;
  signal pwm_a_p_i0_carry_n_0 : STD_LOGIC;
  signal pwm_a_p_i0_carry_n_1 : STD_LOGIC;
  signal pwm_a_p_i0_carry_n_2 : STD_LOGIC;
  signal pwm_a_p_i0_carry_n_3 : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__1/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__1/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__1/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__1/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__1/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__1/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__2/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__2/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__2/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__2/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__2/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__2/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__3/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__3/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__3/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__3/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__3/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__3/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__4/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__4/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__4/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__4/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__4/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__4/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__5/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__5/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__5/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__5/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__5/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__5/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__6/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__6/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__6/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__6/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__6/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__6/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__7/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__7/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__7/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__7/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__7/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__7/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__8/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__8/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__8/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__8/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__8/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_a_p_i0_inferred__8/i__carry_n_3\ : STD_LOGIC;
  signal pwm_b_p_i : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \pwm_b_p_i0_carry__0_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_carry__0_n_3\ : STD_LOGIC;
  signal pwm_b_p_i0_carry_n_0 : STD_LOGIC;
  signal pwm_b_p_i0_carry_n_1 : STD_LOGIC;
  signal pwm_b_p_i0_carry_n_2 : STD_LOGIC;
  signal pwm_b_p_i0_carry_n_3 : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__1/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__1/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__1/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__1/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__1/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__1/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__2/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__2/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__2/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__2/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__2/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__2/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__3/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__3/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__3/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__3/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__3/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__3/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__4/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__4/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__4/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__4/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__4/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__4/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__5/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__5/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__5/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__5/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__5/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__5/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__6/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__6/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__6/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__6/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__6/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__6/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__7/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__7/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__7/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__7/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__7/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__7/i__carry_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__8/i__carry__0_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__8/i__carry__0_n_3\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__8/i__carry_n_0\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__8/i__carry_n_1\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__8/i__carry_n_2\ : STD_LOGIC;
  signal \pwm_b_p_i0_inferred__8/i__carry_n_3\ : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_0 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_1 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_100 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_101 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_102 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_103 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_117 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_118 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_119 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_120 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_121 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_122 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_123 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_137 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_138 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_139 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_140 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_141 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_142 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_143 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_157 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_158 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_159 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_160 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_161 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_162 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_163 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_17 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_177 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_178 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_179 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_18 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_180 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_181 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_182 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_183 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_19 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_197 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_198 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_199 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_2 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_20 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_200 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_201 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_202 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_203 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_204 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_205 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_206 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_207 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_208 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_209 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_21 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_210 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_211 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_212 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_213 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_214 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_215 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_216 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_217 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_218 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_219 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_22 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_220 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_221 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_222 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_223 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_224 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_225 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_226 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_227 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_228 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_229 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_23 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_230 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_231 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_232 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_233 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_234 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_235 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_236 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_237 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_238 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_239 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_240 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_241 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_242 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_243 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_244 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_245 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_246 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_247 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_248 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_249 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_250 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_251 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_252 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_253 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_254 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_255 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_256 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_257 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_258 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_259 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_260 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_261 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_262 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_263 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_264 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_265 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_266 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_267 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_268 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_269 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_3 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_37 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_38 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_39 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_40 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_41 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_42 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_43 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_57 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_58 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_59 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_60 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_61 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_62 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_63 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_77 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_78 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_79 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_80 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_81 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_82 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_83 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_97 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_98 : STD_LOGIC;
  signal pwm_x40_ip_v1_0_S_AXI_inst_n_99 : STD_LOGIC;
  signal q : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal tc : STD_LOGIC;
  signal NLW_pwm_a_p_i0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_a_p_i0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__0/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_a_p_i0_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__1/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__1/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_a_p_i0_inferred__1/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__2/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__2/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_a_p_i0_inferred__2/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__3/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__3/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_a_p_i0_inferred__3/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__4/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__4/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_a_p_i0_inferred__4/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__5/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__5/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_a_p_i0_inferred__5/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__6/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__6/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_a_p_i0_inferred__6/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__7/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__7/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_a_p_i0_inferred__7/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__8/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_a_p_i0_inferred__8/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_a_p_i0_inferred__8/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pwm_b_p_i0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_b_p_i0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__0/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_b_p_i0_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__1/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__1/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_b_p_i0_inferred__1/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__2/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__2/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_b_p_i0_inferred__2/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__3/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__3/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_b_p_i0_inferred__3/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__4/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__4/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_b_p_i0_inferred__4/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__5/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__5/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_b_p_i0_inferred__5/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__6/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__6/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_b_p_i0_inferred__6/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__7/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__7/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_b_p_i0_inferred__7/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__8/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_pwm_b_p_i0_inferred__8/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_pwm_b_p_i0_inferred__8/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \PWM_A_N[0]_INST_0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \PWM_A_N[1]_INST_0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \PWM_A_N[2]_INST_0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \PWM_A_N[3]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \PWM_A_N[4]_INST_0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \PWM_A_N[5]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \PWM_A_N[6]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \PWM_A_N[7]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \PWM_A_N[8]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \PWM_A_N[9]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \PWM_A_P[0]_INST_0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \PWM_A_P[1]_INST_0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \PWM_A_P[2]_INST_0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \PWM_A_P[3]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \PWM_A_P[4]_INST_0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \PWM_A_P[5]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \PWM_A_P[6]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \PWM_A_P[7]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \PWM_A_P[8]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \PWM_A_P[9]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \PWM_B_N[0]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \PWM_B_N[1]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \PWM_B_N[2]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \PWM_B_N[3]_INST_0\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \PWM_B_N[4]_INST_0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \PWM_B_N[5]_INST_0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \PWM_B_N[6]_INST_0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \PWM_B_N[7]_INST_0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \PWM_B_N[8]_INST_0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \PWM_B_N[9]_INST_0\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \PWM_B_P[0]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \PWM_B_P[1]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \PWM_B_P[2]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \PWM_B_P[3]_INST_0\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \PWM_B_P[4]_INST_0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \PWM_B_P[5]_INST_0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \PWM_B_P[6]_INST_0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \PWM_B_P[7]_INST_0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \PWM_B_P[8]_INST_0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \PWM_B_P[9]_INST_0\ : label is "soft_lutpair21";
begin
\PWM_A_N[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_a_p_i(0),
      O => PWM_A_N(0)
    );
\PWM_A_N[1]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_a_p_i(1),
      O => PWM_A_N(1)
    );
\PWM_A_N[2]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_a_p_i(2),
      O => PWM_A_N(2)
    );
\PWM_A_N[3]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_a_p_i(3),
      O => PWM_A_N(3)
    );
\PWM_A_N[4]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_a_p_i(4),
      O => PWM_A_N(4)
    );
\PWM_A_N[5]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_a_p_i(5),
      O => PWM_A_N(5)
    );
\PWM_A_N[6]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_a_p_i(6),
      O => PWM_A_N(6)
    );
\PWM_A_N[7]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_a_p_i(7),
      O => PWM_A_N(7)
    );
\PWM_A_N[8]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_a_p_i(8),
      O => PWM_A_N(8)
    );
\PWM_A_N[9]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_a_p_i(9),
      O => PWM_A_N(9)
    );
\PWM_A_P[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_a_p_i(0),
      I1 => EXTERNAL_RESET,
      O => PWM_A_P(0)
    );
\PWM_A_P[1]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_a_p_i(1),
      I1 => EXTERNAL_RESET,
      O => PWM_A_P(1)
    );
\PWM_A_P[2]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_a_p_i(2),
      I1 => EXTERNAL_RESET,
      O => PWM_A_P(2)
    );
\PWM_A_P[3]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_a_p_i(3),
      I1 => EXTERNAL_RESET,
      O => PWM_A_P(3)
    );
\PWM_A_P[4]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_a_p_i(4),
      I1 => EXTERNAL_RESET,
      O => PWM_A_P(4)
    );
\PWM_A_P[5]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_a_p_i(5),
      I1 => EXTERNAL_RESET,
      O => PWM_A_P(5)
    );
\PWM_A_P[6]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_a_p_i(6),
      I1 => EXTERNAL_RESET,
      O => PWM_A_P(6)
    );
\PWM_A_P[7]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_a_p_i(7),
      I1 => EXTERNAL_RESET,
      O => PWM_A_P(7)
    );
\PWM_A_P[8]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_a_p_i(8),
      I1 => EXTERNAL_RESET,
      O => PWM_A_P(8)
    );
\PWM_A_P[9]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_a_p_i(9),
      I1 => EXTERNAL_RESET,
      O => PWM_A_P(9)
    );
\PWM_B_N[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_b_p_i(0),
      O => PWM_B_N(0)
    );
\PWM_B_N[1]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_b_p_i(1),
      O => PWM_B_N(1)
    );
\PWM_B_N[2]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_b_p_i(2),
      O => PWM_B_N(2)
    );
\PWM_B_N[3]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_b_p_i(3),
      O => PWM_B_N(3)
    );
\PWM_B_N[4]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_b_p_i(4),
      O => PWM_B_N(4)
    );
\PWM_B_N[5]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_b_p_i(5),
      O => PWM_B_N(5)
    );
\PWM_B_N[6]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_b_p_i(6),
      O => PWM_B_N(6)
    );
\PWM_B_N[7]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_b_p_i(7),
      O => PWM_B_N(7)
    );
\PWM_B_N[8]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_b_p_i(8),
      O => PWM_B_N(8)
    );
\PWM_B_N[9]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => EXTERNAL_RESET,
      I1 => pwm_b_p_i(9),
      O => PWM_B_N(9)
    );
\PWM_B_P[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_b_p_i(0),
      I1 => EXTERNAL_RESET,
      O => PWM_B_P(0)
    );
\PWM_B_P[1]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_b_p_i(1),
      I1 => EXTERNAL_RESET,
      O => PWM_B_P(1)
    );
\PWM_B_P[2]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_b_p_i(2),
      I1 => EXTERNAL_RESET,
      O => PWM_B_P(2)
    );
\PWM_B_P[3]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_b_p_i(3),
      I1 => EXTERNAL_RESET,
      O => PWM_B_P(3)
    );
\PWM_B_P[4]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_b_p_i(4),
      I1 => EXTERNAL_RESET,
      O => PWM_B_P(4)
    );
\PWM_B_P[5]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_b_p_i(5),
      I1 => EXTERNAL_RESET,
      O => PWM_B_P(5)
    );
\PWM_B_P[6]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_b_p_i(6),
      I1 => EXTERNAL_RESET,
      O => PWM_B_P(6)
    );
\PWM_B_P[7]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_b_p_i(7),
      I1 => EXTERNAL_RESET,
      O => PWM_B_P(7)
    );
\PWM_B_P[8]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_b_p_i(8),
      I1 => EXTERNAL_RESET,
      O => PWM_B_P(8)
    );
\PWM_B_P[9]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pwm_b_p_i(9),
      I1 => EXTERNAL_RESET,
      O => PWM_B_P(9)
    );
counter_phase0: entity work.system_pwm_x40_ip_0_0_N_bits_counter
     port map (
      CLK_100MHZ => CLK_100MHZ,
      DI(3) => counter_phase0_n_20,
      DI(2) => counter_phase0_n_21,
      DI(1) => counter_phase0_n_22,
      DI(0) => counter_phase0_n_23,
      Q(12 downto 0) => X_MOD_0(13 downto 1),
      S(3) => counter_phase0_n_13,
      S(2) => counter_phase0_n_14,
      S(1) => counter_phase0_n_15,
      S(0) => counter_phase0_n_16,
      \ctr_state_reg[11]_0\(1) => counter_phase0_n_24,
      \ctr_state_reg[11]_0\(0) => counter_phase0_n_25,
      \ctr_state_reg[11]_1\(1) => counter_phase0_n_30,
      \ctr_state_reg[11]_1\(0) => counter_phase0_n_31,
      \ctr_state_reg[11]_2\(1) => counter_phase0_n_36,
      \ctr_state_reg[11]_2\(0) => counter_phase0_n_37,
      \ctr_state_reg[11]_3\(1) => counter_phase0_n_42,
      \ctr_state_reg[11]_3\(0) => counter_phase0_n_43,
      \ctr_state_reg[12]_0\(2) => counter_phase0_n_17,
      \ctr_state_reg[12]_0\(1) => counter_phase0_n_18,
      \ctr_state_reg[12]_0\(0) => counter_phase0_n_19,
      \ctr_state_reg[12]_1\(0) => counter_phase0_n_45,
      \ctr_state_reg[12]_2\(0) => counter_phase0_n_46,
      \ctr_state_reg[12]_3\(0) => counter_phase0_n_47,
      \ctr_state_reg[12]_4\(0) => counter_phase0_n_48,
      \ctr_state_reg[7]_0\(3) => counter_phase0_n_26,
      \ctr_state_reg[7]_0\(2) => counter_phase0_n_27,
      \ctr_state_reg[7]_0\(1) => counter_phase0_n_28,
      \ctr_state_reg[7]_0\(0) => counter_phase0_n_29,
      \ctr_state_reg[7]_1\(3) => counter_phase0_n_32,
      \ctr_state_reg[7]_1\(2) => counter_phase0_n_33,
      \ctr_state_reg[7]_1\(1) => counter_phase0_n_34,
      \ctr_state_reg[7]_1\(0) => counter_phase0_n_35,
      \ctr_state_reg[7]_2\(3) => counter_phase0_n_38,
      \ctr_state_reg[7]_2\(2) => counter_phase0_n_39,
      \ctr_state_reg[7]_2\(1) => counter_phase0_n_40,
      \ctr_state_reg[7]_2\(0) => counter_phase0_n_41,
      dir => dir,
      external_reset_i => external_reset_i,
      \pwm_a_p_i0_inferred__1/i__carry__0\(12 downto 0) => X_MOD_2(13 downto 1),
      \pwm_a_p_i0_inferred__3/i__carry__0\(12 downto 0) => X_MOD_4(13 downto 1),
      \pwm_a_p_i0_inferred__5/i__carry__0\(12 downto 0) => X_MOD_6(13 downto 1),
      \pwm_a_p_i0_inferred__7/i__carry__0\(12 downto 0) => X_MOD_8(13 downto 1),
      \^q\(12 downto 0) => q(12 downto 0),
      tc => tc
    );
counter_phase0_dir_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => dir,
      O => p_0_in
    );
counter_phase0_dir_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => tc,
      CE => '1',
      D => p_0_in,
      PRE => external_reset_i,
      Q => dir
    );
counter_phase180: entity work.system_pwm_x40_ip_0_0_N_bits_counter_0
     port map (
      CLK_100MHZ => CLK_100MHZ,
      Q(12 downto 0) => X_MOD_0(13 downto 1),
      S(3) => counter_phase180_n_13,
      S(2) => counter_phase180_n_14,
      S(1) => counter_phase180_n_15,
      S(0) => counter_phase180_n_16,
      counter_phase180_dir_reg => counter_phase180_dir_reg_n_0,
      counter_phase180_output(12 downto 0) => counter_phase180_output(12 downto 0),
      counter_phase180_tc => counter_phase180_tc,
      \ctr_state_reg[12]_0\(2) => counter_phase180_n_17,
      \ctr_state_reg[12]_0\(1) => counter_phase180_n_18,
      \ctr_state_reg[12]_0\(0) => counter_phase180_n_19,
      \ctr_state_reg[12]_1\(2) => counter_phase180_n_24,
      \ctr_state_reg[12]_1\(1) => counter_phase180_n_25,
      \ctr_state_reg[12]_1\(0) => counter_phase180_n_26,
      \ctr_state_reg[12]_2\(2) => counter_phase180_n_31,
      \ctr_state_reg[12]_2\(1) => counter_phase180_n_32,
      \ctr_state_reg[12]_2\(0) => counter_phase180_n_33,
      \ctr_state_reg[12]_3\(2) => counter_phase180_n_38,
      \ctr_state_reg[12]_3\(1) => counter_phase180_n_39,
      \ctr_state_reg[12]_3\(0) => counter_phase180_n_40,
      \ctr_state_reg[12]_4\(2) => counter_phase180_n_45,
      \ctr_state_reg[12]_4\(1) => counter_phase180_n_46,
      \ctr_state_reg[12]_4\(0) => counter_phase180_n_47,
      \ctr_state_reg[7]_0\(3) => counter_phase180_n_20,
      \ctr_state_reg[7]_0\(2) => counter_phase180_n_21,
      \ctr_state_reg[7]_0\(1) => counter_phase180_n_22,
      \ctr_state_reg[7]_0\(0) => counter_phase180_n_23,
      \ctr_state_reg[7]_1\(3) => counter_phase180_n_27,
      \ctr_state_reg[7]_1\(2) => counter_phase180_n_28,
      \ctr_state_reg[7]_1\(1) => counter_phase180_n_29,
      \ctr_state_reg[7]_1\(0) => counter_phase180_n_30,
      \ctr_state_reg[7]_2\(3) => counter_phase180_n_34,
      \ctr_state_reg[7]_2\(2) => counter_phase180_n_35,
      \ctr_state_reg[7]_2\(1) => counter_phase180_n_36,
      \ctr_state_reg[7]_2\(0) => counter_phase180_n_37,
      \ctr_state_reg[7]_3\(3) => counter_phase180_n_41,
      \ctr_state_reg[7]_3\(2) => counter_phase180_n_42,
      \ctr_state_reg[7]_3\(1) => counter_phase180_n_43,
      \ctr_state_reg[7]_3\(0) => counter_phase180_n_44,
      external_reset_i => external_reset_i,
      \pwm_b_p_i0_inferred__1/i__carry__0\(12 downto 0) => X_MOD_2(13 downto 1),
      \pwm_b_p_i0_inferred__3/i__carry__0\(12 downto 0) => X_MOD_4(13 downto 1),
      \pwm_b_p_i0_inferred__5/i__carry__0\(12 downto 0) => X_MOD_6(13 downto 1),
      \pwm_b_p_i0_inferred__7/i__carry__0\(12 downto 0) => X_MOD_8(13 downto 1)
    );
counter_phase180_dir_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_phase180_dir_reg_n_0,
      O => counter_phase180_dir_i_1_n_0
    );
counter_phase180_dir_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => counter_phase180_tc,
      CE => '1',
      CLR => external_reset_i,
      D => counter_phase180_dir_i_1_n_0,
      Q => counter_phase180_dir_reg_n_0
    );
counter_phase270: entity work.system_pwm_x40_ip_0_0_N_bits_counter_1
     port map (
      CLK_100MHZ => CLK_100MHZ,
      Q(12 downto 0) => X_MOD_1(13 downto 1),
      S(3) => counter_phase270_n_13,
      S(2) => counter_phase270_n_14,
      S(1) => counter_phase270_n_15,
      S(0) => counter_phase270_n_16,
      counter_phase270_output(12 downto 0) => counter_phase270_output(12 downto 0),
      counter_phase270_tc => counter_phase270_tc,
      \ctr_state_reg[0]_0\ => counter_phase270_dir_reg_n_0,
      \ctr_state_reg[12]_0\(2) => counter_phase270_n_17,
      \ctr_state_reg[12]_0\(1) => counter_phase270_n_18,
      \ctr_state_reg[12]_0\(0) => counter_phase270_n_19,
      \ctr_state_reg[12]_1\(2) => counter_phase270_n_24,
      \ctr_state_reg[12]_1\(1) => counter_phase270_n_25,
      \ctr_state_reg[12]_1\(0) => counter_phase270_n_26,
      \ctr_state_reg[12]_2\(2) => counter_phase270_n_31,
      \ctr_state_reg[12]_2\(1) => counter_phase270_n_32,
      \ctr_state_reg[12]_2\(0) => counter_phase270_n_33,
      \ctr_state_reg[12]_3\(2) => counter_phase270_n_38,
      \ctr_state_reg[12]_3\(1) => counter_phase270_n_39,
      \ctr_state_reg[12]_3\(0) => counter_phase270_n_40,
      \ctr_state_reg[12]_4\(2) => counter_phase270_n_45,
      \ctr_state_reg[12]_4\(1) => counter_phase270_n_46,
      \ctr_state_reg[12]_4\(0) => counter_phase270_n_47,
      \ctr_state_reg[7]_0\(3) => counter_phase270_n_20,
      \ctr_state_reg[7]_0\(2) => counter_phase270_n_21,
      \ctr_state_reg[7]_0\(1) => counter_phase270_n_22,
      \ctr_state_reg[7]_0\(0) => counter_phase270_n_23,
      \ctr_state_reg[7]_1\(3) => counter_phase270_n_27,
      \ctr_state_reg[7]_1\(2) => counter_phase270_n_28,
      \ctr_state_reg[7]_1\(1) => counter_phase270_n_29,
      \ctr_state_reg[7]_1\(0) => counter_phase270_n_30,
      \ctr_state_reg[7]_2\(3) => counter_phase270_n_34,
      \ctr_state_reg[7]_2\(2) => counter_phase270_n_35,
      \ctr_state_reg[7]_2\(1) => counter_phase270_n_36,
      \ctr_state_reg[7]_2\(0) => counter_phase270_n_37,
      \ctr_state_reg[7]_3\(3) => counter_phase270_n_41,
      \ctr_state_reg[7]_3\(2) => counter_phase270_n_42,
      \ctr_state_reg[7]_3\(1) => counter_phase270_n_43,
      \ctr_state_reg[7]_3\(0) => counter_phase270_n_44,
      external_reset_i => external_reset_i,
      \pwm_b_p_i0_inferred__2/i__carry__0\(12 downto 0) => X_MOD_3(13 downto 1),
      \pwm_b_p_i0_inferred__4/i__carry__0\(12 downto 0) => X_MOD_5(13 downto 1),
      \pwm_b_p_i0_inferred__6/i__carry__0\(12 downto 0) => X_MOD_7(13 downto 1),
      \pwm_b_p_i0_inferred__8/i__carry__0\(12 downto 0) => X_MOD_9(13 downto 1)
    );
counter_phase270_dir_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_phase270_dir_reg_n_0,
      O => counter_phase270_dir_i_1_n_0
    );
counter_phase270_dir_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => counter_phase270_tc,
      CE => '1',
      D => counter_phase270_dir_i_1_n_0,
      PRE => external_reset_i,
      Q => counter_phase270_dir_reg_n_0
    );
counter_phase90: entity work.system_pwm_x40_ip_0_0_N_bits_counter_2
     port map (
      CLK_100MHZ => CLK_100MHZ,
      DI(3) => counter_phase90_n_20,
      DI(2) => counter_phase90_n_21,
      DI(1) => counter_phase90_n_22,
      DI(0) => counter_phase90_n_23,
      Q(12 downto 0) => X_MOD_1(13 downto 1),
      S(3) => counter_phase90_n_13,
      S(2) => counter_phase90_n_14,
      S(1) => counter_phase90_n_15,
      S(0) => counter_phase90_n_16,
      counter_phase90_dir_reg => counter_phase90_dir_reg_n_0,
      counter_phase90_output(12 downto 0) => counter_phase90_output(12 downto 0),
      counter_phase90_tc => counter_phase90_tc,
      \ctr_state_reg[11]_0\(1) => counter_phase90_n_24,
      \ctr_state_reg[11]_0\(0) => counter_phase90_n_25,
      \ctr_state_reg[11]_1\(1) => counter_phase90_n_30,
      \ctr_state_reg[11]_1\(0) => counter_phase90_n_31,
      \ctr_state_reg[11]_2\(1) => counter_phase90_n_36,
      \ctr_state_reg[11]_2\(0) => counter_phase90_n_37,
      \ctr_state_reg[11]_3\(1) => counter_phase90_n_42,
      \ctr_state_reg[11]_3\(0) => counter_phase90_n_43,
      \ctr_state_reg[12]_0\(2) => counter_phase90_n_17,
      \ctr_state_reg[12]_0\(1) => counter_phase90_n_18,
      \ctr_state_reg[12]_0\(0) => counter_phase90_n_19,
      \ctr_state_reg[12]_1\(0) => counter_phase90_n_45,
      \ctr_state_reg[12]_2\(0) => counter_phase90_n_46,
      \ctr_state_reg[12]_3\(0) => counter_phase90_n_47,
      \ctr_state_reg[12]_4\(0) => counter_phase90_n_48,
      \ctr_state_reg[7]_0\(3) => counter_phase90_n_26,
      \ctr_state_reg[7]_0\(2) => counter_phase90_n_27,
      \ctr_state_reg[7]_0\(1) => counter_phase90_n_28,
      \ctr_state_reg[7]_0\(0) => counter_phase90_n_29,
      \ctr_state_reg[7]_1\(3) => counter_phase90_n_32,
      \ctr_state_reg[7]_1\(2) => counter_phase90_n_33,
      \ctr_state_reg[7]_1\(1) => counter_phase90_n_34,
      \ctr_state_reg[7]_1\(0) => counter_phase90_n_35,
      \ctr_state_reg[7]_2\(3) => counter_phase90_n_38,
      \ctr_state_reg[7]_2\(2) => counter_phase90_n_39,
      \ctr_state_reg[7]_2\(1) => counter_phase90_n_40,
      \ctr_state_reg[7]_2\(0) => counter_phase90_n_41,
      external_reset_i => external_reset_i,
      \pwm_a_p_i0_inferred__2/i__carry__0\(12 downto 0) => X_MOD_3(13 downto 1),
      \pwm_a_p_i0_inferred__4/i__carry__0\(12 downto 0) => X_MOD_5(13 downto 1),
      \pwm_a_p_i0_inferred__6/i__carry__0\(12 downto 0) => X_MOD_7(13 downto 1),
      \pwm_a_p_i0_inferred__8/i__carry__0\(12 downto 0) => X_MOD_9(13 downto 1)
    );
counter_phase90_dir_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counter_phase90_dir_reg_n_0,
      O => counter_phase90_dir_i_1_n_0
    );
counter_phase90_dir_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => counter_phase90_tc,
      CE => '1',
      CLR => external_reset_i,
      D => counter_phase90_dir_i_1_n_0,
      Q => counter_phase90_dir_reg_n_0
    );
external_reset_i_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK_100MHZ,
      CE => '1',
      D => EXTERNAL_RESET,
      Q => external_reset_i,
      R => '0'
    );
pwm_a_p_i0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => pwm_a_p_i0_carry_n_0,
      CO(2) => pwm_a_p_i0_carry_n_1,
      CO(1) => pwm_a_p_i0_carry_n_2,
      CO(0) => pwm_a_p_i0_carry_n_3,
      CYINIT => '1',
      DI(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_0,
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_1,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_2,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_3,
      O(3 downto 0) => NLW_pwm_a_p_i0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => counter_phase0_n_13,
      S(2) => counter_phase0_n_14,
      S(1) => counter_phase0_n_15,
      S(0) => counter_phase0_n_16
    );
\pwm_a_p_i0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => pwm_a_p_i0_carry_n_0,
      CO(3) => \NLW_pwm_a_p_i0_carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_a_p_i(0),
      CO(1) => \pwm_a_p_i0_carry__0_n_2\,
      CO(0) => \pwm_a_p_i0_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_17,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_18,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_19,
      O(3 downto 0) => \NLW_pwm_a_p_i0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase0_n_17,
      S(1) => counter_phase0_n_18,
      S(0) => counter_phase0_n_19
    );
\pwm_a_p_i0_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_a_p_i0_inferred__0/i__carry_n_0\,
      CO(2) => \pwm_a_p_i0_inferred__0/i__carry_n_1\,
      CO(1) => \pwm_a_p_i0_inferred__0/i__carry_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__0/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_20,
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_21,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_22,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_23,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => counter_phase90_n_13,
      S(2) => counter_phase90_n_14,
      S(1) => counter_phase90_n_15,
      S(0) => counter_phase90_n_16
    );
\pwm_a_p_i0_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_a_p_i0_inferred__0/i__carry_n_0\,
      CO(3) => \NLW_pwm_a_p_i0_inferred__0/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_a_p_i(1),
      CO(1) => \pwm_a_p_i0_inferred__0/i__carry__0_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__0/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_37,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_38,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_39,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__0/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase90_n_17,
      S(1) => counter_phase90_n_18,
      S(0) => counter_phase90_n_19
    );
\pwm_a_p_i0_inferred__1/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_a_p_i0_inferred__1/i__carry_n_0\,
      CO(2) => \pwm_a_p_i0_inferred__1/i__carry_n_1\,
      CO(1) => \pwm_a_p_i0_inferred__1/i__carry_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__1/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => counter_phase0_n_20,
      DI(2) => counter_phase0_n_21,
      DI(1) => counter_phase0_n_22,
      DI(0) => counter_phase0_n_23,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__1/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_40,
      S(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_41,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_42,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_43
    );
\pwm_a_p_i0_inferred__1/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_a_p_i0_inferred__1/i__carry_n_0\,
      CO(3) => \NLW_pwm_a_p_i0_inferred__1/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_a_p_i(2),
      CO(1) => \pwm_a_p_i0_inferred__1/i__carry__0_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__1/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_59,
      DI(1) => counter_phase0_n_24,
      DI(0) => counter_phase0_n_25,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__1/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase0_n_45,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_57,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_58
    );
\pwm_a_p_i0_inferred__2/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_a_p_i0_inferred__2/i__carry_n_0\,
      CO(2) => \pwm_a_p_i0_inferred__2/i__carry_n_1\,
      CO(1) => \pwm_a_p_i0_inferred__2/i__carry_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__2/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => counter_phase90_n_20,
      DI(2) => counter_phase90_n_21,
      DI(1) => counter_phase90_n_22,
      DI(0) => counter_phase90_n_23,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__2/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_60,
      S(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_61,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_62,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_63
    );
\pwm_a_p_i0_inferred__2/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_a_p_i0_inferred__2/i__carry_n_0\,
      CO(3) => \NLW_pwm_a_p_i0_inferred__2/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_a_p_i(3),
      CO(1) => \pwm_a_p_i0_inferred__2/i__carry__0_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__2/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_79,
      DI(1) => counter_phase90_n_24,
      DI(0) => counter_phase90_n_25,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__2/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase90_n_45,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_77,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_78
    );
\pwm_a_p_i0_inferred__3/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_a_p_i0_inferred__3/i__carry_n_0\,
      CO(2) => \pwm_a_p_i0_inferred__3/i__carry_n_1\,
      CO(1) => \pwm_a_p_i0_inferred__3/i__carry_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__3/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => counter_phase0_n_26,
      DI(2) => counter_phase0_n_27,
      DI(1) => counter_phase0_n_28,
      DI(0) => counter_phase0_n_29,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__3/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_80,
      S(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_81,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_82,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_83
    );
\pwm_a_p_i0_inferred__3/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_a_p_i0_inferred__3/i__carry_n_0\,
      CO(3) => \NLW_pwm_a_p_i0_inferred__3/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_a_p_i(4),
      CO(1) => \pwm_a_p_i0_inferred__3/i__carry__0_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__3/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_99,
      DI(1) => counter_phase0_n_30,
      DI(0) => counter_phase0_n_31,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__3/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase0_n_46,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_97,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_98
    );
\pwm_a_p_i0_inferred__4/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_a_p_i0_inferred__4/i__carry_n_0\,
      CO(2) => \pwm_a_p_i0_inferred__4/i__carry_n_1\,
      CO(1) => \pwm_a_p_i0_inferred__4/i__carry_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__4/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => counter_phase90_n_26,
      DI(2) => counter_phase90_n_27,
      DI(1) => counter_phase90_n_28,
      DI(0) => counter_phase90_n_29,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__4/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_100,
      S(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_101,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_102,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_103
    );
\pwm_a_p_i0_inferred__4/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_a_p_i0_inferred__4/i__carry_n_0\,
      CO(3) => \NLW_pwm_a_p_i0_inferred__4/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_a_p_i(5),
      CO(1) => \pwm_a_p_i0_inferred__4/i__carry__0_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__4/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_119,
      DI(1) => counter_phase90_n_30,
      DI(0) => counter_phase90_n_31,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__4/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase90_n_46,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_117,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_118
    );
\pwm_a_p_i0_inferred__5/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_a_p_i0_inferred__5/i__carry_n_0\,
      CO(2) => \pwm_a_p_i0_inferred__5/i__carry_n_1\,
      CO(1) => \pwm_a_p_i0_inferred__5/i__carry_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__5/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => counter_phase0_n_32,
      DI(2) => counter_phase0_n_33,
      DI(1) => counter_phase0_n_34,
      DI(0) => counter_phase0_n_35,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__5/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_120,
      S(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_121,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_122,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_123
    );
\pwm_a_p_i0_inferred__5/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_a_p_i0_inferred__5/i__carry_n_0\,
      CO(3) => \NLW_pwm_a_p_i0_inferred__5/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_a_p_i(6),
      CO(1) => \pwm_a_p_i0_inferred__5/i__carry__0_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__5/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_139,
      DI(1) => counter_phase0_n_36,
      DI(0) => counter_phase0_n_37,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__5/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase0_n_47,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_137,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_138
    );
\pwm_a_p_i0_inferred__6/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_a_p_i0_inferred__6/i__carry_n_0\,
      CO(2) => \pwm_a_p_i0_inferred__6/i__carry_n_1\,
      CO(1) => \pwm_a_p_i0_inferred__6/i__carry_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__6/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => counter_phase90_n_32,
      DI(2) => counter_phase90_n_33,
      DI(1) => counter_phase90_n_34,
      DI(0) => counter_phase90_n_35,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__6/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_140,
      S(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_141,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_142,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_143
    );
\pwm_a_p_i0_inferred__6/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_a_p_i0_inferred__6/i__carry_n_0\,
      CO(3) => \NLW_pwm_a_p_i0_inferred__6/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_a_p_i(7),
      CO(1) => \pwm_a_p_i0_inferred__6/i__carry__0_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__6/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_159,
      DI(1) => counter_phase90_n_36,
      DI(0) => counter_phase90_n_37,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__6/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase90_n_47,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_157,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_158
    );
\pwm_a_p_i0_inferred__7/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_a_p_i0_inferred__7/i__carry_n_0\,
      CO(2) => \pwm_a_p_i0_inferred__7/i__carry_n_1\,
      CO(1) => \pwm_a_p_i0_inferred__7/i__carry_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__7/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => counter_phase0_n_38,
      DI(2) => counter_phase0_n_39,
      DI(1) => counter_phase0_n_40,
      DI(0) => counter_phase0_n_41,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__7/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_160,
      S(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_161,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_162,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_163
    );
\pwm_a_p_i0_inferred__7/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_a_p_i0_inferred__7/i__carry_n_0\,
      CO(3) => \NLW_pwm_a_p_i0_inferred__7/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_a_p_i(8),
      CO(1) => \pwm_a_p_i0_inferred__7/i__carry__0_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__7/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_179,
      DI(1) => counter_phase0_n_42,
      DI(0) => counter_phase0_n_43,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__7/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase0_n_48,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_177,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_178
    );
\pwm_a_p_i0_inferred__8/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_a_p_i0_inferred__8/i__carry_n_0\,
      CO(2) => \pwm_a_p_i0_inferred__8/i__carry_n_1\,
      CO(1) => \pwm_a_p_i0_inferred__8/i__carry_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__8/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => counter_phase90_n_38,
      DI(2) => counter_phase90_n_39,
      DI(1) => counter_phase90_n_40,
      DI(0) => counter_phase90_n_41,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__8/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_180,
      S(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_181,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_182,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_183
    );
\pwm_a_p_i0_inferred__8/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_a_p_i0_inferred__8/i__carry_n_0\,
      CO(3) => \NLW_pwm_a_p_i0_inferred__8/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_a_p_i(9),
      CO(1) => \pwm_a_p_i0_inferred__8/i__carry__0_n_2\,
      CO(0) => \pwm_a_p_i0_inferred__8/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_199,
      DI(1) => counter_phase90_n_42,
      DI(0) => counter_phase90_n_43,
      O(3 downto 0) => \NLW_pwm_a_p_i0_inferred__8/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase90_n_48,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_197,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_198
    );
pwm_b_p_i0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => pwm_b_p_i0_carry_n_0,
      CO(2) => pwm_b_p_i0_carry_n_1,
      CO(1) => pwm_b_p_i0_carry_n_2,
      CO(0) => pwm_b_p_i0_carry_n_3,
      CYINIT => '1',
      DI(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_200,
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_201,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_202,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_203,
      O(3 downto 0) => NLW_pwm_b_p_i0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => counter_phase180_n_13,
      S(2) => counter_phase180_n_14,
      S(1) => counter_phase180_n_15,
      S(0) => counter_phase180_n_16
    );
\pwm_b_p_i0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => pwm_b_p_i0_carry_n_0,
      CO(3) => \NLW_pwm_b_p_i0_carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_b_p_i(0),
      CO(1) => \pwm_b_p_i0_carry__0_n_2\,
      CO(0) => \pwm_b_p_i0_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_204,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_205,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_206,
      O(3 downto 0) => \NLW_pwm_b_p_i0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase180_n_17,
      S(1) => counter_phase180_n_18,
      S(0) => counter_phase180_n_19
    );
\pwm_b_p_i0_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_b_p_i0_inferred__0/i__carry_n_0\,
      CO(2) => \pwm_b_p_i0_inferred__0/i__carry_n_1\,
      CO(1) => \pwm_b_p_i0_inferred__0/i__carry_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__0/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_207,
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_208,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_209,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_210,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => counter_phase270_n_13,
      S(2) => counter_phase270_n_14,
      S(1) => counter_phase270_n_15,
      S(0) => counter_phase270_n_16
    );
\pwm_b_p_i0_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_b_p_i0_inferred__0/i__carry_n_0\,
      CO(3) => \NLW_pwm_b_p_i0_inferred__0/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_b_p_i(1),
      CO(1) => \pwm_b_p_i0_inferred__0/i__carry__0_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__0/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_211,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_212,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_213,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__0/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase270_n_17,
      S(1) => counter_phase270_n_18,
      S(0) => counter_phase270_n_19
    );
\pwm_b_p_i0_inferred__1/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_b_p_i0_inferred__1/i__carry_n_0\,
      CO(2) => \pwm_b_p_i0_inferred__1/i__carry_n_1\,
      CO(1) => \pwm_b_p_i0_inferred__1/i__carry_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__1/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_214,
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_215,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_216,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_217,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__1/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => counter_phase180_n_20,
      S(2) => counter_phase180_n_21,
      S(1) => counter_phase180_n_22,
      S(0) => counter_phase180_n_23
    );
\pwm_b_p_i0_inferred__1/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_b_p_i0_inferred__1/i__carry_n_0\,
      CO(3) => \NLW_pwm_b_p_i0_inferred__1/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_b_p_i(2),
      CO(1) => \pwm_b_p_i0_inferred__1/i__carry__0_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__1/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_218,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_219,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_220,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__1/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase180_n_24,
      S(1) => counter_phase180_n_25,
      S(0) => counter_phase180_n_26
    );
\pwm_b_p_i0_inferred__2/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_b_p_i0_inferred__2/i__carry_n_0\,
      CO(2) => \pwm_b_p_i0_inferred__2/i__carry_n_1\,
      CO(1) => \pwm_b_p_i0_inferred__2/i__carry_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__2/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_221,
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_222,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_223,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_224,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__2/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => counter_phase270_n_20,
      S(2) => counter_phase270_n_21,
      S(1) => counter_phase270_n_22,
      S(0) => counter_phase270_n_23
    );
\pwm_b_p_i0_inferred__2/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_b_p_i0_inferred__2/i__carry_n_0\,
      CO(3) => \NLW_pwm_b_p_i0_inferred__2/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_b_p_i(3),
      CO(1) => \pwm_b_p_i0_inferred__2/i__carry__0_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__2/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_225,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_226,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_227,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__2/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase270_n_24,
      S(1) => counter_phase270_n_25,
      S(0) => counter_phase270_n_26
    );
\pwm_b_p_i0_inferred__3/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_b_p_i0_inferred__3/i__carry_n_0\,
      CO(2) => \pwm_b_p_i0_inferred__3/i__carry_n_1\,
      CO(1) => \pwm_b_p_i0_inferred__3/i__carry_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__3/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_228,
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_229,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_230,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_231,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__3/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => counter_phase180_n_27,
      S(2) => counter_phase180_n_28,
      S(1) => counter_phase180_n_29,
      S(0) => counter_phase180_n_30
    );
\pwm_b_p_i0_inferred__3/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_b_p_i0_inferred__3/i__carry_n_0\,
      CO(3) => \NLW_pwm_b_p_i0_inferred__3/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_b_p_i(4),
      CO(1) => \pwm_b_p_i0_inferred__3/i__carry__0_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__3/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_232,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_233,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_234,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__3/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase180_n_31,
      S(1) => counter_phase180_n_32,
      S(0) => counter_phase180_n_33
    );
\pwm_b_p_i0_inferred__4/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_b_p_i0_inferred__4/i__carry_n_0\,
      CO(2) => \pwm_b_p_i0_inferred__4/i__carry_n_1\,
      CO(1) => \pwm_b_p_i0_inferred__4/i__carry_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__4/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_235,
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_236,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_237,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_238,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__4/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => counter_phase270_n_27,
      S(2) => counter_phase270_n_28,
      S(1) => counter_phase270_n_29,
      S(0) => counter_phase270_n_30
    );
\pwm_b_p_i0_inferred__4/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_b_p_i0_inferred__4/i__carry_n_0\,
      CO(3) => \NLW_pwm_b_p_i0_inferred__4/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_b_p_i(5),
      CO(1) => \pwm_b_p_i0_inferred__4/i__carry__0_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__4/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_239,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_240,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_241,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__4/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase270_n_31,
      S(1) => counter_phase270_n_32,
      S(0) => counter_phase270_n_33
    );
\pwm_b_p_i0_inferred__5/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_b_p_i0_inferred__5/i__carry_n_0\,
      CO(2) => \pwm_b_p_i0_inferred__5/i__carry_n_1\,
      CO(1) => \pwm_b_p_i0_inferred__5/i__carry_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__5/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_242,
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_243,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_244,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_245,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__5/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => counter_phase180_n_34,
      S(2) => counter_phase180_n_35,
      S(1) => counter_phase180_n_36,
      S(0) => counter_phase180_n_37
    );
\pwm_b_p_i0_inferred__5/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_b_p_i0_inferred__5/i__carry_n_0\,
      CO(3) => \NLW_pwm_b_p_i0_inferred__5/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_b_p_i(6),
      CO(1) => \pwm_b_p_i0_inferred__5/i__carry__0_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__5/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_246,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_247,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_248,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__5/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase180_n_38,
      S(1) => counter_phase180_n_39,
      S(0) => counter_phase180_n_40
    );
\pwm_b_p_i0_inferred__6/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_b_p_i0_inferred__6/i__carry_n_0\,
      CO(2) => \pwm_b_p_i0_inferred__6/i__carry_n_1\,
      CO(1) => \pwm_b_p_i0_inferred__6/i__carry_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__6/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_249,
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_250,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_251,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_252,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__6/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => counter_phase270_n_34,
      S(2) => counter_phase270_n_35,
      S(1) => counter_phase270_n_36,
      S(0) => counter_phase270_n_37
    );
\pwm_b_p_i0_inferred__6/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_b_p_i0_inferred__6/i__carry_n_0\,
      CO(3) => \NLW_pwm_b_p_i0_inferred__6/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_b_p_i(7),
      CO(1) => \pwm_b_p_i0_inferred__6/i__carry__0_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__6/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_253,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_254,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_255,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__6/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase270_n_38,
      S(1) => counter_phase270_n_39,
      S(0) => counter_phase270_n_40
    );
\pwm_b_p_i0_inferred__7/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_b_p_i0_inferred__7/i__carry_n_0\,
      CO(2) => \pwm_b_p_i0_inferred__7/i__carry_n_1\,
      CO(1) => \pwm_b_p_i0_inferred__7/i__carry_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__7/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_256,
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_257,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_258,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_259,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__7/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => counter_phase180_n_41,
      S(2) => counter_phase180_n_42,
      S(1) => counter_phase180_n_43,
      S(0) => counter_phase180_n_44
    );
\pwm_b_p_i0_inferred__7/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_b_p_i0_inferred__7/i__carry_n_0\,
      CO(3) => \NLW_pwm_b_p_i0_inferred__7/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_b_p_i(8),
      CO(1) => \pwm_b_p_i0_inferred__7/i__carry__0_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__7/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_260,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_261,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_262,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__7/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase180_n_45,
      S(1) => counter_phase180_n_46,
      S(0) => counter_phase180_n_47
    );
\pwm_b_p_i0_inferred__8/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pwm_b_p_i0_inferred__8/i__carry_n_0\,
      CO(2) => \pwm_b_p_i0_inferred__8/i__carry_n_1\,
      CO(1) => \pwm_b_p_i0_inferred__8/i__carry_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__8/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_263,
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_264,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_265,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_266,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__8/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => counter_phase270_n_41,
      S(2) => counter_phase270_n_42,
      S(1) => counter_phase270_n_43,
      S(0) => counter_phase270_n_44
    );
\pwm_b_p_i0_inferred__8/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \pwm_b_p_i0_inferred__8/i__carry_n_0\,
      CO(3) => \NLW_pwm_b_p_i0_inferred__8/i__carry__0_CO_UNCONNECTED\(3),
      CO(2) => pwm_b_p_i(9),
      CO(1) => \pwm_b_p_i0_inferred__8/i__carry__0_n_2\,
      CO(0) => \pwm_b_p_i0_inferred__8/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_267,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_268,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_269,
      O(3 downto 0) => \NLW_pwm_b_p_i0_inferred__8/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => counter_phase270_n_45,
      S(1) => counter_phase270_n_46,
      S(0) => counter_phase270_n_47
    );
pwm_x40_ip_v1_0_S_AXI_inst: entity work.system_pwm_x40_ip_0_0_pwm_x40_ip_v1_0_S_AXI
     port map (
      DI(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_0,
      DI(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_1,
      DI(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_2,
      DI(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_3,
      S(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_40,
      S(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_41,
      S(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_42,
      S(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_43,
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WREADY => S_AXI_WREADY,
      X_MOD_0(12 downto 0) => X_MOD_0(13 downto 1),
      X_MOD_1(12 downto 0) => X_MOD_1(13 downto 1),
      X_MOD_2(12 downto 0) => X_MOD_2(13 downto 1),
      X_MOD_3(12 downto 0) => X_MOD_3(13 downto 1),
      X_MOD_4(12 downto 0) => X_MOD_4(13 downto 1),
      X_MOD_5(12 downto 0) => X_MOD_5(13 downto 1),
      X_MOD_6(12 downto 0) => X_MOD_6(13 downto 1),
      X_MOD_7(12 downto 0) => X_MOD_7(13 downto 1),
      X_MOD_8(12 downto 0) => X_MOD_8(13 downto 1),
      X_MOD_9(12 downto 0) => X_MOD_9(13 downto 1),
      counter_phase180_output(12 downto 0) => counter_phase180_output(12 downto 0),
      counter_phase270_output(12 downto 0) => counter_phase270_output(12 downto 0),
      counter_phase90_output(12 downto 0) => counter_phase90_output(12 downto 0),
      external_reset_i => external_reset_i,
      q(12 downto 0) => q(12 downto 0),
      s_axi_aclk => s_axi_aclk,
      s_axi_araddr(3 downto 0) => s_axi_araddr(3 downto 0),
      s_axi_aresetn => s_axi_aresetn,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awaddr(3 downto 0) => s_axi_awaddr(3 downto 0),
      s_axi_awvalid => s_axi_awvalid,
      s_axi_bready => s_axi_bready,
      s_axi_bvalid => s_axi_bvalid,
      s_axi_rdata(31 downto 0) => s_axi_rdata(31 downto 0),
      s_axi_rready => s_axi_rready,
      s_axi_rvalid => s_axi_rvalid,
      s_axi_wdata(31 downto 0) => s_axi_wdata(31 downto 0),
      s_axi_wstrb(3 downto 0) => s_axi_wstrb(3 downto 0),
      s_axi_wvalid => s_axi_wvalid,
      \x_mod_0_i_reg[13]_0\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_17,
      \x_mod_0_i_reg[13]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_18,
      \x_mod_0_i_reg[13]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_19,
      \x_mod_0_i_reg[13]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_204,
      \x_mod_0_i_reg[13]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_205,
      \x_mod_0_i_reg[13]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_206,
      \x_mod_0_i_reg[8]_0\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_200,
      \x_mod_0_i_reg[8]_0\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_201,
      \x_mod_0_i_reg[8]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_202,
      \x_mod_0_i_reg[8]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_203,
      \x_mod_1_i_reg[13]_0\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_37,
      \x_mod_1_i_reg[13]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_38,
      \x_mod_1_i_reg[13]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_39,
      \x_mod_1_i_reg[13]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_211,
      \x_mod_1_i_reg[13]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_212,
      \x_mod_1_i_reg[13]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_213,
      \x_mod_1_i_reg[8]_0\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_20,
      \x_mod_1_i_reg[8]_0\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_21,
      \x_mod_1_i_reg[8]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_22,
      \x_mod_1_i_reg[8]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_23,
      \x_mod_1_i_reg[8]_1\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_207,
      \x_mod_1_i_reg[8]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_208,
      \x_mod_1_i_reg[8]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_209,
      \x_mod_1_i_reg[8]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_210,
      \x_mod_2_i_reg[12]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_57,
      \x_mod_2_i_reg[12]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_58,
      \x_mod_2_i_reg[13]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_59,
      \x_mod_2_i_reg[13]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_218,
      \x_mod_2_i_reg[13]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_219,
      \x_mod_2_i_reg[13]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_220,
      \x_mod_2_i_reg[8]_0\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_214,
      \x_mod_2_i_reg[8]_0\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_215,
      \x_mod_2_i_reg[8]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_216,
      \x_mod_2_i_reg[8]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_217,
      \x_mod_3_i_reg[12]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_77,
      \x_mod_3_i_reg[12]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_78,
      \x_mod_3_i_reg[13]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_79,
      \x_mod_3_i_reg[13]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_225,
      \x_mod_3_i_reg[13]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_226,
      \x_mod_3_i_reg[13]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_227,
      \x_mod_3_i_reg[8]_0\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_60,
      \x_mod_3_i_reg[8]_0\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_61,
      \x_mod_3_i_reg[8]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_62,
      \x_mod_3_i_reg[8]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_63,
      \x_mod_3_i_reg[8]_1\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_221,
      \x_mod_3_i_reg[8]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_222,
      \x_mod_3_i_reg[8]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_223,
      \x_mod_3_i_reg[8]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_224,
      \x_mod_4_i_reg[12]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_97,
      \x_mod_4_i_reg[12]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_98,
      \x_mod_4_i_reg[13]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_99,
      \x_mod_4_i_reg[13]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_232,
      \x_mod_4_i_reg[13]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_233,
      \x_mod_4_i_reg[13]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_234,
      \x_mod_4_i_reg[8]_0\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_80,
      \x_mod_4_i_reg[8]_0\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_81,
      \x_mod_4_i_reg[8]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_82,
      \x_mod_4_i_reg[8]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_83,
      \x_mod_4_i_reg[8]_1\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_228,
      \x_mod_4_i_reg[8]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_229,
      \x_mod_4_i_reg[8]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_230,
      \x_mod_4_i_reg[8]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_231,
      \x_mod_5_i_reg[12]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_117,
      \x_mod_5_i_reg[12]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_118,
      \x_mod_5_i_reg[13]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_119,
      \x_mod_5_i_reg[13]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_239,
      \x_mod_5_i_reg[13]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_240,
      \x_mod_5_i_reg[13]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_241,
      \x_mod_5_i_reg[8]_0\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_100,
      \x_mod_5_i_reg[8]_0\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_101,
      \x_mod_5_i_reg[8]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_102,
      \x_mod_5_i_reg[8]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_103,
      \x_mod_5_i_reg[8]_1\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_235,
      \x_mod_5_i_reg[8]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_236,
      \x_mod_5_i_reg[8]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_237,
      \x_mod_5_i_reg[8]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_238,
      \x_mod_6_i_reg[12]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_137,
      \x_mod_6_i_reg[12]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_138,
      \x_mod_6_i_reg[13]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_139,
      \x_mod_6_i_reg[13]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_246,
      \x_mod_6_i_reg[13]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_247,
      \x_mod_6_i_reg[13]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_248,
      \x_mod_6_i_reg[8]_0\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_120,
      \x_mod_6_i_reg[8]_0\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_121,
      \x_mod_6_i_reg[8]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_122,
      \x_mod_6_i_reg[8]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_123,
      \x_mod_6_i_reg[8]_1\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_242,
      \x_mod_6_i_reg[8]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_243,
      \x_mod_6_i_reg[8]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_244,
      \x_mod_6_i_reg[8]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_245,
      \x_mod_7_i_reg[12]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_157,
      \x_mod_7_i_reg[12]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_158,
      \x_mod_7_i_reg[13]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_159,
      \x_mod_7_i_reg[13]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_253,
      \x_mod_7_i_reg[13]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_254,
      \x_mod_7_i_reg[13]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_255,
      \x_mod_7_i_reg[8]_0\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_140,
      \x_mod_7_i_reg[8]_0\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_141,
      \x_mod_7_i_reg[8]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_142,
      \x_mod_7_i_reg[8]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_143,
      \x_mod_7_i_reg[8]_1\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_249,
      \x_mod_7_i_reg[8]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_250,
      \x_mod_7_i_reg[8]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_251,
      \x_mod_7_i_reg[8]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_252,
      \x_mod_8_i_reg[12]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_177,
      \x_mod_8_i_reg[12]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_178,
      \x_mod_8_i_reg[13]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_179,
      \x_mod_8_i_reg[13]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_260,
      \x_mod_8_i_reg[13]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_261,
      \x_mod_8_i_reg[13]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_262,
      \x_mod_8_i_reg[8]_0\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_160,
      \x_mod_8_i_reg[8]_0\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_161,
      \x_mod_8_i_reg[8]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_162,
      \x_mod_8_i_reg[8]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_163,
      \x_mod_8_i_reg[8]_1\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_256,
      \x_mod_8_i_reg[8]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_257,
      \x_mod_8_i_reg[8]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_258,
      \x_mod_8_i_reg[8]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_259,
      \x_mod_9_i_reg[12]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_197,
      \x_mod_9_i_reg[12]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_198,
      \x_mod_9_i_reg[13]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_199,
      \x_mod_9_i_reg[13]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_267,
      \x_mod_9_i_reg[13]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_268,
      \x_mod_9_i_reg[13]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_269,
      \x_mod_9_i_reg[8]_0\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_180,
      \x_mod_9_i_reg[8]_0\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_181,
      \x_mod_9_i_reg[8]_0\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_182,
      \x_mod_9_i_reg[8]_0\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_183,
      \x_mod_9_i_reg[8]_1\(3) => pwm_x40_ip_v1_0_S_AXI_inst_n_263,
      \x_mod_9_i_reg[8]_1\(2) => pwm_x40_ip_v1_0_S_AXI_inst_n_264,
      \x_mod_9_i_reg[8]_1\(1) => pwm_x40_ip_v1_0_S_AXI_inst_n_265,
      \x_mod_9_i_reg[8]_1\(0) => pwm_x40_ip_v1_0_S_AXI_inst_n_266
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_pwm_x40_ip_0_0 is
  port (
    PWM_A_P : out STD_LOGIC_VECTOR ( 9 downto 0 );
    PWM_B_P : out STD_LOGIC_VECTOR ( 9 downto 0 );
    PWM_A_N : out STD_LOGIC_VECTOR ( 9 downto 0 );
    PWM_B_N : out STD_LOGIC_VECTOR ( 9 downto 0 );
    CLK_100MHZ : in STD_LOGIC;
    EXTERNAL_RESET : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of system_pwm_x40_ip_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of system_pwm_x40_ip_0_0 : entity is "system_pwm_x40_ip_0_0,pwm_x40_ip_v1_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of system_pwm_x40_ip_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of system_pwm_x40_ip_0_0 : entity is "pwm_x40_ip_v1_0,Vivado 2018.3";
end system_pwm_x40_ip_0_0;

architecture STRUCTURE of system_pwm_x40_ip_0_0 is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of EXTERNAL_RESET : signal is "xilinx.com:signal:reset:1.0 COUNTERS_RESET RST";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of EXTERNAL_RESET : signal is "XIL_INTERFACENAME COUNTERS_RESET, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of s_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S_AXI_CLK CLK";
  attribute x_interface_parameter of s_axi_aclk : signal is "XIL_INTERFACENAME S_AXI_CLK, ASSOCIATED_BUSIF S_AXI, ASSOCIATED_RESET s_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute x_interface_info of s_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S_AXI_RST RST";
  attribute x_interface_parameter of s_axi_aresetn : signal is "XIL_INTERFACENAME S_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARREADY";
  attribute x_interface_info of s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARVALID";
  attribute x_interface_info of s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWREADY";
  attribute x_interface_info of s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWVALID";
  attribute x_interface_info of s_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S_AXI BREADY";
  attribute x_interface_info of s_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI BVALID";
  attribute x_interface_info of s_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S_AXI RREADY";
  attribute x_interface_info of s_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI RVALID";
  attribute x_interface_info of s_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S_AXI WREADY";
  attribute x_interface_info of s_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI WVALID";
  attribute x_interface_info of s_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARADDR";
  attribute x_interface_info of s_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARPROT";
  attribute x_interface_info of s_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWADDR";
  attribute x_interface_parameter of s_axi_awaddr : signal is "XIL_INTERFACENAME S_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 10, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN system_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of s_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWPROT";
  attribute x_interface_info of s_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S_AXI BRESP";
  attribute x_interface_info of s_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S_AXI RDATA";
  attribute x_interface_info of s_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S_AXI RRESP";
  attribute x_interface_info of s_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S_AXI WDATA";
  attribute x_interface_info of s_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S_AXI WSTRB";
begin
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.system_pwm_x40_ip_0_0_pwm_x40_ip_v1_0
     port map (
      CLK_100MHZ => CLK_100MHZ,
      EXTERNAL_RESET => EXTERNAL_RESET,
      PWM_A_N(9 downto 0) => PWM_A_N(9 downto 0),
      PWM_A_P(9 downto 0) => PWM_A_P(9 downto 0),
      PWM_B_N(9 downto 0) => PWM_B_N(9 downto 0),
      PWM_B_P(9 downto 0) => PWM_B_P(9 downto 0),
      S_AXI_ARREADY => s_axi_arready,
      S_AXI_AWREADY => s_axi_awready,
      S_AXI_WREADY => s_axi_wready,
      s_axi_aclk => s_axi_aclk,
      s_axi_araddr(3 downto 0) => s_axi_araddr(5 downto 2),
      s_axi_aresetn => s_axi_aresetn,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awaddr(3 downto 0) => s_axi_awaddr(5 downto 2),
      s_axi_awvalid => s_axi_awvalid,
      s_axi_bready => s_axi_bready,
      s_axi_bvalid => s_axi_bvalid,
      s_axi_rdata(31 downto 0) => s_axi_rdata(31 downto 0),
      s_axi_rready => s_axi_rready,
      s_axi_rvalid => s_axi_rvalid,
      s_axi_wdata(31 downto 0) => s_axi_wdata(31 downto 0),
      s_axi_wstrb(3 downto 0) => s_axi_wstrb(3 downto 0),
      s_axi_wvalid => s_axi_wvalid
    );
end STRUCTURE;

# Timing constraints
#create_clock -add -name sys_clk_pin -period 10.00 -waveform {0 5} [get_ports {CLK_BOARD_100MHZ}];

# --------------------------------------
# MicroZed 7Z010 main board
# --------------------------------------

# It does not have any external component connected to the PL section
# The main button and the main LED are connected to the PS section (MIO)

# --------------------------------------
# MicroZed I/O Carrier board
# --------------------------------------

# Zynq clock
#set_property LOC Y16 [ get_ports FCLK0]
#set_property IOSTANDARD LVCMOS33 [ get_ports FCLK0]

# ----------------------------------------------------------------------------
# Carrier Clock Source - Bank 35
# ---------------------------------------------------------------------------- 
#set_property LOC K17 [ get_ports {CLK_BOARD_100MHZ}]; #PL_CLK
#set_property IOSTANDARD LVCMOS33 [ get_ports {CLK_BOARD_100MHZ}]

set_property LOC K18 [ get_ports {PL_CLK_EN}]
set_property IOSTANDARD LVCMOS33 [ get_ports {PL_CLK_EN}]


# ----------------------------------------------------------------------------
# User DIP Switches - Bank 35
# ---------------------------------------------------------------------------- 
set_property PACKAGE_PIN M14 [get_ports {switches_3b_tri_i[0]}];  # "M14.JX2_LVDS_22_P.JX2.87.DIP_SW0"
set_property IOSTANDARD LVCMOS33 [ get_ports switches_3b_tri_i[0]]

set_property PACKAGE_PIN M15 [get_ports {switches_3b_tri_i[1]}];  # "M15.JX2_LVDS_22_N.JX2.89.DIP_SW1"
set_property IOSTANDARD LVCMOS33 [ get_ports switches_3b_tri_i[1]]

set_property PACKAGE_PIN K16 [get_ports {switches_3b_tri_i[2]}];  # "K16.JX2_LVDS_23_P.JX2.88.DIP_SW2"
set_property IOSTANDARD LVCMOS33 [ get_ports switches_3b_tri_i[2]]

#set_property PACKAGE_PIN J16 [get_ports {EXTERNAL_RESET}];  # "J16.JX2_LVDS_23_N.JX2.90.DIP_SW3"
#set_property IOSTANDARD LVCMOS33 [ get_ports EXTERNAL_RESET]


# ----------------------------------------------------------------------------
# User Push Buttons - Bank 35
# ---------------------------------------------------------------------------- 
set_property PACKAGE_PIN G19 [get_ports {buttons_4b_tri_i[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {buttons_4b_tri_i[0]}]

set_property PACKAGE_PIN G20 [get_ports {buttons_4b_tri_i[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {buttons_4b_tri_i[1]}]

set_property PACKAGE_PIN J20 [get_ports {buttons_4b_tri_i[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {buttons_4b_tri_i[2]}]

set_property PACKAGE_PIN H20 [get_ports {buttons_4b_tri_i[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {buttons_4b_tri_i[3]}]


# ----------------------------------------------------------------------------
# User LEDs - Bank 34
# ---------------------------------------------------------------------------- 
set_property PACKAGE_PIN U14 [get_ports {LEDs_8b_tri_o[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LEDs_8b_tri_o[0]}]

set_property PACKAGE_PIN U15 [get_ports {LEDs_8b_tri_o[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LEDs_8b_tri_o[1]}]

set_property PACKAGE_PIN U18 [get_ports {LEDs_8b_tri_o[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LEDs_8b_tri_o[2]}]

set_property PACKAGE_PIN U19 [get_ports {LEDs_8b_tri_o[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LEDs_8b_tri_o[3]}]

set_property PACKAGE_PIN R19 [get_ports {LEDs_8b_tri_o[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LEDs_8b_tri_o[4]}]

set_property PACKAGE_PIN V13 [get_ports {LEDs_8b_tri_o[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LEDs_8b_tri_o[5]}]

set_property PACKAGE_PIN P14 [get_ports {LEDs_8b_tri_o[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LEDs_8b_tri_o[6]}]

set_property PACKAGE_PIN R14 [get_ports {LEDs_8b_tri_o[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LEDs_8b_tri_o[7]}]

 
# ----------------------------------------------------------------------------
# One wire Security EEPROM - Bank 34
# ---------------------------------------------------------------------------- 
#set_property PACKAGE_PIN T19 [get_ports {1W_EEPROM}]  # "T19.JX1_SE_1.JX1.10.1W-EEPROM"


# ----------------------------------------------------------------------------
# JA Pmod - Bank 34
# ----------------------------------------------------------------------------
#set_property PACKAGE_PIN T11 [get_ports {PWM_A_P[0]}]  # "T11.JX1_LVDS_0_P.JX1.11.JA0-1_P" - JA - Pin 1
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[0]}]
#set_property PACKAGE_PIN T10 [get_ports {PWM_A_P[1]}]  # "T10.JX1_LVDS_0_N.JX1.13.JA0-1_N" - JA - Pin 2
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[1]}]
#set_property PACKAGE_PIN T12 [get_ports {PWM_A_P[2]}]  # "T12.JX1_LVDS_1_P.JX1.12.JA2-3_P" - JA - Pin 3
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[2]}]
#set_property PACKAGE_PIN U12 [get_ports {PWM_A_P[3]}]  # "U12.JX1_LVDS_1_N.JX1.14.JA2-3_N" - JA - Pin 4
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[3]}]
#set_property PACKAGE_PIN V12 [get_ports {PWM_A_P[4]}]  # "V12.JX1_LVDS_3_P.JX1.18.JA4-5_P" - JA - Pin 7
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[4]}]
#set_property PACKAGE_PIN W13 [get_ports {PWM_A_P[5]}]  # "W13.JX1_LVDS_3_N.JX1.20.JA4-5_N" - JA - Pin 8
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[5]}]
#set_property PACKAGE_PIN T14 [get_ports {PWM_A_P[6]}]  # "T14.JX1_LVDS_4_P.JX1.23.JA6-7_P" - JA - Pin 9
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[6]}]
#set_property PACKAGE_PIN T15 [get_ports {PWM_A_P[7]}]  # "T15.JX1_LVDS_4_N.JX1.25.JA6-7_N" - JA - Pin 10
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[7]}]


# ----------------------------------------------------------------------------
# JB Pmod - Bank 34
# ----------------------------------------------------------------------------
#set_property PACKAGE_PIN Y16 [get_ports {TIMEOUT}];  # "Y16.JX1_LVDS_6_P.JX1.29.JB0-1_P" - JB - Pin 1
#set_property IOSTANDARD LVCMOS33 [get_ports {TIMEOUT}]
#set_property PACKAGE_PIN Y17 [get_ports {DSPACE_INIT}];  # "Y17.JX1_LVDS_6_N.JX1.31.JB0-1_N" - JB - Pin 2
#set_property IOSTANDARD LVCMOS33 [get_ports {DSPACE_INIT}]
#set_property PACKAGE_PIN W14 [get_ports {JB2_3_P}];  # "W14.JX1_LVDS_7_P.JX1.30.JB2-3_P" - JB - Pin 3
#set_property IOSTANDARD LVCMOS33 [get_ports { }]
#set_property PACKAGE_PIN Y14 [get_ports {JB2_3_N}];  # "Y14.JX1_LVDS_7_N.JX1.32.JB2-3_N" - JB - Pin 4
#set_property IOSTANDARD LVCMOS33 [get_ports { }]
#set_property PACKAGE_PIN T16 [get_ports {JB4_5_P}];  # "T16.JX1_LVDS_8_P.JX1.35.JB4-5_P" - JB - Pin 7
#set_property IOSTANDARD LVCMOS33 [get_ports { }]
#set_property PACKAGE_PIN U17 [get_ports {JB4_5_N}];  # "U17.JX1_LVDS_8_N.JX1.37.JB4-5_N" - JB - Pin 8
#set_property IOSTANDARD LVCMOS33 [get_ports { }]
#set_property PACKAGE_PIN V15 [get_ports {JB6_7_P}];  # "V15.JX1_LVDS_9_P.JX1.36.JB6-7_P" - JB - Pin 9
#set_property IOSTANDARD LVCMOS33 [get_ports { }]
#set_property PACKAGE_PIN W15 [get_ports {JB6_7_N}];  # "W15.JX1_LVDS_9_N.JX1.38.JB6-7_N" - JB - Pin 10
#set_property IOSTANDARD LVCMOS33 [get_ports { }]


# ----------------------------------------------------------------------------
# JC Pmod - Bank 34
# ----------------------------------------------------------------------------
#set_property PACKAGE_PIN N18 [get_ports {DSPACE_VECTOR_DATA[7]}];  # "N18.JX1_LVDS_12_P.JX1.47.JC0-1_P" - JC - Pin 1
#set_property IOSTANDARD LVCMOS33 [get_ports {DSPACE_VECTOR_DATA[7]}]
#set_property PACKAGE_PIN P19 [get_ports {DSPACE_VECTOR_DATA[6]}];  # "P19.JX1_LVDS_12_N.JX1.49.JC0-1_N" - JC - Pin 2
#set_property IOSTANDARD LVCMOS33 [get_ports {DSPACE_VECTOR_DATA[6]}]
#set_property PACKAGE_PIN N20 [get_ports {DSPACE_VECTOR_DATA[5]}];  # "N20.JX1_LVDS_13_P.JX1.48.JC2-3_P" - JC - Pin 3
#set_property IOSTANDARD LVCMOS33 [get_ports {DSPACE_VECTOR_DATA[5]}]
#set_property PACKAGE_PIN P20 [get_ports {DSPACE_VECTOR_DATA[4]}];  # "P20.JX1_LVDS_13_N.JX1.50.JC2-3_N" - JC - Pin 4
#set_property IOSTANDARD LVCMOS33 [get_ports {DSPACE_VECTOR_DATA[4]}]
#set_property PACKAGE_PIN T20 [get_ports {DSPACE_VECTOR_DATA[11]}];  # "T20.JX1_LVDS_14_P.JX1.53.JC4-5_P" - JC - Pin 7
#set_property IOSTANDARD LVCMOS33 [get_ports {DSPACE_VECTOR_DATA[11]}]
#set_property PACKAGE_PIN U20 [get_ports {DSPACE_VECTOR_DATA[10]}];  # "U20.JX1_LVDS_14_N.JX1.55.JC4-5_N" - JC - Pin 8
#set_property IOSTANDARD LVCMOS33 [get_ports {DSPACE_VECTOR_DATA[10]}]
#set_property PACKAGE_PIN V20 [get_ports {DSPACE_VECTOR_DATA[9]}];  # "V20.JX1_LVDS_15_P.JX1.54.JC6-7_P" - JC - Pin 9
#set_property IOSTANDARD LVCMOS33 [get_ports {DSPACE_VECTOR_DATA[9]}]
#set_property PACKAGE_PIN W20 [get_ports {DSPACE_VECTOR_DATA[8]}];  # "W20.JX1_LVDS_15_N.JX1.56.JC6-7_N" - JC - Pin 10
#set_property IOSTANDARD LVCMOS33 [get_ports {DSPACE_VECTOR_DATA[8]}]


# ----------------------------------------------------------------------------
# JD Pmod - Bank 34
# ----------------------------------------------------------------------------
#set_property PACKAGE_PIN R16 [get_ports {DSPACE_VECTOR_DATA[3]}];  # "R16.JX1_LVDS_18_P.JX1.67.JD0-1_P" - JD - Pin 1
#set_property IOSTANDARD LVCMOS33 [get_ports {DSPACE_VECTOR_DATA[3]}]
#set_property PACKAGE_PIN R17 [get_ports {DSPACE_VECTOR_DATA[2]}];  # "R17.JX1_LVDS_18_N.JX1.69.JD0-1_N" - JD - Pin 2
#set_property IOSTANDARD LVCMOS33 [get_ports {DSPACE_VECTOR_DATA[2]}]
#set_property PACKAGE_PIN T17 [get_ports {DSPACE_VECTOR_DATA[1]}];  # "T17.JX1_LVDS_19_P.JX1.68.JD2-3_P" - JD - Pin 3
#set_property IOSTANDARD LVCMOS33 [get_ports {DSPACE_VECTOR_DATA[1]}]
#set_property PACKAGE_PIN R18 [get_ports {DSPACE_VECTOR_DATA[0]}];  # "R18.JX1_LVDS_19_N.JX1.70.JD2-3_N" - JD - Pin 4
#set_property IOSTANDARD LVCMOS33 [get_ports {DSPACE_VECTOR_DATA[0]}]
#set_property PACKAGE_PIN V18 [get_ports {DSPACE_VECTOR_OK}];  # "V17.JX1_LVDS_20_P.JX1.73.JD4-5_P" - JD - Pin 7
#set_property IOSTANDARD LVCMOS33 [get_ports {DSPACE_VECTOR_OK}]
#set_property PACKAGE_PIN V18 [get_ports {JD4_5_N}];  # "V18.JX1_LVDS_20_N.JX1.75.JD4-5_N" - JD - Pin 8
#set_property IOSTANDARD LVCMOS33 [get_ports { }]
#set_property PACKAGE_PIN W18 [get_ports {JD6_7_P}];  # "W18.JX1_LVDS_21_P.JX1.74.JD6-7_P" - JD - Pin 9
#set_property IOSTANDARD LVCMOS33 [get_ports { }]
#set_property PACKAGE_PIN W19 [get_ports {JD6_7_N}];  # "W19.JX1_LVDS_21_N.JX1.76.JD6-7_N" - JD - Pin 10
#set_property IOSTANDARD LVCMOS33 [get_ports { }]


# ----------------------------------------------------------------------------
# JK Pmod - Bank 34
# ----------------------------------------------------------------------------
#set_property PACKAGE_PIN Y18 [get_ports {PWM_A_P[8]}];  # "Y18.JX1_LVDS_16_P.JX1.61.JK0-1_P" - JK - Pin 1
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[8]}]
#set_property PACKAGE_PIN Y19 [get_ports {PWM_A_N[8]}];  # "Y19.JX1_LVDS_16_N.JX1.63.JK0-1_N" - JK - Pin 2
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_N[8]}]
#set_property PACKAGE_PIN V16 [get_ports {PWM_B_P[8]}];  # "V16.JX1_LVDS_17_P.JX1.62.JK2-3_P" - JK - Pin 3
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_P[8]}]
#set_property PACKAGE_PIN W16 [get_ports {PWM_B_N[8]}];  # "W16.JX1_LVDS_17_N.JX1.64.JK2-3_N" - JK - Pin 4
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_N[8]}]
#set_property PACKAGE_PIN N17 [get_ports {PWM_A_P[9]}];  # "N17.JX1_LVDS_22_P.JX1.81.JK4-5_P" - JK - Pin 7
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[9]}]
#set_property PACKAGE_PIN P18 [get_ports {PWM_A_N[9]}];  # "P18.JX1_LVDS_22_N.JX1.83.JK4-5_N" - JK - Pin 8
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_N[9]}]
#set_property PACKAGE_PIN P15 [get_ports {PWM_B_P[9]}];  # "P15.JX1_LVDS_23_P.JX1.82.JK6-7_P" - JK - Pin 9
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_P[9]}]
#set_property PACKAGE_PIN P16 [get_ports {PWM_B_N[9]}];  # "P16.JX1_LVDS_23_N.JX1.84.JK6-7_N" - JK - Pin 10
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_N[9]}]


# ----------------------------------------------------------------------------
# JE Pmod - Bank 35
# ----------------------------------------------------------------------------
#set_property PACKAGE_PIN E17 [get_ports {PWM_A_P[6]}];  # "E17.JX2_LVDS_2_P.JX2.23.JE0-1_P" - JE - Pin 1
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[6]}]
#set_property PACKAGE_PIN D18 [get_ports {PWM_A_N[6]}];  # "D18.JX2_LVDS_2_N.JX2.25.JE0-1_N" - JE - Pin 2
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_N[6]}]
#set_property PACKAGE_PIN D19 [get_ports {PWM_B_P[6]}];  # "D19.JX2_LVDS_3_P.JX2.24.JE2-3_P" - JE - Pin 3
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_P[6]}]
#set_property PACKAGE_PIN D20 [get_ports {PWM_B_N[6]}];  # "D20.JX2_LVDS_3_N.JX2.26.JE2-3_N" - JE - Pin 4
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_N[6]}]
#set_property PACKAGE_PIN E18 [get_ports {PWM_A_P[7]}];  # "E18.JX2_LVDS_4_P.JX2.29.JE4-5_P" - JE - Pin 7
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[7]}]
#set_property PACKAGE_PIN E19 [get_ports {PWM_A_N[7]}];  # "E19.JX2_LVDS_4_N.JX2.31.JE4-5_N" - JE - Pin 8
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_N[7]}]
#set_property PACKAGE_PIN F16 [get_ports {PWM_B_P[7]}];  # "F17.JX2_LVDS_5_P.JX2.30.JE6-7_P" - JE - Pin 9
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_P[7]}]
#set_property PACKAGE_PIN F17 [get_ports {PWM_B_N[7]}];  # "F16.JX2_LVDS_5_N.JX2.32.JE6-7_N" - JE - Pin 10
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_N[7]}]


# ----------------------------------------------------------------------------
# JF Pmod - Bank 35
# ----------------------------------------------------------------------------
#set_property PACKAGE_PIN M19 [get_ports {PWM_A_P[4]}];  # "L19.JX2_LVDS_6_P.JX2.35.JF0-1_P" - JF - Pin 1
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[4]}]
#set_property PACKAGE_PIN M20 [get_ports {PWM_A_N[4]}];  # "L20.JX2_LVDS_6_N.JX2.37.JF0-1_N" - JF - Pin 2
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_N[4]}]
#set_property PACKAGE_PIN M17 [get_ports {PWM_B_P[4]}];  # "M19.JX2_LVDS_7_P.JX2.36.JF2-3_P" - JF - Pin 3
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_P[4]}]
#set_property PACKAGE_PIN M18 [get_ports {PWM_B_N[4]}];  # "M20.JX2_LVDS_7_N.JX2.38.JF2-3_N" - JF - Pin 4
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_N[4]}]
#set_property PACKAGE_PIN L19 [get_ports {PWM_A_P[5]}];  # "M17.JX2_LVDS_8_P.JX2.41.JF4-5_P" - JF - Pin 7
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[5]}]
#set_property PACKAGE_PIN L20 [get_ports {PWM_A_N[5]}];  # "M18.JX2_LVDS_8_N.JX2.43.JF4-5_N" - JF - Pin 8
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_N[5]}]
#set_property PACKAGE_PIN K19 [get_ports {PWM_B_P[5]}];  # "K19.JX2_LVDS_9_P.JX2.42.JF6-7_P" - JF - Pin 9
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_P[5]}]
#set_property PACKAGE_PIN J19 [get_ports {PWM_B_N[5]}];  # "J19.JX2_LVDS_9_N.JX2.44.JF6-7_N" - JF - Pin 10
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_N[5]}]


# ----------------------------------------------------------------------------
# JG Pmod - Bank 35
# ----------------------------------------------------------------------------
#set_property PACKAGE_PIN F19 [get_ports {PWM_A_P[2]}];  # "G17.JX2_LVDS_14_P.JX2.61.JG0-1_P" - JG - Pin 1
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[2]}]
#set_property PACKAGE_PIN F20 [get_ports {PWM_A_N[2]}];  # "G18.JX2_LVDS_14_N.JX2.63.JG0-1_N" - JG - Pin 2
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_N[2]}]
#set_property PACKAGE_PIN G17 [get_ports {PWM_B_P[2]}];  # "F19.JX2_LVDS_15_P.JX2.62.JG2-3_P" - JG - Pin 3
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_P[2]}]
#set_property PACKAGE_PIN G18 [get_ports {PWM_B_N[2]}];  # "F20.JX2_LVDS_15_N.JX2.64.JG2-3_N" - JG - Pin 4
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_N[2]}]
#set_property PACKAGE_PIN H16 [get_ports {PWM_A_P[3]}];  # "H16.JX2_LVDS_12_P.JX2.53.JG4-5_P" - JG - Pin 7
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[3]}]
#set_property PACKAGE_PIN H17 [get_ports {PWM_A_N[3]}];  # "H17.JX2_LVDS_12_N.JX2.55.JG4-5_N" - JG - Pin 8
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_N[3]}]
#set_property PACKAGE_PIN J18 [get_ports {PWM_B_P[3]}];  # "J18.JX2_LVDS_13_P.JX2.54.JG6-7_P" - JG - Pin 9
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_P[3]}]
#set_property PACKAGE_PIN H18 [get_ports {PWM_B_N[3]}];  # "H18.JX2_LVDS_13_N.JX2.56.JG6-7_N" - JG - Pin 10
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_N[3]}]


# ----------------------------------------------------------------------------
# JH Pmod - Bank 35
# ----------------------------------------------------------------------------
#set_property PACKAGE_PIN H15 [get_ports {PWM_A_P[0]}];  # "K14.JX2_LVDS_18_P.JX2.73.JH0-1_P" - JH - Pin 1
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[0]}]
#set_property PACKAGE_PIN G15 [get_ports {PWM_A_N[0]}];  # "J14.JX2_LVDS_18_N.JX2.75.JH0-1_N" - JH - Pin 2
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_N[0]}]
#set_property PACKAGE_PIN K14 [get_ports {PWM_B_P[0]}];  # "H15.JX2_LVDS_19_P.JX2.74.JH2-3_P" - JH - Pin 3
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_P[0]}]
#set_property PACKAGE_PIN J14 [get_ports {PWM_B_N[0]}];  # "G15.JX2_LVDS_19_N.JX2.76.JH2-3_N" - JH - Pin 4
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_N[0]}]
#set_property PACKAGE_PIN N15 [get_ports {PWM_A_P[1]}];  # "N15.JX2_LVDS_20_P.JX2.81.JH4-5_P" - JH - Pin 7
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_P[1]}]
#set_property PACKAGE_PIN N16 [get_ports {PWM_A_N[1]}];  # "N16.JX2_LVDS_20_N.JX2.83.JH4-5_N" - JH - Pin 8
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_A_N[1]}]
#set_property PACKAGE_PIN L14 [get_ports {PWM_B_P[1]}];  # "L14.JX2_LVDS_21_P.JX2.82.JH6-7_P" - JH - Pin 9
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_P[1]}]
#set_property PACKAGE_PIN L15 [get_ports {PWM_B_N[1]}];  # "L15.JX2_LVDS_21_N.JX2.84.JH6-7_N" - JH - Pin 10
#set_property IOSTANDARD LVCMOS33 [get_ports {PWM_B_N[1]}]

# ----------------------------------------------------------------------------
# JY Pmod - Bank 13 (Available on Z7020 device only)
# ---------------------------------------------------------------------------- 
# set_property PACKAGE_PIN U7 [get_ports {JY0_1_P}];  # "U7.BANK13_LVDS_0_P.JX1.87.JY0-1_P" - JY - Pin 1
# set_property PACKAGE_PIN V7 [get_ports {JY0_1_N}];  # "V7.BANK13_LVDS_0_N.JX1.89.JY0-1_N" - JY - Pin 2
# set_property PACKAGE_PIN T9 [get_ports {JY2_3_P}];  # "T9.BANK13_LVDS_1_P.JX1.88.JY2-3_P" - JY - Pin 3
# set_property PACKAGE_PIN U10 [get_ports {JY2_3_N}];  # "U10.BANK13_LVDS_1_N.JX1.90.JY2-3_N" - JY - Pin 4
# set_property PACKAGE_PIN V8 [get_ports {JY4_5_P}];  # "V8.BANK13_LVDS_2_P.JX1.91.JY4-5_P" - JY - Pin 7
# set_property PACKAGE_PIN W8 [get_ports {JY4_5_N}];  # "W8.BANK13_LVDS_2_N.JX1.93.JY4-5_N" - JY - Pin 8
# set_property PACKAGE_PIN T5 [get_ports {JY6_7_P}];  # "T5.BANK13_LVDS_3_P.JX1.92.JY6-7_P" - JY - Pin 9
# set_property PACKAGE_PIN U5 [get_ports {JY6_7_N}];  # "U5.BANK13_LVDS_3_N.JX1.94.JY6-7_N" - JY - Pin 10


# ----------------------------------------------------------------------------
# JZ Pmod - Bank 13 (Available on Z7020 device only)
# ---------------------------------------------------------------------------- 
# set_property PACKAGE_PIN Y12 [get_ports {JZ0_1_P}];  # "Y12.BANK13_LVDS_4_P.JX2.93.JZ0-1_P" - JZ - Pin 1
# set_property PACKAGE_PIN Y13 [get_ports {JZ0_1_N}];  # "Y13.BANK13_LVDS_4_N.JX2.95.JZ0-1_N" - JZ - Pin 2
# set_property PACKAGE_PIN V11 [get_ports {JZ2_3_P}];  # "V11.BANK13_LVDS_5_P.JX2.94.JZ2-3_P" - JZ - Pin 3
# set_property PACKAGE_PIN V10 [get_ports {JZ2_3_N}];  # "V10.BANK13_LVDS_5_N.JX2.96.JZ2-3_N" - JZ - Pin 4
# set_property PACKAGE_PIN V5 [get_ports {JZ5}];  # "V5.BANK13_SE_0.JX2.100.JZ5" - JZ - Pin 8
# set_property PACKAGE_PIN V6 [get_ports {JZ6_7_P}];  # "V6.BANK13_LVDS_6_P.JX2.97.JZ6-7_P" - JZ - Pin 9
# set_property PACKAGE_PIN W6 [get_ports {JZ6_7_N}];  # "W6.BANK13_LVDS_6_N.JX2.99.JZ6-7_N" - JZ - Pin 10


# ----------------------------------------------------------------------------
# XADC
# ---------------------------------------------------------------------------- 
# set_property PACKAGE_PIN M10 [get_ports {XADC_DX_N}];  # "M10.NetJX1_100.JX1.100.XADC_DX_N"
# set_property PACKAGE_PIN M9 [get_ports {XADC_DX_P}];  # "M9.NetJX1_98.JX1.98.XADC_DX_P"
# set_property PACKAGE_PIN L10 [get_ports {XADC_V_N}];  # "L10.NetJX1_99.JX1.99.XADC_V_N"
# set_property PACKAGE_PIN K9 [get_ports {XADC_V_P}];  # "K9.NetJX1_97.JX1.97.XADC_V_P"
# set_property PACKAGE_PIN B20 [get_ports {XADC_AD0_N}];  # "B20.JX2_LVDS_0_N.JX2.19.XADC_AD0_N"
# set_property PACKAGE_PIN C20 [get_ports {XADC_AD0_P}];  # "C20.JX2_LVDS_0_P.JX2.17.XADC_AD0_P"
# set_property PACKAGE_PIN A20 [get_ports {XADC_AD8_N}];  # "A20.JX2_LVDS_1_N.JX2.20.XADC_AD8_N"
# set_property PACKAGE_PIN B19 [get_ports {XADC_AD8_P}];  # "B19.JX2_LVDS_1_P.JX2.18.XADC_AD8_P"
# set_property PACKAGE_PIN L16 [get_ports {XADC_GIO0}];  # "L16.JX2_LVDS_10_P.JX2.47.XADC_GIO0"
# set_property PACKAGE_PIN G14 [get_ports {XADC_GIO1}];  # "G14.JX2_SE_0.JX2.13.XADC_GIO1"
# set_property PACKAGE_PIN J15 [get_ports {XADC_GIO2}];  # "J15.JX2_SE_1.JX2.14.XADC_GIO2"
# set_property PACKAGE_PIN L17 [get_ports {XADC_GIO3}];  # "L17.JX2_LVDS_10_N.JX2.49.XADC_GIO3"


# ----------------------------------------------------------------------------
# IOSTANDARD Constraints
#
# Note that these IOSTANDARD constraints are applied to all IOs currently
# assigned within an I/O bank.  If these IOSTANDARD constraints are 
# evaluated prior to other PACKAGE_PIN constraints being applied, then 
# the IOSTANDARD specified will likely not be applied properly to those 
# pins.  Therefore, bank wide IOSTANDARD constraints should be placed 
# within the XDC file in a location that is evaluated AFTER all 
# PACKAGE_PIN constraints within the target bank have been evaluated.
#
# Un-comment one or more of the following IOSTANDARD constraints according to
# the bank pin assignments that are required within a design.
# ---------------------------------------------------------------------------- 

# Set the bank voltage for IO Bank 34 to 3.3V by default.
# set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 34]]

# Set the bank voltage for IO Bank 35 to 3.3V by default.
# set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 35]]

# Set the bank voltage for IO Bank 13 to 3.3V by default. (I/O bank available on Z7020 device only)
# set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 13]]

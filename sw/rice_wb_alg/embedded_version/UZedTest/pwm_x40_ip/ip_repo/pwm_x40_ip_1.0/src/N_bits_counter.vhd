----------------------------------------------------------------------------------
-- Company: University of Vigo
-- Engineer: Luis Jacobo Alvarez Ruiz de Ojeda
-- 
-- Create Date:    12:30:59 12/10/2007 
-- Module Name:    N_bits_counter - Behavioral 
-- Target Devices: all
-- Tool versions: ISE 8.2
-- Description: N bits reversible counter with CE, asynchronous reset ("async_reset"), synchronous reset ("sync_reset"),
-- 				 parallel load, selection of the count direction, and programmable terminal counts, both asynchronous (TC) and synchronous (CEO)

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity N_bits_counter is
	 Generic (
		number_of_bits: integer range 0 to 1023:= 8; -- number of bits
		initial_state: integer range 0 to 1023:= 0; -- initial state of the counter
		last_state: integer range 0 to 100000:= 255 -- last state of the counter
	 );
    Port ( clk : in  STD_LOGIC; -- global clock
           async_reset : in  STD_LOGIC; -- global reset
           sync_reset : in  STD_LOGIC; -- synchronous reset
           ce : in  STD_LOGIC; -- clock enable
		   load : in  STD_LOGIC; -- load enable
		   dir : in  STD_LOGIC; -- selection of the count direction (0: descending; 1: ascending)
           d : in std_logic_vector (number_of_bits - 1 downto 0); -- data inputs
           q : out std_logic_vector (number_of_bits - 1 downto 0); -- data outputs
		   tc: out std_logic; -- asynchronous terminal count 
		   ceo: out std_logic -- synchronous terminal count 
				);
end N_bits_counter;

architecture Behavioral of N_bits_counter is

signal ctr_state: integer range 0 to ((2**number_of_bits) - 1):=0;

begin

q <= std_logic_vector (TO_UNSIGNED(ctr_state,number_of_bits));

process (clk, ce, load, dir, async_reset, sync_reset, d, ctr_state)
begin
   if async_reset='1' then  -- asynchronous reset
		ctr_state <= initial_state;
   elsif (clk'event and clk='1') then
	   if sync_reset='1' then -- synchronous reset
			ctr_state <= initial_state;
		elsif load = '1' then
			ctr_state <= TO_INTEGER(unsigned (d)); -- synchronous parallel load
		elsif ce = '1' then -- synchronous counting
				if dir ='0' then  -- Descending counting
		 			if ctr_state = initial_state then ctr_state <= last_state; -- Descending cyclic counting
					else ctr_state <= ctr_state - 1;
					end if;
				else	 -- Ascending counting		
                    if last_state = 0 then
                        ctr_state <= last_state;
		 			elsif ctr_state = last_state then ctr_state <= initial_state; -- Ascending cyclic counting
					else ctr_state <= ctr_state + 1;
					end if;
       		end if;
    	end if;
   end if;

-- Terminal count outputs
-- Asynchronous terminal count (TC)
	if (dir='0' and ctr_state = initial_state) or (dir='1' and ctr_state = last_state) then tc <= '1';
	else tc <= '0';
	end if;

-- Synchronous terminal count (CEO)
	if ce ='1' and ((dir='0' and ctr_state = initial_state) or (dir='1' and ctr_state = last_state)) then ceo <= '1';
	else ceo <= '0';
	end if;

end process;

end Behavioral;


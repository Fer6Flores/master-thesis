----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Fernando Flores Garcia
-- 
-- Create Date: 20.01.2018
-- Design Name: 
-- Module Name: testbench_pwm_x40_ip_v1_0 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: Vivado 2018.3
-- Description: Generic testbench to simulate user IPs using AXI-lite interface in Vivado IP packager
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

--------------------------------------------------
-- Adapt the following lines to your IP name
--------------------------------------------------
--library work;
--use work.pwm_x40_ip_v1_0; -- Change the name to match your user IP name

--------------------------------------------------
-- Leave the following lines unchanged
--------------------------------------------------

entity testbench_pwm_x40_ip_v1_0 is

----------------------------------------------
-- Adapt the following lines to your IP
----------------------------------------------

generic
(
  C_S_AXI_DATA_WIDTH             : integer              := 32;
  C_S_AXI_ADDR_WIDTH             : integer              := 6    -- This value must be the same of the matching parameter in your user IP
                                                                -- It represents a number of bits which is enough to represent the offset value of any of the user registers in your user IP
                                                                -- For instance, for 4 registers, the offset values are: 0, 4, and 8, all of which can be represented with 4 bits   
);
end testbench_pwm_x40_ip_v1_0;

architecture Behavioral of testbench_pwm_x40_ip_v1_0 is

-------------------------------------------------------------------------
-- Define here user signals which correspond with your IP user ports
-------------------------------------------------------------------------

	signal PWM_A_P : std_logic_vector(9 downto 0);
    signal PWM_B_P : std_logic_vector(9 downto 0);
    signal PWM_A_N : std_logic_vector(9 downto 0);
    signal PWM_B_N : std_logic_vector(9 downto 0);
    signal CLK_100MHZ : std_logic;
    signal EXTERNAL_RESET : std_logic := '1';

-------------------------------------------------------------------------
-- Define here user signals which you want to use in this testbench
-------------------------------------------------------------------------

    signal data_read : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0'); 

-------------------------------------------------------------------------
-- Declare here your user IP
-------------------------------------------------------------------------
	component pwm_x40_ip_v1_0 is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 6
		);
		port (
        -- Users to add ports here
		PWM_A_P : out std_logic_vector(9 downto 0);
        PWM_B_P : out std_logic_vector(9 downto 0);
        PWM_A_N : out std_logic_vector(9 downto 0);
        PWM_B_N : out std_logic_vector(9 downto 0);
        CLK_100MHZ : in std_logic;
        EXTERNAL_RESET : in std_logic;
		-- User ports ends
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component pwm_x40_ip_v1_0;

--------------------------------------------------
-- Leave the following lines unchanged
--------------------------------------------------
    
    -- Global Clock Signal
    signal S_AXI_ACLK                     :  std_logic := '0';
    -- Global Reset Signal. This Signal is Active LOW
    signal S_AXI_ARESETN                  :  std_logic := '0';
    -- Write address (issued by master, acceped by Slave)
    signal S_AXI_AWADDR                   :  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0) := (others => '0');
    -- Write channel Protection type. This signal indicates the
    -- privilege and security level of the transaction, and whether
    -- the transaction is a data access or an instruction access.
    -- 000 value recommended.
    -- Xilinx IP generally ignores (as slaves) or generates transactions (as masters)
    -- with Normal, Secure, and Data attributes.
    signal S_AXI_AWPROT                   : std_logic_vector(2 downto 0);
    -- Write address valid. This signal indicates that the master is signaling
    -- valid write address and control information.
    signal S_AXI_AWVALID                  :  std_logic := '0';
    -- Write address ready. This signal indicates that the slave is ready
    -- to accept an address and associated control signals.
    signal S_AXI_AWREADY                  : std_logic;
    -- Write data (issued by master, acceped by Slave)
    signal S_AXI_WDATA                    :  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    -- Write strobes. This signal indicates which byte lanes hold
    -- valid data. There is one write strobe bit for each eight
    -- bits of the write data bus. 
    signal S_AXI_WSTRB                    :  std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0) := (others => '0');
    -- Write valid. This signal indicates that valid write
    -- data and strobes are available.
    signal S_AXI_WVALID                   :  std_logic := '0';
    -- Write ready. This signal indicates that the slave
    -- can accept the write data.
    signal S_AXI_WREADY                   : std_logic;
    -- Write response. This signal indicates the status
    -- of the write transaction.
    signal S_AXI_BRESP                    : std_logic_vector(1 downto 0);
    -- Write response valid. This signal indicates that the channel
    -- is signaling a valid write response.
    signal S_AXI_BVALID                   : std_logic;
    -- Response ready. This signal indicates that the master
    -- can accept a write response.
    signal S_AXI_BREADY                   :  std_logic := '0';
    -- Read address (issued by master, accepted by Slave)
    signal S_AXI_ARADDR                   :  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0) := (others => '0');
    -- Protection type. This signal indicates the privilege
    -- and security level of the transaction, and whether the
    -- transaction is a data access or an instruction access.
    -- 000 value recommended.
    -- Xilinx IP generally ignore (as slaves) or generate transactions (as masters)
    -- with Normal, Secure, and Data attributes.
    signal S_AXI_ARPROT                   : std_logic_vector(2 downto 0);
    -- Read address valid. This signal indicates that the channel
    -- is signaling valid read address and control information.
    signal S_AXI_ARVALID                  :  std_logic := '0';
    -- Read address ready. This signal indicates that the slave is
    -- ready to accept an address and associated control signals.
    signal S_AXI_ARREADY                  : std_logic;
    -- Read data (issued by slave)
    signal S_AXI_RDATA                    : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    -- Read response. This signal indicates the status of the
    -- read transfer.
    signal S_AXI_RRESP                    : std_logic_vector(1 downto 0);
    -- Read valid. This signal indicates that the channel is
    -- signaling the required read data.
    signal S_AXI_RVALID                   : std_logic;
    -- Read ready. This signal indicates that the master can
    -- accept the read data and response information.
    signal S_AXI_RREADY                   :  std_logic := '0';
   

    Constant ClockPeriod : TIME := 10 ns;  -- Adapt to your system
    
begin

-------------------------------------------------------------------------
-- Instantiate here your user IP
-------------------------------------------------------------------------

pwm_x40_ip_v1_0_inst : pwm_x40_ip_v1_0
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S_AXI_ADDR_WIDTH
	)
	port map (
		PWM_A_P => PWM_A_P,
        PWM_B_P => PWM_B_P,
        PWM_A_N => PWM_A_N,
        PWM_B_N => PWM_B_N,
        CLK_100MHZ => CLK_100MHZ,
        EXTERNAL_RESET => EXTERNAL_RESET,
        S_AXI_ACLK		=> S_AXI_ACLK,
		S_AXI_ARESETN	=> S_AXI_ARESETN,
		S_AXI_AWADDR	=> S_AXI_AWADDR,
		S_AXI_AWPROT	=> S_AXI_AWPROT,
		S_AXI_AWVALID	=> S_AXI_AWVALID,
		S_AXI_AWREADY	=> S_AXI_AWREADY,
		S_AXI_WDATA	=> S_AXI_WDATA,
		S_AXI_WSTRB	=> S_AXI_WSTRB,
		S_AXI_WVALID	=> S_AXI_WVALID,
		S_AXI_WREADY	=> S_AXI_WREADY,
		S_AXI_BRESP	=> S_AXI_BRESP,
		S_AXI_BVALID	=> S_AXI_BVALID,
		S_AXI_BREADY	=> S_AXI_BREADY,
		S_AXI_ARADDR	=> S_AXI_ARADDR,
		S_AXI_ARPROT	=> S_AXI_ARPROT,
		S_AXI_ARVALID	=> S_AXI_ARVALID,
		S_AXI_ARREADY	=> S_AXI_ARREADY,
		S_AXI_RDATA	=> S_AXI_RDATA,
		S_AXI_RRESP	=> S_AXI_RRESP,
		S_AXI_RVALID	=> S_AXI_RVALID,
		S_AXI_RREADY	=> S_AXI_RREADY
	); 

     
--------------------------------------------------
-- Leave the following lines unchanged
--------------------------------------------------      

  -- Generate S_AXI_ACLK signal
  GENERATE_REFCLOCK : process
     begin
       wait for (ClockPeriod / 2);
       S_AXI_ACLK <= '1';
       wait for (ClockPeriod / 2);
       S_AXI_ACLK <= '0';
     end process;

-- Generate IO Carrier Bank 100 MHz clock signal
  GENERATE_100MHZ_CLOCK : process
     begin
       wait for (ClockPeriod / 2);
       CLK_100MHZ <= '1';
       wait for (ClockPeriod / 2);
       CLK_100MHZ <= '0';
     end process;

  -- Generate stimuli
  tb : PROCESS

     -- procedure to simulate master AXI Lite write data
     procedure AXI4_Lite_Write (address : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
                                data : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0)) is
     begin
        S_AXI_AWADDR<=address;
        S_AXI_WDATA<=data;
        S_AXI_AWPROT<= "000";
        S_AXI_WSTRB<=b"1111";       
        wait until S_AXI_ACLK= '0';
        S_AXI_AWVALID<='1';
        S_AXI_WVALID<='1';
        wait until (S_AXI_AWREADY and S_AXI_WREADY) = '1';  --Client ready to read address/data        
        S_AXI_BREADY<='1';
        wait until S_AXI_BVALID = '1';  -- Write result valid
        assert S_AXI_BRESP = "00" report "AXI data not written" severity failure;
        S_AXI_AWVALID<='0';
        S_AXI_WVALID<='0';
        S_AXI_BREADY<='1';
        wait until S_AXI_BVALID = '0';  -- All finished
        S_AXI_BREADY<='0';
        S_AXI_WSTRB<=b"0000";
     end AXI4_Lite_Write;
        
     -- procedure to simulate master AXI Lite read data
     procedure AXI4_Lite_Read (address : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
                               signal data : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0)) is
     begin
        S_AXI_ARADDR<=address;
        S_AXI_ARPROT<= "000";
        wait until S_AXI_ACLK= '0';
        S_AXI_ARVALID<='1';
        S_AXI_RREADY<='1';
        wait until (S_AXI_RVALID) = '1'; -- and S_AXI_ARREADY) = '1';  --Client provided data
        assert S_AXI_RRESP = "00" report "AXI data not written" severity failure;
        data<=S_AXI_RDATA;
        S_AXI_ARVALID<='0';
        wait until S_AXI_RVALID = '0';
        S_AXI_RREADY<='0';               
     end AXI4_Lite_Read;    
 
 BEGIN

-------------------------------------------------------------------------
-- Define here your stimuli
-------------------------------------------------------------------------

-------------------------------------------------------------------------
-- Begin by activating the AXI reset signal (low-level active signal) 
-------------------------------------------------------------------------
    S_AXI_ARESETN<='0';
    wait for 40 ns;
    S_AXI_ARESETN<='1';
    EXTERNAL_RESET <= '1';
    wait for 5*ClockPeriod;
    EXTERNAL_RESET <= '0';
-------------------------------------------------------------------------
-- Drive your IP user input ports
-------------------------------------------------------------------------

-- The PWM peripheral does not have any user input ports
    
    -- Example
    -- sdata_first_adc <= '1';

-------------------------------------------------------------------------
-- Simulate write operations to your user IP
-------------------------------------------------------------------------

    AXI4_Lite_Write("000000", x"00000011"); -- write slv_reg0
    AXI4_Lite_Write("000100", x"33000033"); -- write slv_reg1
    AXI4_Lite_Write("001000", x"55000055"); -- write slv_reg2
    AXI4_Lite_Write("001100", x"00000077"); -- write slv_reg3
    AXI4_Lite_Write("010000", x"90000099"); -- write slv_reg4
    EXTERNAL_RESET <= '1';
    wait for 5*ClockPeriod;
    EXTERNAL_RESET <= '0';
    AXI4_Lite_Write("010100", x"BB000000"); -- write slv_reg5
    AXI4_Lite_Write("011000", x"DD000000"); -- write slv_reg6
    AXI4_Lite_Write("011100", x"FF000000"); -- write slv_reg7
    AXI4_Lite_Write("100000", x"00000000"); -- write slv_reg8
    AXI4_Lite_Write("100100", x"22000000"); -- write slv_reg9
    
-------------------------------------------------------------------------
-- Simulate read operations of your user IP
-------------------------------------------------------------------------

    -- AXI4_Lite_Read("000000", data_read); -- read slv_reg0    

    -- Another example
    -- read slv_reg0 until signal bit 31 of slave register 0 goes high
    -- while data_read(31) = '0' loop
    --    AXI4_Lite_Read(x"0", data_read); -- read slv_reg0
    -- end loop;

    wait for 50 ns; -- wait needed for end_of_conv signal go low from previous acquisition
    
    wait for 10000 us;
    
    assert (false) report "Fin simulacion. NO es un error" severity failure;
    
 END PROCESS tb;

end Behavioral;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pwm_x40_ip_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S_AXI
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 6
	);
	port (
		-- Users to add ports here
		PWM_A_P : out std_logic_vector(9 downto 0);
        PWM_B_P : out std_logic_vector(9 downto 0);
        PWM_A_N : out std_logic_vector(9 downto 0);
        PWM_B_N : out std_logic_vector(9 downto 0);
        CLK_100MHZ : in std_logic;
        EXTERNAL_RESET : in std_logic;
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S_AXI
		s_axi_aclk	: in std_logic;
		s_axi_aresetn	: in std_logic;
		s_axi_awaddr	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_awprot	: in std_logic_vector(2 downto 0);
		s_axi_awvalid	: in std_logic;
		s_axi_awready	: out std_logic;
		s_axi_wdata	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		s_axi_wstrb	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		s_axi_wvalid	: in std_logic;
		s_axi_wready	: out std_logic;
		s_axi_bresp	: out std_logic_vector(1 downto 0);
		s_axi_bvalid	: out std_logic;
		s_axi_bready	: in std_logic;
		s_axi_araddr	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_arprot	: in std_logic_vector(2 downto 0);
		s_axi_arvalid	: in std_logic;
		s_axi_arready	: out std_logic;
		s_axi_rdata	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		s_axi_rresp	: out std_logic_vector(1 downto 0);
		s_axi_rvalid	: out std_logic;
		s_axi_rready	: in std_logic
	);
end pwm_x40_ip_v1_0;

architecture arch_imp of pwm_x40_ip_v1_0 is

    -- Constants
    constant counters_N : integer := 13;
    constant pwm_counters_range : integer := 5000;
    constant counter_phase0_load_value : std_logic_vector(counters_N - 1 downto 0) :=   "0000000000000";
    constant counter_phase90_load_value : std_logic_vector(counters_N - 1 downto 0) :=  "0100111000100";
    constant counter_phase180_load_value : std_logic_vector(counters_N - 1 downto 0) := "1001110001000";
    constant counter_phase270_load_value : std_logic_vector(counters_N - 1 downto 0) := "0100111000100";
    
    -- Internal signals
    signal x_mod_0_i : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    signal x_mod_1_i : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    signal x_mod_2_i : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    signal x_mod_3_i : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    signal x_mod_4_i : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    signal x_mod_5_i : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    signal x_mod_6_i : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    signal x_mod_7_i : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    signal x_mod_8_i : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    signal x_mod_9_i : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    
    signal clk_100MHz_i : std_logic := '0';
    
    signal pwm_a_p_i : std_logic_vector(9 downto 0) := (others => '0');
    signal pwm_b_p_i : std_logic_vector(9 downto 0) := (others => '0');
    
    signal external_reset_i : std_logic := '1';
    signal counter_phase0_output : std_logic_vector(counters_N - 1 downto 0) := (others => '0');
    signal counter_phase90_output : std_logic_vector(counters_N - 1 downto 0) := (others => '0');
    signal counter_phase180_output : std_logic_vector(counters_N - 1 downto 0) := (others => '0');
    signal counter_phase270_output : std_logic_vector(counters_N - 1 downto 0) := (others => '0');
    signal counter_phase0_tc : std_logic := '0';
    signal counter_phase90_tc : std_logic := '0';
    signal counter_phase180_tc : std_logic := '0';
    signal counter_phase270_tc : std_logic := '0';
    signal counter_phase0_dir : std_logic := '1';
    signal counter_phase90_dir : std_logic := '0';
    signal counter_phase180_dir : std_logic := '0';
    signal counter_phase270_dir : std_logic := '1';
    
	-- component declaration
	component pwm_x40_ip_v1_0_S_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 6
		);
		port (
		X_MOD_0 : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        X_MOD_1 : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        X_MOD_2 : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        X_MOD_3 : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        X_MOD_4 : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        X_MOD_5 : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        X_MOD_6 : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        X_MOD_7 : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        X_MOD_8 : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        X_MOD_9 : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
        EXTERNAL_RESET : in std_logic;
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component pwm_x40_ip_v1_0_S_AXI;


    component N_bits_counter is
	   Generic (
	   number_of_bits: integer range 0 to 1023:= 8; -- number of bits
	   initial_state: integer range 0 to 1023:= 0; -- initial state of the counter
	   last_state: integer range 0 to 100000:= pwm_counters_range -- last state of the counter
	   );
       Port ( clk : in  STD_LOGIC; -- global clock
       async_reset : in  STD_LOGIC; -- global reset
       sync_reset : in  STD_LOGIC; -- synchronous reset
       ce : in  STD_LOGIC; -- clock enable
	   load : in  STD_LOGIC; -- load enable
	   dir : in  STD_LOGIC; -- selection of the count direction (0: descending; 1: ascending)
       d : in std_logic_vector (number_of_bits - 1 downto 0); -- data inputs
       q : out std_logic_vector (number_of_bits - 1 downto 0); -- data outputs
       tc: out std_logic; -- asynchronous terminal count 
	   ceo: out std_logic -- synchronous terminal count 
	   );
    end component N_bits_counter;

begin

-- Signals assignments
clk_100MHz_i <= CLK_100MHZ;
reset_sync : process (EXTERNAL_RESET, clk_100MHz_i) is
begin
    if rising_edge(clk_100MHz_i) then
        external_reset_i <= EXTERNAL_RESET;
    else 
        external_reset_i <= external_reset_i;
    end if;
end process;


-- Instantiation of Axi Bus Interface S_AXI
pwm_x40_ip_v1_0_S_AXI_inst : pwm_x40_ip_v1_0_S_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S_AXI_ADDR_WIDTH
	)
	port map (
	    X_MOD_0 => x_mod_0_i,
	    X_MOD_1 => x_mod_1_i,
	    X_MOD_2 => x_mod_2_i,
	    X_MOD_3 => x_mod_3_i,
	    X_MOD_4 => x_mod_4_i,
	    X_MOD_5 => x_mod_5_i,
	    X_MOD_6 => x_mod_6_i,
	    X_MOD_7 => x_mod_7_i,
	    X_MOD_8 => x_mod_8_i,
	    X_MOD_9 => x_mod_9_i,
	    EXTERNAL_RESET => external_reset_i,
		S_AXI_ACLK	=> s_axi_aclk,
		S_AXI_ARESETN	=> s_axi_aresetn,
		S_AXI_AWADDR	=> s_axi_awaddr,
		S_AXI_AWPROT	=> s_axi_awprot,
		S_AXI_AWVALID	=> s_axi_awvalid,
		S_AXI_AWREADY	=> s_axi_awready,
		S_AXI_WDATA	=> s_axi_wdata,
		S_AXI_WSTRB	=> s_axi_wstrb,
		S_AXI_WVALID	=> s_axi_wvalid,
		S_AXI_WREADY	=> s_axi_wready,
		S_AXI_BRESP	=> s_axi_bresp,
		S_AXI_BVALID	=> s_axi_bvalid,
		S_AXI_BREADY	=> s_axi_bready,
		S_AXI_ARADDR	=> s_axi_araddr,
		S_AXI_ARPROT	=> s_axi_arprot,
		S_AXI_ARVALID	=> s_axi_arvalid,
		S_AXI_ARREADY	=> s_axi_arready,
		S_AXI_RDATA	=> s_axi_rdata,
		S_AXI_RRESP	=> s_axi_rresp,
		S_AXI_RVALID	=> s_axi_rvalid,
		S_AXI_RREADY	=> s_axi_rready
	);

-- Instantiation of counter C0
counter_phase0 : N_bits_counter
    generic map (
        number_of_bits  => counters_N,
        initial_state   => 0,
        last_state      => pwm_counters_range
    )
    port map (
        clk => clk_100MHz_i,
        async_reset => '0',
        sync_reset => '0',
        ce => '1',               
        load => external_reset_i,
        dir => counter_phase0_dir,                         
        d => counter_phase0_load_value,
        q => counter_phase0_output,
        tc => counter_phase0_tc,
        ceo => open
    );

-- Instantiation of counter C1
counter_phase90 : N_bits_counter
    generic map (
        number_of_bits  => counters_N,
        initial_state   => 0,
        last_state      => pwm_counters_range
    )
    port map (
        clk => clk_100MHz_i,
        async_reset => '0',
        sync_reset => '0',
        ce => '1',                 
        load => external_reset_i,
        dir => counter_phase90_dir,                              
        d => counter_phase90_load_value,
        q => counter_phase90_output,
        tc => counter_phase90_tc,
        ceo => open
    );
    
-- Instantiation of counter C2
counter_phase180 : N_bits_counter
    generic map (
        number_of_bits  => counters_N,
        initial_state   => 0,
        last_state      => pwm_counters_range
    )
    port map (
        clk => clk_100MHz_i,
        async_reset => '0',
        sync_reset => '0',
        ce => '1',                        
        load => external_reset_i,
        dir => counter_phase180_dir,                               
        d => counter_phase180_load_value,
        q => counter_phase180_output,
        tc => counter_phase180_tc,
        ceo => open
    );
    
-- Instantiation of counter C3
counter_phase270 : N_bits_counter
    generic map (
        number_of_bits  => counters_N,
        initial_state   => 0,
        last_state      => pwm_counters_range
    )
    port map (
        clk => clk_100MHz_i,
        async_reset => '0',
        sync_reset => '0',
        ce => '1',                       
        load => external_reset_i,
        dir => counter_phase270_dir,
        d => counter_phase270_load_value,
        q => counter_phase270_output,
        tc => counter_phase270_tc,
        ceo => open
    );
    
	-- Add user logic here
   
    -- PWM counters direction toggle
    process(counter_phase0_tc, counter_phase90_tc, counter_phase180_tc, counter_phase270_tc, external_reset_i)
    begin
        if (external_reset_i = '1') then
            counter_phase0_dir <= '1';
            counter_phase90_dir <= '0';
            counter_phase180_dir <= '0';
            counter_phase270_dir <= '1';
        else
            if rising_edge(counter_phase0_tc) then
                counter_phase0_dir <= not counter_phase0_dir;
            else
                counter_phase0_dir <= counter_phase0_dir;
            end if;
            
            if rising_edge(counter_phase90_tc) then
                counter_phase90_dir <= not counter_phase90_dir;
            else
                counter_phase90_dir <= counter_phase90_dir;
            end if;
            
            if rising_edge(counter_phase180_tc) then
                counter_phase180_dir <= not counter_phase180_dir;
            else
                counter_phase180_dir <= counter_phase180_dir;
            end if;
            
            if rising_edge(counter_phase270_tc) then
                counter_phase270_dir <= not counter_phase270_dir;
            else
                counter_phase270_dir <= counter_phase270_dir;
            end if;
        end if;
    end process;
    
    -- PWM generation
    process(counter_phase0_output, counter_phase90_output, counter_phase180_output, counter_phase270_output, x_mod_0_i, x_mod_1_i,
            x_mod_2_i, x_mod_3_i, x_mod_4_i, x_mod_5_i, x_mod_6_i, x_mod_7_i, x_mod_8_i, x_mod_9_i)
    begin
        -- x_mod[0]
        if (counter_phase0_output < x_mod_0_i(counters_N downto 0)) then pwm_a_p_i(0) <= '1';
        else pwm_a_p_i(0) <= '0';
        end if;
        if (counter_phase180_output < x_mod_0_i(counters_N downto 0)) then pwm_b_p_i(0) <= '1';
        else pwm_b_p_i(0) <= '0';
        end if;
        -- x_mod[1]
        if (counter_phase90_output < x_mod_1_i(counters_N downto 0)) then pwm_a_p_i(1) <= '1';
        else pwm_a_p_i(1) <= '0';
        end if;
        if (counter_phase270_output < x_mod_1_i(counters_N downto 0)) then pwm_b_p_i(1) <= '1';
        else pwm_b_p_i(1) <= '0';
        end if;
        
        -- x_mod[2]
        if (counter_phase0_output < x_mod_2_i(counters_N downto 0)) then pwm_a_p_i(2) <= '1';
        else pwm_a_p_i(2) <= '0';
        end if;
        if (counter_phase180_output < x_mod_2_i(counters_N downto 0)) then pwm_b_p_i(2) <= '1';
        else pwm_b_p_i(2) <= '0';
        end if;
        -- x_mod[3]
        if (counter_phase90_output < x_mod_3_i(counters_N downto 0)) then pwm_a_p_i(3) <= '1';
        else pwm_a_p_i(3) <= '0';
        end if;
        if (counter_phase270_output < x_mod_3_i(counters_N downto 0)) then pwm_b_p_i(3) <= '1';
        else pwm_b_p_i(3) <= '0';
        end if;
        
        -- x_mod[4]
        if (counter_phase0_output < x_mod_4_i(counters_N downto 0)) then pwm_a_p_i(4) <= '1';
        else pwm_a_p_i(4) <= '0';
        end if;
        if (counter_phase180_output < x_mod_4_i(counters_N downto 0)) then pwm_b_p_i(4) <= '1';
        else pwm_b_p_i(4) <= '0';
        end if;
        -- x_mod[5]
        if (counter_phase90_output < x_mod_5_i(counters_N downto 0)) then pwm_a_p_i(5) <= '1';
        else pwm_a_p_i(5) <= '0';
        end if;
        if (counter_phase270_output < x_mod_5_i(counters_N downto 0)) then pwm_b_p_i(5) <= '1';
        else pwm_b_p_i(5) <= '0';
        end if;
        
        -- x_mod[6]
        if (counter_phase0_output < x_mod_6_i(counters_N downto 0)) then pwm_a_p_i(6) <= '1';
        else pwm_a_p_i(6) <= '0';
        end if;
        if (counter_phase180_output < x_mod_6_i(counters_N downto 0)) then pwm_b_p_i(6) <= '1';
        else pwm_b_p_i(6) <= '0';
        end if;
        -- x_mod[7]
        if (counter_phase90_output < x_mod_7_i(counters_N downto 0)) then pwm_a_p_i(7) <= '1';
        else pwm_a_p_i(7) <= '0';
        end if;
        if (counter_phase270_output < x_mod_7_i(counters_N downto 0)) then pwm_b_p_i(7) <= '1';
        else pwm_b_p_i(7) <= '0';
        end if;
        
        -- x_mod[8]
        if (counter_phase0_output < x_mod_8_i(counters_N downto 0)) then pwm_a_p_i(8) <= '1';
        else pwm_a_p_i(8) <= '0';
        end if;
        if (counter_phase180_output < x_mod_8_i(counters_N downto 0)) then pwm_b_p_i(8) <= '1';
        else pwm_b_p_i(8) <= '0';
        end if;
        -- x_mod[9]
        if (counter_phase90_output < x_mod_9_i(counters_N downto 0)) then pwm_a_p_i(9) <= '1';
        else pwm_a_p_i(9) <= '0';
        end if;
        if (counter_phase270_output < x_mod_9_i(counters_N downto 0)) then pwm_b_p_i(9) <= '1';
        else pwm_b_p_i(9) <= '0';
        end if;
    end process;
    
    -- PWM outputs assignment
    process(pwm_a_p_i, pwm_b_p_i, EXTERNAL_RESET)
    begin
        if (EXTERNAL_RESET = '1') then
            PWM_A_P <= (others => '0');
            PWM_B_P <= (others => '0');
            PWM_A_N <= (others => '0');
            PWM_B_N <= (others => '0');
        else
            PWM_A_P <= pwm_a_p_i;
            PWM_B_P <= pwm_b_p_i;
            PWM_A_N <= not pwm_a_p_i;
            PWM_B_N <= not pwm_b_p_i;
        end if;
    end process;
    
	-- User logic ends

end arch_imp;

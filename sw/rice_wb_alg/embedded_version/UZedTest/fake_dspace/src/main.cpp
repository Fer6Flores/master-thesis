#include <Arduino.h>

#define PIN_DATA_OK 22
#define PIN_DATA_INIT 24
#define PIN_DATA_VECTOR_0 28
#define PIN_DATA_VECTOR_1 30
#define PIN_DATA_VECTOR_2 32
#define PIN_DATA_VECTOR_3 34
#define PIN_DATA_VECTOR_4 36
#define PIN_DATA_VECTOR_5 38
#define PIN_DATA_VECTOR_6 40
#define PIN_DATA_VECTOR_7 42
#define PIN_DATA_VECTOR_8 44
#define PIN_DATA_VECTOR_9 46
#define PIN_DATA_VECTOR_10 48
#define PIN_DATA_VECTOR_11 50

void setup() {
  // put your setup code here, to run once:
  pinMode(PIN_DATA_OK, OUTPUT);
  pinMode(PIN_DATA_INIT, INPUT);
  pinMode(PIN_DATA_VECTOR_0, OUTPUT);
  pinMode(PIN_DATA_VECTOR_1, OUTPUT);
  pinMode(PIN_DATA_VECTOR_2, OUTPUT);
  pinMode(PIN_DATA_VECTOR_3, OUTPUT);
  pinMode(PIN_DATA_VECTOR_4, OUTPUT);
  pinMode(PIN_DATA_VECTOR_5, OUTPUT);
  pinMode(PIN_DATA_VECTOR_6, OUTPUT);
  pinMode(PIN_DATA_VECTOR_7, OUTPUT);
  pinMode(PIN_DATA_VECTOR_8, OUTPUT);
  pinMode(PIN_DATA_VECTOR_9, OUTPUT);
  pinMode(PIN_DATA_VECTOR_10, OUTPUT);
  pinMode(PIN_DATA_VECTOR_11, OUTPUT);

  digitalWrite(PIN_DATA_OK, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:

  if (digitalRead(PIN_DATA_INIT))
{
 
    delayMicroseconds(10);
    digitalWrite(PIN_DATA_OK, HIGH);
    digitalWrite(PIN_DATA_VECTOR_0, HIGH);
    digitalWrite(PIN_DATA_VECTOR_1, LOW);
    digitalWrite(PIN_DATA_VECTOR_2, HIGH);
    digitalWrite(PIN_DATA_VECTOR_3, LOW);
    digitalWrite(PIN_DATA_VECTOR_4, HIGH);
    digitalWrite(PIN_DATA_VECTOR_5, LOW);
    digitalWrite(PIN_DATA_VECTOR_6, HIGH);
    digitalWrite(PIN_DATA_VECTOR_7, LOW);
    digitalWrite(PIN_DATA_VECTOR_8, HIGH);
    digitalWrite(PIN_DATA_VECTOR_9, LOW);
    digitalWrite(PIN_DATA_VECTOR_10, HIGH);
    digitalWrite(PIN_DATA_VECTOR_11, LOW);
    delayMicroseconds(10);
    digitalWrite(PIN_DATA_OK, LOW);

    delayMicroseconds(10);
    digitalWrite(PIN_DATA_OK, HIGH);
    digitalWrite(PIN_DATA_VECTOR_0, LOW);
    digitalWrite(PIN_DATA_VECTOR_1, HIGH);
    digitalWrite(PIN_DATA_VECTOR_2, LOW);
    digitalWrite(PIN_DATA_VECTOR_3, HIGH);
    digitalWrite(PIN_DATA_VECTOR_4, LOW);
    digitalWrite(PIN_DATA_VECTOR_5, HIGH);
    digitalWrite(PIN_DATA_VECTOR_6, LOW);
    digitalWrite(PIN_DATA_VECTOR_7, HIGH);
    digitalWrite(PIN_DATA_VECTOR_8, LOW);
    digitalWrite(PIN_DATA_VECTOR_9, HIGH);
    digitalWrite(PIN_DATA_VECTOR_10, LOW);
    digitalWrite(PIN_DATA_VECTOR_11, HIGH);
    delayMicroseconds(10);
    digitalWrite(PIN_DATA_OK, LOW);
   
  }
}
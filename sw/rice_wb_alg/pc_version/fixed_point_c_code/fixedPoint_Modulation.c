/**
 * @file fixedPoint_Modulation.c
 * @author Fernando Flores (Fer6Flores)
 * @date March 4, 2021
 * @brief Fixed-point Modulation file
 */

/* Libraries */
#include <stdint.h>                // For using uint8_t and more types
#include <stdlib.h>                // For pointers
#include <math.h>                  // Math function
#include "fixedPoint_Modulation.h" // Modulation function prototype
#include "fixedPoint_paramFault.h" // Stored matrix and arrays prepared offline

/**
 * @brief Modulation function to be implemented in the ARM.
 * @param Vafbt Alfa-beta voltage input. 
 * @return The 10 modulation signals for a 5-phases converter.
 * @warning The output is scaled by a 24-bits shift to the right.
 */
int32_t *Modulation(int32_t Vafbt[2])
{
    /* Program inits */
    uint8_t i = 0;
    uint8_t j = 0;

    /* Algorithm inits */
    int32_t *x_mod = (int32_t *)malloc(10 * sizeof(int32_t));
    uint32_t yu_all[10] = {0};
    int32_t yu_all_tmp[10] = {0};
    uint32_t yu0 = 0;
    uint8_t n_u = 0;
    int32_t u0[3] = {0};
    int32_t A2[3][8] = {0};
    int32_t x2[8] = {0};
    int32_t x2_threshold = 1;
    int32_t A1x1[3] = {0};
    int32_t A1_ls[2][3] = {0};
    int32_t x1[2] = {0};
    int32_t x_temp[10] = {0};
    uint8_t ind[10] = {0};
    int32_t tmp = 0;

    // Fault-tolerant mode
    // y is vector of known variables - actual vector Vafbt and 0 (last row)
    // for fault-tolerant mode
    int32_t y[3] = {Vafbt[0], Vafbt[1], 0};

    for (uint8_t i = 0; i < 5; i++)
    {
        for (uint8_t j = 0; j < 3; j++)
        {
            yu_all_tmp[i] += (int64_t)y[j] * (int64_t)u_all[j][i] >> (SHIFT_MASK_24);
        }
        yu_all[i] = (uint32_t)abs(yu_all_tmp[i]);
    }

    for (uint8_t i = 0; i < 10; i++)
    {
        if (yu_all[i] > yu0 + 1)
        {
            yu0 = yu_all[i];
            n_u = i;
        }
    }

    // Note: Final y'u0 is the minimum inf. norm value.
    for (uint8_t j = 0; j < 3; j++)
    {
        u0[j] = (Vafbt[1] < 0 || (Vafbt[1] == 0 && Vafbt[0] < 0)) ? -u_all[j][n_u] : u_all[j][n_u];
    }

    for (uint8_t i = 0; i < 3; i++)
    {
        for (uint8_t j = 0; j < 8; j++)
        {
            A2[i][j] = A2_all[n_u][i][j];
        }
    }

    for (uint8_t i = 0; i < 8; i++)
    {
        for (uint8_t j = 0; j < 3; j++)
        {
            x2[i] += (int64_t)A2[j][i] * (int64_t)u0[j] >> SHIFT_MASK_24;
        }

        if (x2[i] > x2_threshold)
        {
            x2[i] = (int32_t)yu0;
        }
        else if (x2[i] < -x2_threshold)
        {
            x2[i] = -(int32_t)yu0;
        }
        else
        {
            x2[i] = 0;
        }
    }

    for (uint8_t i = 0; i < 3; i++)
    {
        for (uint8_t j = 0; j < 8; j++)
        {
            A1x1[i] += (int64_t)A2[i][j] * (int64_t)x2[j] >> SHIFT_MASK_24;
        }
        A1x1[i] = y[i] - A1x1[i];
    }

    for (uint8_t i = 0; i < 2; i++)
    {
        for (uint8_t j = 0; j < 3; j++)
        {
            A1_ls[i][j] = A1_ls_all[n_u][i][j];
        }
    }

    for (uint8_t i = 0; i < 2; i++)
    {
        for (uint8_t j = 0; j < 3; j++)
        {
            x1[i] += (int64_t)A1_ls[i][j] * (int64_t)A1x1[j] >> SHIFT_MASK_24;
        }
    }

    for (uint8_t i = 0; i < 10; i++)
    {
        x_temp[i] = (i < 2) ? x1[i] : x2[i - 2];
    }

    for (uint8_t i = 0; i < 10; i++)
    {
        ind[i] = ind_all[n_u][i];
    }

    for (uint8_t i = 0; i < 10; i++)
    {
        x_mod[(int)ind[i]] = x_temp[i];
    }

    for (uint8_t i = 2; i < 9; i += 2)
    {
        tmp = 0.5 * (x_mod[i] + x_mod[i + 1]);
        x_mod[i] = tmp;
        x_mod[i + 1] = tmp;
    }
    
    return x_mod;
}

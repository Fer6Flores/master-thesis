% MININFNORM_SCRIPT   Perform fixed-point conversion on MinInfNorm.
% 
% Script generated from project 'MinInfNorm.prj' on 26-Feb-2021.
% 
% See also fixedPointConverter, CODER.CONFIG, CODER.TYPEOF, fiaccel

%% Create configuration object of class 'coder.FixPtConfig'.
cfg = coder.config('fixpt');

cfg.TestBenchName = { 'MinInfNorm_tb.m' };
cfg.DefaultWordLength = 12;
cfg.DefaultFractionLength = 9;
cfg.SafetyMargin = 5;
cfg.fimath = sprintf('fimath(''RoundingMethod'', ''Floor'', ''OverflowAction'', ''Wrap'', ''ProductMode'', ''FullPrecision'', ''SumMode'', ''FullPrecision'')');
cfg.LogIOForComparisonPlotting = true;
cfg.TestNumerics = true;
cfg.DetectFixptOverflows = true;

cfg.addTypeSpecification('MinInfNorm', 'Vafbt', numerictype(1,12,9));
cfg.addTypeSpecification('MinInfNorm', 'x_mod', numerictype(1,12,9));

%% Define argument types for entry-point 'MinInfNorm'.
ARGS = cell(1,1);
ARGS{1} = cell(1,1);
ARGS{1}{1} = coder.typeof(0,[2 1]);

%% Define global types and initial values.
GLOBALS = cell(4,2);
GLOBALS{1,1} = coder.typeof(uint8(0),[5 10]);
GLOBALS{1,2} = ind_all;
GLOBALS{2,1} = coder.typeof(0,[2 3 5]);
GLOBALS{2,2} = A1_ls_all;
GLOBALS{3,1} = coder.typeof(0,[3 8 5]);
GLOBALS{3,2} = A2_all;
GLOBALS{4,1} = coder.typeof(0,[3 5]);
GLOBALS{4,2} = u_all;

%% Invoke fixed-point conversion.
fiaccel -float2fixed cfg -globals {'ind_all',GLOBALS(1,:), 'A1_ls_all',GLOBALS(2,:), 'A2_all',GLOBALS(3,:), 'u_all',GLOBALS(4,:)} MinInfNorm -args ARGS{1}


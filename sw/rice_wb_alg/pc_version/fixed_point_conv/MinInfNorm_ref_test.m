%% MinInfNorm_ref test file
% Script to compare the test the of the function |MinInfNorm_ref()| with
% the data of the reference case.

%%
clearvars
disp([newline, 'Running ''', mfilename, '.m''...']);

%% Loading reference case
load Reference_case.mat

%% Comparison
for k=1:length(Vafbtk)
    Vafbt = Vafbtk(:,k);
    x_mod = MinInfNorm_ref(Vafbt);
    
    if norm(x_mod-x_modk(:,k))>1e-9
        disp(Vafbt)
        error('The result of the core function is different from reference case.')
    end
end

disp('The results of core function are equat to the reference case.')
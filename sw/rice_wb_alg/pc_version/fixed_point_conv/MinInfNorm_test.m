%% MinInfNorm test
% Script to verify that the adapted function |MinInfNorm()| provides the
% same results as the reference function |MinInfNorm_ref()|.

%%
clearvars

global u_all; %#ok
global A1_ls_all; %#ok
global A2_all; %#ok
global ind_all;
load param_fault A1_ls_all A2_all u_all ind_all
ind_all=uint8(ind_all);

disp([newline, 'Running ''', mfilename, '.m''...']);

%% Functions comparison
N=1000; % Number of data test points

tic
for k=1:N
    Vafbt=rand(2,1)*5-2;
    if(norm(MinInfNorm_ref(Vafbt)-MinInfNorm(Vafbt))>1e-9)
        Vafbt %#ok
        error('Results of the adapted and reference functions are different.')
    end
end
disp('No differences found in the results provided by the adapted and the reference functions')
toc
% 30. 6. 2020
% Minimum Infinity Norm
% Fault-tolerand five-phase five-level CHB converters
% ---
% Entity ---


disp([newline, 'Running ''', mfilename, '.m''...']); 

% init - load stored matrices and vectors prepared offline (A1_ls, A2, u, ind)
load param_fault

% frequency = 50 Hz
f = 50;
% angular frequency = 314.16 rad/s
w = 2*pi*f;

% time
t = 0;

% delta time (sampling period)
dt = 20e-6;

% discrete time k
k = 1;

% dicrete end time N
N = round(0.06/dt);

% tbd
yu_all = zeros(10,1);
  
o = zeros(N,14);

x_mod = zeros(10,1);

% Periods count (periods of fundamental, 50 Hz default)
n = 0;

for k = 1:N
        
    % Required rotating voltage vector (Valpha, Vbeta): |Vafbt| = 1.5772, f = 50 Hz
    Vafbt = 1.5772*[cos(w*t);sin(w*t)];
    
    % Fault-tolerant mode
    % y is vector of known variables - actual vector Vafbt and 0 (last row)
    % for fault-tolerant mode
    y = [Vafbt;0];
    
    % Search for maximum y'u
    % There are 5 possible vectors u (columns of variable u) - for every
    % column vector u we compute value |y'u| and store to vector yu_all
    for i = 1:5    
        yu_all(i,1) = abs(y'*u_all(:,i));
    end
    
    % Now we need to find y'u0 which is the maximum of all
    % possible y'u values (vector yu_all). Important is the maximum
    % position index n_u.
    [yu0, n_u] = max(yu_all);
    
    % Note: Final y'u0 is the minimum inf. norm value. 
    
    % Now we take from all matrices and vectors (A2_all, A1_ls_all, u and ind_all)
    % those with the index n_u.

    % Take the final vector u0 from all vectors u according to maximum
    % index n_u.
    % In fact, there are alltogether 10 possible vector u, but 5 of them
    % are negative vectors of the rest. So we store only 5 vectors u, but
    % now we have to check, if the component Vbt (Vafbt(2)) is negative
    % (we take negative of u) or not (we take u).
    if (Vafbt(2) < 0)
        u0 = -1*u_all(:,n_u);
    else
        u0 = u_all(:,n_u);
    end
    
    % Actual matrix A2 is chosen according to index n_u.
    A2 = A2_all(:,:,n_u);
    
    % Compute vector x2, x2 is part of final solution vector (final vector x = [x1;x2])
    x2 = yu0*sign(A2'*u0);

    % Compute vector A1x1 [3x1] 
    A1x1 = y - A2*x2;
    
    % Choose actual matrix A1_ls according to index n_u.
    A1_ls = A1_ls_all(:,:,n_u);
    
    % Compute vector x1, x1 is part of final solution vector x.
    x1 = A1_ls*A1x1;
    
    % x_temp is a prelimiary solution vector - its components are valid, but
    % disordered.
    x_temp = [x1;x2];
    
    % The final solution vector with ordered leg voltages (i.e. modulaiton signals)
    % x_mod is built of x_temp. Components are reordered according to index
    % vector ind.
    % 1. The right index vector ind is chosen according to index n_u.
    ind = ind_all(n_u,:);
    
    % 2. The final solution is ordered according to ind.
    x_mod(ind(1)) = x_temp(1);
    x_mod(ind(2)) = x_temp(2);
    x_mod(ind(3)) = x_temp(3);
    x_mod(ind(4)) = x_temp(4);
    x_mod(ind(5)) = x_temp(5);
    x_mod(ind(6)) = x_temp(6);
    x_mod(ind(7)) = x_temp(7);
    x_mod(ind(8)) = x_temp(8);
    x_mod(ind(9)) = x_temp(9);
    x_mod(ind(10)) = x_temp(10);
    
    % This is usually not necessary. But in this case, columns of original
    % matrix A are lineary dependent (due to multilevel converter) and thus,
    % sometimes the solution vector x_mod returns the leg voltage within
    % one phase inequally distributed to upper and lower converter.
    % Following process simply checks distributes the voltage within each
    % phase (except the phase a with the fault) as average voltage on both
    % upper and lower converter. 
    for i = 3:2:9
        tmp = 0.5*(x_mod(i)+x_mod(i+1));
        x_mod(i)   = tmp;
        x_mod(i+1) = tmp;
    end
    
    % Now the final solution vector x_mod is ready and its components 
    % can be used for PWM as modulation signals.
    
    % Data to save 
    Vafbtk(:,k) = Vafbt; 
    tk(k)=t; 
    x_modk(:,k)=x_mod; 
    yu0k(k)=yu0; 

    % Auxiliary matrix for plots.
    o(k,:) = [t, Vafbt(1), Vafbt(2), x_mod', yu0];
    t = t + dt;
end


% Plot resulting leg voltages
figure;
plot(o(:,1),[o(:,2),o(:,3),o(:,4),o(:,5),o(:,6),o(:,7),o(:,8),o(:,9),o(:,10),o(:,11),o(:,12),o(:,13),o(:,14)], 'LineWidth', 2);
grid on;

% % Plot resulting leg voltages and required Vafbt vector
figure;
subplot(5,1,1);
plot(o(:,1), [o(:,4), o(:,5)], 'LineWidth', 2); grid on;
subplot(5,1,2);
plot(o(:,1), [o(:,6), o(:,7)], 'LineWidth', 2); grid on;
subplot(5,1,3);
plot(o(:,1), [o(:,8), o(:,9)], 'LineWidth', 2); grid on;
subplot(5,1,4);
plot(o(:,1), [o(:,10), o(:,11)], 'LineWidth', 2); grid on;
subplot(5,1,5);
plot(o(:,1), [o(:,12), o(:,13)], 'LineWidth', 2); grid on;

%% Save data 
save('Reference_case.mat','Vafbtk','tk','x_modk','yu0k'); 
disp('Data stored in ''Reference_case.mat'' file.'); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% No more






%% Fixed-Point Implementation of the MinInfNorm Algorithm
% This file describes the procedure followed to obtain a fixed-point
% implementation of the <matlab:edit('MinInfNorm_5fCHB_Fault_UniOvi.m')
% |MinInfNorm_5fCHB_Fault_UniOvi.m|> algorithm. The
% <https://es.mathworks.com/help/fixedpoint/ug/automated-fixed-point-conversion-best-practices.html
% Automated Fixed-Point Conversion Best Practices> workflow is followed.

%%
clearvars;
close all;

disp([newline, 'Running ''', mfilename, '.m''...']);

%% Create a Test File. Reference Case
% The original Matlab script file was slightly modified as <matlab:edit('MinInfNorm_5fCHB_Fault_OLS.m')
% |MinInfNorm_5fCHB_Fault_OLS.m|> so save data the results of reference
% case in |Reference_case.mat| file.

MinInfNorm_5fCHB_Fault_OLS;

%% Create a Test File. Separate Core From Testbench. Reference algorithm
% The original algorithm in |MinInfNorm_5fCHB_Fault_OLS.m| is splitted into
% the core of the algorithm and a test file. The core of the algorith is
% the function <matlab:edit('MinInfNorm_ref.m') |MinInfNorm_ref()|> and the
% test file is <matlab:edit('MinInfNorm_ref_test.m')
% |MinInfNorm_ref_test|>:
%
% <include>MinInfNorm_ref_test.m</include>

MinInfNorm_ref_test

%% Create a Test File. Adaptation of the reference algorith to the |Fixed-Point Converter| tool
% The function |MinInfNorm_ref()| is modified to obtain a new
% <matlab:edit('MinInfNorm.m') |MinInfNorm()|> function suitable for the
% _Fixed-Point Converter_ tool. The new file is compared with an input set
% of random data to verify that both functions give the same results. This
% is done by means of the test script <matlab:edit('MinInfNorm_test.m')
% |MinInfNorm_test.m|>
%
% <include>MinInfNorm_test.m</include>

MinInfNorm_test

%% Prepare Your Algorithm for Code Acceleration or Code Generation
% The |MinInfNorm()| is ready to be tested with the testbench script
% <matlab:MinInfNorm_tb |MinInfNorm_tb|>.
%
% <include>MinInfNorm_tb.m</include>
%
% This testbench run the function for a random set of input data points.

%% Convert to Fixed Point
% The _Fixed-Point Converter_ tool is used to convert the code.
% The project <matlab:open('MinInfNorm.prj') |MinInfNorm.prj|> uses the
% function |MinInfNorm()| and the testbench |MinInfNorm_tb.m|.
% The basic settings are:
%
% * Propose fractional length for specified word length.
% * Default word length: 12 bits.
% * Input and output: signed 12 bits word with 9 bits of fraction length.
%
% This project can be converted to a script as follows:
fixedPointConverter -tocode MinInfNorm.prj -script MinInfNorm_script.m

%%
% The result obtained is % <matlab:edit('MinInfNorm_script.m')
% |MinInfNorm_script.m|>:
%
% <include>MinInfNorm_script.m</include>
%
% Running this scrip shows that the error is less than 0.3%.

MinInfNorm_script

%% Reports
% The generated report
% <matlab:web('/codegen/MinInfNorm/fixpt/MinInfNorm_fixpt_report.html')
% |MinInfNorm_fixpt_report.html|> shows the numeric types of all variables,
% their sizes and their ranges.
%

%% Fixed-point Calculation
% The generated Fixed Point MATLAB Code
% <matlab:edit('./codegen/MinInfNorm/fixpt/MinInfNorm_fixpt.m')
% |MinInfNorm_fixpt()|> can be used with the proposed fixed point data
% types by means of the generated wrapper function
% <matlab:edit('./codegen/MinInfNorm/fixpt/MinInfNorm_wrapper_fixpt.m')
% |MinInfNorm_wrapper_fixpt()|>:
%
% <include>./codegen/MinInfNorm/fixpt/MinInfNorm_wrapper_fixpt.m</include>
%

currentpath=pwd;
cd ./codegen/MinInfNorm/fixpt;

Vafbt=[1.2;0.9];
x_mod=MinInfNorm_wrapper_fixpt(Vafbt) %#ok

cd(currentpath);

%% Comparative of the Implementations with Real Input Data
N=1000; %Number of points 
phi=0:2*pi/(N-1):2*pi;
Vafbt_k=2*[cos(phi);sin(phi)]; % sinusoidal reference

% Floating point:
x_mod_k=zeros(5,N);
for k=1:N
    x_mod=MinInfNorm(Vafbt_k(:,k));
    x_mod_k(:,k)=x_mod(1:2:10)+x_mod(2:2:10);
end

% Fixed point:
x_mod_fp_k=zeros(5,N);
currentpath=pwd;
cd ./codegen/MinInfNorm/fixpt;
for k=1:N
    x_mod=MinInfNorm_wrapper_fixpt(Vafbt_k(:,k));
    x_mod_fp_k(:,k)=x_mod(1:2:10)+x_mod(2:2:10);
end
cd(currentpath);

% Plots
clf
for k=1:5
    subplot(5,1,k)
    hold on
        stairs(phi,x_mod_k(k,:),'m')
        stairs(phi,x_mod_fp_k(k,:),'b')
    hold off
    xlim([0,2*pi]);
    ylim([-1,1]*2.1);
    grid on;
    title(['Phase ', '@'+k])
end
sgtitle(['Averaged Inverter Output - Fixed Point vs Floating Point (',...
    datestr(datetime('now'),'HH:MM'),')']);
legend({'floating point','fixed point'})

%%
disp([newline, 'Acabouse o conto.']);


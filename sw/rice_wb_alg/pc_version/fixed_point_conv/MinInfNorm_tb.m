%% MinInfNorm testbench
% Test bench of the |MinInfNorm()| function for the _Fixed-Point Converter_
% Tool.

%%

disp([newline, 'Running ''', mfilename, '.m''...']);

global u_all; %#ok
global A1_ls_all; %#ok
global A2_all; %#ok
global ind_all;
load param_fault A1_ls_all A2_all u_all ind_all
ind_all=uint8(ind_all);


%% Define representative inputs
% Tha algorithm is tested with |N| data points in a centered square of side
% length of five units.

N=1000; % Number of data test points

disp(['Testbench with ', int2str(N), ' random data points.']);
rng(0,'multFibonacci') %Initialize the (pseudo) random number generator

%% Test loop
for i=1:N
    Vafbt=fi(rand(2,1)*5-2.5,1,12,9);
    MinInfNorm(Vafbt.double);
end

# Fixed-Point Implementation of the InfMinNorm Algorithm
Conversion of the `InfMinNorm` algorithm by using the [`Fixed-Point Converter`](https://es.mathworks.com/help/fixedpoint/ref/fixedpointconverter-app.html) tool app from Matlab(R).
## Usage
 1. Publish the script `FixedPoint.m`. This can be done with the Matlab command: `web(publish('FixedPoint.m'))`.
 2. The web page [/html/FixedPoint.html](./html/FixedPoint.html) should get opened automatically.
 3. This web explains the conversion workflow to perform an automatic conversion.
 	



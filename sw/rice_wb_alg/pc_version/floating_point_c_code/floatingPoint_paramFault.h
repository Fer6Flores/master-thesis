/**
 * @file floatingPoint_ParamFault.h
 * @author Fernando Flores (Fer6Flores)
 * @date March 4, 2021
 * @brief Floating-point parameters generated offline
 * 
 * This file contains the declaration of the variables calculated
 * offline and generated through Matlab from the param_fault.mat
 */

#ifndef __FLOATINGPOINT_PARAMFAULT_H__
#define __FLOATINGPOINT_PARAMFAULT_H__

/* Variables */
extern double A1_ls_all[5][2][3];

extern double A2_all[5][3][8];

extern double u_all[3][5];

extern double ind_all[5][10];

#endif
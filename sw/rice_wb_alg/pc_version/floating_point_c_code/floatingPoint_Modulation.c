/**
 * @file floatingPoint_Modulation.c
 * @author Fernando Flores (Fer6Flores)
 * @date March 4, 2021
 * @brief Floating-point Modulation file
 */

/* Libraries */
#include <stdint.h>                     // For using uint8_t and more types
#include <stdlib.h>                     // For pointers
#include <math.h>                       // Math function
#include "floatingPoint_ParamFault.h"   // Stored matrix and arrays prepared offline
#include "floatingPoint_Modulation.h"   // Modulation function prototype

/**
 * @brief Modulation function to be implemented in the ARM.
 * @param Vafbt Alfa-beta voltage input. 
 * @return The 10 modulation signals for a 5-phases converter.
 * @note This floating-point version is only a test to checks 
 * the algorithm performance.
 */
double *Modulation(double Vafbt[2])
{
    /* Program inits */
    uint8_t i = 0;
    uint8_t j = 0;
    double x2_zero_threshold = 0.000001;

    /* Algorithm inits */
    double *x_mod = (double *)malloc(10 * sizeof(double));
    double yu_all[10] = {0};
    double yu0 = 0;
    uint8_t n_u = 0;
    double u0[3] = {0};
    double A2[3][8] = {0};
    double x2[8] = {0};
    double A1x1[3] = {0};
    double A1_ls[2][3] = {0};
    double x1[2] = {0};
    double x_temp[10] = {0};
    double ind[10] = {0};
    double tmp = 0;

    // Fault-tolerant mode
    // y is vector of known variables - actual vector Vafbt and 0 (last row)
    // for fault-tolerant mode
    double y[3] = {Vafbt[0], Vafbt[1], 0};

    for (uint8_t i = 0; i < 5; i++)
    {
        for (uint8_t j = 0; j < 3; j++)
        {
            yu_all[i] += y[j] * u_all[j][i];
        }
        yu_all[i] = fabs(yu_all[i]);
    }

    for (uint8_t i = 0; i < 10; i++)
    {
        if (yu_all[i] > yu0)
        {
            yu0 = yu_all[i];
            n_u = i;
        }
    }

    // Note: Final y'u0 is the minimum inf. norm value.
    for (uint8_t j = 0; j < 3; j++)
    {
        u0[j] = (Vafbt[1] < 0) ? -u_all[j][n_u] : u_all[j][n_u];
    }

    for (uint8_t i = 0; i < 3; i++)
    {
        for (uint8_t j = 0; j < 8; j++)
        {
            A2[i][j] = A2_all[n_u][i][j];
        }
    }

    for (uint8_t i = 0; i < 8; i++)
    {
        for (uint8_t j = 0; j < 3; j++)
        {
            x2[i] += A2[j][i] * u0[j];
        }

        if (x2[i] > x2_zero_threshold)
        {
            x2[i] = yu0;
        }
        else if (x2[i] < -x2_zero_threshold)
        {
            x2[i] = -yu0;
        }
        else
        {
            x2[i] = 0;
        }
    }

    for (uint8_t i = 0; i < 3; i++)
    {
        for (uint8_t j = 0; j < 8; j++)
        {
            A1x1[i] += A2[i][j] * x2[j];
        }
        A1x1[i] = y[i] - A1x1[i];
    }

    for (uint8_t i = 0; i < 2; i++)
    {
        for (uint8_t j = 0; j < 3; j++)
        {
            A1_ls[i][j] = A1_ls_all[n_u][i][j];
        }
    }

    for (uint8_t i = 0; i < 2; i++)
    {
        for (uint8_t j = 0; j < 3; j++)
        {
            x1[i] += A1_ls[i][j] * A1x1[j];
        }
    }

    for (uint8_t i = 0; i < 10; i++)
    {
        x_temp[i] = (i < 2) ? x1[i] : x2[i - 2];
    }

    for (uint8_t i = 0; i < 10; i++)
    {
        ind[i] = ind_all[n_u][i];
    }

    for (uint8_t i = 0; i < 10; i++)
    {
        x_mod[(int)ind[i]] = x_temp[i];
    }

    for (uint8_t i = 2; i < 9; i += 2)
    {
        tmp = 0.5 * (x_mod[i] + x_mod[i + 1]);
        x_mod[i] = tmp;
        x_mod[i + 1] = tmp;
    }

    return x_mod;
}
/**
 * @file floatingPoint_Modulation.h
 * @author Fernando Flores (Fer6Flores)
 * @date March 4, 2021
 * @brief Floating-point Modulation file
 */

#ifndef __FLOATINGPOINT_MODULATION_H__
#define __FLOATINGPOINT_MODULATION_H__

double *Modulation(double Vabft[2]);

#endif
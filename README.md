# Master Thesis

## Introduction
This is my final master's thesis  "Hardware/software codesign for the implementation of carrier-based pulse-width modulation techniques for a cascaded H-bridge five-phase inverter with fault tolerance".

Work In Process.

## Documentation
If you are bored and want to spend your time learning about this project, go to [the project wiki](https://gitlab.com/Fer6Flores/master-thesis/-/wikis/home). The wiki and this README can be outdated until the end of July.

## Repository organization
The files of this project are organized as shown below. In the root folder there are the next folders and general files. This will be changing along the project timeline.

* **doc/ :** There are all the papers, books and etc used to learn concepts needed for the project. Also the datasheets and user manuals of the components used are located here.
    * **bibliography/ :** Papers, books and etc used to learn concepts needed for the project. 
    * **datasheets/ :** Documentation of the hardware used in this project
* **hw/ :** There will be hardware files as the PCB and schematic files. At this moment, only a .gitkeep file is located here.
* **sw/ :** There is all the software used for this project (Matlab code, C code, VHDL code, Simulink files)
    * **FirstAlgorithmPart/ :** There are the files of the first algorithm in Matlab, C, Python and VHDL.
        * **MatlabAncillaryCode/ :** There are the original algorithm files and some auxiliar scripts.
        * **FloatingPointInC/ :** There are all the floating-point C version files.
        * **FixedPointConversion/ :** There are all the files related to the use of the Fixed-point conversion tool of Matlab.
        * **FixedPointInC/ :** There are all the fixed-point C version files.
        * **ARMimp/ :** There is the Vivado project to test the first algorithm in the PS of the Zedboard.
        * **UZedDSpaceTest/ :** There is the Vivado project to test the first algorithm in the microZed through dSpace.
* *.gitignore :* This is the gitignore file of this project. Currently, it has Matlab, Simulink, LaTeX, C and VSCode files to ignore.
* *README.md :* This file :)

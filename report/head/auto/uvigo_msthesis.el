(TeX-add-style-hook
 "uvigo_msthesis"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("book" "a4paper" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontenc" "T1") ("inputenc" "utf8") ("babel" "french" "german" "english") ("xcolor" "table") ("pdfpages" "final") ("titlesec" "explicit")))
   (TeX-run-style-hooks
    "latex2e"
    "book"
    "bk11"
    "fontenc"
    "inputenc"
    "babel"
    "setspace"
    "graphicx"
    "xcolor"
    "subfigure"
    "booktabs"
    "microtype"
    "url"
    "pdfpages"
    "fancyhdr"
    "listings"
    "hyperref"
    "color"
    "tikz"
    "titlesec"
    "amsmath"
    "amsfonts"
    "amsthm"
    "IEEEtrantools")
   (TeX-add-symbols
    "chapterlabel"
    "partlabel"
    "cleardoublepage")
   (LaTeX-add-environments
    "theorem"
    "remark")
   (LaTeX-add-counters
    "myparts")))


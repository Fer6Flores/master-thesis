(TeX-add-style-hook
 "settings_uvigo_template"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("book" "a4paper" "11pt" "fleqn")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontenc" "T1") ("inputenc" "utf8") ("babel" "french" "german" "english") ("xcolor" "table") ("pdfpages" "final") ("titlesec" "explicit")))
   (TeX-run-style-hooks
    "latex2e"
    "book"
    "bk11"
    "fontenc"
    "inputenc"
    "babel"
    "setspace"
    "graphicx"
    "xcolor"
    "subfigure"
    "booktabs"
    "microtype"
    "url"
    "pdfpages"
    "fancyhdr"
    "listings"
    "hyperref"
    "color"
    "tikz"
    "titlesec"
    "amsmath"
    "amsfonts"
    "amsthm")
   (TeX-add-symbols
    "chapterlabel"
    "partlabel"
    "cleardoublepage")
   (LaTeX-add-environments
    "theorem")
   (LaTeX-add-counters
    "myparts")))


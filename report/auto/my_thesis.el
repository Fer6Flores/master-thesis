(TeX-add-style-hook
 "my_thesis"
 (lambda ()
   (TeX-run-style-hooks
    "head/uvigo_msthesis"
    "head/custom_settings"
    "head/titlepage"
    "head/dedication"
    "head/acknowledgements"
    "head/preface"
    "head/abstracts"
    "main/comments"
    "main/chapter1")
   (LaTeX-add-bibliographies
    "tail/frameless")))


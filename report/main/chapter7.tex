\chapter{Testing and validation}

When designing and testing complex control systems, appropriate design procedures are required. Model-based design is a method of addressing problems associated with the design of complex systems,  which is used before a model is deployed to the hardware for production.

\Ac{MIL}, \ac{SIL}, \ac{PIL} and \ac{HIL} testing are verification and validation steps of this design and testing approach. For the validation of the results of this project, \ac{SIL} and \ac{PIL} testing are used.

\Ac{SIL} testing consists on test the code from the algorithm model for verifying if the coding of this algorithm is correct and it is achieving a good performance. On the other hand, \ac{PIL} testing evaluates the implementation of the algorithm in the target embedded processor and identifying if the target processor is capable of executing the developed code with the required timing closure.

\section{Reference results obtained with the original Matlab MIN function}
First of all, reference results are obtained using the code of \autoref{lst:MinInfNorm_5fCHB_Fault_FPGA}, in which the original Matlab MIN function is used. The output of the code are the ten output reference signals for the carrier-based PWM for a given $\alpha$-$\beta$ components input. Both input and output values can be seen in the \autoref{fig:ch7min}.

These two plots shows that the algorithm forces the modulation signals for the faulty cell (Second cell of the first converter leg) to zero, distributing its power output among the other cells. Furthermore, the algorithm does an equal distribution on the value of the output reference signals for the carrier-based PWM of each phase cells, excluding the faulty one.

\begin{figure}
	%	\centering
	\begin{subfigure}[t]{0.5\linewidth}
		\centering
		\includegraphics[trim={2.5cm 0 2.5cm 0}, clip, width=\linewidth]{images/chapter5/minvafbt}
		\caption{$\alpha$-$\beta$ input components.}
		\label{fig:ch7minvafbt}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.5\linewidth}
		\centering
		\includegraphics[trim={2.5cm 0 2.5cm 0}, clip, width=\linewidth]{images/chapter5/mincellv}
		\caption{Output reference signals for the PWM.}
		\label{fig:ch7mincellv}
	\end{subfigure}
	\caption[Inverter $\alpha$-$\beta$ input components and reference signals for the carrier-based PWM from the original Matlab script of the algorithm]{Inverter $\alpha$-$\beta$ input components and reference signals for the carrier-based PWM from the original Matlab script of the algorithm.}
	\label{fig:ch7min}
\end{figure}

\section{Floating-point C code SIL testing}

For validating the conversion of the original Matlab script to floating-point C code \ac{SIL} testing is used. In order to do this, the same input $\alpha$-$\beta$ components that were used in the previous section are provided to the \ac{MIN} algorithm floating-point C code version, so it is expected to obtain an equal output. 

In the plots of \autoref{fig:ch7float}, the outputs from this test are shown. On the left side, the output signals for the PWM obtained from the floating-point C code are shown, which, at simple view, indicates a similar shape to the ones from the original Matlab script. On the other side, these outputs are compared to the original ones and their errors are obtained. 

There can be seen that these error are almost a million times lower than the signal values, so they can be disregarded and the correct performance of the floating-point C code is demonstrated.

\begin{figure}
	%	\centering
	\begin{subfigure}[t]{0.49\linewidth}
		\centering
		\includegraphics[trim={2.5cm 0 2.5cm 0}, clip, width=\linewidth]{images/chapter7/vfloat}
		\caption{Output signals for the PWM obtained from the floating-point C code.}
		\label{fig:ch7vfloat}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\linewidth}
		\centering
		\includegraphics[trim={2.5cm 0 2.5cm 0}, clip, width=\linewidth]{images/chapter7/efloat}
		\caption{Output reference signals errors for the PWM.}
		\label{fig:ch7efloat}
	\end{subfigure}
	\caption[Output reference signals for the carrier-based PWM and corresponding errors from the SIL testing of the algorithm conversion to floating-point C code]{Output reference signals for the carrier-based PWM and corresponding errors from the SIL testing of the algorithm conversion to floating-point C code.}
	\label{fig:ch7float}
\end{figure}

\section{Fixed-point C code SIL testing}

For validating the next step in the code development, the fixed-point C code implementation of the \ac{MIN} algorithm, a similar procedure that the one followed in the previous section is used. 

In this case, it is critical to test singular points where this fixed-point C code could provide an output critically different to the expected one. This need has to be remarked here as some logic of the algorithm has been changed by the use of thresholds and the limits in the data variables size in this adaptation.

The results from \ac{SIL} test are shown in \autoref{fig:ch7fixed}. The error in the output signals remains in values a million times smaller than the signal values, i.e., in negligible levels. So, the conversion to fixed-point C code has a correct performance as the output match with the obtained using the original Matlab code.

\begin{figure}
	%	\centering
	\begin{subfigure}[t]{0.49\linewidth}
		\centering
		\includegraphics[trim={2.5cm 0 2.5cm 0}, clip, width=\linewidth]{images/chapter7/vfixed}
		\caption{Output signals for the PWM obtained from the fixed-point C code.}
		\label{fig:ch7vfixed}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\linewidth}
		\centering
		\includegraphics[trim={2.5cm 0 2.5cm 0}, clip, width=\linewidth]{images/chapter7/efixed}
		\caption{Output reference signals errors for the PWM.}
		\label{fig:ch7efixed}
	\end{subfigure}
	\caption[Normalized output voltage signals and corresponding errors from the SIL testing of the algorithm conversion to fixed-point C code]{Normalized output voltage signals and corresponding errors from the SIL testing of the algorithm conversion to fixed-point C code.}
	\label{fig:ch7fixed}
\end{figure}


\section{Zynq-7000 ARM Cortex-A9 implementation PIL testing}

For testing the implementation in the real embedded system which is the goal of this project, a PIL test is executed, running the algorithm in the Zynq-7000 ARM Cortex-A9 and, moreover, executing the communication through AXI bus between the \ac{PS} and the \ac{PL} of the \ac{SoC}.

In the \autoref{fig:ch7zynq}, the results from the algorithm calculations done in the ARM Cortex-A9 of the Zynq are displayed, as well as their error in comparison with the original Matlab code results. In these plots the correct performance of the implementation can be verified, as it maintains the error values in a negligible level.

\begin{figure}
	%	\centering
	\begin{subfigure}[t]{0.49\linewidth}
		\centering
		\includegraphics[trim={2.5cm 0 2.5cm 0}, clip, width=\linewidth]{images/chapter7/vzynq}
		\caption{Output reference signals for the carrier-based PWM obtained from the implementation in the ARM Cortex-A9.}
		\label{fig:ch7vzynq}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\linewidth}
		\centering
		\includegraphics[trim={2.5cm 0 2.5cm 0}, clip, width=\linewidth]{images/chapter7/ezynq}
		\caption{Output reference signals errors for the carrier-based PWM.}
		\label{fig:ch7ezynq}
	\end{subfigure}
	\caption[Output reference signals for the carrier-based PWM and corresponding errors from the PIL testing in the Zynq-7000 ARM Cortex-A9]{Output reference signals for the carrier-based PWM and corresponding errors from the PL testing in the Zynq-7000 ARM Cortex-A9.}
	\label{fig:ch7zynq}
\end{figure}

On the other hand, the PIL test is also performed to evaluate the capability of the embedded system to run the algorithm with the required timing, including the communication through AXI bus. 

In the \autoref{fig:ch7timingarm} a graph of all the execution and AXI bus communication times, as well as the sum of both of them, is shown. This values are the corresponding ones to the algorithm calculation for the input data for obtaining the output values of the \autoref{fig:ch7zynq}.

An average value of $10,3~\mathrm{\mu} s$ are required to execute a cycle of the algorithm and $2~\mathrm{\mu} s$ is the average time to do the necessary AXI communication between the \ac{PS} and the \ac{PL}. The sum of both values, $12,3~\mathrm{\mu} s$, is enough to broadly satisfy the timing of 100 $100~\mathrm{\mu}s$ required for a 10~kHz switching frequency for the converter.

\begin{figure}
	\centering
	\includegraphics[trim={7.6cm 0 6cm 0}, clip, width=1\linewidth]{images/chapter7/timing_arm}
	\caption[Algorithm execution and AXI bus communication times]{Algorithm execution and AXI bus communication times.}
	\label{fig:ch7timingarm}
\end{figure}

\section{Phase-shifted carrier-based PWM custom IP core PIL testing}
For the validation of the correct behavior of the phase-shifted carrier-based PWM custom IP three tests have been tested:
\begin{enumerate}
	\item AXI reference signals correct communication.
	\item PWM duty cycle corresponding to the reference signals.
	\item Four carrier-based PWM signal arrays synchronization
\end{enumerate}

First of all, the correct sending of the modulation reference signals through AXI bus is checked. For doing that, a fixed set of reference values is sent from the PS to this custom IP core. To easily check the correct sending, the odd values are set to 11111111h and the even values are set to 22222222h. In the \autoref{fig:axi} a simulation of the writing process through AXI is shown. There is shown that the S\_AXI\_WDATA register has the corresponding values and the rest of the AXI signals work as expected in the AXI4 protocol~\cite{AXI_RG}.
\begin{figure}[h]
	\centering
	\includegraphics[trim={0cm 0 0cm 0}, clip, width=1\linewidth]{images/chapter7/axi}
	\caption{AXI reference signals communication.}
	\label{fig:axi}
\end{figure}

For testing that the PWM duty cycles are the corresponding ones to the reference signals, 1111h is written through AXI an the active time of one channel is checked. As the AXI registers size is 32 bits and the triangular counter is 13 bits, the 19 most significant bits are not having into account by the IP core. They are filled with zeros, resulting in a sent value of 00000111h. This value should imply a duty cycle of 43.7\%, which are the result from 0DFAh divided by the full range of 1FFFh.

As seen in the \autoref{fig:val}, the measured PWM signal is active between the instants 147.135 $\mathrm{\mu}s$ and 103.445 $\mathrm{\mu}s$, giving an active period of 43.69 $\mathrm{\mu}s$. If the switching period is $100~\mathrm{\mu}s$, this implies an obtained duty cycle of 43.69\%, as expected.

\begin{figure}
	\centering
	\includegraphics[trim={0cm 0 0cm 0}, clip, width=1\linewidth]{images/chapter7/val}
	\caption{Simulation of the PWM duty cycle update depending on the reference values obtained through AXI bus.}
	\label{fig:val}
\end{figure}

The last step for the validation of this custom IP core, is testing the synchronization of the 40 PWM signals that are generated by this block. In the \autoref{fig:sync} there are shown these 40 PWM signals, divided in two plots, depending if they are positive or negative signals. In these plots, four markers are added, showing the starting time of the active cycle of one of each kind of carrier-based signal. As shown, there is a 25 $\mathrm{\mu}s$ shift between each of them. Considering a switching period of $100~\mathrm{\mu}s$, this is a 90 degrees shift, as desired, so this custom IP core for the carrier-based PWM signals behavior is correct.

\begin{figure}
	%	\centering
	\begin{subfigure}[t]{\linewidth}
		\centering
		\includegraphics[trim={0cm 0 0cm 0}, clip, width=\linewidth]{images/chapter7/sync}
		\caption{Synchronization of the positive PWM signals.}
		\label{fig:syncp}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{\linewidth}
		\centering
		\includegraphics[trim={0cm 0 0cm 0}, clip, width=\linewidth]{images/chapter7/syncn}
		\caption{Synchronization of the negative PWM signals.}
		\label{fig:syncn}
	\end{subfigure}
	\caption{Synchronization of the PWM signals.}
	\label{fig:sync}
\end{figure}
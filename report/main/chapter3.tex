\chapter{State of the Art}

\section{Power converters topologies applied to electrical motor drives}

%% Generic power conversion intro
Over the last decades, power conversion has become a fundamental technology for a wide range of applications due to its high efficiency and performance~\cite{leon16}. Converter topologies, such as cyclo-converters, ac/ac matrix converters, current-source inverters, dc/dc converters, dc/ac converters and diode or thyristor-based rectifiers, cover a broad range of different nominal power applications~\cite{kouro12}.

%% Generic multilevel inverters intro
Particularly, dc/ac converters are used to power different applications because of its robustness, low economical cost and possibility to reach high nominal power with reduced passive filters~\cite{bose09}. To fulfill all the variety of demanding requirements from these applications currently needed by the industry~\cite{yang11}, it has been developed a wide range of \acp{VSI}, a type of dc/ac converters, with improvements in their reliability, power capability and development times. Some of these topologies have been introduced in the industry, specifically, medium-voltage converters. In this field, multilevel topologies have been under research and development for several decades and have become increasingly popular and really demanded~\cite{kouro10}. 

%% Multilevel topologies and characteristics
The popularity of multilevel topologies arouse from their advantages as \ac{HV} capability, low switching losses, high power quality waveforms and low \ac{EMC} concerns, despite of its disadvantage of requiring a large number of semiconductor devices. Multilevel conversion has already been proven to be a mature enabling technology and a vast diversity of multilevel topologies can be found in the industry and the literature~\cite{corzine02,tolbert99}. In \autoref{fig:ch3multitopkouro10} a classification of some of them can be found. The most well-established multilevel converter topologies are \ac{FC}, \ac{NPC} and \ac{CHB}. Furthermore, new multilevel inverter topologies have been introduced in the last years, such as \acp{5L-ANPC}, \acp{MMC}, \acp{TCC}~\cite{leon16} and topologies with a higher number of levels have received an increased attention~\cite{malinowski2010tie}. However, a significant related problem associated to a higher number of devices is an increase in the probability of converter internal fault. 

\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{images/chapter3/multilevel_top_kouro10}
	\caption[Multilevel power converters classification]{Multilevel power converters classification~\cite{kouro10}.}
	\label{fig:ch3multitopkouro10}
\end{figure}

%% Multilevel converters brief comparative
Each of these topologies will provide different features and will imply more or less complex circuit structures as well as expensive devices. As a case in point, \ac{CHB} converters are based on several single-phase inverters connected in series, as seen in \autoref{fig:ch3chbtopcorzine02}, enabling to reach higher voltage and power levels than \ac{NPC} converters. They also exclusively need low-voltage \acp{IGBT}, whereas \ac{NPC} converters use medium-voltage/high-voltage \acp{IGBT} or \acp{IGCT}~\cite{kouro10}.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/chapter3/chb_top_corzine02}
	\caption[Generic structure of a multilevel cascaded H-bridge drive, where each phase consists of multilevel H-bridge cells with an isolated dc source]{Generic structure of a multilevel cascaded H-bridge drive, where each phase consists of multilevel H-bridge cells with an isolated dc source~\cite{corzine02}.}
	\label{fig:ch3chbtopcorzine02}
\end{figure}

%% Cascaded H-bridge
Due to the use of low-voltage \acp{IGBT}, \ac{CHB} converters solve the disadvantage of multilevel topologies as the increase of semiconductor devices does not lead to an significant cost rise. In addition, \ac{CHB} features a high modularity degree because each inverter can be considered as a module with equal circuit topology, modulation and control structure~\cite{malinowski2010tie}. Thanks to this fact, it is possible to replace a faulty module quickly and easily. Moreover, depending on the applied control strategy, it is possible to bypass the affected faulty module without stopping the system, which allows an almost continuous converter operation~\cite{lezana2009tie}. 

Because of all the advantages of \acp{VSI}, specifically, \ac{CHB}, multilevel \ac{CHB} is a really good option for a wide number of  applications, including  high voltage direct current distribution systems and medium voltage engines. They have proven to be an effective solution for \ac{HV} and motor drive issues~\cite{singh20}.


\section{Fault-tolerant systems and fault-tolerant control algorithms}

%% Fault-tolerance intro
Several high-power applications of \ac{CHB} converters require service continuation even under faulty conditions. This is called fault-tolerance and it is considered an important specification for this kind of power converters. The modularity of \ac{CHB} topologies allows to increase the system reliability by using redundant cells, which become operative once the cell affected by a fault is bypassed~\cite{ouni17}. This method implies a proportional growth in the converter cost, which can be significant. Other different strategies to cell redundancy have been under research and gaining more attention in the industry.

%% Fault detection
Detecting the fault in a cell or cells is the first step to achieve a fault-tolerant system. Several fault detection methods are proposed in the literature. For instance, a quick but complicated method is suggested in~\cite{ouni17}. On the other hand, another simpler ones provide acceptable detection time (less than one switching period), while another simple method features a relatively slow detection (more than one fundamental period)~\cite{ouni17}.

%% Faulty cell bypass
After fault detection, fault-tolerant control strategies are required to guarantee the continuous inverter operation as close to normal operation as possible. When the fault is detected, the affected cell has to be short-circuited. In \autoref{fig:ch3multicelltopouni17} an 11-level \ac{CHB} multilevel inverter with fault-tolerance is shown, where the bypass switch to short-circuit each cell affected by a fault is shown.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/chapter3/multicell_top_ouni17}
	\caption[An 11-level cascaded H-bridge multilevel inverter with fault-tolerance]{An 11-level cascaded H-bridge multilevel inverter with fault-tolerance~\cite{ouni17}.}
	\label{fig:ch3multicelltopouni17}
\end{figure}

%% Fault-tolerance algorithms
With the faulty cell bypassed, a control algorithm which has this fact into account is required. The design of a post-fault operation strategy is commonly preceded by an analysis of the fault impact on the system behavior. Depending on the algorithm used, the converter features performance may decrease in a higher or lower degree. For example, in \autoref{fig:ch3ispectrumrodriguez05} it is shown the current spectrum when using the fault-tolerance method proposed in~\cite{rodriguez2005tie}. There, it is confirmed the increase in the current harmonics as the converter modules are bypassed.

For a long time, iterative algorithms for achieving fault-tolerance systems have been studied and developed, as they have been proved to be effective, with the cost of a high computational cost~\cite{zhou95}. Furthermore, predictive algorithms are currently being developed as they have some improvements on their results and they have an increase in their complexity and execution time~\cite{wang22} in comparison with other traditional algorithms.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/chapter3/i_spectrum_rodriguez05}
	\caption[Converter current spectrum for different number of bypassed cells]{Converter current spectrum for different number of bypassed cells~\cite{rodriguez2005tie}.}
	\label{fig:ch3ispectrumrodriguez05}
\end{figure}

\section{Multiphase electrical machines}\label{sec:ch3multi}

Due to the benefits resulting from the use of a phase order higher than three, multiphase machines have received a lot of attention. Multiphase applications have been being more interesting because of some advantages as higher power density, lower torque ripples and a greater fault-tolerant operation~\cite{singh02, levi07, levi08}.

In a three-phase induction motor, one phase of the motor or the inverter could be rendered inoperable, the currents in the remaining two phases become identically equal in magnitude with $180^{\circ}$ phase displacement if the machine is star-connected with isolated neutral point. Hence, independent control of the two remaining currents becomes impossible, unless a divided DC bus and neutral connection are provided. In other words, a zero-sequence component is necessary in a three-phase induction motor to provide an undisturbed rotating magneto-motive force after one supply phase is opened. 

The situation is very different in multiphase motor drives, where existence of more than three phase currents (with only two being required for the machine control) enables development of various strategies for post-fault operation, without the need for the neutral connection and the split dc bus. This property of multiphase motor drives was recognized in the early days of their development and is one of their most beneficial features~\cite{levi07}. 

Furthermore, some approach to drives control have been enhanced taking advantage of the multiple degrees of freedom that multiphase machines have. One example of these cases is the \ac{MIN} approach. This method minimizes the maximum element of solution vector and finds a unique one in the infinite solutions set \cite{komrska16}.

In essence, there exists phase redundancy. The machine can still continue to operate in post-fault conditions with the rotating magneto-motive force, provided that an appropriate post-fault current control strategy is developed. More deeply, the existing degrees of freedom that can be used to enhance torque production by higher stator current harmonic injection are now utilized to design post-fault control strategies. For instance, for a five phases machine, a post-fault iterative control algorithm will iterate modifying its five components to obtain the better possible combination.

Methods of speed control for multiphase induction machines are in principle the same as for three-phase induction machines. The problem that arises in \ac{PWM} control of multiphase \acp{VSI} is how to avoid generation of unwanted low-order harmonics that lead to the flow of stator harmonic currents. 

The most straightforward approach is undoubtedly utilization of the carrier-based \ac{PWM} methods. Similar to the carrier-based \ac{PWM} with third harmonic injection for a three-phase \ac{VSI}, it is possible to improve the dc bus utilization in multiphase VSIs by injecting the appropriate zero-sequence harmonic (or adding the offset) into leg voltage references~\cite{levi07}.

\section{Modulation techniques for VSIs}

%% Intro
New complex inverter topologies and new application fields imply additional control challenges, such as power-quality issues, voltage imbalances, higher efficiency needs, and fault-tolerant operation, which necessarily requires the parallel development of innovative modulation schemes~\cite{lopez08, leon16}. Modulation is the intermediary between the controller and the power converter, as shown in \autoref{fig:ch3vsimodcontrolleon16}. 

The modulator input is the output control signal from the controller module and its output is a set of digital switching signals that will control the power converter. Modulators's input reference comes from a control loop, which can be in open or closed loop. More deeply explained, modulation techniques are used to generate the gate signal of the switching semiconductor devices of the power converter in order to obtain an output waveform with the desired frequency, magnitude and phase. This is done with the goal of achieving a proper control of the process fed by the inverter.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/chapter3/vsi_mod_control_leon16}
	\caption[Typical structure of a modulation and control method of a voltage-source inverter]{Typical structure of a modulation and control method of a voltage-source inverter~\cite{leon16}.}
	\label{fig:ch3vsimodcontrolleon16}
\end{figure}

%% Generic classification
In order to achieve an optimal generation of these gate signals, a lot of modulation techniques have been developed in the last decades. In the \autoref{fig:ch3gatesignalsgenclass} a general classification of these techniques is introduced, showing the ones with a grater degree of academic and industrial impact.

As each converter topology has particular modulation and control challenge, there are a wide range of schemes applied to the modulation stage. In the case of multilevel power converters, and specifically multilevel \acp{VSI}, \ac{PWM} techniques have been the mainstream concept as the modulation technique applied to these devices. The reason behind this is that \ac{PWM} techniques are an effective and simple way to produce a pulsed waveform whose average value is equal to the desired reference of the modulator. As shown in the \autoref{fig:ch3gatesignalsgenclass}, some of the most relevant techniques of this kind are carrier-based \ac{PWM} (\ac{LS-PWM} and \ac{PS-PWM} can be highlighted), \ac{SVM} and pre-programmed \ac{PWM}.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{images/chapter3/gate_signals_gen_class}
	\caption[General classification of modulation techniques for inverters]{General classification of modulation techniques for inverters~\cite{leon16}.}
	\label{fig:ch3gatesignalsgenclass}
\end{figure}

%% Phase-shifted PWM
Some of the main aspects which are evaluated when designing a modulator for a power system are the implementation complexity, losses, harmonics performance and the power converter that is going to be applied to. \Ac{PS-PWM} is the most commonly used modulation technique for cascaded multilevel inverters because it offers an evenly power distribution among cells and good losses equalization. Furthermore, it is easy to implement independently of the number of inverters, despite of the complexity added because of the phase-shifting synchronization~\cite{malinowski2010tie}.

\section{Systems on Chip for the implementation of control algorithms in power applications}\label{sec:ch3socs}

%% Intro
All these new control and modulation techniques introduced in the industry, with their increasingly computational cost, have led to the rise of the complexity in the control and processing systems used for these tasks. Also, these devices are required to have a high flexibility. With the aim to mix flexibility, reliability and high computational power hardware/software co-design has been increasingly demanded~\cite{araujo12, noguera02}. 

Moreover, embedded real-time systems have been classified as hard real-time and soft real-time. The first ones are those where failing to meet a timing constraint can potentially have catastrophic consequences, while soft real-time systems can, under certain circumstances, tolerate a determined amount of delays~\cite{bashir06}. In the case of power conversion systems, they are classified as hard real-time, as a fail in the timing of the control could led to serious problems in the power side of the system.

Grounded on this framework, a new concept of devices has been developed in the last years, the \acp{SoC}, with a really powerful set of characteristics, which have been widely accepted for a broad range of industrial control applications in the industry. 

%% SoCs characteristics
A \ac{SoC} is an electronic circuit that consists of a \ac{CPU} and other additional circuitry and it is orientated to implement a complex application. These devices have a more complex programming and configuration process, as well as their performance verification, but provide an improvement on their characteristics for advanced applications. 

More in depth, \acp{SoC} enhance performance for demanding control applications by using a special purpose hardware coprocessor, support fixed and floating point calculations through vendor supplied and optimized arithmetic \ac{IP} cores. They offer advanced and flexible integration options thanks to the microcontroller and the common peripherals connected to it~\cite{economakos15}.

A couple of examples of device families of this kind are the \acp{SoC} Cyclone V SoC FPGA by Intel (formerly Altera) and the Zynq-7000 SoC by AMD (formerly Xilinx), which is being widely used in real-time control applications~\cite{yun14,rojas19, flores19}. An example of a generic \ac{SoC} system set of components is shown in the \autoref{fig:ch3embedded_sys} and , actually, the most common device components are the following:
\begin{itemize}
	\item A microcontroller, microprocessor or \ac{DSP}.
	\item An array of configurable logic cells.
	\item Several memory modules. For instance, \acp{ROM}, \acp{RAM}, \acp{EEPROM} and/or Flash memories.
	\item Fixed frequency generators, as quartz oscillators and \acp{PLL}.
	\item Communication interfaces controllers of standards like \ac{USB}, Ethernet and/or \ac{HDMI}.
	\item \Acp{ADC} and \acp{DAC}.
	\item Output and input pins controlled both by the microcontroller or similar and by the configurable logic cells.
\end{itemize}

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/chapter3/embedded_sys}
	\caption[Generic \ac{SoC} system set of components]{Generic \ac{SoC} system set of components~\cite{bashir06}.}
	\label{fig:ch3embedded_sys}
\end{figure}

With this complete set of device components, remaining the system fully synchronous, it is becoming progressively harder to obtain the necessary timing closure. Distribution of a coherent clock signal to every component of the \ac{SoC} is already becoming hard~\cite{claasen06}, as well as an optimal use of the hardware resources of the embedded systems, which is already being studied for modulation schemes implementation in \ac{FPGA}~\cite{lopez19}.

Furthermore, an optimal use of the hardware resources and an efficient communication between the \ac{PS} and the \ac{PL} is a key factor to achieve maximum performance in \acp{SoC}. The communication between these parts is being studied to characterize the communication delays~\cite{costas17}, as they can be an important time in the processing time in real-time applications when both the \ac{PL} and the \ac{PS} are used. 

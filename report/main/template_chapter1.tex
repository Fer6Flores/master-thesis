\chapter{Rules and advice for preparing the manuscript}

\section{Document structure}
A scholarly text is always clearly organized and structured. It may have
\emph{parts}, \emph{chapters}, \emph{sections}, \emph{subsections},
\emph{subsubsections}, and so on. In most cases, three nested levels of
hierarchy should suffice (i.e., chapters, sections, subsections). Hardly ever
are lower levels (e.g., subsubsections) advisable or necessary, and the same
holds for superlevels (parts) unless the text is really long and has clearly
very different parts in its contents.

The hierarchy of structure must be reflected in the numbering, shape and font
size of their headings. Usually, the headings are numbered in arabic and in
hierarchy (e.g., chapternumber. sectionnumber.subsectionnumber), typeset in
boldface and in a larger font than the normal text, but many other styles are
possible. Whatever your personalized style, just be consistent since
regularity aids the reader to know where he/she is.

All chapters open on the right side, that is, on an odd-numbered page, and the
first page does not contain a running head at its top margin. After all, that
first page already contains the chapter title, which is exactly the purpose of
the running head (look at \emph{this} page). If the preceding chapter ends in
a page with odd number, insert a blank page before starting the new
chapter. Blank pages do not need page numbers or running heads, they are
just... blank pages. Since they contain nothing to read, no other part of the
document could ever refer to a blank page. In fact, it is \emph{confusing} if
a blank page has running heads or numbering, for they are a suggestion that
the page was not intended to be blank.\footnote{There is an exception to this
  caveat. A page can have no text in its main box, but it can carry (part of)
  footnotes hanging out from the preceding page, and in such case it must be
  composed as any regular page. Nevertheless, it is better not to incur in
  these situation by writing footnotes carefully or typesetting then
  manually.}

\section{Page style}
In long text documents, pages are double-sided, namely both sides of the sheet
have text. In professional publishing, double-sided pages can be printed in
\emph{rectoverso} ---the areas reserved for text in the front side and the
reverse side match perfectly--- or not, depending on the physical binding of
the book. The reason is that the \emph{inner margins} of the page, i.e., the
right margin in an even page and the left margin in an odd page, are usually
slightly larger than the outer margins. In electronic documents, this
difference is not an issue, and the recommendation is to choose a page layout
in rectoverso.

Choosing the best size of the main text box (width and height of the text) is
an art. It depends on the page dimensions (A4, A5), the font size and even on
the kind of work, its subject. As a general rule, long lines tend to induce
fatigue in the reader, and too many lines per page are visually
unpleasant. Many standard formats take these decisions for you, and it is wise
not to change them unless you have strong reasons. Page composition is a skill
learned through centuries of printing practice, and your personal preferences
are likely to be wrong.

Use running heads in the top margin of the page, and page numbers at the
bottom margin. Follow these rules:
\begin{itemize}
\item Even pages use the chapter title as running head, while odd pages
  usually use the current section title as running head.
\item Page numbers may appear centered in all pages, but its better if the
  numbers go at the right bottom in odd pages and at the left bottom in even
  pages.
\end{itemize}

Technically, a text page is nothing more than a set of \emph{boxes} vertically
and horizontally stacked. These boxes are the paragraphs in the page, but also
the rest of <<floating>> elements ---textual or graphical--- included in it,
like figures anf tables. It is important to signal the start of a new
paragraph. This is done in two ways: 1) indenting slightly to the right the
first word in the paragraph; 2) adding extra space between consecutive
paragraphs. The first one is the orthodox way in written Spanish, but today it
is acceptable to use the other. However, do not use both of them in the same
document.

It is bad practice to leave \emph{widow lines} at the end of a page, that is,
isolated lines which continue in the next page. This can happen with a heading
title, the first line of a new paragraph, a single line preceding an
enumeration (a list) or a line just before an equation. \LaTeX\ is able to
avoid automatically most of these cases, but other word processors don't, so
be careful.

\section{Types: size, fonts, style}
First, a bit of terminology. A font is the set of glyphs for the letters and
signs of an alphabet. Fonts have \emph{types}, \emph{sizes} and \emph{shapes},
the features that define their visual appearance~\cite{Granz00}. The font type
is the general design and appearance of the font signs. Informally, it is the
<<name>> of the font (e.g., Times New Roman, New Baskerville, Arial, etc.)
From a design viewpoint, fonts can be \emph{serif} fonts or \emph{sans
  serif}. A serif is the short horizontal stroke under a letter (look at these
letters carefully: m, n, r) Serifs help the rader to follow the written lines,
so they are the standard choice in almost every printed document. Sans serif
types are those without the small strokes (\textsf{like this}). These fonts
tend to be uncomfortable for reading long documents but, in contrast, sans
serif types look much better on screens and in large sizes, thus being
preferable for slides, presentations or brief reports. With the exception of
the title page, do not mix two or more fonts in the same document for the
normal text. Change of font is only allowed for typesetting some kinds of
content. For instance computer programs, if included in the document, are
usually typeset in \texttt{monospaced font} (one in which the width of the box
for each character is constant).

For the thesis dissertation, use a font size of $10$, $11$ or $12$
points\footnote{A point is a traditional measure of printers.} (pt), according
to your preferences. Be consistent across the document: do not mix arbitrarily
different font sizes in a paragraph,\footnote{In some academic disciplines,
  quotations are highlighted by typesetting the text indented on both sides
  and in a smaller size.} a section, or a chapter. Recall that line spacing
must be proportional to the font size.

For a given font type and size, a text may appear in different shapes: normal
or upright, \emph{italic}, \textbf{boldface}, \textsc{small caps}, and
others. As a general rule, shapes should not be abused. Instead, use them only
sporadicly, since otherwise they will loose their intrinsic meaning. For
instance, italic shapes are used for emphasis and for signaling foreign words;
boldface is used for signaling structure, like in section/chapter titles or
headings.

\section{Typesetting mathematics}

The golden rule for typesetting mathematics correctly is this: variables,
symbols, single-letter functions and symbolic operators are typeset in italic
shape; numbers, standard mathematical functions (e.g., $\log$, $\exp$) or
user-defined functions are typeset in upright shape. Accordingly, a formula
appears as
\begin{equation}
  H(x) := -x \log x - (1 - x) \log (1 - x) \quad \forall x \in [0, 1].
\end{equation}
Formulas must appear in a separate line, centered and possibly numbered, for
further reference from other parts of the document. Note that formulas end
with a period (or another proper sign) when another sentence begins
immediately after. In addition, numerical constants with well-defined values
must appear in roman shape: $\textrm{e} = 2.718281...$. This also applies to
the imaginary unit $\sqrt{-1}$ symbolized as $i$ or $j$, and to $\pi$, but
these rules are neglected frequently. To avoid confusion with other symbols,
the imaginary unit is often typeset in italic but without its upper point, as
$\imath$ or $\jmath$. The differential operator inside an integral \emph{is}
an operator, and must be in roman shape
\begin{equation}
  F(\omega) = \int_{-\infty}^\infty \mathrm{e}^{-\jmath 2\pi \omega t} \; \mathrm{d}F(t).
\end{equation}

The names of physical units are symbols, not abbreviations, and are
standardized. Use them properly, for instance $5\mathrm{\, kWh}$,
$2.5\mathrm{\, cm}$, and the like.\footnote{In particular, note that the unit's
name is slightly separated from its preceding value by a small space, and that
unit symbols do not admit plural form: do not write $2.5\mathrm{\, cms}$.} For
the numbers, use scientific notation preferably ($2.1 \cdot 10^{-5}$) instead
of decimal notation, and do not forget the leading zero for numbers in $[-1,
1]$, e.g., $-0.123$, not $-.123$.

All the rules for typesetting mathematical expressions apply to any place
where these expressions may appear, including tables or figures. A good
tutorial that summarizes the standard rules for typesetting maathematics in
science and engineering texts is
\href{https://tug.org/TUGboat/tb18-1/tb54becc.pdf}{this online document}~\cite{Beccari97}. Also
notice that, in particular areas of mathematics, physics or engineering,
different, idiosyncratic conventions for typesetting mathematical expressions
are used. Just try to learn them before writing your own manuscript. Writing
mathematical texts in consistent, clear and aesthetically beautiful form
---not to mention writing \emph{good} mathematics in style and depth--- is
really difficult~\cite{Moser16}.

\section{Figures and tables}
Figures and tables are inserted in a documents as floating objects. In
general, it is better if they appear at the top or the bottom of the page, and
not in the middle of the text. If a figure or table is big and does not leave
much space for other text, place it alone in a page.

\begin{table}[t]
  \centering
  \caption{\label{table:symbols} Binary operators in \LaTeX}
  \begin{tabular}{llllllll}
    $\amalg$ & \verb|\amalg| & $\cup$ & \verb|\cup| & $\oplus$ & \verb|\oplus|
    & $\times$ & \verb|times| \\ 
    $\ast$ & \verb|\ast| & $\dagger$ & \verb|\dagger| & $\oslash$ &
                                                                    \verb|\oslash|
    & $\triangleleft$ & \verb|\triangleleft| \\
    $\bigcirc$ & \verb|\bigcirc| & $\ddagger$ & \verb|\ddagger| & $\otimes$ &
                                                                              \verb|\otimes|
    & $\triangleright$ &\verb|\triangleright| \\
    $\bigtriangledown$ & \verb|\bigtriangledown| & $\diamond$ &
                                                                \verb|\diamond|
                                                    & $\pm$ & \verb|\pm| &
                                                                           $\unlhd$
               & \verb|\unlhd| \\
    $\bigtriangleup$ & \verb|\bigtriangleup| & $\div$ & \verb|\div| & $\rhd$ &
                                                                         \verb|\rhd|
    & $\unrhd$ & \verb|\unrhd| \\
    $\bullet$ & \verb|\bullet| & $\lhd$ & \verb|\lhd| & $\setminus$ &
                                                                      \verb|\setminus|
    & $\uplus$ & \verb|\uplus| \\
    $\cap$ & \verb|\cap| & $\mp$ & \verb|mp| & $\sqcap$ & \verb|\sqcap| &
                                                                          $\vee$
               & \verb|\vee| \\
    $\cdot$ & \verb|\cdot| & $\odot$ & \verb|\odot| & $\sqcup$ & \verb|\sqcup|
    & $\wedge$ & \verb|\wedge| \\
    $\circ$ & \verb|\circ| & $\ominus$ & \verb|\ominus| & $\star$ &
                                                                    \verb|\star|
    & $\wr$ & \verb|\wr| \\
  \end{tabular}
\end{table}

Place the figure or table \emph{closest to the point where it is referenced
  the first time}, ideally in the same page. If this is not possible, choose
the nearest page (befor or after that point), as in Table~\ref{table:symbols}
and Figure~\ref{fig:example}, so that the reader must not turn the current
sheet to see the figure. Figures and tables must be numbered with independent
sequences, and must include a caption describing their contents, purpose or
meaning. \emph{Never} include a figure or table without its associated
caption.

\begin{figure}[t]
  \centering
  \includegraphics[width=0.8\textwidth]{./images/mud2}
  \caption{\label{fig:example} Example of plotting a function.}
\end{figure}

Bitmap images with high resolution and quality require large digital
representations, and can make the final document very large in size. Try to
use compressed formats (JPEG), usually these do not loose much quality
---particularly in print copies. Also recall that, no matter how high the
resolution of your image is, your document will be limited by the resolution
of the screen or device used for displaying it.

\section{Pseudo Code}
Including algorithms and pseudo-code into technical documents poses a number
of stylistic problems not always easy to bypass. Probably the best solution is
to consider the script, algorithm or pseudo-code block as a new floating
element, and then follow the same guidelines as for other floating objects
like tables or figures. Namely, algorithms are numbered (se the example of
Algorithm~\ref{euclid}, on the next page). It is not customary to include an
explanatory caption for them. Their most remarkable feature is that the lines
in a piece of pseudo-code are typically numbered, so that the explanation of
how the algorithms works, which goes in the text, can refere to specific lines
in the implementation. In addition, unless the number of algorithms included
in the work is large, there is no need to include a <<List of Algorithms>>
page in the frontmatter of the document.

\begin{algorithm}[t]
\caption{My algorithm}\label{euclid}
\begin{algorithmic}[1]
\Procedure{MyProcedure}{}
\State $\textit{stringlen} \gets \text{length of }\textit{string}$
\State $i \gets \textit{patlen}$
\BState \emph{top}:
\If {$i > \textit{stringlen}$} \Return false
\EndIf
\State $j \gets \textit{patlen}$
\BState \emph{loop}:
\If {$\textit{string}(i) = \textit{path}(j)$}
\State $j \gets j-1$.
\State $i \gets i-1$.
\State \textbf{goto} \emph{loop}.
\State \textbf{close};
\EndIf
\State $i \gets i+\max(\textit{delta}_1(\textit{string}(i)),\textit{delta}_2(j))$.
\State \textbf{goto} \emph{top}.
\EndProcedure
\end{algorithmic}
\end{algorithm}

\section{Bibliography}
The bibliography is the list of bibliographic references used along the main
body of the document. It appears as an \emph{unnumbered chapter}, the very
last one (including the appendices, if present). Since in scholarly works the
bibliography is usually long, the complete list of references may appear in a
smaller size than the rest of the document, and with a number of different
typographical conventions.

In the text, references are inserted as typographic marks in a variety of
forms, depending on the area of study, author's preference and customary
practice. A bibliographic reference might appear inside brackets (e.g., <<In
[4] the authors claim that...>>), between parentheses (e.g., <<As (Darwin07)
shows...>>) or even as superscripts (e.g., <<see the works$^{3, 7, 8}$ for
further information>>. The style of citation is diverse, too. It may be a
number, authors and year (e.g., (Dawson \& Jones, 1995)) or some mnemonic
(e.g., (PFT16), typically the initial authors' letters and the year). There
are plenty of style guides for using and citing bibliographies, many of them
with specific recommendations for every field. Some of the most popular are,
for example, the APA (American Psychological Association) style guide
(author/date), the MLA style (arts and humanities), the Harvard style guide,
the Vancouver style guide (medical and biology works), or the
Chicago~\cite{chicago10} and Turabian guides (mostly used in History and
Economics).

In this template for dissertations, we advocate the use of the IEEE
style~\cite{Shell15}, which formats the bibliographic entries as numbered
list, sorted according to the order the reference appears first in the text.

Depending on the style, bibliographic entries vary widely in appearance and
formatting, but all follow a simple rule: a bibliographic entry must contain
enough information to locate without ambiguity the \emph{printed} or
\emph{electronic} document it refers to. At the very least, this includes the
list of authors, the title, the date of publishing, the journal or magazine
which published the text, the pages of the periodical, and so on. Online
resources ---web pages, online documents, etc.--- can be cited, too, and it is
correct to include \textsc{url}s in the corresponding entry. Just notice that
information in the web is \emph{mutable}, that is, it may change with
time. Hence, do not put \emph{dynamic} \textsc{url}: it is better to cite the
source of the data; and, if the document is not permanent, include in the
bibliographic reference the access date when the referenced object contains
data or information which could change in the future.

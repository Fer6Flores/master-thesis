\chapter{Lattice-based Cryptography}
\label{chap:crypo}

\section{Introduction}
During the last years, lattices have become as a basic mathematical object for
robust cryptography. The main reason lies in the fact that their security can
be based on worst-case hardness assumptions, and also because some well-known
lattice problems seem to be secure even against a quantum attacker, i.e., an
attacker capable of using quantum algorithms. In this Chapter, we review
briefly the theoretical foundations of lattice-based cryptography, so as to
have a compact but complete description of the security solutions that we will
design for embedded devices and the IoT scenarios. For a background in
algebraic number theory and algebra, see~\cite{Shoup09}, for instance.

\section{Ring-LWE}

\subsection{Algebraic Number Theory}
Let $\mathcal{R} \simeq \mathbb{Z}[x]/\langle \Phi_m(x) \rangle$ denote the
ring of integer polynomials modulo a \emph{cyclotomic polynomial} $\Phi_m(x)$,
where the $m$-th cyclotomic polynomial $\Phi_m(x) \in \mathbb{Z}[x]$ of
degree\footnote{$\varphi(m)$ is the Euler's totient function: the number of
  integers $1 \leq i < m$ coprime to $m$.}  $n = \varphi(m)$ is the polynomial
whose roots are all the primitive roots of unity $\omega_m^i \in \mathbb{C}$,
for $\omega_m = \exp(2\pi \sqrt{-1} / m)$ with $i$ coprime to $m$. The case
where $m$ is a power of two is particularly relevant in that we have
$\Phi_m(x) = x^n + 1$ for $n = m/2$, that is, $n$ is also a power of two. In
many cases, it is more natural to view this ring as the ring of algebraic
integers in a cyclotomic number field, the latter defined as follows. A
\emph{number field} is a field extension $K = \mathbb{Q}(\zeta)$ formed after
adjoining the abstract element $\zeta$ to the field of rationals, where
$f(\zeta) = 0$ for some irreducible polynomial $f(x)$ of degree $n$ with
rational coefficients. $f$ is the \emph{minimal polynomial} of $\zeta$, and
can be taken as monic without loss of generality. Alternatively,
$f(\zeta) = 0$ allows to view $K$ as an $n$-dimensional vector space over
$\mathbb{Q}$ with basis $\{ 1, \zeta, \dots, \zeta^{n-1} \}$, the so-called
\emph{power basis} of $K$. $K$ is naturally isomorphic to
$\mathbb{Q}[x]/\langle f(x) \rangle$ via the mapping $\zeta \rightarrow x$,
which in turn transforms the power basis into the monomial basis
$\{1, x, \dots, x^n-1 \}$. The $m$-th \emph{cyclotomic number field} is
$K = \mathbb{Q}(\zeta)$ when $\zeta$ is an element of multiplicative order
$m$. The minimal polynomial of $\zeta$ is therefore
\begin{equation*}
  \Phi_m(x) = \prod_{i \in \mathbb{Z}^*_m} (x - \omega_m^i) \in \mathbb{Z}[x],
\end{equation*}
where $\omega_m$ is any primitive $m$-th complex root of unity.

The elements of ring $\mathcal{R}$ can easily be \emph{embedded} into a
\emph{lattice}, a discrete additive subgroup of a given ambient group $H$. In
other words, a lattice is the set of all integer linear combinations of some
set of $n$ linearly independent basis vectors
$\mathbf{B} = \{ \mathbf{b}_1, \dots, \mathbf{b}_n \}$ in $H$
\begin{equation*}
  \Lambda(\mathbf{B}) = \left\{ \sum_{i = 1}^n z_i \mathbf{b}_i: \mathbf{z}
    \in \mathbb{Z}^n \right\} = \mathbb{Z} \mathbf{b}_1 + \cdots + \mathbb{Z}
  \mathbf{b}_n.
\end{equation*}
One such form of embedding is the na\"ive \emph{coefficient embedding} that
maps any $r \in \mathcal{R}$ to the integer vector in $\mathbb{Z}^n$ ---a
lattice point--- whose coordinates are the coefficients of $r$ when expressed
as a polynomial residue. Consequently, in general, an embedding is a mapping
from $\mathcal{R}$ to some lattice $\sigma(\mathcal{R})$ in
$\mathbb{R}^n$. For the embedding $\sigma(\cdot)$, the set of \emph{ideal
  lattices} is the set of all lattices $\sigma(\mathcal{I})$ for ideals
$\mathcal{I}$ in $\mathcal{R}$. Another more interesting embedding is the
\emph{canonical embedding} $\tau: K \rightarrow \mathbb{R}^{s_1} \times
\mathbb{C}^{2 s_2}$ that acts as
\begin{equation*}
  \tau(x) = \bigl( \tau_1(x), \dots, \tau_n(x) \bigr),
\end{equation*}
where $n = s_1 + 2 s_2$, $\tau_j$ for $j = 1, \dots, s_1$ is the isomorphism
that maps $\zeta$ to the $j$-th real root of unity, and $\tau_j$ for
$j = s_1 + 1, \dots, 2 s_2$ are the isomorphisms that map $\zeta$ to each
complex conjugate roots of unity. We assume that
$\tau_{s_1 + s_2 + j} = \overline{\tau_{s_1 + j}}$ for $j = 1, \dots,
s_2$. The canonical embedding is a ring isomorphism that has the convenient
feature that additions and multiplications in
$\mathbb{R}^{s_1} \times \mathbb{C}^{2 s_2}$ can be done both
component-wise.\footnote{In contrast, \emph{multiplications} with the
  coefficient embedding are polynomial multiplications modulo $\Phi_m(x)$,
  which depending on the choice of the minimal polinomial $\Phi_m(x)$ might
  become rather complex. When $m$ is a prime or a prime power, $\Phi_m(x)$
  takes a simple form with small coefficients, e.g.
  $\Phi_p(x) = 1 + x + \dots + x^{p-1}$.}

Incidentally, note that the image space is 
\begin{equation*}
  H = \{ (x_1, \dots, x_n) \in \mathbb{R}^{s_1} \times \mathbb{C}^{2 s_2}:
  x_{s_1 + s_2 + j} = \overline{x_{s_1 + k}}, \forall j \in [s_2] \} \subseteq
  \mathbb{C}^n.
\end{equation*}
This space is also isomorphic to $\mathbb{R}^n$ as an inner product space.

Through the (canonical) embedding we can endow $K$ with a suitable geometry,
where the $\ell_p$-norm of $a \in K$ is $|| a ||_p = ||\sigma(a) ||_p$ for any
$p \geq 1$ and $p = \infty$. Notice that, since the canonical embedding is a
ring homomorphism, we have $||a \cdot b|| \leq ||a||_\infty \cdot ||b||$ both
for the $\ell_2$ and $\ell_\infty$ norms. In words, this means that the error
obtained in multiplying two noisy elements is bounded. Furthermore, given the
individual embeddings $\sigma_i: K \rightarrow \mathbb{C}$ the \emph{trace}
$\operatorname{Tr} = \operatorname{Tr}_{K / \mathbb{Q}}: K \rightarrow
\mathbb{Q}$ is the sum of all $\mathbb{Q}$-homomorphisms from $K$ to
$\mathbb{Q}$
\begin{equation*}
  \operatorname{Tr}_{K/\mathbb{Q}}(a) = \sum_{i \in \mathbb{Z}^*_m} \sigma_i(a).
\end{equation*}
The trace is a $\mathbb{Q}$-linear map.  The \emph{norm}
$N = N_{K/\mathbb{Q}}: K \rightarrow \mathbb{Q}$ is defined as the product of
all embeddings
\begin{equation*}
  N_{K/\mathbb{Q}(a)} = \prod_{i \in \mathbb{Z}^*_m} \sigma_i(a).
\end{equation*}

An \emph{algebraic integer} of $K$ is any element whose minimal polynomial
over $\mathbb{Q}$ has integer coefficients. The set $\mathcal{O}_K$ of
algebraic integers in $K$ forms a ring called the \emph{ring of integers} of
the number field. An \emph{(integral) ideal}
$\mathcal{I} \subseteq \mathcal{O}_K$ is a nontrivial additive subgroup closed
under multiplications. All ideals are finitely generated, and can be given as
the set of all $\mathbb{Z}$-linear combinations of some basis in $K$. The ring
$\mathcal{O}_K$ has the property of \emph{unique factorization of ideals}:
every ideal $\mathcal{I} \subseteq \mathcal{O}_K$ can be expressed uniquely as
a product of powers of prime ideals. Moreover, in $\mathcal{O}_K$ an ideal is
prime if and only if it is maximal, namely if it is not contained in another
proper ideal. Finally, a \emph{fractional ideal} $\mathcal{I}$ is a set such
that $d \mathcal{I}$ is an integral ideal for some $d \in \mathcal{O}_K$. Note
that a fractional ideal is not always an ideal, but rather a submodule of the
ring. Since any fractional ideal $\mathcal{I}$ has a $\mathbb{Z}$-basis
$U = \{ u_1, \dots, u_n \}$, the canonical embedding acting on $\mathcal{I}$
yields an \emph{ideal lattice} $\sigma(\mathcal{I})$ with basis
$\{ \sigma(u_1), \dots, \sigma(u_n) \}$. Therefore, with a small abuse of
language, we may identify an ideal with its embedded lattice. For any lattice
$\mathcal{L}$ in $K$, it \emph{dual ideal} is defined as
\begin{equation*}
  \mathcal{L}^\vee = \{ x \in K: \operatorname{Tr}(x \mathcal{L}) \subseteq
  \mathbb{Z} \}.
\end{equation*}
For the canonical embedding,
$\sigma(\mathcal{L}^\vee) = \overline{\sigma(\mathcal{L})^*}$. An easy property is
$(\mathcal{L}^\vee)^\vee = \mathcal{L}$, and
$\mathcal{L}^\vee$ is a fractional ideal when $\mathcal{L}$ so is. For the
ring 4R4 of algebraic integers, its dual satisfies that $R^\vee$ is simply a
scaling of $R$.

\subsection{Ring LWE}

The \emph{minimum distance} $\lambda_1(\Lambda)$ of a lattice $\Lambda$ in a
given norm $||\cdot||$ is the length of the shortest nonzero vector,
$\lambda_1(\Lambda) = \min_{\mathbf{0} \neq \mathbf{x} \in \Lambda} ||
\mathbf{x} ||$.

The following are three (widely believed) hard computational problems on ideal
lattices: the \emph{Shortest Vector Problem} (\textsf{SVP}), the
\emph{Shortest Independent Vector Problem} (\textsf{SIVP}) and the
\emph{Bounded-Distance Decoding Problem} (\textsf{BDD}). The three can be
considered without loss of generality only for integral ideals in
$\mathcal{O}_K$, where $K$ is a cyclotomic number field.
\begin{definition}[\textsf{SVP} and \textsf{SIVP}]
    Let $K$ be a number field equipped with some norm, and let $\gamma \geq
    1$. The $K$-\textsf{SVP} problem is: given a fractional ideal
    $\mathcal{I}$ in $K$, find some nonzero $x \in \mathcal{I}$ such that
    $||x|| \leq \gamma \lambda_1(\mathcal{I})$. The $K$-\textsf{SIVP} problem
    is to find $n$ linearly independent elements in $\mathcal{I}$ with bounded
    norm, at nost $\gamma \lambda_1(\mathcal{I})$.
\end{definition}
\begin{definition}[\textsf{BDD}]
  Let $K$ be a number field endowed with a norm. Let $\mathcal{I}$ be a
  fractional ideal in $K$, and let $d < \lambda_1(\mathcal{I}$. The
  $K$-\textsf{BDD} problem in the given norm is: given $\mathcal{I}$ and $y$
  of the form $y = x + e$ for some $x \in \mathcal{I}$ and $||e|| \leq d$,
  find $x$.
\end{definition}

Thus, informally, \textsf{SVP} is the problem of finding a ``short'' vector in
the ideal $\mathcal{I}$ (to within a factor $\gamma$), and \textsf{BDD} is the
problem of decoding a noisy version of $x \in \mathcal{I}$ when the error term
$e$ has a small norm. Equivalently, \textsf{BDD} is the problem of finding the
nearest (in the given norm) lattice point to $y = x + e$ when $e$ is not ``too
large''.

Next, we recall the definitions of the ring-learning with errors
(ring-\textsf{LWE}) problems. Originally, the proposal of \textsf{LWE} as a
computationally difficult problem is due to~\cite{Regev09}. Its ring-based
versions appeared later in~\cite{Lyubashevsky13a}
and~\cite{Lyubashevsky13b}. Again, let $K$ be a number field and
$R = \mathcal{O}_K$ its ring of integers. Fix an integer modulus $q \geq
2$. Denote by $\mathcal{I}_q$ the set $\mathcal{I}/q\mathcal{I}$ for any
fractional ideal $\mathcal{I}$, and finally let
$\mathbb{T} = K_\mathbb{R} / R^\vee$.
\begin{definition}[Ring-\textsf{LWE}, distribution]
  For $s \in R^\vee$ and an error distribution $\psi$ over $K_\mathbb{R}$,
  generate a sample $A_{s,\psi}$ from the ring-\textsf{LWE} distribution over
  $R_q \times \mathbb{T}$ by choosing $a \leftarrow R_q$ uniformly at random
  and $e\leftarrow \psi$. Output the pair
  $(a, b = (a \cdot s) / q + e \mod R^\vee)$.
\end{definition}
\begin{definition}[Ring-\textsf{LWE}, search version]
  Let $\Psi$ be a family of distributions over $K_\mathbb{R}$. Given access to
  arbitrarily many independent samples from $A_{s,\psi$, for some arbitrary $s
    \in R^\vee_q$ and $\psi \in \Psi$, find $s$.
\end{definition}
\begin{definition}[Ring-\textsf{LWE}, average-case decision version]
  Let $\Xi$ be a distribution over a family of error distributions over
  $K_\mathbb{R}$. The average-case decision version of the ring-\textsf{LWE}
  problem is to distinguish between arbitrarily many independent samples from
  $A_{s,\psi}$ for a random choice of $(s, \psi) \leftarrow U(R^\vee_q) \times
  \Xi$ and the same number of uniformly random and independent samples from
  $R_q \times \mathbb{T}$.
\end{definition}

Usually, for cryptographic applications, $K = \mathbb{Q}(\zeta_m)$, where
$\zeta_m$ is a $m$-th primitive root of unity, and $q = 1 \mod m$. The
distribution is in many cases a discretized Gaussian distribution over
$K_\mathbb{R}$.

Therefore, the search variant of the ring-\textsf{LWE} is this: given a list
of pairs $(a_i, b_i)$, for $i = 1, \dots, t$, where $b_i = a_i \cdot s + e_i$
and the $\{ e_i \}$ were randomly chose with small norm, solve for the
``secret'' $s$. In the equivalent decision version, one is given the list
$(a_i, b_i)$ and the task consists in deciding whether the list has been truly
generated in a random fashion, or it is a construction of form $b_i = a \cdot
s + e_i$ for some $s$.


\subsection{Hardness of ring-\textsf{LWE}}
We summarize in this Section the main results about the computational hardness
of the above problems on ideal lattices and learning with errors. For our
particular purposes, it suffices to state that the decision version of
ring-\textsf{LWE} is provably equivalent to the search
version~\cite{Lyubashevsky13a}, and that \textsf{SVP} has been proven to be
NP-hard~\cite{Micciancio02}. More importantly, it is proven
in~\cite{Lyubashevsky13a} that there is a reduction from \textsf{SVP} to
ring-\textsf{LWE}. The result is too technical ti quote here, but an informal
statement is like this.
\begin{theorem}[\cite{Lyubashevsky13a}]
  Assume it is hard for polynomial-time \emph{quantum} algorithms to
  approximate the \textsf{SVP} problem on ideal lattices in $K =
  \mathbb{Q}(\zeta_m)$ to within a fixed $\operatorname{poly}(m)$ factor. Then
  any $\operatorname{poly}(m)$ number of samples drawn from the
  ring-\textsf{LWE} distribution are pseudorandom for any polynomial-time
  (quantum) attacker.
\end{theorem}
Consequently, if an efficient (quantum) algorithm for solving
ring-\textsf{LWE} exists, then the approximate \textsf{SVP} could be also
solved efficiently. Yet, so far, the fastest known algorithms for obtaining an
approximation to \textsf{SVP} on ideal lattices to within polynomial factors
require time $2^{\Omega(n)}$ just as in the case of general lattices. This
suggests with good confidence that the hardness of computational problems on
ideal lattices is very high too, despite the fact that a rigorous proof of
this claim has not been found yet. 

\section{Cryptographic Schemes}
 
In this Section, we summarize the operations of a \emph{public key encryption
  scheme} based on the ring-\textsf{LWE} problem. The ensuing scheme is based
on the one described in~\cite{Lyubashevsky13b}, adapted later in~\cite{Roy14}
for a more efficient implementation.

To understand the scheme, let $R_q = \mathbb{Z}_q[x] / \langle f(x) \rangle$
be the underlying polynomial ring, let $\chi_\sigma$ denote a discrete
Gaussian distribution with standard deviation $\sigma$, and for a polynomial
$z$, let $\tilde{z} = \operatorname{NTT}(z)$ be the \emph{number theoretic
  transform} (NTT) of $z$. The NTT is simply the discrete Fourier Transform of
$z$, or equivalently the evaluation of polynomial $z$ on the odd primitive
roots of unity.

The fundamental cryptographic primitives of the scheme are the following:
\begin{enumerate}
\item Sample $a \in R_q$ uniformly at random.
\item $\mathsf{KeyGen}(\tilde{a})$. Sample two polynomials $r_1, r_2$ from
    $R_q$ according to the distribution $\chi_\sigma$ using a discrete Gaussian
    sampler. Compute
    \begin{equation*}
      \tilde{r}_1 \leftarrow \operatorname{NTT}(r_1) \qquad \tilde{r}_2
      \leftarrow \operatorname{NTT}(r_2) \qquad \tilde{p} \leftarrow
      \tilde{r}_1 - \tilde{a} \cdot \tilde{r}_2
    \end{equation*}
    The private key is $\tilde{r}_2$ and the public key is $(\tilde{a},
    \tilde{p})$.

\item $\mathsf{Encrypt}(\tilde{a}, \tilde{p}, m)$. For a message $m$,
  $\mathsf{Encode}(m) = \overline{m} \in R_q$. Randomly draw three error
  polynomials $e_1, e_2, e_3 \leftarrow \chi_\sigma$ from the discrete Gaussian
  distribution $\chi_\sigma$ and compute
  \begin{equation*}
    (\tilde{c}_1, \tilde{c}_2) \leftarrow \bigl( \tilde{a} \cdot \tilde{e}_1 +
    \tilde{e}_2, \tilde{p} \cdot \tilde{e}_1 + \operatorname{NTT}(e_3 +
    \overline{m}) \bigr).
  \end{equation*}

\item $\mathsf{Decrypt}(\tilde{c}_1, \tilde{c}_2, \tilde{r}_2)$. Output
  \begin{equation*}
    m^\prime = \operatorname{INTT}(\tilde{c}_1 \cdot \tilde{r}_2 + \tilde{c}_2)
  \end{equation*}
  where $\operatorname{INTT}$ is the inverse $\operatorname{NTT}$.  Recover
  the message as $m = \mathsf{Decode}(m^\prime)$.
  \end{equation*}
\end{enumerate}
The recommended parameter sets for the above scheme are $(n, q, \sigma) =
(256, 7681, 11.31/\sqrt{2\pi})$ for medium-term security, and $(n, q, \sigma)
= 512, 12289, 12.18/\sqrt{2\pi})$ for long-term security. We refer the reader
to~\cite{Lyubashevsky13a} for a detailed proof of the security with this
cryptographic scheme.

(TeX-add-style-hook
 "chapter4"
 (lambda ()
   (LaTeX-add-labels
    "eq:decomp:problem-I"
    "eq:decomp:constraints"
    "eq:decomp:constraints-upper"
    "eq:decomp:constraints-lower"
    "eq:decomp:constraints-dl"
    "eq:decomp:constraints-ul"
    "eq:decomp:utility"
    "eq:decomp:full-dual"
    "eq:decomp:subproblem-dl"
    "eq:decomp:subproblem-ul"
    "eq:decomp:master-dual"
    "eq:decomp:subproblem:full"
    "eq:decomp:slack"
    "eq:decomp:primal"
    "eq:decomp:admm")))


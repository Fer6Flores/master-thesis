\chapter{Comments to the frontmatter style}

\section{The title page}

\paragraph{}
In a printed work, the \highlight{frontmatter} is the set of pages that precede the
main body of text. They include the title page, the dedication page, the
acknowledgements page, the preface, the abstract, the table of contents, the
list of figures and the list of tables. Some, though not all, of these pages
are numbered in lowercase roman numbers in English
documents,\footnote{Uppercase roman numerals, in documents written in
  Spanish.} and all the pages count to determine the total number of pages in
the frontmatter.

\paragraph{}
In printed works, the \highlight{cover} and the \highlight{title page} are
different: the cover is the front exterior jacket that encloses the pages of
the book, and which usually reproduces the same information as in the title
page, whereas the title page is the first inner page containing the title of
the work, its authors and any other basic data. In electronic documents, the
title page fills both uses, so there is no cover page. In every case, the
title page shows up to the right and \highlight{without number}.

\paragraph{}
In this template, the \highlight{title page is standardized} and its elements
must not be changed in aspect or position. Simply replace each variable
element with the particular information of the thesis.

\paragraph{}
The title should be chosen to be descriptive and for helping future search or
queries to bibliographic databases. For those reasons, it should contain the
key words which describe the contents of the work. We recommend a concise
title (one sentence) avoiding unusual or infrequent acronyms. The title
\highlight{never} finishes with a period, and the same holds for any other
textual element in the title page which appears as a single entity.

The title may be typeset (i) all in uppercase shape; (ii) with only the first
letter of its first word in uppercase. However, recall that the title must
respect all the usual orthographic rules of the language (e.g., graphical
accents, individual names, etc.)

\paragraph{}\textbf{Orthotypographic rules}
Words in foreign languages ---foreign to the principal language of the
document--- must be typeset in \highlight{italic} shape, which is also the
shape used to \emph{emphasize} or highlight words or terms. The use of
\textbf{boldface} typefaces or \highlight{colored text} for highlighting words
in a normal paragraph is annoying, and it is highly discouraged in academic
works and in general.\footnote{Certainly, the present documents violates this
  precept. Nobody is perfect.} Acronyms must appear always in
uppercase. Person names must apper in direct form, and initials must be
followed by a period (e.g., Johann Wolfgang von Goethe, or J. W. von Goethe;
but not JW von Goethe). In Spanish, the initial letters of names carry its
graphical accent (e.g. \'A. (\'Angel) de la Guarda). The title page is
\highlight{never} numbered.
  
\section{The dedication page}

\paragraph{}
The dedication page is optional. The page is not numbered, but counts to the
total number of pages in the frontmatter. The dedication page admits literary
style, and any aesthetic figures that the author considers, such as poems or
quotations. 

\paragraph{}
The dedication page is the place for giving thanks for the moral or
sentimental support received by the author.

\paragraph{}
Dedications appear in a single paragraph, and do not finish with a
period. They are usually aligned to the right and typeset in italic shape.
More than one dedication is possible, in that case add some vertical blank
space between them to be able to distinguish easily each dedication paragraph.

\section{The acknowledgements page}

\paragraph{}
The acknowledgement page is optional. It is intended for recognizing and
thanking the help given by other people, instituions, or firms during the
planning, execution or writing of the work.

\paragraph{}
The acknowledgements page may be numbered in roman numerals.

\paragraph{}
It is recommended that the date is included in the acknowledgement
page.

\section{The preface page}

\paragraph{} The \highlight{preface page} is unusual in scholar works, yet
might be justified in some cases. The preface presents certain auxiliary
information about the work, not essential but helpful to put the ensuing work
in a wider technical, historical or philosophical context.

\section{The abstract page}

\paragraph{}
The abstract and keywords are \highlight{mandatory} in a thesis. The abstract
is just a summary of the complete contents of the report, from the motivation
to the goals and the final conclusions. The abstract anticipates the main body
of text, thus one of its functions is to invite the readers to engage the
story. That's why the abstract should be especially clear and specific about
the topic. The typical practice is that the abstract comes in one paragraph
and one page, having between $150$ and $250$ words. The abstract is written in
present or past tense, never in future tense, and must not include new
acronyms nor references to tables, figures or bibliography.

\paragraph{}
Include $5$-$8$ keywords, listed in alphabetical order and capitalizing the
first letter in each keyword. Keywords are well-known terms used in the field,
not too much general (e.g., Mathematics).

\paragraph{}
If the main language of the document is not English, then either the reverse face of
the abstract page or the next odd-numbered page must contain its translation
to English. Use the heading 'Abstract' without translation, and put the text
of the abstract and the keywords in normal typeface, not in italic shape.

\section{Table of contents}

\paragraph{}
If the document contains more than a few pages (say, $10$), a table of
contents is mandatory. The table of contents is a depiction of the structure
of the work, abd lists the hierarchy of headings for the parts, chapters,
sections, etc. of the document, along with the page where each element
starts. The hierarchy must be visualized with different indentations

\paragraph{}
All the pages in the frontmatter, except the title page and the table of
contents itself\footnote{After all, if you are reading the table of contents,
  it is obvious that you do not need how to locate the table of contents.}
must appear in the table of contents.

\paragraph{}
The page numbers where each section begins must appear alone, without any
qualifier, which is unnecessary. Just use '1  Chapter one.........   1',
not '1  Chapter one.........   Page 1'.

\section{List of figures and list of tables}

\paragraph{}
These elements are optional. The lists of figures and tables form an index for
the floating elements (tables and figures) included in the rest of the
text. These are called <<floating>> because their actual position within the
page may change as a result of the algorithm used for composing the page
visually, and also because figures and tables are numbered independently,
without reference to the page where they appear.
 
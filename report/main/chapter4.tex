\chapter{Motor drive architecture based on a CHB multilevel converter and a Zynq-7000 SoC}
\vskip0.5cm
%% MIN
The algorithm to be implemented is a variation of the \ac{MIN} one. As mentioned in the \autoref{sec:ch3multi}, the \ac{MIN} algorithm takes advantage of the multiple degrees of freedom multiphase machines have, using it to find an optimal solution which has into account the constraints of the output because of the faulty state of the converter. 

For this project, the studied algorithm is designed for a \ac{VSI} with a 5-phases machine as load. This algorithm will deal with one faulty H-bridge in the \ac{VSI}. A scheme of the global system can be seen in the \autoref{fig:ch4systemsch}, where is shown how the controller will manage the inverter, which is the power converter that powers the motor.

%% Global scheme
\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{images/chapter4/ch4_systemsch}
	\caption[Motor drive architecture based on a CHB multilevel converter and a Zynq-7000 SoC]{Motor drive architecture based on a CHB multilevel converter and a Zynq-7000 SoC.}
	\label{fig:ch4systemsch}
\end{figure}

Regarding the \ac{MIN} algorithm, it uses the $\alpha$-$\beta$ components as input, which are the output of the current controller used to attain the flux and the torque references of the machine. Using these as base components, the \ac{MIN} algorithm injects appropriate $x$-$y$ and $zero$ sequences component to avoid over-modulation because of the faulty cell.

%% Why Zynq
The device target that will be the core of the embedded system in which this algorithm is going to be implemented is a MicroZed board \cite{MicroZed_HW_UG}. This device is a great option for the project objectives because of two different, but linked, reasons. On the one hand, it has a Zynq-7000 as its core, a \ac{SoC} that is being widely used in real-time power applications, as previously mentioned in the \autoref{sec:ch3socs}, so this device will be a good basis one to test the real implementation of the \ac{MIN} algorithm. On the other hand, this \ac{SoC}, has a microcontroller and an array of configurable logic cells, which will allow to test different implementations in the board.

%% Holy Zynq
More in deep, the MicroZed has a Zynq-7010 All Programmable SoC, which uses, as \ac{PS}, a dual ARM Cortex-A9 processor core with a maximum frequency of 667 MHz and 1 GB of DDR3 external memory and, as \ac{PL} a Artix-7 \ac{FPGA} with 28000 programmable logic cells and 80 programmable \ac{DSP} slices. In the board, a dedicated 33.33 MHz clock source is connected to the Zynq-7000 \ac{SoC}. The \ac{PS} infrastructure can generate up to four \ac{PLL}-based clocks for the \ac{PL} system. Furthermore, several features like external communication interfaces, analog interfaces, standard connectors and so on are available, as shown in \autoref{fig:ch3zynqscheme}.

\begin{figure}
	\centering
	\includegraphics[height=0.67\linewidth]{images/chapter3/zynq_scheme}
	\caption[General scheme of the MicroZed board structure, from the Zynq-7000 devices family]{General scheme of the MicroZed board structure, from the Zynq-7000 devices family~\cite{MicroZed_HW_UG}.}
	\label{fig:ch3zynqscheme}
\end{figure}

Regarding the external communication interfaces of the \ac{PS}, it has the following peripherals: two \acp{UART}, two \ac{CAN}, two \ac{I2C}, two \ac{SPI} and four 32-bits \acp{GPIO}. It also has \ac{USB} and Ethernet peripherals. On the other hand, the \ac{PL} has two \acp{XADC} to interact with analog signals~\cite{Zynq7000_overview}.

The MicroZed is designed to be used either as a standalone evaluation kit or as a \ac{SOM} connected to a carrier card. Supporting these multiple use cases, the board has been required to be designed with multiple power input sources. This device also integrates some user I/O as push buttons and user LEDs, as well as an expansion header compatible with Digilent Pmod standard~\cite{pmod_spec} and two 100-pin microheaders for connection to expansion carrier cards like the MicroZed I/O Carrier Card~\cite{IOCC_HW_UG}. For instance, in the \autoref{fig:ch4zed} appears the Microzed coupled to the MicroZed I/O Carrier Card, which is used in this project.

\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{images/chapter4/ch4zed}
	\caption[Microzed connected to the MicroZed I/O Carrier Card]{Microzed connected to the MicroZed I/O Carrier Card.}
	\label{fig:ch4zed}
\end{figure}

%% Zynq SoCs issues
This \ac{SoC} also performs some \ac{PS} to \ac{PL} interface ports. The most relevant is the \ac{AXI} protocol. \ac{AXI} is part of ARM AMBA, a family of microcontroller buses first introduced in 1996. The second version of this protocol is AXI4, which is used in this \ac{SoC}~\cite{AXI_RG}. There are three types of \ac{AXI}4 interfaces:
\begin{itemize}
	\item AXI4: for high-performance memory-mapped requirements.
	\item AXI4-Lite: for simple, low throughput memory-mapped communication.
	\item AXI4-Stream: for high-speed streaming data.
\end{itemize} 

The specifications of this protocol describe an interface between a single master and a single slave with five channel types for connecting them. A scheme of the channels available in \ac{AXI}4 interfaces is shown in the \autoref{fig:ch3axi}. These five different channels, which have just been shown are:
\begin{itemize}
	\item Read Address Channel.
	\item Write Address Channel.
	\item Read Data Channel.
	\item Write Data Channel.
	\item Write Response Channel.
\end{itemize}

\begin{figure}
	%	\centering
	\begin{subfigure}[t]{0.49\linewidth}
		\centering
		\includegraphics[width=\linewidth, valign=t]{images/chapter3/axi_read}
		\caption{AXI4 channel architecture of readings}
		\label{fig:ch3axi_read}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.48\linewidth}
		\centering
		\includegraphics[width=\linewidth, valign=t]{images/chapter3/axi_write}
		\caption{AXI4 channel architecture of writings}
		\label{fig:ch3axi_write}
	\end{subfigure}
	\caption[AXI4 channel architecture of readings and writings]{AXI4 channel architecture of readings and writings~\cite{AXI_RG}}
	\label{fig:ch3axi}
\end{figure}

As shown in these schemes, data can move in both directions between the master and slave simultaneously, and data transfer sizes can vary. Using AXI4, a single address is required and then, up to 256 data word are bursted up, followed by a response in the case of writings. Using these channels, master and slave interfaces can be connected together through a structure called Interconnect block, which is provided as a Xilinx \ac{IP} core. This block has AXI-compliant master and slave interfaces and it is used to route transactions between one or more AXI master and slave interfaces. 

Having into account all the available resources in the MicroZed board, the embedded system architecture has being designed as shown in the \autoref{fig:ch4microzedsch}. In this designed architecture, the ARM Cortex-A9 of the Zynq-7000 is used for the calculations related to the implementation of the \ac{MIN} algorithm and the Artix-7 \ac{FPGA} performs the modulator and I/O interfaces. Both parts have external communication interfaces and use the AXI4 bus for the communication between them. This embedded system will perform \ac{MIN} algorithm as well as the auxiliary modules.
 
\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{images/chapter4/ch4_microzedsch}
	\caption[Embedded system modules scheme]{Embedded system modules scheme.}
	\label{fig:ch4microzedsch}
\end{figure}

\chapter[Frame Asynchronous CSA with MUD]{Frame Asynchronous Coded Slotted Aloha with Multipacket Detection}

\section{Introduction}

Coded slotted ALOHA (CSA) combines a classical uncoordinated multiple access
technique (ALOHA~\cite{Abramson70}) with coded transmissions by the sender to
provide highly reliable uncoordinated multiple access. In \emph{frame
  synchronous} CSA (FS-CSA), transmissions are organized in frames, each
consisting of the same number of slots. Users arrive to the system at random
times, wait until the next frame and transmit $\ell$ replicas of its packet in
randomly chosen slots within the frame. In contrast, in \emph{frame
  asynchronous} CSA users are not synchronized. A new user waits only until
the next slot and selects randomly a number of subsequent slots for
transmitting the replicas of its message. The possible slots that its
transmission can span forms the user's \emph{local frame}, which is assumed of
the same length for all the users in the system.  Clearly, FA-CSA reduces
delay, compared to FS-CSA. Moreover, it has been shown using simulations that
FA-CSA outperforms FS-CSA in terms of throughput.

In this chapter, we characterize the asymptotic and finite-length throughput
and delay of FA-CSA and FS-CSA under the assumption that the intended receiver
is able to decode multiple packets ($k$) received in the same
slot. Multi-packet detection is currently under the reach of technological
progress. We will show that the combination of FA-CSA and multi-packet
detection greatly improves the throughput and the energy-efficiency of large
multiple access systems, thus being a promising technology for the practical
realization of the Internet of Things (IoT).
 
\section{System Model}

We consider a time-slotted system with a variable number of users transmitting
to a common receiver (the AP). A user who joins the system and wishes to
transmit a packet selects a repetition factor $\ell$ according to a predefined
distribution (to be described later). All the messages have duration equal to
one slot. The user transmits its $\ell$ replicas in randomly selected slots. A
user that repeats its transmission $\ell$ times is called a degree-$\ell$
user, and a slot containing $r$ packets is a degree-$r$ slot. We assume that,
for any $i = 1, 2, \dots, k$, all the packets in a degree-$i$ slot can be
correctly decoded, and that once a packet has been decoded, perfect SIC
(successive interference cancellation) can be performed to remove the
interference of the decoded packets in other slots.

We further assume that users join the system according to a slot-based Poisson
arrival process, i.e., the number $N$ of users joining in a given slot has the
probability mass function
\begin{equation}
  \Prob(N = j) = e^{-\lambda_a} \frac{\lambda_a}{j!}, \qquad j = 0, 1, \dots
\end{equation}
where $\lambda_a$ is the mean arrival rate per slot. Arrivals in different
slots are statistically independent.  Finally, we suppose that the
transmission of each replica consumes an amount of energee $n$ y $e$,
independent of the transmitting user.

The three fundamental performance measures for this system are the following.
\begin{definition}
  The packet loss ratio $p$ (PLR) is the average probability that the packet of an
  arbitrary user is never resolved.
\end{definition}
\begin{definition}
  The delay of a resolved packet is the number of slots between the user's
  arrival and the slot where the packet is resolved and correctly decoded.
\end{definition}
\begin{definition}
  The average energy consumption per packet is $\ell e / (1 - p)$.
\end{definition} 
Note that all the packets, either successful or unsuccessful, are transmitted
$\ell$ times.

\subsection{Frame Synchronous Coded Slotted ALOHA}
In FS-CSA, communication takes place during global frames consisting of $n$
slots each. A degree-$\ell$ user that joins the system waits until the next
frame and transmits its $\ell$ replicas in randomly chosen slots of that
frame. We then say that the user is active during the whole duration of the
frame. We denote by $M \sim \text{Poisson}(n \lambda_a)$ the random variable
representing the number of active users per frame, which follows a Poisson pmf
with mean $n \lambda_a$. Note that the active users in a frame are all the
users that joined the system during the previous frame.  We consider decoding
of FS-CSA performed on a slot-by-slot basis. Assume the decoding of slot
$i$. First, the interference caused by packets for which replicas in previous
slots have already been decoded is canceled from the slot. The receiver then
checks if slot $i$ is a degree-$k$ or less slot and, if not, the decoding of
slot $i$ is stopped. Otherwise, the packets in slot $i$ are decoded and the
interference from all its replicas canceled from the corresponding past
slots. The receiver then proceeds to iteratively find any degree-$k$ or less
slots in its memory, decode the packets in these slots, and cancel the
interference of all replicas of the decoded packets. This process continues
until no new degree-$k$ or less slots are found or a maximum number of
iterations is reached.

\subsection{Frame Asynchronous Coded Slotted ALOHA}
In FA-CSA, when a degree-$\ell$ user joins the system it waits until the next
slot and transmits a first replica in that slot. This enforces users to be
slot-synchronous, though not frame-synchronous. The remaining $\ell - 1$
replicas are distributed uniformly within the $n - 1$ subsequent
slots. Similarly to FS-CSA, $n$ is the frame length of FA-CSA. However,
contrary to FS-CSA, slots are not arranged in global frames. We say that a
user is active during the $n$ slots following the slot where it joins the
system and, accordingly, we call the $n$ slots where a user is active its
local frame. Note that a user is not active in the slot where it joins the
system, but only during the $n$ slots it can transmit in. Because a user
always transmits in the first slot of its local frame, we call this system
FA-CSA with first slot fixed (FA-CSA-F).  Decoding of FA-CSA is performed in a
similar way as for FS-CSA, with the only difference that the receiver needs to
consider not only the slots of a current frame, but all slots of the entire
history of the system. In practice, the receiver cannot consider infinitely
many slots and has a finite memory. We denote by $n_\text{RX}$ the size of the
receiver memory in number of slots. A finite $n_\text{RX}$ creates the notion
of a sliding-window decoder.

Let $M_i$ denote the number of active users in slot $i$, which is the number
of users who arrived in slots $[i - n, i - 1]$. $M_i$ is therefore Poisson
distributed with mean $\mu_i$. We distinguish two cases for the first $n$
slots. If there were no active users at the beginning $i = 0$ then
\begin{equation}
  \label{eq:boundary}
  \mu_i = \begin{cases}
    i \lambda_a, & \text{for $1 \leq i < n$} \\
    n \lambda_a, & \text{for $i \geq n$},
  \end{cases}
\end{equation}
that is, the mean $\mu_i$ grows linearly during the start-up slots
$[1, n - 1]$, and then reaches its steady-state value. In the second model, we
neglect this start-up phase and assume that there are already
$M \sim \text{Poisson}(n \lambda_a)$ users initially in the system, at
$i = 0$, as if the system had been already operating for an infinite
time. Thus $\mu_i = n \lambda_a$ for all $i \geq 1$, and all the slots have
the same average number of active users.

\subsection{Bipartite graph representation}

Any instance of a CSA system can be represented by a bipartite graph
$\mathcal{G} = \{ \mathcal{V}, \mathcal{C}, \mathcal{E} \}$ where
$\mathcal{V}$ is the set of variable nodes, $\mathcal{C}$ is the set of check
or constraint nodes and $\mathcal{E}$ is the set of edges connecting the
variable and check nodes. In $\mathcal{G}$, there is an edge
$(i, j) \in \mathcal{E}$ if user $i$ transmits a replica packet in slot
$j$. The degree of a node is the number of edges incident to that node. With
this bipartite graph representation, decoding of CSA can be viewed as a form
of message passing on the underlying
graph~\cite{Sandgren17,Lazaro17,Ivanov15,Ivanov17}

\section{Degree distributions}
Similarly to the general setting of codes defined on graphs, define the
variable-node (VN) degree polynomial and the check-node (VN) degree polynomials
as
\begin{equation}
  \Lambda(x) := \sum_i \Lambda_i x^i, \quad P(x) := \sum_j P_j x^j
\end{equation}
respectively, where $\Lambda_i$ is the probability that an arbitrary variable
node has degree $i$ and $P_j$ is the probability that a check node has degree
$j$. Note that $\Lambda(x)$ is subject to design optimization. We can define
in an analogous manner the edge-perspective VN and CN degree distributions as
\begin{equation}
  \lambda(x) := \sum_i \lambda_i x^{i-1}, \quad \rho(x) :=
  \sum_j \rho_j x^{j - 1},
\end{equation}
where now $\lambda_i$ denotes the probability that an edge is connected to a
degree-$i$ variable node and $\rho_j$ is the probability that the edge is
connected to a degree-$j$ check node. These probabilities are given by
\begin{equation}
  \lambda_i = \frac{i \Lambda_i}{\sum_m m \Lambda_m}, \quad \rho_j = \frac{j
    P_j}{\sum_m m P_m}.
\end{equation}
In other words, $\lambda(x) = \Lambda^\prime(x) / \Lambda^\prime(1)$ and
$\rho(x) = P^\prime(x) / P^\prime(1)$, where $f^\prime$ is the derivative of
function $f$.

\subsection{FA-CSA with First Slot Fixed}
If the CSA system starts empty, the first $n$ check nodes corresponding to the
first $n$ slots have different degree distribution than the subsequent
ones. Note that a variable node at position (i.e., time index) $i$ has an edge
toward a check node at position $i$ and other $\ell - 1$ edges to check nodes
selected randomly at position in the interval
$\mathcal{S}_i = [i + 1, i + n - 1]$. From the point of view of a variable
node at position $i$, its degree distribution is given by
\begin{subequations}
  \begin{align}
    \Lambda^{i \to i}(x) &= x \\
    \Lambda^{i \to \mathcal{S}_i}(x) &= \sum_m \Lambda^{i \to \mathcal{S}_i}_m x^m
    = \sum_m \Lambda_m x^{m - 1}
  \end{align}
\end{subequations}
where $\Lambda_m^{i \to \mathcal{S}_i} = \Lambda_{m + 1}$ is the probability
that a variable node at position $i$ has $m$ connections to check nodes in
$\mathcal{S}_i$. From these, the edge-perspective degree distributions are
\begin{subequations}
  \begin{align}
    \lambda^{i \to i}(x) &= x \\ 
    \lambda^{i \to \mathcal{S}_i}(x) &= \frac{\bigl( \Lambda^{i \to
        \mathcal{S}_i} \bigr)^\prime(x)}{\bigl( \Lambda^{i \to \mathcal{S}_i}
      \bigr)^\prime(1)} = \sum_m \lambda_m^{i \to \mathcal{S}_i} x^{m - 2}
  \end{align}
\end{subequations}
with
$\lambda_m^{i \to \mathcal{S}_i} = \Lambda_m(m - 1) / \sum_m \Lambda_m(m -
1)$. In turn, a check node at position $i$ is connected to $a$ variable nodes
at position $i$ and to $b$ variable nodes at positions in the range
\begin{equation}
  \label{eq:T}
  \mathcal{T}_i = \begin{cases}
    \emptyset, & \quad \text{for $i = 1$} \\
    [1, i - 1], & \quad \text{for $2 \leq i < n$} \\
    [i- n + 1, i - 1], & \quad \text{for $i \geq n$}.
  \end{cases}
\end{equation}
With these, we can define the corresponding check node degree distributions as
\begin{IEEEeqnarray}{rCl}
    P^{i \to i}(x) &=& \sum_m P_m^{i \to i} x^m \\ \IEEEyessubnumber*
    P^{i \to \mathcal{T}_i}(x) &= \sum_m P_m^{i \to \mathcal{T}_i} x^m,
\end{IEEEeqnarray}
where here $P_m^{i \to i}$ is the probability that a check node at position
$i$ has $m$ edges incident to variable nodes at position $i$; and
$P_m^{i \to \mathcal{T}_i}$ denotes the probability that a check node at
position $i$ has $m$ connections to variable nodes in $\mathcal{T}_i$. Again,
as in the above derivation, the edge-perspective degree distribution is
\begin{IEEEeqnarray}{rCl}
    \rho^{i \to i}(x) &=& \frac{\bigl( P^{i \to i} \bigr)^\prime(x)}{\bigl(
      P^{i \to i} \bigr)^\prime(1)} = \sum_m \rho_m^{i \to i} x^{m - 1} \\ \IEEEyessubnumber*
    \rho^{i \to \mathcal{T}_i}(x) &=& \frac{ \bigl( P^{i \to \mathcal{T}_i}
      \bigr)^\prime(x)}{\bigl( P^{i \to \mathcal{T}_i} \bigr)^\prime(1)} = \sum_m
    \rho_m^{i \to \mathcal{T}_i} x^{m - 1}.
\end{IEEEeqnarray}
The following theorem gives the closed-form expressions for the check node
degree distributions.
\begin{theorem}~\cite{Sandgren17}
  \label{th:degree-dist}
  The degree distribution for a check node at position $i$ for FA-CSA with
  first slot fixed is
  \begin{IEEEeqnarray}{rCl}
      P^{i \to i}(x) &=& \rho^{i \to i}(x) = \exp \bigl( -\lambda_a (1 - x)
      \bigr) \\ \IEEEyessubnumber*
      P^{i \to \mathcal{T}_i} &=& \rho^{i \to \mathcal{T}_i}(x) = \exp \left( -
        \frac{ \delta_i \bigl( \Lambda^\prime(1) - 1 \bigr)}{n - 1} (1 - x)
      \right)
  \end{IEEEeqnarray}
\end{theorem}
with
\begin{equation}
  \label{eq:delta-i}
  \delta_i = \begin{cases}
    \lambda_a \min \{ i - 1, n - 1 \}, & \quad\text{for FA-CSA with start-up
      phase} \\
    \lambda_a (n - 1), & \quad\text{for FA-CSA without start-up phase}.
  \end{cases}
\end{equation}
\begin{proof}
  Let $M$ be the random variable measuring the number of edges connecting a
  variable node at position $i$ to a check node at position $i$. It is clear
  that $M \sim \text{Poisson}(\lambda)$, since each user transmits immediately
  when it becomes active. Consequently,
  \begin{equation}
    \label{eq:exponential}
    P_m^{i \to i} = \Prob(A = m) = \exp(-\lambda_a) \frac{\lambda_a^m}{m!} 
  \end{equation}
  and so
  \begin{equation}
    \label{eq:pii}
    P^{i \to i}(x) = \sum_m P_m^{i \to i} x^m = \sum_{m = 0}^\infty
    \exp(-\lambda_a) \frac{\lambda_a^m}{m!} x^m = \exp \bigl(-\lambda_a (1 - x) \bigr).
  \end{equation}
  Furthermore,
  \begin{equation}
    \label{eq:rhoii}
    \rho^{i \to i}(x) = \frac{ \bigl( P^{i \to i} \bigr)^\prime(x)}{ \bigl(
      P^{i \to i} \bigr)^\prime(1)} = \frac{\lambda_a \exp(-\lambda_a (1 -
        x))}{\lambda_a \exp(0)} = \exp \bigl(-\lambda_a (1 - x) \bigr).
  \end{equation}
  For the second part, let $B_i$ denote the random variable that measures the
  number of edges connecting a check node at position $i$ with a variable node
  in the set $\mathcal{T}_i$. The number of variable nodes pertaining to
  $\mathcal{T}_i$ is another Poissonian random variable $|\mathcal{T}_i|$ with
  mean $\delta_i$ in~\eqref{eq:delta-i} and each variable node in
  $\mathcal{T}_i$ is connected to the check node of position $i$ with
  probability
  \begin{equation}
    z = \frac{\Lambda^\prime(1) - 1}{n - 1}.
  \end{equation}
  By conditioning on the values of $|\mathcal{T}_i|$ we have
  \begin{equation}
    P_b^{i \to \mathcal{T}_i}(x) = \sum_{m = b}^\infty \Prob(B_i = b |
    |\mathcal{T}_i| = m) \Prob(|\mathcal{T}_i| = m ) = \sum_{m = b}^\infty
    \binom{m}{b} z^b (1 - z)^{m - b} \exp (-\delta_i) \frac{\delta_i^m}{m!}
  \end{equation}
  which after some simple algebraic manipulations gives
  \begin{equation}
    \label{eq:pb}
    P_b^{i \to \mathcal{T}_i}(x) = \exp (-z \delta_i) \frac{(z \delta_i)^b}{b!}.
  \end{equation}
  Following similar steps as in~\eqref{eq:pii}-\eqref{eq:rhoii}
  \begin{equation}
    P^{i \to \mathcal{T}_i}(x) = \rho^{i \to \mathcal{T}_i}(x) = \exp \left(
      -\frac{ \delta_i \bigl( \Lambda^\prime(1) - 1 \bigr)}{n - 1} (1 - x) \right).
  \end{equation}
\end{proof}

\subsection{FA-CSA with Uniform Slot Selection}
A variation of FA-CSA with fixed first slot is to allow the user to choose the
slots for transmitting its $\ell$ replicas uniformly within a window of $n$
slots, with $n \geq \ell$. In this case, the analysis of the previous Section
simplifies, since we only have to handle one degree distribution per check
node.
\begin{theorem}
  The degree distribution of a check node at position $i$ for FA-CSA with
  uniform slot selection is
  \begin{equation}
    P_i(x) = \rho_i(x) = \exp \left( -\frac{\mu_i}{n} \Lambda^\prime(1) (1 -
      x) \right)
  \end{equation}
  where $\mu_i$ i given by~\eqref{eq:boundary} for FA-CSA with uniform slot selection
  and boundary effect, and $\mu_i = n \lambda$ when there is no boundary effect.
\end{theorem}
\begin{proof}
  The proof is similar to that of Theorem~\ref{th:degree-dist} and is omitted.
\end{proof}

\section{Density evolution}
In the limit of large system size. i.e., when $n \to \infty$, coded slotted
Aloha can be analyzed via the density evolution technique. We address next the
computation of the density evolution. To that end, let us identify un packet
not yet uncoded in a slot as an ``erasure''.\footnote{This is just for keeping
  the terminology familiar, because the approach via density evolution comes
  from the analysis of graph codes over binary erasure channels.}

\subsection{Density evolution for Frame Asynchronous CSA with First Slot
  Fixed}
Recall that in FA-CSA with first slot fixed, a sender transmits always the
first replica in the first slot after its arrival to the system. Therefore, a
variable node at position (time index) $i$ is always connected to the check
node at position $i$, while the remaining edges are connected to other future
positions (randomly chosen). Thus, there are two different edge types and the
density evolution involves the quantities $p_{i \to i}$, $p_{i \to j}$,
$q_{i \to i}$ and $q_{i \to j}$ for the messages exchanged between variable
nodes and check nodes. The subscripts make clear the orientation and type of
the edges.

Let us derive first $p_{i \to i}$ and $p_{i \to j}$, the erasure probabilities
of edges $i \to i$ and $i \to j$, seen from a variable node. An outgoing
message from variable node $i$ is an erasure if all of its incoming messages
are erasures, i.e., if there is no slot such that the replica can be
resolved. Suppose the variable node $v$ has degree $\ell$ and is at position
$i$, so it has an edge to the check node at position $i$ and other $\ell - 1$ edges
at positions in the range $\mathcal{S}_i = [i + 1, i + n - 1]$. Denote the set
of edges from $v$ as $e_0, e_1, \dots, e_{\ell - 1}$. So, the erasure probability
for edge $e_0$ is the probability that all incoming messages along $e_1,
\dots, e_{\ell - 1}$ are erasures. Since the edges $e_1, \dots, e_{\ell - 1}$
are drawn uniformly from $\mathcal{S}_i$ the average incoming erasure
probability along $e_k$, for $k = 1, \dots, \ell - 1$ is the same and is given
by
\begin{equation}
  \label{eq:density1}
  \tilde{q}_i = \frac{1}{n - 1} \sum_{j \in \mathcal{S}_i} q_{j \to i}.
\end{equation}
With this, the probability that the message sent along $e_0$ is an erasure is
$p_{e_0} = \tilde{q}_i^{\ell - 1}$. If we average over the degree
distribution $\Lambda^{i \to \mathcal{S}_i}(x)$, we finally get
\begin{equation}
  \label{eq:density2}
  p_{i \to i} = \sum_{\ell} \Lambda_\ell \tilde{q}_i^{\ell - 1} = \Lambda^{i
    \to \mathcal{S}_i}(\tilde{q}_i).
\end{equation}
With a similar reasoning, $p_{i \to j}$ can be obtained as
\begin{equation}
  \label{eq:density3}
  p_{i \to j} = q_{i \to i} \sum_{\ell} \lambda_\ell^{i \to \mathcal{S}_i}
  \tilde{q}_i^{\ell - 2} = q_{i \to i} \lambda^{i \to \mathcal{S}_i}(\tilde{q}_i)
\end{equation}
for $j \in \mathcal{S}_i$.

The derivation of probabilities $q_{i \to i}$ and $q_{i \to \mathcal{T}_i}$
can be done as follows. An edge outgoing from check node $i$ is an erasure if
at least $k$ of its incoming edges (distinct from the outgoing edges) are
erasures. This is because the receiver can decode perfectly any slot with $k$
or less packets, so only slots with more than $k$ packets are
unsuccessful. Conversely, a message from a check node is not an erasure if
less than $k$ of its other $r_1 + r_2 - 1$ incoming edges are erasures.

Consider a check node of degree $\ell$ at position $i$, say $w$. Check node
$w$ has $r_1$ edges connected to type $i$ variable nodes, and $r_2$ edges
connected to nodes in the range $\mathcal{T}_i$ given in~\eqref{eq:T}. Then
the (conditional) probability that an outgoing message along an edge $e$
connecting $w$ to a variable node of type $i$ is an erasure, $\hat{p}_e$, is
the probability that $k_1$ of the remaining $r_1 - 1$ edges connecting $w$ to
type $i$ variable nodes, and $k_2$ out of the $r_2$ edges connecting $w$ to
some variable node in $\mathcal{T}_i$ are erasures, for any pair $(k_1, k_2)$
such that $k_1 + k_2 \geq k$. Hence
\begin{equation}
  \label{eq:erasure-prob}
  \hat{p}_e = \sum_{k_1 + k_2 \geq k} \binom{r_1 - 1}{k_1} p_{i \to i}^{k_1}
  (1 - p_{i \to i})^{r_1 - 1 - k_1} \binom{r_2}{k_2} \tilde{p}_i^{k_2} (1 -
  \tilde{p}_i)^{r_2 - k_2} 
\end{equation}
where it must be understood that $\binom{a}{b} = 0$ if $a < b$. The particular
case $k = 1$, i.e. without multiuser detection, gives
\begin{equation}
  1 - \hat{p}_e = (1 - p_{i \to i})^{r_1 - 1} (1 - \tilde{p}_i)^{r_2}.
\end{equation}
In the above expressions
\begin{equation}
  \tilde{p}_i = \begin{cases}
    0, & \quad\text{for $i = 1$} \\
    \sum_{k \in \mathcal{T}_i} p_{k \to i} / (i - 1), & \quad\text{for $1 < i
      <n$} \\
    \sum_{k \in \mathcal{T}_i} p_{k \to i} / (n - 1), & \quad\text{for $i \geq n$}
  \end{cases}
\end{equation}
is the average erasure probability of incoming messages from variable nodes in
the set $\mathcal{T}_i$.

Now, we average~\eqref{eq:erasure-prob} over the edge-perspective check node
distribution and the node-perspective check node distribution,
jointly. Consequently
\begin{multline}
  q_{i\to i} = \Esp_{\rho^{i \to i}} \Esp_{P^{i \to \mathcal{T}_i}} \left(
        \sum_{k_1 + k_2 \geq k} \binom{r_1 - 1}{k_1} p_{i \to i}^{k_1} (1 -
        p_{i \to i})^{r_1 - 1 - k_1} \binom{r_2}{k_2} \tilde{p}_i^{k_2} (1 -
        \tilde{p}_i)^{r_2 - k_2} \right) \\
      = \sum_{r_1 = 1}^\infty \sum_{r_2 = 0}^\infty \rho_{r_1}^{i \to i}
      P_{r_2}^{i \to \mathcal{T}_i} \left( \sum_{k_1 + k_2 \geq k} \binom{r_1
          - 1}{k_1} p_{i \to i}^{k_1} (1 - p_{i \to i})^{r_1 - 1 - k_1}
        \binom{r_2}{k_2} \tilde{p}_i^{k_2} (1 - \tilde{p}_i)^{r_2 - k_2} \right).
\end{multline}
Interchanging the outer and inner summations this can be rearranged as
\begin{equation}
  \label{eq:churro}
  q_{i \to i} = \sum_{k_1 + k_2 \geq k} \left( \sum_{r_1 = 1}^\infty
  \rho_{r_1}^{i \to i} \binom{r_1 - 1}{k_1} p_{i \to i}^{k_1} (1 - p_{i \to
    i})^{r_1 - 1 - k_1} \right) \left(
  \sum_{r_2 = 0}^\infty P_{r_2}^{i \to \mathcal{T}_i} \binom{r_2}{k_2}
  \tilde{p}_i^{k_2} (1 - \tilde{p}_i)^{r_2 - k_2} \right). 
\end{equation}
Now, for the first sum
\begin{multline}
  \label{eq:partial}
  \sum_{r_1 = 1}^\infty \rho_{r_1}^{i \to i} \binom{r_1 - 1}{k_1} p_{i \to
    i}^{k_1} (1 - p_{i \to i})^{r_1 - 1 - k_1} \stackrel{(a)}{=} \sum_{r_1 =
    0}^\infty e^{-\lambda_a} \frac{\lambda_a^{r_1}}{r_1!} \binom{r_1}{k_1} p_{i
    \to i}^{k_1} (1 - p_{i \to i})^{r_1 - k_1} \\
  = \frac{(\lambda_a p_{i \to i})^{k_1}}{k_1!} e^{-\lambda_a} \sum_{r_1 =
    0}^\infty \frac{(\lambda_a (1 - p_{i \to i}))^{r_1}}{r_1!} \stackrel{(b)}{=}
  \exp(-\lambda_a p_{i \to i}) \frac{(\lambda_a p_{i \to i})^{k_1}}{k_1!}
\end{multline}
where equality $(a)$ follows from the fact that
$\rho_{r_1}^{i \to i} = P_{r_1 - 1}^{i \to i}$ and~\eqref{eq:exponential}, and
$(b)$ follows because $\sum_{n = 0}^\infty x^n / n! = \exp(x)$.  For the
second term we have, using~\eqref{eq:pb},
\begin{multline}
  \label{eq:partial2}
  \sum_{r_2 = 0}^\infty P_{r_2}^{i \to \mathcal{T}_i} \binom{r_2}{k_2}
  \tilde{p}_i^{k_2} (1 - \tilde{p}_i)^{r_2 - k_2} = \sum_{r_2 = 0}^\infty
  \binom{r_2}{k_2} \tilde{p}_i^{k_2} (1 - \tilde{p}_i)^{r_2 - k_2}
  \exp(-z \delta_i) \frac{(z \delta_i)^{r_2}}{r_2!} \\
  = \frac{(z \tilde{p}_i
    \delta_i)^{k_2}}{k_2!} \exp(-z \tilde{p}_i \delta_i).
\end{multline}
Now, using~\eqref{eq:partial} and~\eqref{eq:partial2} in~\eqref{eq:churro} we
have
\begin{equation}
  q_{i \to i} = \sum_{k_1 + k_2 \geq k} \exp(-\lambda_a p_{i \to i} - z
  \tilde{p}_i \delta_i) \frac{(\lambda p_{i \to i})^{k_1}}{k_1!} \frac{(z
    \tilde{p}_i \delta_i)^{k_2}}{k_2!} = \Prob(V_1  + V_2 \geq k)
\end{equation}
where $V_1 \sim \text{Poisson}(\lambda_a p_{i \to i})$,
$V_2 \sim \text{Poisson}(z \tilde{p}_i \delta_i)$ and $V_1$, $V_2$ are
\emph{independent} random variables. Since the sum of independent Poisson
variables is Poisson, we finally arrive at
\begin{equation}
  \label{eq:density4}
  q_{i \to i} = 1 - \sum_{m = 0}^{k - 1} \exp(-\nu) \frac{\nu^m}{m!}
\end{equation}
for $\nu := \lambda p_{i \to i} + z \tilde{p}_i \delta_i$.

In a similar way, the probability $q_{i \to j}$ can be derived after
averaging
\begin{equation}
  \label{eq:erasure-prob-2}
  \hat{\hat{p}}_e = \sum_{k_1 + k_2 \geq k} \binom{r_1}{k_1} p_{i \to i}^{k_1}
  (1 - p_{i \to i})^{r_1 - k_1} \binom{r_2 - 1}{k_2}  \tilde{p}_i^{k_2} (1 -
  \tilde{p}_i)^{r_2 - 1 - k_2}
\end{equation}
over the node perspective and the edge perspective check node degree
distributions, i.e.
\begin{multline}
  q_{i\to j} = \Esp_{\rho^{i \to \mathcal{T}_i}} \Esp_{P^{i \to i}} \left(
        \sum_{k_1 + k_2 \geq k} \binom{r_1}{k_1} p_{i \to i}^{k_1} (1 -
        p_{i \to i})^{r_1 - k_1} \binom{r_2 - 1}{k_2} \tilde{p}_i^{k_2} (1 -
        \tilde{p}_i)^{r_2 - 1 - k_2} \right) \\
      = \sum_{r_1 = 0}^\infty \sum_{r_2 = 1}^\infty \rho_{r_2}^{i \to i}
      P_{r_1}^{i \to \mathcal{T}_i} \left( \sum_{k_1 + k_2 \geq k}
        \binom{r_1}{k_1} p_{i \to i}^{k_1} (1 - p_{i \to i})^{r_1 - k_1}
        \binom{r_2 - 1}{k_2} \tilde{p}_i^{k_2} (1 - \tilde{p}_i)^{r_2 - 1 -
          k_2} \right).
\end{multline}
Noting that~\eqref{eq:erasure-prob} and~\eqref{eq:erasure-prob-2}, and $q_{i
  \to i}$ and $q_{i \to j}$ are the same if we swap indices $1$ and $2$, we
obtain
\begin{equation}
  \label{eq:density5}
  q_{i \to j} = q_{i \to i} = 1 - \sum_{m = 0}^{k - 1} \exp(-\nu) \frac{\nu^m}{m!}.
\end{equation} 
Equality follows from the fact that $P^{i \to i}(x) = \rho^{i \to i}(x)$ and
$P^{i \to \mathcal{T}_i}(x) = \rho^{i \to \mathcal{T}_i}(x)$ which is in turn
a consequence of the underlying Poisson distributions.

Density evolution can be evaluated by computing
iteratively~\eqref{eq:density1}-\eqref{eq:density3}, \eqref{eq:density4}
and~\eqref{eq:density5} with all values initialized to $1$, until convergence.
The packet loss ratio of position $i$ can be computed as $\overline{p}_i =
\Lambda(\tilde{q}_i) q_{i \to i} / \tilde{q}_i$ and the threshold $\lambda^\star$ is
found by searching for the largest value of $\lambda_a$ for which
$\overline{p}_i$ converges to $0$ for all positions.

\subsection{Density evolution for Frame Asynchronous CSA with Uniform Slot}

When the slots for transmitting the replicas are chosen uniformly within the
local frame there is no need to consider different types of edges. Therefore,
the probability $p_i$ that an erasure message is passed from a variable node
at position $i$ is
\begin{equation}
  \label{eq:de-fa-csa-u1}
  p_i = \sum_\ell \lambda_\ell \tilde{q}_i^{\ell - 1} = \lambda(\tilde{q}_i)
\end{equation} 
where
\begin{equation}
  \label{eq:de-fa-csa-u2}
  \tilde{q}_i = \frac{1}{n} \sum_{j = i}^{i + n - 1} q_j
\end{equation}
is the average probability of the incoming edges to the variable node at
position $i$. Similarly, it is possible to repeat the analysis in the previous
Section for deriving that the probability that a check node at position $i$
sends an erasure message is
\begin{equation}
  \label{eq:de-fa-csa-u3}
  q_i = \sum_r \rho_{i,r} \sum_{j \geq k} \binom{r - 1}{j} \tilde{p}_i^j (1 -
  \tilde{p}_i)^{r - 1 - j} = 1 - \sum_{j = 0}^{k - 1} \exp(-\eta) \frac{\eta^j}{j!}
\end{equation}
where $\eta := \mu_i \tilde{p}_i \Lambda^\prime(1)/ n$ and
\begin{equation}
  \label{eq:de-fa-csa-u4}
  \tilde{p}_i = \begin{cases}
    \sum_{j = 1}^i \frac{p_j}{i}, \quad & 1 \leq i < n \\
    \sum_{j = i - n + 1}^i \frac{p_j}{n}, \quad & i \geq n.
  \end{cases}
\end{equation}
Equations~\eqref{eq:de-fa-csa-u1}-\eqref{eq:de-fa-csa-u4}, evaluated
iteratively, allow the computation of the density evolution, with all the
probabilities initialized to $1$. The packet loss ratio of position $i$ is
$\overline{p}_i = \Lambda(\tilde{q}_i)$.

\section{Potential function}
For $i \gg n$, the density evolution for FA-CSA with uniform slot selection
converges to unique values $p$, $q$, independent of the slot position $i$,
which are the solution to the fixed point equation
\begin{equation}
  x = \lambda \bigl( g(x) \bigr)
\end{equation}
where
\begin{equation}
  \label{eq:potential-g}
  g(x) = 1 - \exp(-\zeta x) \sum_{j = 0}^{k - 1} \frac{(\zeta x)^j}{j!}
\end{equation}
where $\zeta:= \lambda_a \Lambda^\prime(1)$, and and $\lambda_a$ is
the arrival rate of transmitters per slot. The \emph{potential function} is
defined as
\begin{equation}
  U(x) = x g(x) - G(x) - F\bigl(g(x) \bigr)
\end{equation}
where $G(x) := \int_0^x g(z) dz$ and $F(x) = \int_0^x \lambda(z)
dz$. Recalling that $\lambda(x) = \Lambda(x)^\prime / \Lambda^\prime(1)$, we
have
$F(x) = (\Lambda(x) - \Lambda(0)) / \Lambda^\prime(1) = \Lambda(x) /
\Lambda^\prime(1)$. The usefulness of the potential function for the density
evolution comes from the fact that
\begin{equation}
  \lambda^\star_a = \sup \left( \lambda_a: U^\prime(x) > 0 \forall x \in [0, 1] \right)
\end{equation}
is the threshold for the feasible arrival rates. The closed-form expression
for the potential function is given in the following theorem.
\begin{theorem}
  The potential function for FA-CSA with $k$-multiuser detection is given by
  \begin{equation}
    U(x) = k - \frac{1}{\zeta} \exp(-\zeta x) \sum_{j = 0}^{k - 1}
    \frac{(\zeta x)^j}{j!} (\zeta x + k - j) - \frac{\Lambda\bigl( g(x)
      \bigr)}{\Lambda^\prime(1)}
  \end{equation}
  where $\zeta := \lambda_a \Lambda^\prime(1)$.
\end{theorem}
\begin{proof}
  First, consider the function $g(x)$ in~\eqref{eq:potential-g}, i.e.,
  \begin{equation*}
    g(x) = 1 - \exp(-\zeta x) \sum_{j = 0}^{k - 1} \frac{(\zeta x)^j}{j!}.
  \end{equation*}
  Integrating by parts each of the terms in the sum we get
  \begin{equation*}
    I_j(x) := \int_0^x \exp(-\zeta t) \frac{(\zeta t)^j}{j!} dt =
    I_{j-1}(x) - \frac{1}{\zeta} \exp(-\zeta x) \frac{(\zeta x)^j}{j!} = \dots
    = I_0(x) - \frac{1}{\zeta} \exp(-\zeta x) \sum_{m = 1}^j \frac{(\zeta x)^m}{m!}
  \end{equation*}
  Using that $I_0(x) = 1 - 1/\zeta \exp(-\zeta x)$ we obtain
  \begin{equation*}
    I_j(x) = 1 - \frac{1}{\zeta} \exp(-\zeta x) \sum_{m = 0}^{j} \frac{(\zeta x)^j}{j!}
  \end{equation*}
  and thus
  \begin{equation*}
    G(x) = x - \sum_{j = 0}^{k - 1} I_j(x) = x - k + \frac{1}{\zeta} \sum_{j =
      0}^{k - 1} \sum_{m = 0}^j \frac{(\zeta x)^m}{m!} \stackrel{(a)}{=} x - k
    + \frac{1}{\zeta} \exp(-\zeta x) \sum_{j = 0}^{k - 1} (k - j) \frac{(\zeta x)^j}{j!},
  \end{equation*}
  where $(a)$ follows after exchanging the order of the summations. Now, since
  \begin{equation*}
    x g(x) = x - \frac{1}{\zeta} \exp(-\zeta x) \sum_{j = 0}^{k - 1}
    \frac{(\zeta x)^{j + 1}}{j!}
  \end{equation*}
  we have
  \begin{equation*}
    U(x) - xg(x) - F(x) = k - \frac{1}{\zeta} \exp(-\zeta x) \sum_{j = 0}^{k -
      1} \frac{(\zeta x)^j}{j!} (\zeta x + k - j) -
    \frac{\Lambda\bigl( g(x) \bigr)}{\Lambda^\prime(1)}.
  \end{equation*}
  This finishes the proof.
\end{proof}
\begin{figure}[t]
   \centering
   \label{fig:potential}
   \includegraphics[width=0.9\textwidth]{../images/mud2}
   \caption{An example of the potential function for
     FA-CSA with uniform slot selection vs. users arrival rate; $k = 2$,
     $\Lambda(x) = x^5$.} 
\end{figure}
\begin{figure}[t]
   \centering
   \label{fig:potential}
   \includegraphics[width=0.9\textwidth]{../images/mud3}
   \caption{An example of the potential function for
     FA-CSA with uniform slot selection vs. users arrival rate; $k = 3$,
     $\Lambda(x) = x^5$.}
\end{figure}

\section{Finite length analysis}
To be done

\section{Numerical results}
To be done